#include <stdio.h>
#include <winsock2.h>	// WinSockで必要
#include"data.h"
#include "PlayerManager.h"
#include "Object3DXfileAnim.h"
#pragma comment( lib, "ws2_32.lib" )	// WinSockで必要
#include "GameUpdate.h"
#include "BulletManager.h"
SOCKET		g_Socket;
sockaddr_in g_addressSend;
void main(void)
{
	int ret;

	// WinSock初期化
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2,2),&wsaData);


	// ホスト名・IPアドレス取得
	char host[256];
	char ip[16];
	PHOSTENT phostent;
	IN_ADDR in;

	gethostname(host,256);
	phostent = gethostbyname(host);

	memcpy(&in,phostent->h_addr,4);
	sprintf(ip,inet_ntoa(in));
	printf("%s:%s\n",host,ip);


	// ソケット生成
	g_Socket = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);


	// 受信アドレス
	sockaddr_in addressServer;
	addressServer.sin_port = htons(20000);
	addressServer.sin_family = AF_INET;
	addressServer.sin_addr.s_addr = htonl(INADDR_ANY);

	ret = bind(g_Socket,(sockaddr*)&addressServer,sizeof(addressServer));



	// 送信先アドレス
	g_addressSend.sin_port = htons(20001);
	g_addressSend.sin_family = AF_INET;
	g_addressSend.sin_addr.s_addr = inet_addr("239.0.0.17");//マルチキャストアドレス


	GameUpdater::Init();
	PlayerManager::init();
	// データ受信
	DATA data;
	while(true)
	{
		// 受信
		ret = recv(g_Socket,(char*)&data,sizeof(data),0);
		if(ret != SOCKET_ERROR)
		{
			// データタイプ解析
			switch(data.Type)
			{
				//イベント
				case DATA_TYPE_EVENT:
				{
					switch(data.Event.Type)
					{
						case DATA_EVENT_TYPE_DISCONECT:
						{

							PlayerManager::removePlayerList(data.ID);
						}
					}
					break;
				}

				// 座標
				case DATA_TYPE_POSITION:
				{
					if(data.Object == OBJECT_PLAYER)
					{
						Object3DXfileAnim *player = PlayerManager::getPlayer(data.ID);

						if(player)
						{
							D3DXVECTOR3 pos;

							pos.x = data.Position.x;
							pos.y = data.Position.y;
							pos.z = data.Position.z;

							player->setPos(pos);
						}
					}
					else
						if(data.Object == OBJECT_ENEMY_BULLET || data.Object == OBJECT_PLAYER_BULLET)
						{
						BULLET_TYPE bullettype;
						if(data.Object == OBJECT_ENEMY_BULLET)
						{
							bullettype = BULLET_ENEMY;
						}
						else
							if(data.Object == OBJECT_PLAYER_BULLET)
							{
							bullettype = BULLET_PLAYER;

							}
						ObjectBullet *bullet = BulletManager::getBulletObject(bullettype,data.ID,data.Number);
						if(bullet)
						{
							D3DXVECTOR3 pos;

							pos.x = data.Position.x;
							pos.y = data.Position.y;
							pos.z = data.Position.z;

							bullet->setPos(pos);
						}
						}
					break;
				}

				// 回転角度
				case DATA_TYPE_ROTATION:
				{
					if(data.Object == OBJECT_PLAYER)
					{

						Object3DXfileAnim *player = PlayerManager::getPlayer(data.ID);
						if(player)
						{
							D3DXVECTOR3 rot;

							rot.x = data.Rotation.x;
							rot.y = data.Rotation.y;
							rot.z = data.Rotation.z;

							player->setRot(rot);
						}
					}
					else
						if(data.Object == OBJECT_ENEMY_BULLET || data.Object == OBJECT_PLAYER_BULLET)
						{
						BULLET_TYPE bullettype;
						if(data.Object == OBJECT_ENEMY_BULLET)
						{
							bullettype = BULLET_ENEMY;
						}
						else
							if(data.Object == OBJECT_PLAYER_BULLET)
							{
							bullettype = BULLET_PLAYER;

							}
						ObjectBullet *bullet = BulletManager::getBulletObject(bullettype,data.ID,data.Number);

						if(bullet)
						{
							D3DXVECTOR3 rot;

							rot.x = data.Rotation.x;
							rot.y = data.Rotation.y;
							rot.z = data.Rotation.z;

							bullet->setRot(rot);
						}
						}
					break;
				}

				// 大きさ
				case DATA_TYPE_SCALE:
				{
					if(data.Object == OBJECT_ENEMY_BULLET || data.Object == OBJECT_PLAYER_BULLET)
					{
						BULLET_TYPE bullettype;
						if(data.Object == OBJECT_ENEMY_BULLET)
						{
							bullettype = BULLET_ENEMY;
						}
						else
							if(data.Object == OBJECT_PLAYER_BULLET)
							{
							bullettype = BULLET_PLAYER;

							}
						ObjectBullet *bullet = BulletManager::getBulletObject(bullettype,data.ID,data.Number);

						if(bullet)
						{
							D3DXVECTOR3 scl;

							scl.x = data.Scale.x;
							scl.y = data.Scale.y;
							scl.z = data.Scale.z;

							bullet->setScl(scl);
						}
					}
					break;
				}
				//時間
				case DATA_TYPE_TIME:
				{
					if(data.Object == OBJECT_ENEMY_BULLET || data.Object == OBJECT_PLAYER_BULLET)
					{
						BULLET_TYPE bullettype;
						if(data.Object == OBJECT_ENEMY_BULLET)
						{
							bullettype = BULLET_ENEMY;
						}
						else
							if(data.Object == OBJECT_PLAYER_BULLET)
							{
							bullettype = BULLET_PLAYER;

							}
						ObjectBullet *bullet = BulletManager::getBulletObject(bullettype,data.ID,data.Number);

						if(bullet)
						{
							bullet->setBulletTime(data.Time);
						}
					}
					break;
				}
			}

		}
		else{
		}

		// 送信
		ret = sendto(g_Socket,(char*)&data,sizeof(data),0,(sockaddr*)&g_addressSend,sizeof(g_addressSend));
	}


	// ソケット終了
	closesocket(g_Socket);
	GameUpdater::Uninit();

	// WinSock終了処理
	WSACleanup();

}

void MainSendData(DATA Data)
{
	int ret;

	// サーバにデータ送信
	ret = sendto(g_Socket,(char*)&Data,sizeof(Data),0,(sockaddr*)&g_addressSend,sizeof(g_addressSend));

}