/******************************************************************************/
/*! @addtogroup Object_Object3DXfileAnim
@file       Object.h
@brief      Object3DXfileAnimクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECT3DXFILEANIM_FILE
#define COBJECT3DXFILEANIM_FILE
#include "main.h"
#include "Object3D.h"
#include "XFileManager.h"

class Object3DXfileAnim: public Object3D
{
	//コンストラクタ
	public:
	/*! Object3DXfileAnim Object3DXfileAnimのコンストラクタ
	@param[in]     type    3DXfileオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	Object3DXfileAnim(PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_3DXFILEANIM,OBJECT3D_TYPE type = OBJCT3D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! Object3DXfileAnim Object3DXfileAnimのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~Object3DXfileAnim(void);
	//操作
	public:
	/*! Create Object3Dの作成
	@param[in]     xfileName    Xfileの名前定義から設定(XFILEANIM_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static Object3DXfileAnim* Create(XFILEANIM_LIST xfileName = XFILEANIM_PLAYER,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT3D_TYPE type = OBJCT3D_NORMAL,PRIORITY_MODE priority = PRIORITY_2);
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	void setXfile(XFILEANIM_LIST xfile){ xfileData_m = XFileManager::getXFileAnimPointer(xfile); }
	XFILEANIM_DATA* getXfileAnim(void){ return xfileData_m; }
	//メンバ変数
	XFILEANIM_DATA* xfileData_m;//ロード済みxファイルのデータのメンバ
};
#endif

//End Of File