/******************************************************************************
@file       ObjectEnemy.cpp
@brief      ObjectEnemyの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectEnemy.h"
#include "SystemManager.h"
#include "ObjectBullet.h"
#include "CollisionManager.h"
#include "PlayerManager.h"
#include "ObjectBullet.h"
#include "ObjectEffect.h"
#include "BulletManager.h"
#include "EnemyManager.h"
#include "ObjectManager.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectEnemy* ObjectEnemy::Create(ENEMY_TYPE enemytype,XFILEANIM_LIST xfileName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	ObjectEnemy *objectPointer;

	objectPointer = new ObjectEnemy(enemytype,position,rot,priority);
	if(objectPointer)
	{
		objectPointer->setSize(size);
		objectPointer->setXfile(xfileName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectEnemy::ObjectEnemy(ENEMY_TYPE enemytype,D3DXVECTOR3 position,D3DXVECTOR3 rot,PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3DXfileAnim(priority,objType)
{
	enemyType_m = enemytype;
	enemyNumber_m = EnemyManager::addEnemyList(enemyType_m,this);
	pos_m = posDest_m = position;
	rot_m.y = rotDest_m.y = rot.y;
	init();
	if(enemyHundle_m < 0)
	{
		uninit();
	}
}
//==============================================================================
//デストラクタ
ObjectEnemy::~ObjectEnemy(void)
{

}
//==============================================================================
//初期化
void ObjectEnemy::init(void)
{
	enemyHundle_m = getObjHandle();
	enemyMode_m = ENEMY_MODE_MOVE;
	if(enemyType_m == ENEMY_SHOOTER)
	{
		posSpeed_m.x = posSpeed_m.z = 0;
	}
	posSpeed_m.x = posSpeed_m.z = 10;
	posDeffSpeed = 0.1f;
	rotSpeed_m.y = 0.01f;
	moveCnt = 0;
	height_m = ( 100 * scl_m.x ) / 2.0f;
	getCollisionData()->radius = ( 100 * scl_m.x ) / 2;

	if(enemyType_m != ENEMY_MOVER)
	{
		height_m = ( 700 * scl_m.x ) / 2.0f;
		getCollisionData()->radius = ( 700 * scl_m.x ) / 2;

	}
	float posY = CollisionManager::HitchkField(pos_m,NULL);
	if(posY != -99999)
	{
		posDest_m.y = pos_m.y = posY + height_m;
	}

	if(enemyType_m == ENEMY_SHOOTER)
	{
		posSpeed_m.x = posSpeed_m.z = 0;
	}
	if(enemyType_m == ENEMY_DRAGON_MOVER)
	{
		posSpeed_m.x = posSpeed_m.z = 40;
	}
	attackCnt_m = 0;
	getCollisionData()->pos = pos_m;
	enemyUpdate_m = true;
}
//==============================================================================
//解放
void ObjectEnemy::uninit(void)
{
	EnemyManager::removeEnemyList(enemyType_m,enemyNumber_m);
	Object3DXfileAnim::uninit();
}
//==============================================================================
//更新
void ObjectEnemy::update(void)
{

		Object3DXfileAnim::update();
		moveCnt++;
		moveCnt %= 300;
		COLLISION_DATA col;
		COLLISION_DATA* col2;
		Hitchk();

		col.pos = pos_m;
		col.radius = 1000;

		if(enemyType_m == ENEMY_DRAGON_MOVER)
		{
			col.radius = 4000;

		}
		if(col2 = PlayerManager::hitchkList(&col))
		{
			D3DXVECTOR3 pos = col2->pos;
			D3DXVECTOR3 rot = col2->rotOut;
			float fRot = 0;
			fRot = atan2f(( pos.z - pos_m.z ),( pos.x - pos_m.x ));
			if(( pos.z - pos_m.z ) == 0 && ( pos.x - pos_m.x ) == 0)
			{
				rotDest_m.y = 0;;
			}
			else{
				fRot = -D3DX_PI / 2 + atan2f(-( pos.z - pos_m.z ),( pos.x - pos_m.x ));
			}
			rotDest_m.y = fRot;

		}
		else{
			if(moveCnt == 0)
			{
				int Rot = 1000 * DATA_PI;
				rotDest_m.y = ( rand() % ( Rot ) ) / 1000;
			}
		}
		if(moveCnt % 100 == 0)
		{
			enemyMode_m = enemyMode_m == ENEMY_MODE_MOVE ? ENEMY_MODE_ATTACK : ENEMY_MODE_MOVE;

		}
		if(enemyType_m == ENEMY_SHOOTER)
		{
			rotDest_m.y = rot_m.y + 0.01f;
			if(moveCnt % 10 == 0)
			{
				D3DXVECTOR3 posBullet = pos_m;
				posBullet.x -= sinf(rot_m.y)*( height_m * 1.5f );
				posBullet.z -= cosf(rot_m.y)*( height_m * 1.5f );

				ObjectBullet::Create(BULLET_ENEMY,pos_m,rot_m.y);
			}
		}

		//ゆっくり回転
		float diffRotY;
		diffRotY = rotDest_m.y - rot_m.y;
		ROT_CHK(diffRotY);
		rot_m.y += ( diffRotY * rotDeffSpeed );
		if(enemyMode_m == ENEMY_MODE_MOVE)
		{
			if(rotDest_m.y == rot_m.y || pos_m.x <= -MOVE_MAX || pos_m.x >= MOVE_MAX || pos_m.z <= -MOVE_MAX || pos_m.z >= MOVE_MAX)
			{

				posDest_m.x -= sinf(rot_m.y)*posSpeed_m.x;
				posDest_m.z -= cosf(rot_m.y)*posSpeed_m.z;
			}
		}
		ROTS_CHK(rotDest_m);
		if(pos_m.x <= -MOVE_MAX)
		{
			posDest_m.x = -MOVE_MAX + 1;
			pos_m.x = -MOVE_MAX + 1;

		}
		else
			if(pos_m.x >= MOVE_MAX)
			{
			posDest_m.x = MOVE_MAX - 1;
			pos_m.x = MOVE_MAX - 1;
			}
		if(pos_m.z <= -MOVE_MAX)
		{
			posDest_m.z = -MOVE_MAX + 1;
			pos_m.z = -MOVE_MAX + 1;
		}
		else
			if(pos_m.z >= MOVE_MAX)
			{
			posDest_m.z = MOVE_MAX - 1;
			pos_m.z = MOVE_MAX - 1;
			}

		//移動量セット
		D3DXVECTOR3 posDiff;
		posDiff.x = ( posDest_m.x - pos_m.x ) * posDeffSpeed;
		posDiff.z = ( posDest_m.z - pos_m.z ) * posDeffSpeed;

		pos_m.x += posDiff.x;
		pos_m.z += posDiff.z;

	float posY = CollisionManager::HitchkField(pos_m,NULL);
	//前の高さと現在の高さが違いすぎていたら修正
	if(posY >= pos_m.y + 400 || posY <= pos_m.y - 400)
	{
	}
	else{
		pos_m.y = posY + height_m;
	}
	getCollisionData()->pos = pos_m;
	if(enemyType_m != ENEMY_MOVER)
	{
		if(scl_m.x <= 0.1f + 0.02f)
		{

			uninit();
		}

	}
	else
		if(scl_m.x >= 6.0f + 0.1f)
		{

		uninit();
		}
}
//==============================================================================
//当たり判定
void ObjectEnemy::Hitchk(void)
{

	if(BulletManager::hitchk(getCollisionData(),BULLET_PLAYER))
	{
		if(enemyType_m != ENEMY_MOVER)
		{
			scl_m.z -= 0.02f;
			scl_m.x -= 0.02f;
			scl_m.y -= 0.02f;
		}
		else{
			scl_m.x += 0.1f;
			scl_m.y += 0.1f;
			scl_m.z += 0.1f;
		}
	}

	collisionData_m.radius = ( 100 * scl_m.x ) / 2.0f;
	height_m = ( 100 * scl_m.x ) / 2.0f;
	if(enemyType_m != ENEMY_MOVER)
	{
		collisionData_m.radius = ( 700 * scl_m.x ) / 2.0f;
		height_m = ( 700 * scl_m.x ) / 2.0f;

	}

}
//End Of FIle