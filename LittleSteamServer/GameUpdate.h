/******************************************************************************/
/*! @addtogroup System_GameUpdater
@file       GameUpdater.h
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef NET_CLIENT_FILE
#define NET_CLIENT_FILE

#include "Common.h"
#include "main.h"
#include <winsock.h>
#include <process.h>
#include "data.h"

#pragma comment( lib, "wsock32.lib" )

class Object3DXfile;
class SystemManager;
/*! @class GameUpdater
@brief  各管理クラスを更新する
*/

class GameUpdater {
	//コンストラクタ
	/*! GameUpdater　コンストラクタ*/
	public:
	GameUpdater(void);
	//デストラクタ
	/*! ~GameUpdater　デストラクタ*/
	public:
	~GameUpdater(void);
	//操作
	public:

	static void Init(void);
	static void Uninit(void);

	static unsigned __stdcall Update(LPVOID Param);

	static void SendData(DATA Data);
	//static void SendPacket( int PacketType, ... );

	//メンバ関数
	private:

	static SOCKET		m_Socket;
	static int			m_ID;

	static SOCKADDR_IN	m_ServerAddress;

	static bool UpdaterFlag;
	static SystemManager* systemManager;
};

#endif
//End Of File