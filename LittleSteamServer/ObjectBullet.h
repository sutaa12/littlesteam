/******************************************************************************/
/*! @addtogroup Object_ObjectBullet
@file       Object.h
@brief      ObjectBulletクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef OBJECTBULLET_FILE
#define OBJECTBULLET_FILE
#include "main.h"
#include "Object3DBillboard.h"
typedef enum{
	BULLET_PLAYER = 0,
	BULLET_ENEMY,
	BULLET_TYPE_MAX
}BULLET_TYPE;
typedef struct
{
	int bulletID;//誰のたま家
	int bulletNum;//何番目の弾か
}BULET_ID;
class ObjectBullet: public Object3DBillboard
{
	//コンストラクタ
	public:
	/*! ObjectBullet ObjectBulletのコンストラクタ
	@param[in]     type    3DBillboardオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectBullet(BULLET_TYPE bulletType,float rot,PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_3DBILLBOARD,OBJECT3D_TYPE type = OBJCT3D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectBullet ObjectBulletのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectBullet(void);
	//操作
	public:
	/*! Create ObjectBulletの作成
	@param[in]     bulletType    弾の名前定義から設定(BULLET_TYPE)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static ObjectBullet* Create(BULLET_TYPE bulletType,D3DXVECTOR3 position,float rot,OBJECT3D_TYPE type = OBJCT3D_BULLET,PRIORITY_MODE priority = PRIORITY_7);
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	void setBulletID(BULET_ID bulletID){ bulletID_m = bulletID; }
	BULET_ID getBulletID(void){ return bulletID_m; }
	BULLET_TYPE getBulletType(void){ return bulletType_m; }
	int getBulletTime(void){ return cnt_m; }
	void setBulletTime(int cnt){ cnt_m = cnt; }
	//属性
	public:
	BULLET_TYPE bulletType_m;
	float sclSpeed_m;
	float rotY_m;//進行する角度
	int cnt_m;
	int bullethundle_m;
	BULET_ID bulletID_m;//弾のID
};
#endif

//End Of File
