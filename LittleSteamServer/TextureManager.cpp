/******************************************************************************
@file       TextureManager.cpp
@brief      テクスチャを管理するクラス テクスチャのポインタを返す
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//==============================================================================
#include "TextureManager.h"
LPDIRECT3DTEXTURE9 TextureManager::d3DTextures_m[TEXTURE_MAX];	// テクスチャへのポインタの配列
//==============================================================================
//コンストラクタ
TextureManager::TextureManager(void)
{
	init();
}
//==============================================================================
//デストラクタ
TextureManager::~TextureManager(void)
{
	for(int loop = 0;loop < TEXTURE_MAX;loop++)
	{
		SAFE_RELEASE(d3DTextures_m[loop]);
	}
}
//==============================================================================
//初期化
HRESULT TextureManager::init(void)
{
	return S_OK;
}

//==============================================================================

//End Of FIle

