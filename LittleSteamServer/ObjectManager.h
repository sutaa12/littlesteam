/******************************************************************************/
/*! @addtogroup Object_ObjectManager
@file       ObjectManager.h
@brief      ゲームのオブジェクトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef OBJECT_MANAGE_FILE
#define OBJECT_MANAGE_FILE
#include "Object.h"

/*! @class ObjectManager
@brief  オブジェクト管理クラス 各オブジェクトの管理クラスの管理をする ハンドルの登録も行う
*/
class ObjectManager
{
	//メンバ関数
	//コンストラクタ
	public:
	/*! ObjectManager　コンストラクタ*/
	ObjectManager(void);
	//デストラクタ
	public:
	/*! ~ObjectManager　デストラクタ*/
	virtual ~ObjectManager(void);
	//操作
	public:
	//操作
	public:
	/*! update Objectのアップデートをする*/
	void update(void);
	/*! setGameObjectHandle ハンドルをリストに登録
	@param[in]      objectPointer    登録したオブジェクト
	@param[out]      handleId    登録されたID番号が返る
	*/
	static bool setGameObjectHandle(Object* objectPointer,unsigned int& handleId);
	/*! setGameObjectHandle ハンドルをリストから解除
	@param[out]      handleId    登録されたID番号が返る
	*/
	static void unsetGameObjectHandle(unsigned int& handleId);
	//属性
	public:
	/*! setGameObjectHandle ハンドルとOBJの種類から検索一致しない場合NULLを返す
	@param[in]      handleId    検索するID
	@param[in]      uniqueId    IDの種類
	*/
	static Object* getGameObjectHandle(unsigned int handleId,OBJTYPE uniqueId);
	//属性
	/*! setGameObjectHandle OBJの種類から検索 一致しない場合NULLを返す
	@param[in]      uniqueId    IDの種類
	@param[out]      objData    一致したデータのオブジェ全て返す　ポインタ配列の先頭アドレス
	@param[out]      objNum    一致したオブジェの数を返す
	@return          オブジェクトのポインターリストを返す１つもなければNULLを返す
	*/
	static Object** getGameObjectHandle(OBJTYPE uniqueId,unsigned int& objNum);
	//属性
	/*! getGameObjectHandleNotOnly OBJの種類以外検索 一致しない場合NULLを返す
	@param[in]      uniqueId    除外するIDの種類
	@param[out]      objData    一致したデータのオブジェ全て返す　ポインタ配列の先頭アドレス
	@param[out]      objNum    一致したオブジェの数を返す
	@return          オブジェクトのポインターリストを返す１つもなければNULLを返す
	*/
	static Object** getGameObjectHandleNotOnly(OBJTYPE uniqueId,unsigned int& objNum);
	static const unsigned int MAX_OBJECT = 100000 * PRIORITY_MAX;/*< オブジェクトの最大数*priority分 */
	//メンバ変数
	private:
	static Object* objectHandle_m[MAX_OBJECT + 1];/*< 定数でハンドルの配列を持っておく*/
	static Object** objectHandlePointer_m;/*< 指定したIDのオブジェクトをまとめて確保する時に使用する*/

	unsigned int handle_m;
};
#endif

//End Of File