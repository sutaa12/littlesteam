/******************************************************************************
@file       SystemManager.cpp
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#include "SystemManager.h"
#include "ObjectManager.h"
#include "EnemyManager.h"
#include "EffectManager.h"
#include "Object3DFigure.h"
#include "PlayerManager.h"
#include "BulletManager.h"
//コンストラクタ
SystemManager::SystemManager(void)
{
	//各管理クラスを作成
	enemyManager_m = new EnemyManager;
	objectManager_m = new ObjectManager;
	fpsTimer_m = 0;
	//引数の必要な物は初期化
	enemyManager_m->init();
	Object3DFigure::Create(FIGURE_MESHFIELD,TEXTURE_FIELD,0,0,D3DXVECTOR3(0,0,0),D3DXVECTOR3(FIELD_MAX,0,FIELD_MAX));
	enemyManager_m->setEnemy();
	BulletManager::init();
}
//デストラクタ
SystemManager::~SystemManager(void)
{
	//各管理クラスを解放
	SAFE_DELETE(enemyManager_m);
	SAFE_DELETE(objectManager_m);
}
//更新
void SystemManager::update(void)
{
	objectManager_m->update();
	enemyManager_m->update();
	PlayerManager::update();
	BulletManager::update();
}
