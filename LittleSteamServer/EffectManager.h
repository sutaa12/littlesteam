/******************************************************************************/
/*! @addtogroup Object_EffectManager
@file       EffectManager.h
@brief      ゲームの弾を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef EFFECTMANAGER_FILE
#define EFFECTMANAGER_FILE
#include "Common.h"
#include "main.h"
#include "XFileManager.h"
#include "ObjectEffect.h"
class ObjectEffect;
//敵管理クラス
class EffectManager
{
	public:
	EffectManager(void);//コンストラクタ
	~EffectManager(void);//デストラクタ
	HRESULT init(void);//初期化
	void update(void);//ライト更新
	static int addEffectList(EFFECT_TYPE type,ObjectEffect* effectPointer);
	static void removeEffectList(EFFECT_TYPE type,int num);
	static bool hitchk(COLLISION_DATA* obj1,EFFECT_TYPE type);

	private:
	//メンバ変数
	static const unsigned int MAX_EFFECT = 500;
	static ObjectEffect* effectList_m[EFFECT_TYPE_MAX][MAX_EFFECT];//弾のリスト

};

#endif

//End Of FIle