/******************************************************************************
@file       ObjectBullet.cpp
@brief      ObjectBulletの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectBullet.h"
#include "BulletManager.h"
#include "SystemManager.h"
#include "CollisionManager.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectBullet* ObjectBullet::Create(BULLET_TYPE bulletType,D3DXVECTOR3 position,float rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	ObjectBullet *objectPointer;

	objectPointer = new ObjectBullet(bulletType,rot,priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		if(bulletType == BULLET_PLAYER)
		{
			objectPointer->setPos(D3DXVECTOR3(position.x,CollisionManager::HitchkField(position,NULL),position.z));
		}
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectBullet::ObjectBullet(BULLET_TYPE bulletType,float rot,PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3DBillboard(priority,objType)
{
	TEXTURE_LIST texlist[BULLET_TYPE_MAX] = {
		TEXTURE_SMOKE,
		TEXTURE_SMOKE,
	};
	rotY_m = rot;
	bulletType_m = bulletType;
	setTexName(texlist[bulletType]);
	bullethundle_m = BulletManager::addBulletList(bulletType_m,this);
	init();
	if(bullethundle_m < 0)
	{
		uninit();
	}
	bulletID_m.bulletID = -1;
	bulletID_m.bulletNum = -1;
}
//==============================================================================
//デストラクタ
ObjectBullet::~ObjectBullet(void)
{

}
//==============================================================================
//初期化
void ObjectBullet::init(void)
{
	cnt_m = 0;
	sclSpeed_m = 0;
	D3DXVECTOR3 moveSpeed[BULLET_TYPE_MAX] =
	{
		D3DXVECTOR3(( rand() % 10 ) + 50,rand() % 10,0),
		D3DXVECTOR3(20,0,0)
	};
	D3DXVECTOR3 size[BULLET_TYPE_MAX] =
	{
		D3DXVECTOR3(256,256,0),
		D3DXVECTOR3(256,256,0)
	};
	if(bulletType_m == BULLET_PLAYER)
	{
		setDrawAlphaMode();
		setColorSpeed(D3DXCOLOR(0,0,0,0.006f));
		setColorEd(D3DXCOLOR(1,1,1,0));
		sclSpeed_m = 0.01f;
		rotY_m += ( ( rand() & 400 ) - 200 ) / 1000.0f;
	}
	else
		if(bulletType_m == BULLET_ENEMY)
		{
		setDrawAlphaMode();
		setColorSpeed(D3DXCOLOR(0,0,0,0.006f));
		setColorEd(D3DXCOLOR(0.4f,0.4f,0.8f,0));
		sclSpeed_m = 0.01f;
		rotY_m += ( ( rand() & 400 ) - 200 ) / 1000.0f;
		}
	posSpeed_m = moveSpeed[bulletType_m];
	setSize(size[bulletType_m]);
	getCollisionData()->radius = size_m.x / 2;
	getCollisionData()->pos = pos_m;
}
//==============================================================================
//解放
void ObjectBullet::uninit(void)
{
	BulletManager::removeBulletList(bulletType_m,bullethundle_m);
	Object3DBillboard::uninit();
}
//==============================================================================
//更新
void ObjectBullet::update(void)
{

}

//End Of FIle