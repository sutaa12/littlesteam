/******************************************************************************/
/*! @addtogroup Object_Object3DFigure
@file       Object.h
@brief      Object3DFigureクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECT3DFIGURE_FILE
#define COBJECT3DFIGURE_FILE
#include "main.h"
#include "Object3D.h"
//図形の種類
typedef enum
{
	FIGURE_MESHBORAD = 0,
	FIGURE_MESHCYRINDER,
	FIGURE_MESHDOOM,
	FIGURE_MESHFIELD,
	FIGURE_MAX
}FIGURE_MODE;

class Object3DFigure: public Object3D
{
	//コンストラクタ
	public:
	void ( Object3DFigure::*func_m[FIGURE_MAX] )( void );

	/*! Object3DFigure Object3DFigureのコンストラクタ
	@param[in]     numW    分割数W
	@param[in]     numH    分割数H
	@param[in]     type    3DFigureオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	Object3DFigure(FIGURE_MODE mode = FIGURE_MESHBORAD,OBJECT3D_TYPE type = OBJCT3D_MESHBOARD,unsigned int numW = 1,unsigned int numH = 1,PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_FIGURE);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! Object3DFigure Object3DFigureのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~Object3DFigure(void);
	//操作
	public:
	/*! Create Object3Dの作成
	@param[in]     objMode    図形の形(OBJECTTYPE)
	@param[in]     texName    テクスチャの名前定義から設定(TEXTURE_LIST)
	@param[in]     numW    分割数W
	@param[in]     numH    分割数H
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static Object3DFigure* Create(FIGURE_MODE mode = FIGURE_MESHBORAD,TEXTURE_LIST texName = TEXTURE_NONE,unsigned int numW = 1,unsigned int numH = 1,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),PRIORITY_MODE priority = PRIORITY_5);
	/*! init 変数を全て初期化
	*/
	void init(FIGURE_MODE objMode);/*!< 初期化*/

	/*! loadHightMap 地面画像をロード
	*/
	void loadHightMap(void);/*!< 初期化*/

	/*! initMeshField 地面を初期化
	*/
	void initMeshField(void);/*!< 初期化*/
	/*! initMeshBoard 板を初期化
	*/
	void initMeshBoard(void);/*!< 初期化*/
	/*! initMeshBoard 円柱を初期化
	*/
	void initMeshCyrinder(void);/*!< 初期化*/
	/*! initMeshBoard ドームを初期化
	*/
	void initMeshDoom(void);/*!< 初期化*/

	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	VERTEX_3D* getVtxBuff(void){ return pvtx; }
	int getNumVtx(void){ return numVertex_m; }
	int getNumPolygon(void){ return (( numBlockW_m * numBlockH_m ) * 2 + ( ( numBlockH_m - 1 ) * 4 )); }
	D3DXVECTOR2 getTexScl(void){ return texScl_m; }
	void setTexScl(D3DXVECTOR2 texScl){ texScl_m = texScl; }
	int getNumBlockW(void){ return numBlockW_m; }
	int getNumBlockH(void){ return numBlockH_m; }
	private:
	// Direct3D オブジェクト
	VERTEX_3D* pvtx;
	D3DXVECTOR3 *nor_m;//法線の情報
	D3DXVECTOR2 texScl_m;//テクスチャーの大きさ
	int numBlockW_m;
	int	numBlockH_m;				// ブロック数
	int imgSizeW_m;
	int imgSizeH_m;
	float* height_m;
	int numVertex_m;							// 総頂点数
	int numPolygon_m;							// 総ポリゴン数
	float sizeBlockW_m;// ブロックサイズ
	float sizeBlockH_m;
};
#endif

//End Of File