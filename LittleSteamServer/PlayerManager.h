/******************************************************************************/
/*! @addtogroup Object_PlayerManager
@file       PlayerManager.h
@brief      ゲームの弾を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef PLAYERMANAGER_FILE
#define PLAYERMANAGER_FILE
#include "Common.h"
#include "main.h"
#include "XFileManager.h"
#include "CollisionManager.h"
#include "data.h"
class ObjectPlayer;
class Object3DXfileAnim;
//敵管理クラス
class PlayerManager
{
	public:
	PlayerManager(void);//コンストラクタ
	~PlayerManager(void);//デストラクタ
	static HRESULT init(void);//初期化
	static void update(void);//ライト更新
	static int addPlayerList(int id,ObjectPlayer* bulletPointer);
	static void removePlayerList(int id);
	static bool hitchk(COLLISION_DATA* obj1);
	static COLLISION_DATA* hitchkList(COLLISION_DATA* obj1);
	static void setPlayerUpdate(bool update){ update_m = update; }
	static void setPlayer(void);
	static Object3DXfileAnim* getPlayer(int id);
	static void resetPlayer(void);
	static void setPlayerDATA(int id);
	private:
	//メンバ変数
	static const unsigned int MAX_PLAYER = 5;
	static Object3DXfileAnim* playerList_m[MAX_PLAYER];//プレイヤーリスト
	static bool update_m;
	static XFILEANIM_DATA* xfileData_m;//ロード済みxファイルのデータのメンバ
	static int cnt_m[MAX_PLAYER];
	static int playerNum_m;
	static int idNeum[MAX_PLAYER];
};

#endif

//End Of FIle