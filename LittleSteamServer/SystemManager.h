/******************************************************************************/
/*! @addtogroup System_SystemManager
@file       SystemManager.h
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef SYSTEM_MANAGE_FILE
#define SYSTEM_MANAGE_FILE
#include "Common.h"
#define FIELD_MAX (400000)
#define MOVE_MAX ((FIELD_MAX / 2) - (FIELD_MAX / 4))

//前方宣言
class ObjectManager;
class CollisionManager;
class EnemyManager;
/*! @class SystemManager
@brief  各管理クラスを更新する
*/

class SystemManager {
	//コンストラクタ
	/*! SystemManager　コンストラクタ*/
	public:
	SystemManager(void);
	//デストラクタ
	/*! ~SystemManager　デストラクタ*/
	public:
	~SystemManager(void);
	//操作
	public:
	/*! update　管理クラスを更新*/
	void update(void);
	/*! setFPSTimer 1フレームの時間を入れる*/
	void setFPSTimer(unsigned long fpsTimer){ fpsTimer_m = fpsTimer; }
	private:
	ObjectManager* objectManager_m; /*!< オブジェクト管理 */
	CollisionManager* collisionManager_m; /*!< 当たり判定管理 */
	EnemyManager* enemyManager_m;/*!< 敵の管理*/
	unsigned long fpsTimer_m;/*!< 1フレームの計測値*/
};

#endif
//End Of File