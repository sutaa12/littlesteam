/******************************************************************************
@file       EnemyManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"EnemyManager.h"
#include "ObjectEnemy.h"
#include"ObjectManager.h"
#include "SystemManager.h"
#include "CollisionManager.h"
#include "EnemyManager.h"
#include "GameUpdate.h"
ObjectEnemy* EnemyManager::enemyList_m[ENEMY_TYPE_MAX][MAX_ENEMY];
int EnemyManager::enemyID_m[ENEMY_TYPE_MAX][MAX_ENEMY];
int EnemyManager::cnt_m[ENEMY_TYPE_MAX][MAX_ENEMY];
int EnemyManager::enemyNum_m;
bool EnemyManager::update_m;
//==============================================================================
//コンストラクタ
EnemyManager::EnemyManager(void)
{
	XFILEANIM_LIST enemyAnim[ENEMY_TYPE_MAX] = 
	{
		XFILEANIM_ENEMY,
		XFILEANIM_ENEMY_DEAGON_MOVER,
		XFILEANIM_ENEMY_DEAGON_FIRE,
	};
	for(int loop = 0;loop < ENEMY_TYPE_MAX;loop++)
	{
		for(int loop1 = 0;loop1 < MAX_ENEMY;loop1++)
		{
			enemyList_m[loop][loop1] = nullptr;
			cnt_m[loop][loop1] = 0;
			enemyID_m[loop][loop1] = -1;
		}
	}
	update_m = false;
	for(int loop = 0;loop < ENEMY_TYPE_MAX;loop++)
	{
		xfileData_m[loop] = XFileManager::getXFileAnimPointer(enemyAnim[loop]);
	}
}
//==============================================================================
//デストラクタ
EnemyManager::~EnemyManager(void)
{
}
//==============================================================================
//初期化
HRESULT EnemyManager::init(void)
{

	return S_OK;
}

//==============================================================================
//更新
void EnemyManager::update(void)
{
	if(update_m)
	{
		for(int loop1 = 0;loop1 < ENEMY_TYPE_MAX;loop1++)
		{

			for(int loop = 0; loop < MAX_ENEMY;loop++)
			{
				if(enemyList_m[loop1][loop])
				{
					DATA_OBJECT dataobject[ENEMY_TYPE_MAX] =
					{
						OBJECT_ENEMY_MOVER,
						OBJECT_ENEMY_DRAGON_MOVER,
						OBJECT_ENEMY_DRAGON_SHOOTER
					};
					DATA data;
					data.ID = loop;
					D3DXVECTOR3 vec3;
					data.Object = dataobject[enemyList_m[loop1][loop]->getEnemyType()];

					data.Type = DATA_TYPE_POSITION;
					vec3 = enemyList_m[loop1][loop]->getPos();
					data.Position.x = vec3.x;
					data.Position.y = vec3.y;
					data.Position.z = vec3.z;
					GameUpdater::SendData(data);
					data.Type = DATA_TYPE_ROTATION;
					vec3 = enemyList_m[loop1][loop]->getRot();
					data.Rotation.x = vec3.x;
					data.Rotation.y = vec3.y;
					data.Rotation.z = vec3.z;
					GameUpdater::SendData(data);

				}
			}
		}

	}
}
//==============================================================================
//追加
int EnemyManager::addEnemyList(ENEMY_TYPE type,ObjectEnemy* enemyPointer)
{
	for(int loop = 0; loop < MAX_ENEMY;loop++)
	{
		if(!enemyList_m[type][loop])
		{
			enemyList_m[type][loop] = enemyPointer;
			return loop;
		}
	}
	return -1;
}
//==============================================================================
//削除
void EnemyManager::removeEnemyList(ENEMY_TYPE type,int num)
{
	if(num >= 0)
	{
		enemyList_m[type][num] = nullptr;
		cnt_m[type][num] = 1;
		enemyID_m[type][num] = -1;

	}
	enemyNum_m--;
}

bool EnemyManager::hitchk(COLLISION_DATA* obj1,ENEMY_TYPE type)
{
	for(int loop1 = 0;loop1 < ENEMY_TYPE_MAX;loop1++)
	{
		for(int loop = 0; loop < MAX_ENEMY;loop++)
		{
			if(enemyList_m[loop1][loop])
			{
				if(enemyList_m[loop1][loop])
					if(CollisionManager::HitchkCircle(obj1,enemyList_m[loop1][loop]->getCollisionData()))
					{
					obj1->rotOut = enemyList_m[loop1][loop]->getRot();
					return true;
					}
			}
		}
	}
	return false;
}
void EnemyManager::setEnemy( void )
{
	XFILEANIM_LIST enemyAnim[ENEMY_TYPE_MAX] =
	{
		XFILEANIM_ENEMY,
		XFILEANIM_ENEMY_DEAGON_MOVER,
		XFILEANIM_ENEMY_DEAGON_FIRE,
	};
	ENEMY_TYPE enemyType[ENEMY_TYPE_MAX] = 
	{
		ENEMY_MOVER,
		ENEMY_DRAGON_MOVER,
		ENEMY_SHOOTER
	};

	enemyNum_m = 0;
	for(int loop1 = 0;loop1 < ENEMY_TYPE_MAX;loop1++)
	{

		for(int loop = 0; loop < MAX_ENEMY;loop++)
		{
			cnt_m[loop1][loop]++;
			if(!enemyList_m[loop1][loop])
			{
				int setPos = MOVE_MAX / 4.0f;

				short dist = ( rand() % 4000 ) / 1000;
				D3DXVECTOR3 pos = D3DXVECTOR3(( rand() % setPos ) - setPos,0,( rand() % setPos ) - setPos);
				switch(dist)
				{
					case 0:
					pos.x = -setPos;
					break;
					case 1:
					pos.x = setPos;
					break;
					case 2:
					pos.z = -setPos;
					break;
					case 3:
					pos.z = setPos;
					break;
				};
				int Rot = 1000 * DATA_PI;
				float rotY = ( rand() % ( Rot ) ) / 1000;
				ObjectEnemy::Create(enemyType[loop1],enemyAnim[loop1],pos,D3DXVECTOR3(0,0,0),D3DXVECTOR3(0,rotY,0));
				enemyNum_m++;

			}
		}
	}
	update_m = true;
}
ObjectEnemy* EnemyManager::getEnemyObject(ENEMY_TYPE type,int id)
{
	XFILEANIM_LIST enemyAnim[ENEMY_TYPE_MAX] =
	{
		XFILEANIM_ENEMY,
		XFILEANIM_ENEMY_DEAGON_MOVER,
		XFILEANIM_ENEMY_DEAGON_FIRE,
	};
	ENEMY_TYPE enemyType[ENEMY_TYPE_MAX] =
	{
		ENEMY_MOVER,
		ENEMY_DRAGON_MOVER,
		ENEMY_SHOOTER
	};
	if(enemyList_m[type][id])
		{
			return enemyList_m[type][id];
	}
	else{
		enemyList_m[type][id] = ObjectEnemy::Create(enemyType[type],enemyAnim[type]);
	}
	update_m = true;
	return nullptr;

}

//End Of FIle

