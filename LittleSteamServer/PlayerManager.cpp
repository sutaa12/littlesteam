/******************************************************************************
@file       PlayerManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"PlayerManager.h"
#include"ObjectManager.h"
#include "SystemManager.h"
#include "CollisionManager.h"
#include "Object3DXfileAnim.h"
#include "data.h"
Object3DXfileAnim* PlayerManager::playerList_m[MAX_PLAYER];
int PlayerManager::cnt_m[MAX_PLAYER];
int PlayerManager::playerNum_m;
bool PlayerManager::update_m;
int PlayerManager::idNeum[MAX_PLAYER];
XFILEANIM_DATA* PlayerManager::xfileData_m;
//==============================================================================
//コンストラクタ
PlayerManager::PlayerManager(void)
{

}
//==============================================================================
//デストラクタ
PlayerManager::~PlayerManager(void)
{
}
//==============================================================================
//リセット
void PlayerManager::resetPlayer(void)
{
	for(int loop = 0;loop < MAX_PLAYER;loop++)
	{
		cnt_m[loop] = 1;
		idNeum[loop] = -1;
	}

}
//==============================================================================
//初期化
HRESULT PlayerManager::init(void)
{
	xfileData_m = XFileManager::getXFileAnimPointer(XFILEANIM_PLAYER);

	for(int loop = 0;loop < MAX_PLAYER;loop++)
	{
		playerList_m[loop] = nullptr;
		cnt_m[loop] = 1;
		idNeum[loop] = -1;
	}
	update_m = false;

	for(int loop = 0;loop < MAX_PLAYER;loop++)
	{
		cnt_m[loop] = 1;
		idNeum[loop] = -1;
		playerList_m[loop] = Object3DXfileAnim::Create(XFILEANIM_PLAYER);
		playerList_m[loop]->setDrawFlag(false);


	}

	return S_OK;
}

//==============================================================================
//更新
void PlayerManager::update(void)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{

			if(playerList_m[loop])
			{
				if(playerList_m[loop]->getDrawFlag())
				{
					COLLISION_DATA *col;
					col = playerList_m[loop]->getCollisionData();
					col->rotOut = playerList_m[loop]->getRot();
				}

			}
		}
}
//==============================================================================
//削除
void PlayerManager::removePlayerList(int num)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(idNeum[loop] == num)
		{
			cnt_m[loop] = 1;
			idNeum[loop] = -1;
			if(playerList_m[loop])
			{
				playerList_m[loop]->setDrawFlag(false);
			}
		}
	}
}

bool PlayerManager::hitchk(COLLISION_DATA* obj1)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(playerList_m[loop])
		{
			if(playerList_m[loop])
				if(CollisionManager::HitchkCircle(obj1,playerList_m[loop]->getCollisionData()))
				{
				obj1->rotOut = playerList_m[loop]->getRot();
				return true;
				}
		}
	}
	return false;
}
COLLISION_DATA* PlayerManager::hitchkList(COLLISION_DATA* obj1)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(playerList_m[loop])
		{
			if(playerList_m[loop]->getDrawFlag())
				if(CollisionManager::HitchkCircle(obj1,playerList_m[loop]->getCollisionData()))
				{
				obj1->rotOut = playerList_m[loop]->getRot();
				return playerList_m[loop]->getCollisionData();
				}
		}
	}
	return false;
}

void PlayerManager::setPlayer(void)
{

}
Object3DXfileAnim* PlayerManager::getPlayer(int id)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(idNeum[loop] == id)
		{
			return playerList_m[loop];

		}
	}

	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(idNeum[loop] == -1)
		{
			idNeum[loop] = id;
			if(playerList_m[loop])
			{
				playerList_m[loop]->setDrawFlag(true);
				return playerList_m[loop];
			}
		}
	}
	return nullptr;

}
//End Of FIle

