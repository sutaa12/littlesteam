/******************************************************************************/
/*! @addtogroup Object_ObjectBar
@file       Object.h
@brief      ObjectBarクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECTBAR_FILE
#define COBJECTBAR_FILE
#include "main.h"
#include "Common.h"
#include "Object2D.h"
#include "TextureManager.h"
class Object2D;
class ObjectBar: public Object2D
{
	//コンストラクタ
	public:
	/*! ObjectBar ObjectBarのコンストラクタ
	@param[in]     type    2Dオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectBar(int maxLifeSize,D3DXCOLOR col,PRIORITY_MODE priority,OBJECT2D_TYPE type,OBJTYPE objType = OBJTYPE_2D);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectBar ObjectBarのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectBar(void);
	//操作
	public:
	/*! Create ObjectBarの作成
	@param[in]     maxLifeSize   ライフのサイズ
	@param[in]     col    色設定
	@param[in]     texName    テクスチャの名前定義から設定(TEXTURE_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot    角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    2Dのタイプの設定の設定
	*/
	static ObjectBar* Create(int maxLifeSize,D3DXCOLOR col = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),TEXTURE_LIST texName = TEXTURE_NONE,D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT2D_TYPE type = OBJCT2D_NORMAL,PRIORITY_MODE priority = PRIORITY_7);/*!< 作成時設定*/
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	int getBarLife(void){ return barLife_m; }
	void AddBarLife(int life){ barLifeSpeed_m += life; }
	void setBarLifeMax(int lifeMax){ barLifeMax_m = lifeMax; }
	
	private:
	//メンバ変数
	int barLife_m;//ライフの値
	int barLifeSpeed_m;//ライフの値
	int barLifeMax_m;//設定するライフの最大値
};
#endif

//End Of FIle