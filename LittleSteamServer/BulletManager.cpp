/******************************************************************************
@file       BulletManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"BulletManager.h"
#include "ObjectBullet.h"
#include"ObjectManager.h"
#include "CollisionManager.h"
#include "GameUpdate.h"
ObjectBullet* BulletManager::bulletList_m[BULLET_TYPE_MAX][MAX_BULLET];
//==============================================================================
//コンストラクタ
BulletManager::BulletManager(void)
{

}
//==============================================================================
//デストラクタ
BulletManager::~BulletManager(void)
{
}
//==============================================================================
//初期化
HRESULT BulletManager::init(void)
{
	for(int loop = 0;loop < BULLET_TYPE_MAX;loop++)
	{
		for(int loop1 = 0;loop1 < MAX_BULLET;loop1++)
		{
			bulletList_m[loop][loop1] = nullptr;
		}
	}	return S_OK;
}

//==============================================================================
//更新
void BulletManager::update(void)
{
	for(int loop1 = 0;loop1 < BULLET_TYPE_MAX;loop1++)
	{
		for(int loop = 0;loop < MAX_BULLET;loop++)
		{
			if(bulletList_m[loop1][loop])
			{
				DATA_OBJECT dataobject[BULLET_TYPE_MAX] =
				{
					OBJECT_PLAYER_BULLET,
					OBJECT_ENEMY_BULLET,
				};
				BULET_ID bulletID = bulletList_m[loop1][loop]->getBulletID();
				DATA data;
				data.ID = bulletID.bulletID;
				data.Number = bulletID.bulletNum;
				D3DXVECTOR3 vec3;

				data.Object = dataobject[bulletList_m[loop1][loop]->getBulletType()];

				data.Type = DATA_TYPE_POSITION;
				vec3 = bulletList_m[loop1][loop]->getPos();
				data.Position.x = vec3.x;
				data.Position.y = vec3.y;
				data.Position.z = vec3.z;
				GameUpdater::SendData(data);
				data.Type = DATA_TYPE_ROTATION;
				vec3 = bulletList_m[loop1][loop]->getRot();
				data.Rotation.x = vec3.x;
				data.Rotation.y = vec3.y;
				data.Rotation.z = vec3.z;
				GameUpdater::SendData(data);

				data.Type = DATA_TYPE_SCALE;
				vec3 = bulletList_m[loop1][loop]->getScl();
				data.Scale.x = vec3.x;
				data.Scale.y = vec3.y;
				data.Scale.z = vec3.z;
				GameUpdater::SendData(data);
				data.Type = DATA_TYPE_TIME;
				data.Time = bulletList_m[loop1][loop]->getBulletTime();
				GameUpdater::SendData(data);

			}
		}
	}
}
//==============================================================================
//追加
int BulletManager::addBulletList(BULLET_TYPE type,ObjectBullet* bulletPointer)
{
	for(int loop = 0; loop < MAX_BULLET;loop++)
	{
		if(!bulletList_m[type][loop])
		{
			bulletList_m[type][loop] = bulletPointer;
			return loop;
		}
	}
	return -1;
}
//==============================================================================
//削除
void BulletManager::removeBulletList(BULLET_TYPE type,int num)
{
	if(num >= 0)
	{
		bulletList_m[type][num] = nullptr;
	}
}

bool BulletManager::hitchk(COLLISION_DATA* obj1,BULLET_TYPE type)
{
	for(int loop = 0; loop < MAX_BULLET;loop++)
	{
		if(bulletList_m[type][loop])
		{
			if(bulletList_m[type][loop])
				if(CollisionManager::HitchkCircle(obj1,bulletList_m[type][loop]->getCollisionData()))
				{

				bulletList_m[type][loop]->uninit();
				return true;
				}
		}
	}
	return false;
}

ObjectBullet* BulletManager::getBulletObject(BULLET_TYPE type,int id,short number)
{

	BULET_ID bulletID;
	for(int loop = 0; loop < MAX_BULLET;loop++)
	{
		if(bulletList_m[type][loop])
		{
			bulletID = bulletList_m[type][loop]->getBulletID();
			if(bulletID.bulletID == id && bulletID.bulletNum == number)
			{
				return bulletList_m[type][loop];
			}
		}
	}
	BULLET_TYPE bulletType[BULLET_TYPE_MAX] =
	{
		BULLET_PLAYER,
		BULLET_ENEMY,
	};
	bulletID.bulletID = id;
	bulletID.bulletNum = number;

	ObjectBullet* objBullet;
	objBullet = ObjectBullet::Create(bulletType[type],D3DXVECTOR3(0,0,0),0);
	objBullet->setBulletID(bulletID);

	return objBullet;
}
//End Of FIle

