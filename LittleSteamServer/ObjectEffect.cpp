/******************************************************************************
@file       ObjectEffect.cpp
@brief      ObjectEffectの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectEffect.h"
#include "EffectManager.h"
#include "CollisionManager.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectEffect* ObjectEffect::Create(EFFECT_TYPE effectType,D3DXVECTOR3 position,float rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	ObjectEffect *objectPointer;

	objectPointer = new ObjectEffect(effectType,rot,priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectEffect::ObjectEffect(EFFECT_TYPE effectType,float rot,PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3DBillboard(priority,objType)
{
	TEXTURE_LIST texlist[EFFECT_TYPE_MAX] = {
		TEXTURE_SMOKE,
	};
	rotY_m = rot;
	effectType_m = effectType;
	setTexName(texlist[effectType]);
	effecthundle_m = EffectManager::addEffectList(effectType_m,this);
	init();
	if(effecthundle_m < 0)
	{
		uninit();
	}
}
//==============================================================================
//デストラクタ
ObjectEffect::~ObjectEffect(void)
{

}
//==============================================================================
//初期化
void ObjectEffect::init(void)
{
	sclSpeed_m = 0;
	D3DXVECTOR3 moveSpeed[EFFECT_TYPE_MAX] =
	{
		D3DXVECTOR3(( rand() % 10 ) + 25,(rand() % 10) - 10,0)
	};
	D3DXVECTOR3 size[EFFECT_TYPE_MAX] =
	{
		D3DXVECTOR3(256,256,0)
	};
	if(effectType_m == EFFECT_EXPLOSION)
	{
		setDrawAlphaMode();
		setColorSpeed(D3DXCOLOR(0,0,0,0.006f));
		setColor(D3DXCOLOR(1.0f,0.6f,0.6f,1.0f));
		setColorEd(D3DXCOLOR(1.0f,0.2f,0.2f,0.0f));
		sclSpeed_m = 0.01f;
		rotY_m += ( ( rand() & 400 ) - 200 ) / 1000.0f;
	}
	posSpeed_m = moveSpeed[effectType_m];
	setSize(size[effectType_m]);
	getCollisionData()->radius = size_m.x / 2;
	getCollisionData()->pos = pos_m;
	cnt_m = 0;
}
//==============================================================================
//解放
void ObjectEffect::uninit(void)
{
	EffectManager::removeEffectList(effectType_m,effecthundle_m);
	Object3DBillboard::uninit();
}
//==============================================================================
//更新
void ObjectEffect::update(void)
{
		Object3DBillboard::update();
	getCollisionData()->pos = pos_m;
	pos_m.x -= sinf(rotY_m)*posSpeed_m.x;
	pos_m.y += posSpeed_m.y;
	pos_m.z -= cosf(rotY_m)*posSpeed_m.x;
	if(pos_m.y + ( scl_m.x * size_m.x ) <= CollisionManager::HitchkField(pos_m,NULL))
	{
		pos_m.y = CollisionManager::HitchkField(pos_m,NULL) + ( scl_m.x * size_m.x );
	}
	cnt_m++;
	if(effectType_m == EFFECT_EXPLOSION)
	{
		scl_m.x += sclSpeed_m;
		scl_m.y += sclSpeed_m;
		scl_m.z += sclSpeed_m;
		getCollisionData()->radius = ( scl_m.x * size_m.x ) / 2;
	}
	if(getColor().a <= 0 || cnt_m >= 100)
	{
		uninit();
	}
}

//End Of FIle