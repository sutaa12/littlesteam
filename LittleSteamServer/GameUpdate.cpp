/******************************************************************************
@file       GameUpdater.cpp
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#include "GameUpdate.h"
#include "SystemManager.h"
//*****************************************************************************
// 静的変数
//*****************************************************************************
SOCKET GameUpdater::m_Socket = INVALID_SOCKET;
int GameUpdater::m_ID = -1;
SOCKADDR_IN	GameUpdater::m_ServerAddress;
bool GameUpdater::UpdaterFlag = true;
SystemManager* GameUpdater::systemManager;


//=============================================================================
// 初期化処理
//=============================================================================
void GameUpdater::Init(void)
{

	systemManager = new SystemManager;
	uintptr_t ptr;
	// 受信スレッド開始
	ptr = _beginthreadex(NULL,0,Update,NULL,0,NULL);
	UpdaterFlag = true;
}


//=============================================================================
// 終了処理
//=============================================================================
void GameUpdater::Uninit(void)
{
	UpdaterFlag = false;

	SAFE_DELETE(systemManager);

}

//=============================================================================
// 更新スレッド
//=============================================================================
unsigned __stdcall GameUpdater::Update(LPVOID Param)
{
	while(UpdaterFlag)
	{
		systemManager->update();
	}
	return 0;
}


//=============================================================================
// データ送信
//=============================================================================
void GameUpdater::SendData(DATA Data)
{
	if(UpdaterFlag)
	{
		MainSendData(Data);
	}
}
