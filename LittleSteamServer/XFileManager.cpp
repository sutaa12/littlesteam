/******************************************************************************
@file       XFileManager.cpp
@brief      テクスチャを管理するクラス テクスチャのポインタを返す
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//==============================================================================
#include "XFileManager.h"
#include "TextureManager.h"
XFILE_DATA XFileManager::d3DXFiles_m[XFILE_MAX];	// テクスチャへのポインタの配列
XFILEANIM_DATA XFileManager::d3DXAnimFiles_m[XFILEANIM_MAX];	// テクスチャへのポインタの配列
//==============================================================================
//コンストラクタ
XFileManager::XFileManager(void)
{
	init();
}
//==============================================================================
//デストラクタ
XFileManager::~XFileManager(void)
{
	for(int loop = 0;loop < XFILE_MAX;loop++)
	{
		SAFE_RELEASE(d3DXFiles_m[loop].d3DXBuffMatModel);
		SAFE_RELEASE(d3DXFiles_m[loop].d3DXMeshModel);
	}
	for(int loop = 0;loop < XFILEANIM_MAX;loop++)
	{
		d3DXAnimFiles_m[loop].AnimCtr.Release();
		d3DXAnimFiles_m[loop].Alloc.Release();
	}
}
//==============================================================================
//初期化
HRESULT XFileManager::init(void)
{
	return S_OK;
}

//==============================================================================

//End Of FIle

