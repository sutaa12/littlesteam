/******************************************************************************
@file       Object2D.cpp
@brief      Object2Dの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "Object2D.h"
//================================================================================
//タイプ別作成
//================================================================================
Object2D* Object2D::Create(TEXTURE_LIST texName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT2D_TYPE type,PRIORITY_MODE nPriority)
{
	Object2D *objectPointer;
	
	objectPointer = new Object2D(nPriority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setSize(size);
		objectPointer->setTexName(texName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
Object2D::Object2D(PRIORITY_MODE priority,OBJTYPE objType,OBJECT2D_TYPE type):Object(priority,objType)
{
	//メンバ初期化
	pos_m = D3DXVECTOR3(0,0,0);
	rot_m = D3DXVECTOR3(0,0,0);
	rotSpeed_m = D3DXVECTOR3(0,0,0);
	texPos_m[0] = D3DXVECTOR2(0.0f,0.0f);
	texPos_m[1] = D3DXVECTOR2(1.0f,0.0f);
	texPos_m[2] = D3DXVECTOR2(0.0f,1.0f);
	texPos_m[3] = D3DXVECTOR2(1.0f,1.0f);
	size_m = D3DXVECTOR3(0,0,0);
	length_m = 0;
	angle_m = 0;
	col_m = colSt_m = colEd_m = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
	colSpeed_m= D3DXCOLOR(0,0,0,0);
	length_m = sqrtf(( size_m.x / 2.0f* size_m.x / 2.0f ) + ( size_m.y / 2.0f* size_m.y / 2.0f ));
	angle_m = atan2(size_m.x / 2.0f,size_m.y / 2.0f);
	rot_m.z += rotSpeed_m.z;
	type2D_m = type;
	textureName_m = TEXTURE_NONE;
	init();

}
//==============================================================================
//デストラクタ
Object2D::~Object2D(void)
{

}
//==============================================================================
//初期化
void Object2D::init(void)
{

}
//==============================================================================
//解放
void Object2D::uninit(void)
{

	release();
}
//==============================================================================
//更新
void Object2D::update(void)
{
	length_m = sqrtf(( size_m.x / 2.0f* size_m.x / 2.0f ) + ( size_m.y / 2.0f* size_m.y / 2.0f ));
	angle_m =  atan2(size_m.x / 2.0f,size_m.y / 2.0f);
	rot_m.z += rotSpeed_m.z;
	col_m += colSpeed_m;
	COLOR_CHK(colSpeed_m.r,col_m.r,colSt_m.r,colEd_m.r);
	COLOR_CHK(colSpeed_m.g,col_m.g,colSt_m.g,colEd_m.g);
	COLOR_CHK(colSpeed_m.b,col_m.b,colSt_m.b,colEd_m.b);
	COLOR_CHK(colSpeed_m.a,col_m.a,colSt_m.a,colEd_m.a);
}

//End Of FIle