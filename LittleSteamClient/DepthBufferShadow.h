//=============================================================================
//
// アニメーション処理処理 [DepethBufferShadow.h]
// Author : NARITADA SUZUKI
//
//=============================================================================
#ifndef _DEPTHBUFFER_H_
#define _DEPTHBUFFER_H_
#include "Common.h"

class SOFTSHADOW1
{
private:
   LPD3DXEFFECT m_pEffect;
   D3DXHANDLE m_pTechnique, m_pWVP, m_pWVPT, m_pLWVP, m_pLWVPT, m_pLightDir, m_pAmbient, m_pBias, m_pShadowColor;
   D3DXMATRIX m_matView, m_matProj;
   LPDIRECT3DDEVICE9 m_pd3dDevice;

public:
   SOFTSHADOW1( LPDIRECT3DDEVICE9 pd3dDevice );
   ~SOFTSHADOW1();
   void Invalidate();
   void Restore();
   HRESULT Load();
   void Begin();
   void BeginPass( UINT Pass );
   void SetAmbient( float Ambient );
   void SetAmbient( D3DXVECTOR4* pAmbient );
   void SetMatrix( D3DXMATRIX* pMatWorld, D3DXMATRIX* pMatLight, D3DXVECTOR4* pLightDir );
   void SetBias( float pBias );
   void SetShadowColor( float pShadowColor );
   void CommitChanges();
   void EndPass();
   void End();
   BOOL IsOK();
   LPD3DXEFFECT GetEffect(){ return m_pEffect; };
};

#endif
//End Of File