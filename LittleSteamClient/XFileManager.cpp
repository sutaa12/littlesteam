/******************************************************************************
@file       XFileManager.cpp
@brief      テクスチャを管理するクラス テクスチャのポインタを返す
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//==============================================================================
#include "XFileManager.h"
#include "DrawManager.h"
#include "TextureManager.h"
XFILE_DATA XFileManager::d3DXFiles_m[XFILE_MAX];	// テクスチャへのポインタの配列
XFILEANIM_DATA XFileManager::d3DXAnimFiles_m[XFILEANIM_MAX];	// テクスチャへのポインタの配列
//==============================================================================
//コンストラクタ
XFileManager::XFileManager(void)
{
	init();
}
//==============================================================================
//デストラクタ
XFileManager::~XFileManager(void)
{
	for(int loop = 0;loop < XFILE_MAX;loop++)
	{
		SAFE_RELEASE(d3DXFiles_m[loop].d3DXBuffMatModel);
		SAFE_RELEASE(d3DXFiles_m[loop].d3DXMeshModel);
	}
	for(int loop = 0;loop < XFILEANIM_MAX;loop++)
	{
		d3DXAnimFiles_m[loop].AnimCtr.Release();
		d3DXAnimFiles_m[loop].Alloc.Release();
	}
}
//==============================================================================
//初期化
HRESULT XFileManager::init(void)
{
	const char* XFILE_NAME_LIST[XFILE_MAX] =
	{
		"./data/MODEL/builiding01.x"
	};
	const TEXTURE_LIST XFILETEXTURE_NAME_LIST[XFILE_MAX] =
	{
		TEXTURE_BILL01
	};

	const char* XFILEANIM_NAME_LIST[XFILEANIM_MAX] =
	{
		"./data/MODEL/player.x",
		"./data/MODEL/enemy.x",
		"./data/MODEL/enemydrangon.x",
		"./data/MODEL/enemydrangon.x",
	};
	const TEXTURE_LIST XFILEANIMTEXTURE_NAME_LIST[XFILEANIM_MAX] =
	{
		TEXTURE_PLAYER,
		TEXTURE_ENEMY2,
		TEXTURE_ENEMY,
		TEXTURE_ENEMY,
	};
	LPDIRECT3DDEVICE9 device = DrawManager::getD3DDEVICE();

	for(int loop = 0;loop < XFILE_MAX;loop++)
	{
		if(FAILED(D3DXLoadMeshFromX(XFILE_NAME_LIST[loop],D3DXMESH_SYSTEMMEM,device,NULL,&d3DXFiles_m[loop].d3DXBuffMatModel,NULL,&d3DXFiles_m[loop].numMatModel,&d3DXFiles_m[loop].d3DXMeshModel)))
		{
			return E_FAIL;
		}
		d3DXFiles_m[loop].d3DTexture = TextureManager::getTexturePointer(XFILETEXTURE_NAME_LIST[loop]);
	}
	for(int loop = 0;loop < XFILEANIM_MAX;loop++)
	{
		LPDIRECT3DTEXTURE9 texPointer = TextureManager::getTexturePointer(XFILEANIMTEXTURE_NAME_LIST[loop]);
		d3DXAnimFiles_m[loop].Alloc.Load(device,XFILEANIM_NAME_LIST[loop],texPointer);
		d3DXAnimFiles_m[loop].AnimCtr.SetAnimationController(d3DXAnimFiles_m[loop].Alloc.GetAnimeCtrl());
	}
	return S_OK;
}

//==============================================================================

//End Of FIle

