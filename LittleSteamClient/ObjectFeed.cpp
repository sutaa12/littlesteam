//=============================================================================
//
// シーン2D処理 [ObjectFeed.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "ObjectFeed.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectFeed* ObjectFeed::Create()
{
	ObjectFeed *pScene2D;
	pScene2D = new ObjectFeed;
	if(pScene2D)
	{
		return pScene2D;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectFeed::ObjectFeed(PRIORITY_MODE nPriority,OBJECT2D_TYPE objType):Object2D(nPriority,OBJTYPE_2D,objType)
{
	//メンバ初期化
	m_bFeedFlag = false;
	m_Color = D3DXCOLOR(1.0f,1.0f,1.0f,0.0f);
	m_fAlfa = 0.0f;
	m_FeedMode = FEED_NONE;
	m_nTime = 0;
	setPos(D3DXVECTOR3(SCREEN_WIDTH / 2.0f,SCREEN_HEIGHT / 2.0f,0.0f));
	setSize(D3DXVECTOR3(SCREEN_WIDTH,SCREEN_HEIGHT,0));
	init();

}
//==============================================================================
//デストラクタ
ObjectFeed::~ObjectFeed(void)
{

}
//==============================================================================
//初期化
void ObjectFeed::init()
{
	Object2D::init();
}
//==============================================================================
//解放
void ObjectFeed::uninit(void)
{
	Object2D::uninit();
}
//==============================================================================
//更新
void ObjectFeed::update(void)
{
	setDrawFlag(m_bFeedFlag);
	//フェードフラグがあれば更新
	if(m_bFeedFlag)
	{
		//フェード処理
		if(m_FeedMode == FEED_IN_END)
		{
			m_FeedMode = FEED_NONE;
		}
		else

			if(m_FeedMode == FEED_OUT_END)
			{
			m_FeedMode = FEED_NONE;
			}

		//フェード処理
		if(m_FeedMode == FEED_IN_PROGRESS)
		{
			m_fAlfa += ( 1.0f / m_nTime );
			//フェードが1を超えたら
			if(m_fAlfa > 1.0f)
			{
				m_fAlfa = 1.0f;
				m_FeedMode = FEED_IN_END;
			}

		}


		if(m_FeedMode == FEED_OUT_PROGRESS)
		{
			//フェードアウト
			if(m_fAlfa >= 0.0f)
			{
				m_fAlfa -= ( 1.0f / m_nTime );
			}
			//フェードが0以下なら０にする
			if(m_fAlfa < 0.0f)
			{
				m_fAlfa = 0.0f;
				m_FeedMode = FEED_OUT_END;
			}
		}


		//フェードが1以上なら1にする
		if(m_fAlfa > 1.0f)
		{
			m_fAlfa = 1.0f;
		}
		//フェードが0以下なら０にする
		if(m_fAlfa < 0.0f)
		{
			m_fAlfa = 0.0f;
		}
		if(m_FeedMode == FEED_NONE)
		{
			m_fAlfa = 0.0f;
			m_nTime = 0;
			m_bFeedFlag = false;
		}
		if(m_FeedMode == FEED_IN_END)
		{
			m_fAlfa = 1.0f;
		}

		m_Color.a = m_fAlfa;
		Object2D::setColor(m_Color);
		Object2D::update();
	}
}

//==============================================================================
//フェードスタート
void ObjectFeed::feedStart(FEED_MODE FeedMode,int Time,D3DXCOLOR Col)
{
	m_FeedMode = FeedMode;
	m_nTime = Time - 1;
	m_Color = Col;
	m_bFeedFlag = true;
	if(m_FeedMode == FEED_IN_PROGRESS)
	{
		m_fAlfa = 0.0f;
	}
	else
		if(m_FeedMode == FEED_OUT_PROGRESS)
		{
		m_fAlfa = 1.0f;
		}
}

//End Of FIle