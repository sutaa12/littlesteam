/******************************************************************************/
/*! @addtogroup Draw_XFileManager
@file       XFileManager.h
@brief      テクスチャを管理するクラス テクスチャのポインタを返す
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef XFILE_MANAGE_FILE
#define XFILE_MANAGE_FILE
#include "Common.h"
#include "Animation.h"
#include "Xallocatehierachy.h"
/*! XFILE_LIST アニメーションなしのXファイル割り当て*/
typedef enum XFILE_LIST
{
	XFILE_BILL = 0,
	XFILE_MAX,
};
/*! XFILEANIM_LIST アニメーションありのXファイル割り当て*/
typedef enum XFILEANIM_LIST
{
	XFILEANIM_PLAYER = 0,
	XFILEANIM_ENEMY,
	XFILEANIM_ENEMY_DEAGON_MOVER,
	XFILEANIM_ENEMY_DEAGON_FIRE,
	XFILEANIM_MAX,
};

/*! XFILE_DATA アニメーションなしのXファイル構造体*/
typedef struct
{
	LPD3DXMESH d3DXMeshModel;				//メッシュ情報へのポインタ
	LPD3DXBUFFER d3DXBuffMatModel;			//マテリアル情報へのポインタ
	LPDIRECT3DTEXTURE9 d3DTexture;//テクスチャのポインタ
	DWORD numMatModel;						//マテリアル情報数
}XFILE_DATA;
/*! XFILEANIM_DATA アニメーションありのXファイル構造体*/
typedef struct
{
	AllocateHierarchy Alloc;		//メッシュの構造クラス
	HighLevelAnimController AnimCtr;	//アニメーションコントローラー
}XFILEANIM_DATA;
/*! @class XFileManager
@brief  オブジェクト管理クラス 各オブジェクトの管理クラスの管理をする ハンドルの登録も行う
*/
class XFileManager
{
	//メンバ関数
	//コンストラクタ
	public:
	/*! XFileManager　コンストラクタ*/
	XFileManager(void);
	//デストラクタ
	public:
	/*! ~XFileManager　デストラクタ*/
	~XFileManager(void);

	//操作
	/*! init 用意したテクスチャをポインタにしておく*/
	HRESULT init(void);
	/*! getTexPointer テクスチャのポインタを取得*/
	static XFILE_DATA* getXFilePointer(XFILE_LIST xfilelist){ return &d3DXFiles_m[xfilelist]; }
	static XFILEANIM_DATA* getXFileAnimPointer(XFILEANIM_LIST xfilelist){ return &d3DXAnimFiles_m[xfilelist]; }


	//属性
	public:
	static XFILE_DATA d3DXFiles_m[XFILE_MAX];	// テクスチャへのポインタの配列
	static XFILEANIM_DATA d3DXAnimFiles_m[XFILEANIM_MAX];	// テクスチャへのポインタの配列
	private:

};
#endif

//End Of File