/******************************************************************************
@file       ObjectManager.cpp
@brief      ゲームのオブジェクトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#include "ObjectManager.h"
#include "Object.h"
#include "InputKeyboard.h"
#include "TextureManager.h"
#include "MessageProc.h"
Object* ObjectManager::objectHandle_m[MAX_OBJECT + 1];
Object** ObjectManager::objectHandlePointer_m;
//==============================================================================
//コンストラクタ
//==============================================================================
ObjectManager::ObjectManager(void)
{

}
//==============================================================================
//デストラクタ
//==============================================================================
ObjectManager::~ObjectManager(void)
{
	Object::releaseAll();
	SAFE_DELETE_ARRAY(objectHandlePointer_m);
}
//==============================================================================
//OBJのアップデートをする
//==============================================================================
void ObjectManager::update(void)
{
	Object::updateAll();
}
//==============================================================================
//ハンドルをリストに登録
//==============================================================================
bool ObjectManager::setGameObjectHandle(Object* objectPointer,unsigned int& handleId)
{

	//開いてるハンドルを探す
	PRIORITY_MODE priority = objectPointer->getPriority();
	for(int loop = (priority)* ( MAX_OBJECT / PRIORITY_MAX );loop < ( priority + 1 ) * ( MAX_OBJECT / PRIORITY_MAX );loop++)
	{
		if(objectHandle_m[loop] == nullptr)
		{
			objectHandle_m[loop] = objectPointer;
			handleId = loop;
			return true;
		}
	}
	return false;
}
//==============================================================================
//ハンドルをリストから解除
//==============================================================================
void ObjectManager::unsetGameObjectHandle(unsigned int& handleId)
{
	if(!NUM_CHK(handleId,0,MAX_OBJECT))
	{
		return;
	}
	//ハンドルに値があれば
	if(objectHandle_m[handleId] != nullptr)
	{
		//解放する
		objectHandle_m[handleId]->release();
		objectHandle_m[handleId] = nullptr;
	}
}
//==============================================================================
//ハンドルとOBJの種類から検索一致しない場合NULLを返す
//==============================================================================
Object* ObjectManager::getGameObjectHandle(unsigned int handleId,OBJTYPE uniqueId)
{
	if(!NUM_CHK(handleId,0,MAX_OBJECT))
	{
		return nullptr;
	}
	//ハンドルが殻でなくオブジェクトのIDが一致すればOBJのポインタを返す
	if(objectHandle_m[handleId] != nullptr && objectHandle_m[handleId]->getObjType() == uniqueId)
	{
		return objectHandle_m[handleId];
	}
	return nullptr;
}
//==============================================================================
//OBJの種類から検索 一致しない場合NULLを返す
//==============================================================================
Object** ObjectManager::getGameObjectHandle(OBJTYPE uniqueId,unsigned int& objNum)
{
	objNum = 0;
	//指定した種類のオブジェの数をカウントする
	for(int loop = 0;loop < MAX_OBJECT;loop++)
	{
		if(objectHandle_m[loop] != nullptr && objectHandle_m[loop]->getObjType() == uniqueId)
		{
			objNum++;
		}
	}
	if(objNum == 0)
	{
		return nullptr;
	}
	//objDataが空かチェック空でなければ解放
	SAFE_DELETE_ARRAY(objectHandlePointer_m);
	objectHandlePointer_m = new Object*[objNum];

	int objNumChk = 0;//オブジェの数が一致したらfor分終了

	//指定した種類のオブジェの数をカウントする 指定したオブジェクトタイプ分格納したら抜ける
	for(int loop = 0;loop < MAX_OBJECT;loop++)
	{
		if(objectHandle_m[loop] != nullptr && objectHandle_m[loop]->getObjType() == uniqueId)
		{
			objectHandlePointer_m[objNumChk] = objectHandle_m[loop];
			objNumChk++;
			if(objNumChk == objNum)
			{
				return objectHandlePointer_m;
			}
		}
	}
	return nullptr;
}

//==============================================================================
//OBJの種類以外の検索 一致しない場合NULLを返す
//==============================================================================
Object** ObjectManager::getGameObjectHandleNotOnly(OBJTYPE uniqueId,unsigned int& objNum)
{
	objNum = 0;
	//指定した以外の種類のオブジェの数をカウントする
	for(int loop = 0;loop < MAX_OBJECT;loop++)
	{
		if(objectHandle_m[loop] != nullptr && objectHandle_m[loop]->getObjType() != uniqueId)
		{
			objNum++;
		}
	}
	if(objNum == 0)
	{
		return nullptr;
	}
	//objDataが空かチェック空でなければ解放
	SAFE_DELETE_ARRAY(objectHandlePointer_m);
	objectHandlePointer_m = new Object*[objNum];

	int objNumChk = 0;//オブジェの数が一致したらfor分終了

	//指定した以外の種類のオブジェの数をカウントする 指定したオブジェクトタイプ分格納したら抜ける
	for(int loop = 0;loop < MAX_OBJECT;loop++)
	{
		if(objectHandle_m[loop] != nullptr && objectHandle_m[loop]->getObjType() != uniqueId)
		{
			objectHandlePointer_m[objNumChk] = objectHandle_m[loop];
			objNumChk++;
			if(objNumChk == objNum)
			{
				return objectHandlePointer_m;
			}
		}
	}
	return nullptr;
}
//End Of Files
