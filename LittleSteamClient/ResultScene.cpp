//=============================================================================
//
// リザルトシーン [ResultScene.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "SceneManager.h"
#include "ObjectManager.h"
#include "ResultScene.h"
#include "ObjectNumbers.h"
#include "Object3DFigure.h"
#include "Object3DXfileAnim.h"
#include "XFileManager.h"
#include "TreeManager.h"
#include "ObjectBullet.h"
int ResultScene::score_m = 0;
class Impl
{
 public:
 Impl(void)
  :treeMnager_m()
  ,sky_m()
  ,EnenmyRot_m()
  ,tutolial_m()
 {}
 ~Impl(void){
  SAFE_DELETE(treeMnager_m);
 }
 static const int TITLE_ENEMY_MAX = 6;
 static const int MOVESPEED = -15000;
 static const int MOVESPEED_Y = 50;
 static const int POS_Y = 6000;
 static const int ENEMY_RADIUS = 100;
 static const int PLAYER_RADIUS = 100;
 static const int PLAYER_SPEED = 20;
 static const int ENEMY_SPEED = 20;
 Object3DXfileAnim* Enenmy_m[TITLE_ENEMY_MAX];
 float EnenmyRot_m[TITLE_ENEMY_MAX];
 TreeManager* treeMnager_m;
 Object3DFigure* sky_m;
 Object2D* tutolial_m;

};
//==============================================================================
//コンストラクタ
ResultScene::ResultScene(void)
{

}
//==============================================================================
//デストラクタ
ResultScene::~ResultScene(void)
{
	Uninit();
}
//==============================================================================
//初期化
HRESULT ResultScene::Init(void)
{
 imp_m = new Impl();
 Object3DFigure::Create(FIGURE_MESHCYRINDER,TEXTURE_MOUNTAIN,12,2,D3DXVECTOR3(0,-300,0),D3DXVECTOR3(16000,0,1500));
 imp_m->sky_m = Object3DFigure::Create(FIGURE_MESHDOOM,TEXTURE_SKY,12,12,D3DXVECTOR3(0,-500,0),D3DXVECTOR3(30000,0,40000));
 imp_m->sky_m->setRotSpeed(D3DXVECTOR3(0,0.002f,0));
 Object3DFigure::Create(FIGURE_MESHFIELD,TEXTURE_FIELD,0,0,D3DXVECTOR3(0,-3200,0),D3DXVECTOR3(FIELD_MAX,0,FIELD_MAX));
 float rot_one = D3DX_PI * 2 / Impl::TITLE_ENEMY_MAX;
 for(int loop = 0;loop < Impl::TITLE_ENEMY_MAX;loop++)
 {
  float rot_by = rot_one * loop;
  imp_m->Enenmy_m[loop] = Object3DXfileAnim::Create(XFILEANIM_ENEMY_DEAGON_MOVER);
  if(loop % 2 == 0)
  {
   imp_m->Enenmy_m[loop]->setPos(D3DXVECTOR3(sinf(rot_by) * Impl::MOVESPEED,Impl::POS_Y + sinf(rot_by) * Impl::MOVESPEED_Y,cosf(rot_by) * Impl::MOVESPEED));
  }
  else{
   imp_m->Enenmy_m[loop]->setPos(D3DXVECTOR3(sinf(rot_by) * Impl::MOVESPEED,Impl::POS_Y + cosf(rot_by) * Impl::MOVESPEED_Y,cosf(rot_by) * Impl::MOVESPEED));
  }
  imp_m->Enenmy_m[loop]->setRot(D3DXVECTOR3(0,rot_by + D3DX_PI / 2,0));
  imp_m->Enenmy_m[loop]->setScl(D3DXVECTOR3(0.2f,0.2f,0.2f));

 }
 player_m = Object3DXfileAnim::Create();
 player_m->setPos(D3DXVECTOR3(0,-80,0));
 player_m->setRot(D3DXVECTOR3(0,0,0));

 CAMERA_DATA* cameraData = &SystemManager::getCameraData();
 D3DXVECTOR3 pos = D3DXVECTOR3(400,0,100);
 D3DXVECTOR3 cameraR = pos;
 D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0);
 cameraData->rot.y = cameraData->rotDef.y = rot.y;
 cameraR.y = 0;
 D3DXVECTOR3 cameraP = D3DXVECTOR3(500,-100,1500);
 cameraData->cameraR = cameraData->cameraRDef = cameraR;
 cameraData->cameraP = cameraData->cameraPDef = cameraP;
 cameraData->cameraLookObject = false;
 cameraData->cameraUpdateMode = true;
 player_m->getXfileAnim()->AnimCtr.ChangeAnimation(3);
 player_m->getXfileAnim()->AnimCtr.AdvanceTime(1.0f);
 imp_m->treeMnager_m = new TreeManager(-3300);

	if(score_m > 0)
	{
		saveScore();
	}
	number_m = ObjectNumbers::Create(10,TEXTURE_NUMBERFONT,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2.0f,0),D3DXVECTOR3(40,40,0));
	number_m->setNumber(score_m);
	Object2D* obj2DPointer;
	Object2D::Create(TEXTURE_RESULTLOGO,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 4.0f,0),D3DXVECTOR3(800,400,0));
	obj2DPointer = Object2D::Create(TEXTURE_PRESSENETER,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2 + 200,0),D3DXVECTOR3(200,40,0));
	obj2DPointer->setColor(D3DXCOLOR(0.6f,0.6f,1.0f,1.0f));
	obj2DPointer->setColorEd(D3DXCOLOR(0.6f,0.6f,1.0f,0.2f));
	obj2DPointer->setColorSpeed(D3DXCOLOR(0,0,0,0.015f));
	SoundManager::playSound(SOUND_LAVEL_SCENEBGM);
 SystemManager::getObjectFeed()->feedStart(FEED_OUT_PROGRESS);
 return S_OK;
}
//==============================================================================
//解放
void ResultScene::Uninit(void)
{
 SAFE_DELETE(imp_m);
 SoundManager::stopSound();
	Object::releasePriority(PRIORITY_FEED);
}
//==============================================================================
//更新
void ResultScene::Update(void)
{
 imp_m->treeMnager_m->Update();
 imp_m->Enenmy_m[0]->getXfileAnim()->AnimCtr.AdvanceTime(0.01f);
 imp_m->Enenmy_m[0]->getXfileAnim()->AnimCtr.ChangeAnimation(0);
 for(int loop = 0;loop < Impl::TITLE_ENEMY_MAX;loop++)
 {
  float rot_by = imp_m->Enenmy_m[loop]->getRot().y - D3DX_PI / 2;
  rot_by += 0.003f;
  float rot_by_2 = rot_by * 2;
  if(loop % 2 == 0)
  {
   imp_m->Enenmy_m[loop]->setPos(D3DXVECTOR3(sinf(rot_by) * Impl::MOVESPEED,Impl::POS_Y + sinf(rot_by_2) * Impl::MOVESPEED_Y,cosf(rot_by) * Impl::MOVESPEED));
  }
  else{
   imp_m->Enenmy_m[loop]->setPos(D3DXVECTOR3(sinf(rot_by) * Impl::MOVESPEED,Impl::POS_Y + cosf(rot_by_2) * Impl::MOVESPEED_Y,cosf(rot_by) * Impl::MOVESPEED));
  }
  imp_m->Enenmy_m[loop]->setRot(D3DXVECTOR3(0,rot_by + D3DX_PI / 2,0));
 }

 player_m->getXfileAnim()->AnimCtr.ChangeAnimation(3);
 player_m->getXfileAnim()->AnimCtr.AdvanceTime(0.008f);
	if(InputKeyboard::getKeyboardTrigger(DIK_RETURN) && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
	{
		SoundManager::playSound(SOUND_LABEL_BUTTON000);
		SystemManager::getObjectFeed()->feedStart(FEED_IN_PROGRESS);

	}
	if(SystemManager::getObjectFeed()->getFeedMode() == FEED_IN_END)
	{
		SceneManager::SetScene(SCENE_RANKING);
	}
}
void ResultScene::saveScore(void)
{
	FILE *fp;
	fp = fopen("./data/SAVE/score.bin","ab");
	fwrite(&score_m,sizeof(int),1,fp);
	fclose(fp);
}

//End Of FIle

