/******************************************************************************
@file       ObjectPlayer.cpp
@brief      ObjectPlayerの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectPlayer.h"
#include "InputKeyboard.h"
#include "SystemManager.h"
#include "CameraManager.h"
#include "MessageProc.h"
#include "CollisionManager.h"
#include "ObjectBullet.h"
#include "EnemyManager.h"
#include "GameScene.h"
#include "ObjectBar.h"
#include "Object2D.h"
#include "ObjectNumbers.h"
#include "BulletManager.h"
#include "NetClient.h"
#include "data.h"
#include "ObjectMiniMap.h"
#include "ObjectSnowBall.h"
unsigned int ObjectPlayer::playerHundle_m;
ObjectNumbers* ObjectPlayer::score_m;
//================================================================================
//タイプ別作成
//================================================================================
ObjectPlayer* ObjectPlayer::Create(XFILEANIM_LIST xfileName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	ObjectPlayer *objectPointer;

	objectPointer = new ObjectPlayer(priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setSize(size);
		objectPointer->setXfile(xfileName);
		objectPointer->init();
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectPlayer::ObjectPlayer(PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3DXfileAnim(priority,objType)
{
	playerMode_m = PLAYER_MODE_WAIT;
}
//==============================================================================
//デストラクタ
ObjectPlayer::~ObjectPlayer(void)
{

}
//==============================================================================
//初期化
void ObjectPlayer::init(void)
{
 barHpBack_m = NULL; 
 barMpBack_m = NULL;
 jumpSpeed = 0;
	playerHundle_m = Object::getObjHandle();
	CameraManager::setLookObjHundle(playerHundle_m,OBJTYPE_3DXFILEANIM);
	SystemManager::getCameraData().cameraLookObject = true;
	posSpeed_m.x = posSpeed_m.z = 80;
	posDeffSpeed = 0.1f;
	rotSpeed_m.y = 0.015f;
	playerAnimTime_m[PLAYER_MODE_NONE] = 0.02f;
	playerAnimTime_m[PLAYER_MODE_WAIT] = 0.02f;
	playerAnimTime_m[PLAYER_MODE_MOVE] = 0.06f;
	playerAnimTime_m[PLAYER_MODE_ATTACK] = 0.05f;
	playerAnimTime_m[PLAYER_MODE_DOWN] = 0.05f;
	moveCnt = 0;
	height_m = 250;
	float posY = CollisionManager::HitchkField(pos_m,NULL);
	if(posY != -99999)
	{
		pos_m.y = posY + height_m;
  posDest_m.y = posY + height_m;
 }
	damegeCnt_m = 0;
	playerDamege_m = false;
	attackCnt_m = 0;
	collisionData_m.pos = pos_m;
	collisionData_m.radius = ( 800 / 2.0f );
	hpMax = 250;
	mpMax = 250;
 if(!score_m)
 {
  score_m = ObjectNumbers::Create(7,TEXTURE_NUMBERFONT,D3DXVECTOR3(SCREEN_WIDTH / 5,SCREEN_HEIGHT /
   9.0f,0),D3DXVECTOR3(40,40,0));
 }
 if(!barHpBack_m)
 {
  barHpBack_m = Object2D::Create(TEXTURE_LIFEBAR,D3DXVECTOR3(SCREEN_WIDTH - 200,SCREEN_HEIGHT - 80,0),D3DXVECTOR3(hpMax + 4,44,0));
 }
  barHpBack_m->setColor(D3DXCOLOR(0.2f,0.1f,0.1f,1.0f));
  if(!barMpBack_m)
  {
   barMpBack_m = Object2D::Create(TEXTURE_LIFEBAR,D3DXVECTOR3(200,SCREEN_HEIGHT - 80,0),D3DXVECTOR3(mpMax + 4,44,0));
  }
  barMpBack_m->setColor(D3DXCOLOR(0.2f,0.1f,0.1f,1.0f));

 barMp_m = ObjectBar::Create(mpMax,D3DXCOLOR(0.2f,0.2f,0.9f,0.95f),D3DXVECTOR3(SCREEN_WIDTH - 200,SCREEN_HEIGHT - 80,0),D3DXVECTOR3(mpMax,40,0),TEXTURE_LIFEBAR);
	barHp_m = ObjectBar::Create(hpMax,D3DXCOLOR(0.9f,0.2f,0.2f,0.95f),D3DXVECTOR3(200,SCREEN_HEIGHT - 80,0),D3DXVECTOR3(hpMax,40,0),TEXTURE_LIFEBAR);
	mpCnt_m = 0;
	rot_m.y = rotDest_m.y = 0.0001f;
	CAMERA_DATA* cameraData = &SystemManager::getCameraData();
	D3DXVECTOR3 cameraR = pos_m;
	cameraData->rot.y = cameraData->rotDef.y = rot_m.y;
	cameraR.x -= sinf(rot_m.y) * cameraData->lookObjDist;
	cameraR.y -= cameraData->cameraR.y;
	cameraR.z -= cosf(rot_m.y) * cameraData->lookObjDist;
	cameraData->cameraR = cameraData->cameraRDef = cameraR;
	D3DXVECTOR3 cameraP = D3DXVECTOR3(cameraData->cameraR.x + sinf(rot_m.y) * cameraData->distance,cameraR.y + 1100.0f,cameraData->cameraR.z + cosf(rot_m.y) * cameraData->distance);
	cameraData->cameraP = cameraData->cameraPDef = cameraP;
	damegeFeed_m = Object2D::Create(TEXTURE_NONE,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2,0),D3DXVECTOR3(SCREEN_WIDTH,SCREEN_HEIGHT,0));
	damegeFeed_m->setColor(D3DXCOLOR(0.9f,0.2f,0.2f,0.0f));
	damegeFeed_m->setColorEd(D3DXCOLOR(1.0f,0.2f,0.2f,1.0f));
	damegeFeed_m->setDrawFlag(false);
 snowBallModel_m = ObjectSnowBall::Create(pos_m,rot_m.y);
	playerDownTime = 0;
 aiMode_m = false;
}
//==============================================================================
//解放
void ObjectPlayer::uninit(void)
{

	score_m = nullptr;
	Object3DXfileAnim::uninit();
}
//==============================================================================
//更新
void ObjectPlayer::update(void)
{
 if(aiMode_m)
 {
  hpMax = 250;
  mpMax = 250;
  barHp_m->AddBarLife(hpMax);
  score_m->setDraw(false);
  barMp_m->setDrawFlag(false);
  barHp_m->setDrawFlag(false);
  damegeFeed_m->setDrawFlag(false);
  damegeFeed_m->setDrawFlag(false);
  barHpBack_m->setDrawFlag(false);
  barMpBack_m->setDrawFlag(false);
  updatePlayerAI();
  updatePlayerAIMove();
 }
 else{
  Object3DXfileAnim::update();
  collisionData_m.pos = pos_m;
  if(playerDownTime < 20)
  {
   getXfileAnim()->AnimCtr.ChangeAnimation(playerMode_m);
   if(posSpeed_m.y != 0)
   {
    getXfileAnim()->AnimCtr.AdvanceTime(playerAnimTime_m[PLAYER_MODE_NONE] *= -1);
   }
   else{
    getXfileAnim()->AnimCtr.AdvanceTime(playerAnimTime_m[playerMode_m]);
   }
  }
  if(playerMode_m == PLAYER_MODE_DOWN)
  {
   rotDest_m.x = D3DX_PI / 2;
   height_m = height_m / 2;
   SystemManager::getCameraData().cameraRotMode = true;
   setDrawFlag(true);
   if(playerDownTime < 80)
   {
    playerDownTime++;
   }
   if(playerDownTime % 80 == 0)
   {
    GameScene::setGameOverFlag(true);
   }
  }
  else{
   updatePlayerKey();
   hitChk();
   float cameraRot = SystemManager::getCameraData().rot.y;
   if(playerMode_m != PLAYER_MODE_MOVE && cameraRot != rot_m.y)
   {
    moveCnt++;
    if(moveCnt > 30)
    {
     SystemManager::getCameraData().cameraRotMode = true;
    }
   }
   else{
    SystemManager::getCameraData().cameraRotMode = false;
    moveCnt = 0;
   }

   if(playerDir_m[0] == PLAYER_DIRECTION_DOWN)
   {
    if(playerDir_m[1] == PLAYER_DIRECTION_RIGHT)
    {
     rotDest_m.y = cameraRot + ( D3DX_PI / 2 + ( D3DX_PI / 2 ) / 2 );
     posDest_m.x -= sinf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z -= cosf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z -= sinf(cameraRot) * ( posSpeed_m.z / 2 );
     posDest_m.x += cosf(cameraRot) * ( posSpeed_m.z / 2 );
    }
    else
     if(playerDir_m[1] == PLAYER_DIRECTION_LEFT)
     {
      rotDest_m.y = cameraRot + ( -D3DX_PI / 2 - ( D3DX_PI / 2 ) / 2 );
      posDest_m.x -= sinf(cameraRot) * ( posSpeed_m.x / 2 );
      posDest_m.z -= cosf(cameraRot) * ( posSpeed_m.x / 2 );
      posDest_m.z += sinf(cameraRot) * ( posSpeed_m.z / 2 );
      posDest_m.x -= cosf(cameraRot) * ( posSpeed_m.z / 2 );
     }
     else{
      rotDest_m.y = cameraRot + D3DX_PI;

      posDest_m.x -= sinf(cameraRot)*posSpeed_m.x;
      posDest_m.z -= cosf(cameraRot)*posSpeed_m.z;
     }
   }
   if(playerDir_m[0] == PLAYER_DIRECTION_UP)
   {
    if(playerDir_m[1] == PLAYER_DIRECTION_RIGHT)
    {
     rotDest_m.y = cameraRot + D3DX_PI / 4;
     posDest_m.x += sinf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z += cosf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z -= sinf(cameraRot) * ( posSpeed_m.z / 2 );
     posDest_m.x += cosf(cameraRot) * ( posSpeed_m.z / 2 );
    }
    else
     if(playerDir_m[1] == PLAYER_DIRECTION_LEFT)
     {
      rotDest_m.y = cameraRot - D3DX_PI / 4;
      posDest_m.x += sinf(cameraRot) * ( posSpeed_m.x / 2 );
      posDest_m.z += cosf(cameraRot) * ( posSpeed_m.x / 2 );
      posDest_m.z += sinf(cameraRot) * ( posSpeed_m.z / 2 );
      posDest_m.x -= cosf(cameraRot) * ( posSpeed_m.z / 2 );
     }
     else{
      rotDest_m.y = cameraRot;

      posDest_m.x += sinf(cameraRot)*posSpeed_m.x;
      posDest_m.z += cosf(cameraRot)*posSpeed_m.z;
     }
   }
   if(playerDir_m[0] == PLAYER_DIRECTION_NONE)
   {
    if(playerDir_m[1] == PLAYER_DIRECTION_RIGHT)
    {
     rotDest_m.y = cameraRot + D3DX_PI / 2;

     posDest_m.x += cosf(cameraRot)*posSpeed_m.x;
     posDest_m.z -= sinf(cameraRot)*posSpeed_m.z;
    }
    else
     if(playerDir_m[1] == PLAYER_DIRECTION_LEFT)
     {
      rotDest_m.y = cameraRot - D3DX_PI / 2;

      posDest_m.x -= cosf(cameraRot)*posSpeed_m.x;
      posDest_m.z += sinf(cameraRot)*posSpeed_m.z;
     }
   }
   if(snowBallModel_m->getSnowMode() != SNOW_DISABLE)
   {
    if(snowBallModel_m->getSnowMode() != SNOW_SHOT)
    {
     snowBallModel_m->setSnowMode(SNOW_NONE);
    }
   }

   if(playerDir_m[0] == PLAYER_DIRECTION_NONE && playerDir_m[1] == PLAYER_DIRECTION_NONE)
   {
   }
    else{
     if(snowBallModel_m->getSnowMode() == SNOW_CLLECT || snowBallModel_m->getSnowMode() == SNOW_NONE)
     {
      snowBallModel_m->SetRotY(rot_m.y);
      snowBallModel_m->AddRotX(0.08f);
      snowBallModel_m->setSnowMode(SNOW_CLLECT);

     }
    }
   
  

   if(pos_m.x <= -MOVE_MAX)
   {
    posDest_m.x = -MOVE_MAX + 1;
    pos_m.x = -MOVE_MAX + 1;

   }
   else
    if(pos_m.x >= MOVE_MAX)
    {
     posDest_m.x = MOVE_MAX - 1;
     pos_m.x = MOVE_MAX - 1;
    }
   if(pos_m.z <= -MOVE_MAX)
   {
    posDest_m.z = -MOVE_MAX + 1;
    pos_m.z = -MOVE_MAX + 1;
   }
   else
    if(pos_m.z >= MOVE_MAX)
    {
     posDest_m.z = MOVE_MAX - 1;
     pos_m.z = MOVE_MAX - 1;
    }
   playerDir_m[0] = PLAYER_DIRECTION_NONE;
   playerDir_m[1] = PLAYER_DIRECTION_NONE;
   ROTS_CHK(rotDest_m);
   if(InputKeyboard::getKeyboardPress(DIK_E))
   {
    rotDest_m.y += rotSpeed_m.y;
    rot_m.y = rotDest_m.y;
    ROTS_CHK(rotDest_m);
    D3DXVECTOR3 camRot;
    camRot = SystemManager::getCameraData().rot;
    camRot.y += rotSpeed_m.y;
    ROTS_CHK(camRot);
    SystemManager::getCameraData().rot = camRot;
    SystemManager::getCameraData().cameraRDef = camRot;
   }

   if(InputKeyboard::getKeyboardPress(DIK_Q))
   {
    rotDest_m.y -= rotSpeed_m.y;
    rot_m.y = rotDest_m.y;
    ROTS_CHK(rotDest_m);
    D3DXVECTOR3 camRot;
    camRot = SystemManager::getCameraData().rot;
    camRot.y -= rotSpeed_m.y;
    ROTS_CHK(camRot);
    SystemManager::getCameraData().rot = camRot;
    SystemManager::getCameraData().rotDef = camRot;
   }

  }
  //ゆっくり回転
  float diffRotY;
  diffRotY = rotDest_m.y - rot_m.y;
  ROT_CHK(diffRotY);
  rot_m.y += ( diffRotY * rotDeffSpeed );
  //ゆっくり回転
  diffRotY = rotDest_m.x - rot_m.x;
  ROT_CHK(diffRotY);
  rot_m.x += ( diffRotY * rotDeffSpeed );		//移動量セット
  D3DXVECTOR3 posDiff;
  posDiff.x = ( posDest_m.x - pos_m.x ) * posDeffSpeed;
  posDiff.z = ( posDest_m.z - pos_m.z ) * posDeffSpeed;

  posDest_m.y += posSpeed_m.y;
  posDiff.y = ( posDest_m.y - pos_m.y ) * 0.3f;

  posSpeed_m.y -= PLAYER_GRAVITY;

  pos_m.x += posDiff.x;
  pos_m.y += posDiff.y;
  pos_m.z += posDiff.z;

  float posY = CollisionManager::HitchkField(pos_m,NULL);
  if(pos_m.y <= posY + height_m)
  {
   posSpeed_m.y = 0;
  }

  //前の高さと現在の高さが違いすぎていたら修正
  if(( posY >= pos_m.y + 400 || posY <= pos_m.y - 400 ))
  {
   posY = pos_m.y;
  }
  else
   if(posSpeed_m.y == 0)
   {
    pos_m.y = posY + height_m;
    posDest_m.y = posY + height_m;
   }

   float cameraRotMap = SystemManager::getCameraData().rot.y - D3DX_PI;
   GameScene::getObjectMiniMap()->setMapPos(pos_m,D3DXCOLOR(0,0,1.0f,1.0f),rot_m.y,true);
   if(snowBallModel_m->getSnowMode() != SNOW_SHOT)
   {
   float snowRadian = snowBallModel_m->getCollisionData()->radius * 2.2f;
   D3DXVECTOR3 posVec = D3DXVECTOR3(pos_m.x + sinf(rot_m.y) * snowRadian,
                                    pos_m.y,
                                    pos_m.z + cosf(rot_m.y) * snowRadian);

   snowBallModel_m->setPos(posVec);
  }
 }

	MessageProc::SetDebugMessage("(%f,%f,%f)\n",pos_m.x,pos_m.y,pos_m.z);
}
//==============================================================================
//入力更新
void ObjectPlayer::updatePlayerKey(void)
{
 playerMode_m = PLAYER_MODE_WAIT;
 if(InputKeyboard::getKeyboardPress(DIK_W))
 {
  playerDir_m[0] = PLAYER_DIRECTION_UP;
 }
 if(InputKeyboard::getKeyboardPress(DIK_S))
 {
  playerDir_m[0] = PLAYER_DIRECTION_DOWN;
 }
 if(InputKeyboard::getKeyboardPress(DIK_W) && InputKeyboard::getKeyboardTrigger(DIK_S))
 {
  playerDir_m[0] = PLAYER_DIRECTION_NONE;
 }
 if(InputKeyboard::getKeyboardPress(DIK_A))
 {
  playerDir_m[1] = PLAYER_DIRECTION_LEFT;
 }
 if(InputKeyboard::getKeyboardPress(DIK_D))
 {
  playerDir_m[1] = PLAYER_DIRECTION_RIGHT;
 }
 if(InputKeyboard::getKeyboardPress(DIK_D) && InputKeyboard::getKeyboardTrigger(DIK_A))
 {
  playerDir_m[1] = PLAYER_DIRECTION_NONE;
 }
 if(InputKeyboard::getKeyboardRelease(DIK_SPACE))
 {
  SoundManager::stopSound(SOUND_LAVEL_STEAMSE);
 }
 if(posSpeed_m.y == 0)
 {
  if(InputKeyboard::getKeyboardPress(DIK_LSHIFT))
  {
   if(InputKeyboard::getKeyboardTrigger(DIK_LSHIFT))
   {
    jumpSpeed += 50;
   }
   if(jumpSpeed <= PLAYER_JUMP_MAX)
   {
    jumpSpeed += 10;
    scl_m.y -= 0.015f;
   }
  }
 }
 if(InputKeyboard::getKeyboardRelease(DIK_LSHIFT))
 {
  if(posSpeed_m.y == 0)
  {
   posSpeed_m.y = jumpSpeed;

  }
  jumpSpeed = 0;
  scl_m.y = 1.0f;
 }
 if(InputKeyboard::getKeyboardTrigger(DIK_F))
 {

  if(snowBallModel_m->getSnowMode() != SNOW_DISABLE && snowBallModel_m->getSnowMode() != SNOW_SHOT)
  {
   snowBallModel_m->SetRotY(rot_m.y);
   snowBallModel_m->setSnowMode(SNOW_SHOT);
  }
  if(snowBallModel_m->getSnowMode() == SNOW_DISABLE)
  {
   snowBallModel_m->setScl(D3DXVECTOR3(0.2f,0.2f,0.2f));
   snowBallModel_m->setSnowMode(SNOW_CLLECT);
  }
 }
 if(InputKeyboard::getKeyboardPress(DIK_SPACE))
 {
  if(InputKeyboard::getKeyboardTrigger(DIK_SPACE))
  {
   SoundManager::playSound(SOUND_LAVEL_STEAMSE);
  }
  if(attackCnt_m == 0 && barMp_m->getBarLife() != 0)
  {
   ObjectBullet::Create(BULLET_PLAYER,pos_m,rot_m.y);
   ObjectBullet::Create(BULLET_PLAYER,pos_m,rot_m.y);
   barMp_m->AddBarLife(-2);
  }
  mpCnt_m = 1;
  attackCnt_m++;
  attackCnt_m %= 2;
  playerMode_m = PLAYER_MODE_ATTACK;
 }
 else{
  attackCnt_m = 0;
  mpCnt_m++;
  mpCnt_m %= 5;
  if(mpCnt_m == 0)
  {
   barMp_m->AddBarLife(4);
  }
 }
 if(playerDir_m[0] || playerDir_m[1])
 {
  playerMode_m = PLAYER_MODE_MOVE;
 }
 if(barHp_m->getBarLife() <= 0)
 {
  playerMode_m = PLAYER_MODE_DOWN;
  SoundManager::stopSound();
  SoundManager::playSound(SOUND_LAVEL_GAMEOVERBGM);
 }
 if(InputKeyboard::getKeyboardPress(DIK_F3))
 {
  ObjectEnemy* enemy;
  enemy = EnemyManager::getEnemyObject();
  if(enemy)
  {
   posDest_m = pos_m = enemy->getPos();
  }
 }

 // データ送信
 DATA data;

 data.Type = DATA_TYPE_POSITION;
 data.Position.x = Object3DXfileAnim::getPos().x;
 data.Position.y = Object3DXfileAnim::getPos().y;
 data.Position.z = Object3DXfileAnim::getPos().z;

 NetClient::SendData(data);


 data.Type = DATA_TYPE_ROTATION;
 data.Rotation.x = Object3DXfileAnim::getRot().x;
 data.Rotation.y = Object3DXfileAnim::getRot().y;
 data.Rotation.z = Object3DXfileAnim::getRot().z;

 NetClient::SendData(data);

}
//==============================================================================
//AI更新
void ObjectPlayer::updatePlayerAI(void)
{
 playerMode_m = PLAYER_MODE_WAIT;
 playerDir_m[0] = PLAYER_DIRECTION_UP;
 playerDir_m[1] = PLAYER_DIRECTION_NONE;
 ObjectEnemy* enemy;
 enemy = EnemyManager::getEnemyObject();
 if(enemy)
 {
  COLLISION_DATA col = collisionData_m;
  col.radius *= 8;
  if(CollisionManager::HitchkCircle(enemy->getCollisionData(),&col))
  {
   if(attackCnt_m == 0)
   {
    ObjectBullet::Create(BULLET_PLAYER,pos_m,rot_m.y);
    ObjectBullet::Create(BULLET_PLAYER,pos_m,rot_m.y);
   }
   mpCnt_m = 1;
   attackCnt_m++;
   attackCnt_m %= 2;
   playerDir_m[0] = PLAYER_DIRECTION_NONE;
   playerDir_m[1] = PLAYER_DIRECTION_NONE;
   playerMode_m = PLAYER_MODE_ATTACK;
   D3DXVECTOR3 target = enemy->getPos();
   float fRot = 0;
   fRot = atan2f(( pos_m.z - target.z ),( pos_m.x - target.x ));

   if(( pos_m.z - target.z ) == 0 && ( pos_m.x - target.x ) == 0)
   {
    fRot = 0;;
   }
   else{
    fRot = -D3DX_PI / 2 + atan2f(-( pos_m.z - target.z ),( pos_m.x - target.x ));
   }
   ROT_CHK(fRot);
   D3DXVECTOR3 camRot;
   camRot = SystemManager::getCameraData().rot;
   float deffrot = fRot - camRot.y ;
   ROT_CHK(deffrot);
   SystemManager::getCameraData().rot.y += deffrot * 0.1f;
   SystemManager::getCameraData().cameraRDef.y = SystemManager::getCameraData().rot.y;
   rotDest_m.y = camRot.y;
   

  }
  else{
   if(posSpeed_m.y == 0 && rand() % 500 == 0)
   {
    posSpeed_m.y = PLAYER_JUMP_MAX;
   }
   D3DXVECTOR3 targetPos_m = enemy->getPos();
   float fRot = 0;
   fRot = atan2f(( targetPos_m.z - pos_m.z ),( targetPos_m.x - pos_m.x ));

   if(( targetPos_m.z - pos_m.z ) == 0 && ( targetPos_m.x - pos_m.x ) == 0)
   {
    fRot = 0;;
   }
   else{
    fRot = -D3DX_PI / 2 + atan2f(-( targetPos_m.z - pos_m.z ),( targetPos_m.x - pos_m.x ));
   }
   ROT_CHK(fRot);
   D3DXVECTOR3 camRot;
   camRot = SystemManager::getCameraData().rot;
   float deffrot = fRot - camRot.y;
   ROT_CHK(deffrot);
   SystemManager::getCameraData().rot.y += deffrot * 0.1f;
   SystemManager::getCameraData().cameraRDef.y = SystemManager::getCameraData().rot.y;
   rotDest_m.y = camRot.y;
  }
 }
	else{
		attackCnt_m = 0;
		mpCnt_m++;
		mpCnt_m %= 5;
		if(mpCnt_m == 0)
		{
			barMp_m->AddBarLife(4);
		}
	}
	if(playerDir_m[0] || playerDir_m[1])

	{
		playerMode_m = PLAYER_MODE_MOVE;
	}
	if(barHp_m->getBarLife() <= 0)
	{
		playerMode_m = PLAYER_MODE_DOWN;
		SoundManager::stopSound();
		SoundManager::playSound(SOUND_LAVEL_GAMEOVERBGM);
	}

}
//==============================================================================
//当たり判定更新
void ObjectPlayer::hitChk(void)
{
	if(!playerDamege_m)
	{

		if(EnemyManager::hitchk(&collisionData_m,ENEMY_MOVER) || BulletManager::hitchk(&collisionData_m,BULLET_ENEMY))
		{
			playerDamege_m = true;
			damegeCnt_m = 0;
			barHp_m->AddBarLife(-25);
			damegeFeed_m->setColorSpeed(D3DXCOLOR(0,0,0,0.04f));
			damegeFeed_m->setColor(D3DXCOLOR(0.9f,0.2f,0.2f,0.2f));
			damegeFeed_m->setDrawFlag(true);
		}
	}
	else{
		damegeCnt_m++;
		if(damegeCnt_m % 2 == 0)
		{
			setDrawFlag(false);
		}
		else{
			setDrawFlag(true);
		}
		if(damegeCnt_m > 40)
		{
			setDrawFlag(true);
			damegeCnt_m = 0;
			playerDamege_m = false;
			damegeFeed_m->setColorSpeed(D3DXCOLOR(0,0,0,0.0f));
			damegeFeed_m->setColor(D3DXCOLOR(0.9f,0.2f,0.2f,0.0f));
			damegeFeed_m->setDrawFlag(false);
		}

	}
}

void ObjectPlayer::updatePlayerAIMove()
{
 Object3DXfileAnim::update();
 collisionData_m.pos = pos_m;
 getXfileAnim()->AnimCtr.ChangeAnimation(playerMode_m);
 if(posSpeed_m.y != 0)
 {
  getXfileAnim()->AnimCtr.AdvanceTime(playerAnimTime_m[PLAYER_MODE_WAIT]);
 }
 else{
  getXfileAnim()->AnimCtr.AdvanceTime(playerAnimTime_m[playerMode_m]);
 }
 hitChk();
  float cameraRot = SystemManager::getCameraData().rot.y;
  if(playerMode_m != PLAYER_MODE_MOVE && cameraRot != rot_m.y)
  {
   moveCnt++;
   if(moveCnt > 30)
   {
    SystemManager::getCameraData().cameraRotMode = true;
   }
  }
  else{
   SystemManager::getCameraData().cameraRotMode = false;
   moveCnt = 0;
  }

  if(playerDir_m[0] == PLAYER_DIRECTION_UP)
  {
   if(playerDir_m[1] == PLAYER_DIRECTION_RIGHT)
   {
    rotDest_m.y = cameraRot + ( D3DX_PI / 2 + ( D3DX_PI / 2 ) / 2 );
    posDest_m.x -= sinf(cameraRot) * ( posSpeed_m.x / 2 );
    posDest_m.z -= cosf(cameraRot) * ( posSpeed_m.x / 2 );
    posDest_m.z -= sinf(cameraRot) * ( posSpeed_m.z / 2 );
    posDest_m.x += cosf(cameraRot) * ( posSpeed_m.z / 2 );
   }
   else
    if(playerDir_m[1] == PLAYER_DIRECTION_LEFT)
    {
     rotDest_m.y = cameraRot + ( -D3DX_PI / 2 - ( D3DX_PI / 2 ) / 2 );
     posDest_m.x -= sinf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z -= cosf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z += sinf(cameraRot) * ( posSpeed_m.z / 2 );
     posDest_m.x -= cosf(cameraRot) * ( posSpeed_m.z / 2 );
    }
    else{
     rotDest_m.y = cameraRot + D3DX_PI;

     posDest_m.x -= sinf(cameraRot)*posSpeed_m.x;
     posDest_m.z -= cosf(cameraRot)*posSpeed_m.z;
    }
  }
  if(playerDir_m[0] == PLAYER_DIRECTION_DOWN)
  {
   if(playerDir_m[1] == PLAYER_DIRECTION_RIGHT)
   {
    rotDest_m.y = cameraRot + D3DX_PI / 4;
    posDest_m.x += sinf(cameraRot) * ( posSpeed_m.x / 2 );
    posDest_m.z += cosf(cameraRot) * ( posSpeed_m.x / 2 );
    posDest_m.z -= sinf(cameraRot) * ( posSpeed_m.z / 2 );
    posDest_m.x += cosf(cameraRot) * ( posSpeed_m.z / 2 );
   }
   else
    if(playerDir_m[1] == PLAYER_DIRECTION_LEFT)
    {
     rotDest_m.y = cameraRot - D3DX_PI / 4;
     posDest_m.x += sinf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z += cosf(cameraRot) * ( posSpeed_m.x / 2 );
     posDest_m.z += sinf(cameraRot) * ( posSpeed_m.z / 2 );
     posDest_m.x -= cosf(cameraRot) * ( posSpeed_m.z / 2 );
    }
    else{
     rotDest_m.y = cameraRot;

     posDest_m.x += sinf(cameraRot)*posSpeed_m.x;
     posDest_m.z += cosf(cameraRot)*posSpeed_m.z;
    }
  }

  if(pos_m.x <= -MOVE_MAX)
  {
   posDest_m.x = -MOVE_MAX + 1;
   pos_m.x = -MOVE_MAX + 1;

  }
  else
   if(pos_m.x >= MOVE_MAX)
   {
    posDest_m.x = MOVE_MAX - 1;
    pos_m.x = MOVE_MAX - 1;
   }
  if(pos_m.z <= -MOVE_MAX)
  {
   posDest_m.z = -MOVE_MAX + 1;
   pos_m.z = -MOVE_MAX + 1;
  }
  else
   if(pos_m.z >= MOVE_MAX)
   {
    posDest_m.z = MOVE_MAX - 1;
    pos_m.z = MOVE_MAX - 1;
   }
  playerDir_m[0] = PLAYER_DIRECTION_NONE;
  playerDir_m[1] = PLAYER_DIRECTION_NONE;


  ROTS_CHK(rotDest_m);
  //ゆっくり回転
 float diffRotY;
 diffRotY = rotDest_m.y - rot_m.y;
 ROT_CHK(diffRotY);
 rot_m.y += ( diffRotY * rotDeffSpeed );
 //ゆっくり回転
 diffRotY = rotDest_m.x - rot_m.x;
 ROT_CHK(diffRotY);
 rot_m.x += ( diffRotY * rotDeffSpeed );		//移動量セット
 D3DXVECTOR3 posDiff;
 posDiff.x = ( posDest_m.x - pos_m.x ) * posDeffSpeed;
 posDiff.z = ( posDest_m.z - pos_m.z ) * posDeffSpeed;

 posDest_m.y += posSpeed_m.y;
 posDiff.y = ( posDest_m.y - pos_m.y ) * 0.3f;

 posSpeed_m.y -= PLAYER_GRAVITY;

 pos_m.x += posDiff.x;
 pos_m.y += posDiff.y;
 pos_m.z += posDiff.z;

 float posY = CollisionManager::HitchkField(pos_m,NULL);
 if(pos_m.y <= posY + height_m)
 {
  posSpeed_m.y = 0;
 }

 //前の高さと現在の高さが違いすぎていたら修正
 if(( posY >= pos_m.y + 400 || posY <= pos_m.y - 400 ))
 {
  posY = pos_m.y;
 }
 else
  if(posSpeed_m.y == 0)
  {
   pos_m.y = posY + height_m;
   posDest_m.y = posY + height_m;
  }
}
//End Of FIle