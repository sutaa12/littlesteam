/******************************************************************************/
/*! @addtogroup Object_BulletManager
@file       BulletManager.h
@brief      ゲームの弾を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef BULLETMANAGER_FILE
#define BULLETMANAGER_FILE
#include "Common.h"
#include "main.h"
#include "XFileManager.h"
#include "ObjectBullet.h"
class ObjectBullet;
//敵管理クラス
class BulletManager
{
 public:
 BulletManager(void);//コンストラクタ
 ~BulletManager(void);//デストラクタ
 HRESULT init(void);//初期化
 static void update(void);//ライト更新
 static int addBulletList(BULLET_TYPE type,ObjectBullet* bulletPointer);
 static void removeBulletList(BULLET_TYPE type,int num);
 static bool hitchk(COLLISION_DATA* obj1,BULLET_TYPE type);

 private:
 //メンバ変数
 static const unsigned int MAX_BULLET = 500;///弾の最大数
 static ObjectBullet* bulletList_m[BULLET_TYPE_MAX][MAX_BULLET];//弾のリスト

};

#endif

//End Of FIle