/******************************************************************************/
/*! @addtogroup Object_ObjectMiniMap
@file       Object.h
@brief      ObjectMiniMapクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECTMINIMAP_FILE
#define COBJECTMINIMAP_FILE
#include <vector>
#include "main.h"
#include "Common.h"
#include "Object2D.h"
#include "TextureManager.h"
using namespace std;
class Object2D;
class ObjectMiniMap: public Object2D
{
	//コンストラクタ
	public:
	/*! ObjectMiniMap ObjectMiniMapのコンストラクタ
	@param[in]     type    2Dオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectMiniMap(D3DXCOLOR col,PRIORITY_MODE priority,OBJECT2D_TYPE type,OBJTYPE objType = OBJTYPE_2D);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectMiniMap ObjectMiniMapのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectMiniMap(void);
	//操作
	public:
	/*! Create ObjectMiniMapの作成
	@param[in]     texName    テクスチャの名前定義から設定(TEXTURE_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot    角度の設定
	@param[in]     col    色設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    2Dのタイプの設定の設定
	*/
	static ObjectMiniMap* Create(D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),TEXTURE_LIST texName = TEXTURE_NONE,D3DXCOLOR col = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),D3DXVECTOR3 size = D3DXVECTOR3(MINI_MAP_SIZE,MINI_MAP_SIZE,0),OBJECT2D_TYPE type = OBJCT2D_NORMAL,PRIORITY_MODE priority = PRIORITY_7);/*!< 作成時設定*/
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
 void setMapRot(float rot){ rot_m = D3DXVECTOR3(rot,rot,rot); }
	//属性
	public:
 void setMapPos(D3DXVECTOR3 pos,D3DXCOLOR col,float rot = 0,bool size_large = false);
	static const int MINI_MAP_SIZE = 150;
	private:
	static const int MAP_CHARACTOR_SIZE = 8;
 Object2D* mapMax_m;
	vector<Object2D*> mapPos_m;
	vector<Object2D*> mapOldPos_m;
};
#endif

//End Of FIle