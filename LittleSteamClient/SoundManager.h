/******************************************************************************/
/*! @addtogroup Sound_SoundManager
@file       SoundManager.h
@brief      ゲームの音を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef SOUND_MANAGE_FILE
#define SOUND_MANAGE_FILE
#include "Common.h"

/*! @class SoundManager
@brief  各管理クラスを更新する
*/
// サウンドファイル
typedef enum
{
	SOUND_LABEL_BUTTON000 = 0,			// ボタン
	SOUND_LAVEL_STEAMSE,
	SOUND_LAVEL_EXPLOSE,
	SOUND_LAVEL_GAMEBGM,
	SOUND_LAVEL_GAMEOVERBGM,
	SOUND_LAVEL_SCENEBGM,
	SOUND_LABEL_MAX,
} SOUND_LABEL;

class SoundManager {
	//コンストラクタ
	/*! SoundManager　コンストラクタ*/
	public:
	SoundManager(void);
	//デストラクタ
	/*! ~SoundManager　デストラクタ*/
	public:
	~SoundManager(void);
	//操作
	public:
	/*! init　初期化
	@param[in] hWnd  windowハンドル
	@return  成功かどうか返す
	*/
	HRESULT init(HWND hWnd);
	/*! uninit 音データの解放*/
	void uninit(void);
	/*! playSound 音の再生
	@param[in]  label  再生する音をSOUND_LABELから選ぶ
	*/
	static HRESULT playSound(SOUND_LABEL label);
	/*! stopSound 音の停止
	@param[in]  label  停止する音をSOUND_LABELから選ぶ
	*/
	static void stopSound(SOUND_LABEL label);
	/*! stopSound 全ての音データの停止
	*/
	static void stopSound(void);
	private:
	/*! checkChunk チャンクチェック
	@param[in]  fileHandle  音のファイルのハンドル
	@param[in]  format  指定するフォーマット
	@param[in]  chunkSize  チャンクサイズ
	@param[in]  chunkDataPosition  チャンクの位置
	*/
	HRESULT checkChunk(HANDLE fileHandle,DWORD format,DWORD *chunkSize,DWORD *chunkDataPosition);
	/*! readChunkData チャンク読み込み
	@param[in]  fileHandle  音のファイルのハンドル
	@param[in]  bufferPointer  バッファーのポインタ
	@param[in]  bufferSize  バッファーサイズ
	@param[in]  bufferOffset  バッファーの開始位置
	*/
	HRESULT readChunkData(HANDLE fileHandle,void *bufferPointer,DWORD bufferSize,DWORD bufferOffset);
	private:
	typedef struct
	{
		char *fileName;	/*!< ファイル名*/
		bool loop;			/*!< ループするかどうか bool型 0でループしない1でループ*/
	} PARAM;

	IXAudio2 *xAudio2_m;								/*!< XAudio2オブジェクトへのインターフェイス*/
	IXAudio2MasteringVoice *masteringVoice_m;			/*!< マスターボイス*/
	static IXAudio2SourceVoice *sourceVoice_m[SOUND_LABEL_MAX];	/*!< ソースボイス*/
	static BYTE *dataAudio_m[SOUND_LABEL_MAX];					/*!< オーディオデータ*/
	static DWORD sizeAudio_m[SOUND_LABEL_MAX];					/*!< オーディオデータサイズ*/
 static PARAM SoundManager::fileName[SOUND_LABEL_MAX];
};

#endif
//End Of File