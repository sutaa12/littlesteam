/******************************************************************************/
/*! @addtogroup Object_TreeManager
@file       TreeManager.h
@brief      ゲームの弾を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef EFFECTMANAGER_FILE
#define EFFECTMANAGER_FILE
#include "Common.h"
#include "main.h"
typedef enum
{
	TREE_ONE,
	TREE_TWO,
	TREE_TREE,
	TREE_MAX
}TREE_TYPE;
class Object3DBillboard;
//敵管理クラス
class TreeManager
{
	public:
	TreeManager(int pos = 0);//コンストラクタ
	~TreeManager(void);//デストラクタ
	HRESULT init(void);//初期化
	void Update(void);
	private:
	//メンバ変数
	static const unsigned int MAX_TREE = 2500;
	Object3DBillboard* treeList_m[TREE_MAX][MAX_TREE];//弾のリスト

};

#endif

//End Of FIle