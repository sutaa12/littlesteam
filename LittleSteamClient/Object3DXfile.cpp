/******************************************************************************
@file       Object3DXfile.cpp
@brief      Object3DXfileの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "Object3DXfile.h"
//================================================================================
//タイプ別作成
//================================================================================
Object3DXfile* Object3DXfile::Create(XFILE_LIST xfileName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	Object3DXfile *objectPointer;

	objectPointer = new Object3DXfile(priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setSize(size);
		objectPointer->setXfile(xfileName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
Object3DXfile::Object3DXfile(PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3D(priority,objType)
{
	init();
}
//==============================================================================
//デストラクタ
Object3DXfile::~Object3DXfile(void)
{

}
//==============================================================================
//初期化
void Object3DXfile::init(void)
{

}
//==============================================================================
//解放
void Object3DXfile::uninit(void)
{

	Object3D::uninit();
}
//==============================================================================
//更新
void Object3DXfile::update(void)
{
	Object3D::update();
}

//End Of FIle