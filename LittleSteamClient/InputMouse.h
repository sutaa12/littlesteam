/******************************************************************************/
/*! @addtogroup Input_InputMouse
@file       InputMouse.h
@brief      マウス制御クラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef MOUSEINPUT_FILE
#define MOUSEINPUT_FILE
#include "Common.h"
#include "InputManager.h"
class InputMouse: public Input
{
	public:
	InputMouse(void);//コンストラクタ
	~InputMouse(void);//デストラクタ
	HRESULT init(HINSTANCE hInstance,HWND hWnd);//初期化
	void uninit(void);//解放
	void update(void);//更新
	static LONG getMouseSpeedX(void){ return moveMouse_m.x; }
	static LONG getMouseSpeedY(void){ return moveMouse_m.y; }
	static POINT getMouse(void){ return posMouse_m; }
	static POINT getMouseSpeed(void){ return moveMouse_m; }
	static bool getMouseRightPress(void);
	static bool getMouseLeftPress(void);

	private:
	HWND m_hwnd;
	static POINT posMouse_m;
	static POINT moveMouse_m;
	static DIMOUSESTATE2 dimouseState_m;
};
#endif