/******************************************************************************/
/*! @addtogroup Object_ObjectFeed
@file       Object.h
@brief      ObjectFeedクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECTFEED_FILE
#define COBJECTFEED_FILE
#include "main.h"
#include "Common.h"
#include "Object2D.h"
#include "TextureManager.h"
typedef enum
{
	FEED_NONE,
	FEED_IN_PROGRESS,
	FEED_IN_END,
	FEED_OUT_PROGRESS,
	FEED_OUT_END
}FEED_MODE;

class ObjectFeed: public Object2D
{
	public:
	ObjectFeed(PRIORITY_MODE nPriority = PRIORITY_FEED,OBJECT2D_TYPE objType = OBJCT2D_NORMAL);//コンストラクタ
	~ObjectFeed(void);//デストラクタ

	static ObjectFeed* Create();//作成時設定


	void init(void);//初期化
	void uninit(void);//解放
	void update(void);//更新

	void feedStart(FEED_MODE FeedMode,int Time = 20,D3DXCOLOR Col = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
	FEED_MODE getFeedMode(void){ return m_FeedMode; }//フェード情報取得
	void setFeed(FEED_MODE FeedMode){ m_FeedMode = FeedMode; }//フェード情報設定

	private:
	//メンバ変数

	int m_nTime;//時間
	FEED_MODE m_FeedMode;//状態
	D3DXCOLOR m_Color;//色
	float m_fAlfa;//アルファ値
	bool m_bFeedFlag;//描画時に切る
};

#endif

//End Of FIle