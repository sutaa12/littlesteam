/******************************************************************************/
/*! @addtogroup Object_ObjectEnemy
@file       Object.h
@brief      ObjectEnemyクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECTENEMY_FILE
#define COBJECTENEMY_FILE
#include "Object3DXfileAnim.h"
typedef enum{
	ENEMY_MOVER = 0,
	ENEMY_DRAGON_MOVER,
	ENEMY_SHOOTER,
	ENEMY_TYPE_MAX
}ENEMY_TYPE;
typedef enum
{
	ENEMY_MODE_MOVE,
	ENEMY_MODE_FIRE,
	ENEMY_MODE_ATTACK,
	ENEMY_MODE_DEAD,
	ENEMY_MODE_MAX
}ENEMY_MODE;
class ObjectNumbers;
class ObjectEnemy: public Object3DXfileAnim
{
	//コンストラクタ
	public:
	/*! ObjectEnemy ObjectEnemyのコンストラクタ
	@param[in]     type    3DXfileオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectEnemy(ENEMY_TYPE enemytype,D3DXVECTOR3 position,D3DXVECTOR3 rot,PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_3DXFILEANIM,OBJECT3D_TYPE type = OBJCT3D_ENEMY);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectEnemy ObjectEnemyのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectEnemy(void);
	//操作
	public:
	/*! Create Object3Dの作成
	@param[in]     xfileName    Xfileの名前定義から設定(XFILEANIM_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static ObjectEnemy* Create(ENEMY_TYPE enemytype,XFILEANIM_LIST xfileName = XFILEANIM_ENEMY,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT3D_TYPE type = OBJCT3D_ENEMY,PRIORITY_MODE priority = PRIORITY_7);
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	void Hitchk(void);
	//属性
	public:
	unsigned int getEnemy(void){ return enemyHundle_m; }
	private:
	ENEMY_MODE getEnemyMode(void){ return enemyMode_m; }
	private:
	unsigned int enemyHundle_m;//プレイヤーのハンドル
	ENEMY_MODE enemyMode_m;
	int moveCnt;//移動のカウント
	int attackCnt_m;//攻撃カウント
	ENEMY_TYPE enemyType_m;
	int enemyNumber_m;
 D3DXVECTOR3 targetPos_m;
 bool target_m;
};
#endif

//End Of File