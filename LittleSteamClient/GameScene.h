//=============================================================================
//
// ゲームシーン [GameScene.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef GAMESCNE_FILE
#define GAMESCNE_FILE
#include "main.h"
#include "Scene.h"
#include "ObjectMiniMap.h"

class NetClient;
class PlayerManager;
class ObjectPose;
class ObjectGameOver;
class TreeManager;
class GameScene: public Scene
{
	public:
	GameScene(void);//コンストラクタ
	~GameScene(void);//デストラクタ
	HRESULT Init(void);//初期化
	void Uninit(void);//解放
	void Update(void);//更新
	static void setGameOverFlag(bool flag){ gameOverFlag_m = flag; }
	static void setGameClearFlag(bool flag){ gameClearFlag_m = flag; }
	static ObjectMiniMap* getObjectMiniMap(void){ return minimap_m; }
	private:
	ObjectPose* pose_m;
	ObjectGameOver* gameOver_m;
	static bool gameOverFlag_m;
	static bool gameClearFlag_m;
	NetClient* netClient;
	TreeManager* treeMnager_m;
	static ObjectMiniMap* minimap_m;
};

#endif

//End Of FIle