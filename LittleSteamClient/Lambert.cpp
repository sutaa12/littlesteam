
//=============================================================================
//
// 光処理 [Lambert.cpp]
// Author : NARITADA SUZUK
//
//=============================================================================
#include "Lambert.h"
LAMBERT3::LAMBERT3( LPDIRECT3DDEVICE9 pd3dDevice )
{
   m_pd3dDevice = pd3dDevice;
   m_pEffect = NULL;
}

LAMBERT3::~LAMBERT3()
{
   //SafeReleaseは関数ではなくマクロ
   //#define SafeRelease(x) { if(x) { (x)->Release(); (x)=NULL; } }
   SAFE_RELEASE( m_pEffect );
}

void LAMBERT3::Invalidate()
{
   if( m_pEffect )
      m_pEffect->OnLostDevice();
}

void LAMBERT3::Restore()
{
   if( m_pEffect )
      m_pEffect->OnResetDevice();
}

HRESULT LAMBERT3::Load()
{
   D3DCAPS9 caps;
   
   //ハードウェアがサポートするバーテックスシェーダーとピクセルシェーダーのバージョンをチェックする
   m_pd3dDevice->GetDeviceCaps( &caps );
   if( caps.VertexShaderVersion >= D3DVS_VERSION( 1, 1 ) && caps.PixelShaderVersion >= D3DPS_VERSION( 2, 0 ) )
   {
      LPD3DXBUFFER pErr = NULL;

      HRESULT hr = D3DXCreateEffectFromFile( m_pd3dDevice, ("./data/SHADER/Lambert3.fx"), NULL, NULL, 0, NULL, &m_pEffect, &pErr );
      if( SUCCEEDED( hr ) )
      {
         m_pTechnique = m_pEffect->GetTechniqueByName( "TShader" );
         m_pWVP       = m_pEffect->GetParameterByName( NULL, "m_WVP" );
//         m_pWVPT      = m_pEffect->GetParameterByName( NULL, "m_WVPT" );

         m_pEffect->SetTechnique( m_pTechnique );
      }

      else
         return -1;
   }

   else
      return -2;

   return S_OK;
}

void LAMBERT3::Begin()
{
   if(  m_pEffect )
   {
      m_pEffect->Begin( NULL, 0 );
   }
}

void LAMBERT3::BeginPass( UINT Pass )
{
   if( m_pEffect )
   {
      m_pEffect->BeginPass( Pass );
   }
}

void LAMBERT3::SetMatrix( D3DXMATRIX* pMatWVP )
{
   if( m_pEffect )
   {
//      D3DXMATRIX m, m1, m2;

      m_pEffect->SetMatrix( m_pWVP, pMatWVP );

//      //行列変換マトリックスをテクスチャー座標系へ変換
//      D3DXMatrixScaling( &m1, 0.5f, -0.5f, 1.0f );
//      D3DXMatrixTranslation( &m2, 0.5f, 0.5f, 0.0f );
//      m = (*pMatWVP) * m1 * m2;
//      m_pEffect->SetMatrix( m_pWVPT, &m );
   }
}

void LAMBERT3::CommitChanges()
{
   if( m_pEffect )
      m_pEffect->CommitChanges();
}

void LAMBERT3::EndPass()
{
   if( m_pEffect )
      m_pEffect->EndPass();
}

void LAMBERT3::End()
{
   if( m_pEffect )
      m_pEffect->End();
}

BOOL LAMBERT3::IsOK()
{
   if( m_pEffect )
      return TRUE;

   return FALSE;
}