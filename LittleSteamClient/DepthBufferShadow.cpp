
//=============================================================================
//
// 影処理 [DepthBufferShadow.cpp]
// Author : NARITADA SUZUK
//
//=============================================================================
#include "DepthBufferShadow.h"
SOFTSHADOW1::SOFTSHADOW1(LPDIRECT3DDEVICE9 pd3dDevice)
{
   m_pd3dDevice = pd3dDevice;
   m_pEffect = NULL;
}

SOFTSHADOW1::~SOFTSHADOW1()
{
   SAFE_RELEASE( m_pEffect );
}

void SOFTSHADOW1::Invalidate()
{
   if( m_pEffect )
      m_pEffect->OnLostDevice();
}

void SOFTSHADOW1::Restore()
{
   if( m_pEffect )
      m_pEffect->OnResetDevice();
}

HRESULT SOFTSHADOW1::Load()
{
   D3DCAPS9 caps;

   m_pd3dDevice->GetDeviceCaps( &caps );
   if( caps.VertexShaderVersion >= D3DVS_VERSION( 1, 1 ) && caps.PixelShaderVersion >= D3DPS_VERSION( 2, 0 ) )
   {
      LPD3DXBUFFER pErr = NULL;
      HRESULT hr = D3DXCreateEffectFromFile( m_pd3dDevice,("./data/SHADER/SoftShadow.fx"), NULL, NULL, 0, NULL, &m_pEffect, &pErr );
      if( SUCCEEDED( hr ) )
      {
         m_pTechnique   = m_pEffect->GetTechniqueByName( "TShader" );
         m_pWVP         = m_pEffect->GetParameterByName( NULL, "m_WVP" );
         m_pWVPT        = m_pEffect->GetParameterByName( NULL, "m_WVPT" );
         m_pLWVP        = m_pEffect->GetParameterByName( NULL, "m_LWVP" );
         m_pLWVPT       = m_pEffect->GetParameterByName( NULL, "m_LWVPT" );
         m_pLightDir    = m_pEffect->GetParameterByName( NULL, "m_LightDir" );
         m_pAmbient     = m_pEffect->GetParameterByName( NULL, "m_Ambient" );
         m_pBias        = m_pEffect->GetParameterByName( NULL, "m_Bias" );
         m_pShadowColor = m_pEffect->GetParameterByName( NULL, "m_ShadowColor" );

         m_pEffect->SetTechnique( m_pTechnique );   
      }

      else
      {
         return -1;
      }
   }

   else
   {
      return -2;
   }

   return S_OK;
}

void SOFTSHADOW1::Begin()
{
   if(  m_pEffect )
   {
      m_pd3dDevice->GetTransform( D3DTS_VIEW, &m_matView );
      m_pd3dDevice->GetTransform( D3DTS_PROJECTION, &m_matProj );
      m_pEffect->Begin( NULL, 0 );
   }
}

void SOFTSHADOW1::BeginPass( UINT Pass )
{
   if( m_pEffect )
   {
      m_pEffect->BeginPass( Pass );
   }
}

void SOFTSHADOW1::SetAmbient( float Ambient )
{
   if( m_pEffect )
   {
      D3DXVECTOR4 A;
      A = D3DXVECTOR4( Ambient, Ambient, Ambient, 1.0f );
      m_pEffect->SetVector( m_pAmbient, &A );
   }

   else
   {
      D3DMATERIAL9 old_material;
      m_pd3dDevice->GetMaterial( &old_material );
      old_material.Ambient.r = Ambient;
      old_material.Ambient.g = Ambient;
      old_material.Ambient.b = Ambient;
      old_material.Ambient.a = 1.0f;
      m_pd3dDevice->SetMaterial( &old_material );
   }
}

void SOFTSHADOW1::SetAmbient( D3DXVECTOR4* pAmbient )
{
   if( m_pEffect )
      m_pEffect->SetVector( m_pAmbient, pAmbient );

   else
   {
      D3DMATERIAL9 old_material;
      m_pd3dDevice->GetMaterial( &old_material );
      old_material.Ambient.r = pAmbient->x;
      old_material.Ambient.g = pAmbient->y;
      old_material.Ambient.b = pAmbient->z;
      old_material.Ambient.a = pAmbient->w;
      m_pd3dDevice->SetMaterial( &old_material );
   }
}

//ローカル座標系
void SOFTSHADOW1::SetMatrix( D3DXMATRIX* pMatWorld, D3DXMATRIX* pMatLight, D3DXVECTOR4* pLightDir )
{
   if( m_pEffect )
   {
      D3DXMATRIX m, ms, mt;
      D3DXVECTOR4 LightDir;
      D3DXVECTOR4 v;

      //テクスチャー座標系への変換行列
      D3DXMatrixScaling( &ms, 0.5f, -0.5f, 1.0f );
      D3DXMatrixTranslation( &mt, 0.5f, 0.5f, 0.0f );

      //カメラ基準の行列変換マトリックスをセットする
      m = (*pMatWorld) * m_matView * m_matProj;
      m_pEffect->SetMatrix( m_pWVP, &m );

      //テクスチャー座標系へ変換
      m = m * ms * mt;
      m_pEffect->SetMatrix( m_pWVPT, &m );

      LightDir = *pLightDir;
      D3DXMatrixInverse( &m, NULL, pMatWorld );
      D3DXVec4Transform( &v, &LightDir, &m );
      D3DXVec4Normalize( &v, &v );
      m_pEffect->SetVector( m_pLightDir, &v );

      //ライト基準の行列変換マトリックスをセットする
      m = (*pMatWorld) * (*pMatLight);
      m_pEffect->SetMatrix( m_pLWVP, &m );

      //テクスチャー座標系へ変換
      m = m * ms * mt;
      m_pEffect->SetMatrix( m_pLWVPT, &m );
   }

   else
      m_pd3dDevice->SetTransform( D3DTS_WORLD, pMatWorld );
}

//Z値の比較によって発生する計算誤差を軽減するためのバイアス値
//シェーダー内の計算方法の関係で0.0f 〜 1.0fの範囲内の値にならないので注意すること
void SOFTSHADOW1::SetBias( float pBias )
{
   if( m_pEffect )
      m_pEffect->SetFloat( m_pBias, pBias );
   
}

//影によるメッシュのカラーの減算値
//1.0fで黒くなる
void SOFTSHADOW1::SetShadowColor( float pShadowColor )
{
   if( m_pEffect )
      m_pEffect->SetFloat( m_pShadowColor, pShadowColor );
   
}

void SOFTSHADOW1::CommitChanges()
{
   if( m_pEffect )
      m_pEffect->CommitChanges();
}

void SOFTSHADOW1::EndPass()
{
   if( m_pEffect )
   {
      m_pEffect->EndPass();
   }
}

void SOFTSHADOW1::End()
{
   if( m_pEffect )
   {
      m_pEffect->End();
   }
}

BOOL SOFTSHADOW1::IsOK()
{
   if( m_pEffect )
      return TRUE;

   return FALSE;
}
