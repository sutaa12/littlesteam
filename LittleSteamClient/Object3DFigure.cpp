/******************************************************************************
@file       Object3DFigure.cpp
@brief      Object3DFigureの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "Object3DFigure.h"
#include "SystemManager.h"
#include "CollisionManager.h"

#define HEIGT_MAX_IMG ("./data/TEXTURE/heightmap.bmp")
//================================================================================
//タイプ別作成
//================================================================================
Object3DFigure* Object3DFigure::Create(FIGURE_MODE mode,TEXTURE_LIST texName,unsigned int numW,unsigned int numH,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,PRIORITY_MODE priority)
{
	Object3DFigure *objectPointer;
	const OBJECT3D_TYPE type[FIGURE_MAX] =
	{
  OBJCT3D_MESHBOARD,OBJCT3D_MESHCYLINDER,OBJCT3D_MESHDOOM,OBJCT3D_MESHFIELD,OBJCT3D_MESHSPHERE
	};
	objectPointer = new Object3DFigure(mode,type[mode],numW,numH,priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setSize(size);
		objectPointer->setTexName(texName);
		objectPointer->init(mode);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
Object3DFigure::Object3DFigure(FIGURE_MODE mode,OBJECT3D_TYPE type,unsigned int numW,unsigned int numH,PRIORITY_MODE priority,OBJTYPE objType):Object3D(priority,objType,type)
{
	D3DDevice_m = NULL;
	D3DVtxBuff_m = NULL;
	D3DIndexBuff_m = NULL;
	nor_m = NULL;
	numBlockW_m = numW;
	numBlockH_m = numH;
	sizeBlockW_m = size_m.x;
	sizeBlockH_m = size_m.z;
	numVertex_m = ( numBlockW_m + 2 )*( numBlockH_m * 2 ) - 2;//総数検出
	numPolygon_m = ( numBlockW_m + 1 ) * ( numBlockH_m + 1 );//ポリゴン数
	imgSizeW_m = 0;
	imgSizeH_m = 0;
	height_m = nullptr;

	func_m[0] = &Object3DFigure::initMeshBoard;
	func_m[1] = &Object3DFigure::initMeshCyrinder;
	func_m[2] = &Object3DFigure::initMeshDoom;
 func_m[3] = &Object3DFigure::initMeshField;
 func_m[4] = &Object3DFigure::initMeshSphere;

	texScl_m = D3DXVECTOR2(1.0f,1.0f);
}
//==============================================================================
//デストラクタ
Object3DFigure::~Object3DFigure(void)
{

}
//==============================================================================
//初期化
void Object3DFigure::init(FIGURE_MODE objMode)
{


	sizeBlockW_m = size_m.x;
	sizeBlockH_m = size_m.z;
	if(objMode == FIGURE_MESHFIELD)
	{
		loadHightMap();
		numVertex_m = ( numBlockW_m + 2 )*( numBlockH_m * 2 ) - 2;//総数検出
		numPolygon_m = ( numBlockW_m + 1 ) * ( numBlockH_m + 1 );//ポリゴン数
	}
	D3DDevice_m = DrawManager::getD3DDEVICE();
	//頂点情報の設定
	if(FAILED(D3DDevice_m->CreateVertexBuffer(	//頂点バッファ作成
		sizeof(VERTEX_3D) * numPolygon_m,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&D3DVtxBuff_m,
		NULL)))
	{
	}
	//インデックス頂点情報の設定
	if(FAILED(D3DDevice_m->CreateIndexBuffer(	//頂点バッファ作成
		sizeof(DWORD)*numVertex_m,
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX32,
		D3DPOOL_MANAGED,
		&D3DIndexBuff_m,
		NULL)))
	{
	}
	(this->*func_m[objMode])();
}
//==============================================================================
//解放
void Object3DFigure::uninit(void)
{
	SAFE_RELEASE(D3DVtxBuff_m);
	SAFE_RELEASE(D3DIndexBuff_m);
	SAFE_DELETE_ARRAY(nor_m);
	SAFE_DELETE_ARRAY(height_m);
	Object3D::uninit();
}
//==============================================================================
//更新
void Object3DFigure::update(void)
{
	if(textureName_m == TEXTURE_MOUNTAIN || textureName_m == TEXTURE_SKY)
	{
		if(&SystemManager::getCameraData() != NULL)
		{
			pos_m.x = SystemManager::getCameraData().cameraP.x;
			pos_m.z = SystemManager::getCameraData().cameraP.z;
		}
	}
	Object3D::update();
}
//初期化
void Object3DFigure::initMeshBoard(void)
{
	VERTEX_3D *pvtx;


	nor_m = new D3DXVECTOR3[numBlockW_m * numBlockH_m * 2];

	D3DVtxBuff_m->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック
	int nNum = 0;
	for(int nLoopZ = 0;nLoopZ <= numBlockH_m;nLoopZ++)//Zブロック数+1まで
	{
		for(int nLoopX = 0;nLoopX <= numBlockW_m;nLoopX++)//Xブロック数+1まで
		{
			pvtx[nNum].vtx = D3DXVECTOR3(( sizeBlockW_m / (numBlockW_m)* nLoopX ) - sizeBlockW_m / 2,
										 0,
										 ( sizeBlockH_m / (numBlockH_m)* ( numBlockH_m - nLoopZ ) ) - sizeBlockH_m / 2);//頂点の座標格納
			pvtx[nNum].diffuse = col_m; //色設定
			pvtx[nNum].tex = D3DXVECTOR2(( texScl_m.x / numBlockW_m ) * nLoopX,( texScl_m.y / numBlockH_m ) * nLoopZ);
			nNum++;

		}
	}


	//各面の方選出
	int nNumNor = 0;
	int vtx = 0;
	for(int nBuff = 0; nBuff < numBlockW_m * numBlockH_m;nBuff++)
	{
		D3DXVECTOR3 vec0,vec1;
		D3DXVECTOR3 nor0,nor1;
		vec0 = pvtx[vtx].vtx - pvtx[vtx + numBlockW_m + 1].vtx;
		vec1 = pvtx[vtx + numBlockW_m + 2].vtx - pvtx[vtx + numBlockW_m + 1].vtx;
		D3DXVec3Cross(&nor0,&vec0,&vec1);
		D3DXVec3Normalize(&nor_m[nNumNor],&nor0);

		vec0 = pvtx[vtx + 1].vtx - pvtx[vtx].vtx;
		vec1 = pvtx[vtx + numBlockW_m + 2].vtx - pvtx[vtx].vtx;
		D3DXVec3Cross(&nor0,&vec0,&vec1);
		D3DXVec3Normalize(&nor_m[nNumNor + 1],&nor0);
		nNumNor += 2;

		vtx++;

		if(( vtx + 1 ) % ( numBlockW_m + 1 ) == 0)
		{
			vtx++;
		}
	}

	//面を設定
	for(int nBuff = 0; nBuff < ( numBlockW_m + 1 ) * ( numBlockH_m + 1 );nBuff++)
	{
		D3DXVECTOR3 nor0;
		//最初か最後の頂点か
		if(nBuff == 0 || nBuff == ( numBlockW_m + 1 ) * ( numBlockH_m + 1 ) - 1)
		{
			if(nBuff == 0)
			{
				nor0 = nor_m[nBuff] + nor_m[nBuff + 1];
			}
			else{
				nor0 = nor_m[( numBlockW_m + 1 ) * ( numBlockH_m + 1 ) + 1] + nor_m[( numBlockW_m + 1 ) * ( numBlockH_m + 1 )];
			}
		}
		else
			//角の頂点か
			if(nBuff == numBlockW_m || nBuff == ( numBlockW_m + 1 ) * numBlockH_m)
			{
			if(nBuff == numBlockW_m)
			{
				nor0 = nor_m[(numBlockW_m)* 2 - 1];
			}
			else{
				nor0 = nor_m[numBlockW_m * ( numBlockH_m - 1 )];

			}

			}
			else
				//四隅以外の外側の頂点か
				if(nBuff < numBlockW_m || nBuff % ( numBlockW_m + 1 ) == 0 || ( nBuff + 1 ) % ( numBlockW_m + 1 ) == 0 || nBuff >(numBlockW_m + 1) * numBlockH_m)
				{
			if(nBuff < numBlockW_m)
			{

				nor0 = nor_m[nBuff * 2 - 1] + nor_m[nBuff * 2] + nor_m[nBuff * 2 + 1];

			}
			else
				if(nBuff % ( numBlockW_m + 1 ) == 0)
				{
				int buff = ( nBuff / ( numBlockW_m + 1 ) * ( numBlockW_m * 2 ) );

				nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff - ( numBlockW_m * 2 )];

				}
				else
					if(( nBuff + 1 ) % ( numBlockW_m + 1 ) == 0)
					{
				int buff = ( ( nBuff + 1 ) / ( numBlockW_m + 1 ) * ( numBlockW_m * 2 ) - 1 );

				nor0 = nor_m[buff] + nor_m[buff - 1] + nor_m[buff - ( numBlockW_m * 2 ) - 1];
					}
					else{
						int buff = ( numBlockW_m * ( numBlockH_m - 1 ) * 2 ) - 1 + ( nBuff % numBlockW_m ) * 2;
						nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff + 2];

					}
				}
				else{
					//それ以外の頂点か
					int buff = ( numBlockW_m * 2 ) * ( nBuff / ( numBlockW_m + 1 ) ) + ( ( ( nBuff % ( numBlockW_m + 1 ) ) - 1 ) * 2 );
					nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff + 2] + nor_m[buff - ( numBlockW_m * 2 )] + nor_m[buff + 1 - ( numBlockW_m * 2 )] + nor_m[buff + 2 - ( numBlockW_m * 2 )];


				}
				D3DXVec3Normalize(&nor0,&nor0);
				pvtx[nBuff].nor = nor0;

	}

	D3DVtxBuff_m->Unlock();//ロックの解除を忘れずに

	DWORD *pIndex;

	D3DIndexBuff_m->Lock(0,0,(void**)&pIndex,0);

	int nIndexNum = 0;//インデックス頂点の上位置
	int nIndexZcnt = numBlockW_m + 1;//下頂点の位置
	int nIndexReturn = 0;//折り返し回数

	for(int nBuff = 0; nBuff < numVertex_m;)
	{
		pIndex[nBuff] = nIndexZcnt;//下の設定
		nBuff++;
		pIndex[nBuff] = nIndexNum;//上の設定
		nIndexZcnt++;//下の数加算
		nBuff++;

		if(numBlockH_m > 0 && nIndexNum == (numBlockW_m)+( ( numBlockW_m + 1 ) * nIndexReturn ) && nBuff < numVertex_m)//上の頂点が横サイズで割り切れれば
		{
			pIndex[nBuff] = nIndexNum;//上の設定
			nBuff++;
			pIndex[nBuff] = nIndexZcnt;//下の設定
			nBuff++;
			nIndexReturn++;
		}
		nIndexNum++;//上の数加算	
	}
	D3DIndexBuff_m->Unlock();
}
//初期化
void Object3DFigure::initMeshCyrinder(void)
{
	VERTEX_3D *pvtx;
	if(textureName_m == TEXTURE_MOUNTAIN)
	{
		setTexScl(D3DXVECTOR2(16,1));
		lightMode = true;
	}

	D3DVtxBuff_m->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック
	int nNum = 0;
	for(int nLoopZ = 0;nLoopZ <= numBlockH_m;nLoopZ++)//Zブロック数+1まで
	{
		for(int nLoopX = 0;nLoopX <= numBlockW_m;nLoopX++)//Xブロック数+1まで
		{
			pvtx[nNum].vtx = D3DXVECTOR3(( cosf(D3DX_PI * 2 * nLoopX / numBlockW_m) * ( sizeBlockW_m ) ),
										 ( sizeBlockH_m / ( numBlockH_m ) ) * ( nLoopZ ),
										 ( sinf(D3DX_PI * 2 * nLoopX / numBlockW_m) * ( sizeBlockW_m ) ));//頂点の座標格納

			pvtx[nNum].nor = D3DXVECTOR3(( cosf(D3DX_PI * 2 * nLoopX / numBlockW_m) ),0.0f,( sinf(D3DX_PI * 2 * nLoopX / numBlockW_m) ));//法線の座標格納
			pvtx[nNum].diffuse = col_m; //色設定
   pvtx[nNum].tex = D3DXVECTOR2(( texScl_m.x / numBlockW_m ) * nLoopX,( texScl_m.y / numBlockH_m ) *( numBlockH_m - nLoopZ ) + 0.01f);
			nNum++;
		}
	}
	D3DVtxBuff_m->Unlock();//ロックの解除を忘れずに

	DWORD *pIndex;

	D3DIndexBuff_m->Lock(0,0,(void**)&pIndex,0);
	int nIndexNum = 0;//インデックス頂点の上位置
	int nIndexZcnt = numBlockW_m + 1;//下頂点の位置
	int nIndexReturn = 0;//折り返し回数

	for(int nBuff = 0; nBuff < numVertex_m;)
	{
		pIndex[nBuff] = nIndexZcnt;//下の設定
		nBuff++;
		pIndex[nBuff] = nIndexNum;//上の設定
		nIndexZcnt++;//下の数加算
		nBuff++;

		if(numBlockH_m > 0 && nIndexNum == (numBlockW_m)+( ( numBlockW_m + 1 ) * nIndexReturn ) && nBuff < numVertex_m)//上の頂点が横サイズで割り切れれば
		{
			pIndex[nBuff] = nIndexNum;//上の設定
			nBuff++;
			pIndex[nBuff] = nIndexZcnt;//下の設定
			nBuff++;
			nIndexReturn++;
		}
		nIndexNum++;//上の数加算	
	}

	D3DIndexBuff_m->Unlock();//ロック
}
//初期化
void Object3DFigure::initMeshDoom(void)
{
	VERTEX_3D *pvtx;

	if(textureName_m == TEXTURE_SKY)
	{
		lightMode = true;
	}
	D3DVtxBuff_m->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック
	int nNum = 0;
	for(int nLoopZ = numBlockH_m;nLoopZ >= 0;nLoopZ--)//Zブロック数+1まで
	{
		for(int nLoopX = 0;nLoopX <= numBlockW_m;nLoopX++)//Xブロック数+1まで
		{
			float fLengthXZ = D3DX_PI / 2 * nLoopZ / numBlockH_m;
			float fLengthXZI = ( D3DX_PI / 2 ) * nLoopZ / numBlockH_m;
			float fLengthY = D3DX_PI * 2 * nLoopX / numBlockW_m;
			float fLengthYI = D3DX_PI / 2 * ( numBlockW_m - nLoopX ) / numBlockW_m;
			float fRot = sinf(fLengthXZ);
			pvtx[nNum].vtx = D3DXVECTOR3(fRot * cosf(fLengthY) * ( sizeBlockW_m ),
										 cosf(fLengthXZ) * ( sizeBlockW_m ),
										 fRot * sinf(fLengthY) * ( sizeBlockW_m ));//頂点の座標格納

			pvtx[nNum].nor = D3DXVECTOR3(fRot * cosf(fLengthY),
										 cosf(fLengthXZ),
										 fRot * sinf(fLengthY));//法線の座標格納

			pvtx[nNum].diffuse = col_m; //色設定
			pvtx[nNum].tex = D3DXVECTOR2(( texScl_m.x / numBlockW_m ) * nLoopX,( texScl_m.y / numBlockH_m ) * ( nLoopZ + 0.1f ));
			nNum++;
		}
	}
	D3DVtxBuff_m->Unlock();//ロックの解除を忘れずに

	DWORD *pIndex;

	D3DIndexBuff_m->Lock(0,0,(void**)&pIndex,0);
	int nIndexNum = 0;//インデックス頂点の上位置
	int nIndexZcnt = numBlockW_m + 1;//下頂点の位置
	int nIndexReturn = 0;//折り返し回数

	for(int nBuff = 0; nBuff < numVertex_m;)
	{
		pIndex[nBuff] = nIndexZcnt;//下の設定
		nBuff++;
		pIndex[nBuff] = nIndexNum;//上の設定
		nIndexZcnt++;//下の数加算
		nBuff++;

		if(numBlockH_m > 0 && nIndexNum == (numBlockW_m)+( ( numBlockW_m + 1 ) * nIndexReturn ) && nBuff < numVertex_m)//上の頂点が横サイズで割り切れれば
		{
			pIndex[nBuff] = nIndexNum;//上の設定
			nBuff++;
			pIndex[nBuff] = nIndexZcnt;//下の設定
			nBuff++;
			nIndexReturn++;
		}
		nIndexNum++;//上の数加算	
	}

	D3DIndexBuff_m->Unlock();//ロック
}
//初期化
void Object3DFigure::initMeshSphere(void)
{
 VERTEX_3D *pvtx;

 D3DVtxBuff_m->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック
 int nNum = 0;
 for(int nLoopZ = 0;nLoopZ < numBlockH_m;nLoopZ++)//Zブロック数+1まで
 {
  for(int nLoopX = 0;nLoopX <= numBlockW_m;nLoopX++)//Xブロック数+1まで
  {
   float fLengthXZ = D3DX_PI * 2 * nLoopZ / numBlockH_m;
   float fLengthXZI = ( D3DX_PI ) * nLoopZ / numBlockH_m;
   float fLengthY = D3DX_PI * 2 * nLoopX / numBlockW_m;
   float fLengthYI = D3DX_PI * ( numBlockW_m - nLoopX ) / numBlockW_m;
   float fRot = sinf(fLengthXZ);
   pvtx[nNum].vtx = D3DXVECTOR3(fRot * cosf(fLengthY) * ( sizeBlockW_m ),
                                cosf(fLengthXZ) * ( sizeBlockW_m ),
                                fRot * sinf(fLengthY) * ( sizeBlockW_m ));//頂点の座標格納

   pvtx[nNum].nor = D3DXVECTOR3(fRot * cosf(fLengthY),
                                cosf(fLengthXZ),
                                fRot * sinf(fLengthY));//法線の座標格納

   pvtx[nNum].diffuse = col_m; //色設定
   pvtx[nNum].tex = D3DXVECTOR2(( texScl_m.x / numBlockW_m ) * nLoopX,( texScl_m.y / numBlockH_m ) * ( nLoopZ + 0.1f ));
   nNum++;
  }
 }
 D3DVtxBuff_m->Unlock();//ロックの解除を忘れずに

 DWORD *pIndex;

 D3DIndexBuff_m->Lock(0,0,(void**)&pIndex,0);
 int nIndexNum = 0;//インデックス頂点の上位置
 int nIndexZcnt = numBlockW_m + 1;//下頂点の位置
 int nIndexReturn = 0;//折り返し回数

 for(int nBuff = 0; nBuff < numVertex_m;)
 {
  pIndex[nBuff] = nIndexZcnt;//下の設定
  nBuff++;
  pIndex[nBuff] = nIndexNum;//上の設定
  nIndexZcnt++;//下の数加算
  nBuff++;

  if(numBlockH_m > 0 && nIndexNum == (numBlockW_m)+( ( numBlockW_m + 1 ) * nIndexReturn ) && nBuff < numVertex_m)//上の頂点が横サイズで割り切れれば
  {
   pIndex[nBuff] = nIndexNum;//上の設定
   nBuff++;
   pIndex[nBuff] = nIndexZcnt;//下の設定
   nBuff++;
   nIndexReturn++;
  }
  nIndexNum++;//上の数加算	
 }

 D3DIndexBuff_m->Unlock();//ロック
}
//画像ロード
void Object3DFigure::loadHightMap(void)
{
	FILE *file;
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;//ヘッダー情報
	int scale = 40;
	file = fopen(HEIGT_MAX_IMG,"rb");
	if(file != NULL)
	{
		fread(&bmfh,sizeof(BITMAPFILEHEADER),1,file);
		fread(&bmih,sizeof(BITMAPINFOHEADER),1,file);

		height_m = new float[bmih.biWidth * bmih.biHeight];

		BYTE Height;
		if(bmih.biBitCount == 24)
		{

			for(int y = bmih.biHeight - 1; y >= 0; y--)
			{
				for(int x = 0; x < bmih.biWidth; x++)
				{
					fread(&Height,1,1,file);
					fread(&Height,1,1,file);
					fread(&Height,1,1,file);
					if(Height == '\0')
					{

						fread(&Height,1,1,file);
						fread(&Height,1,1,file);
						fread(&Height,1,1,file);
					}
					height_m[bmih.biWidth * y + x] = (float)Height - 255;
					height_m[bmih.biWidth * y + x] += 255;
					height_m[bmih.biWidth * y + x] *= scale;
				}
			}
			imgSizeW_m = bmih.biWidth;
			imgSizeH_m = bmih.biHeight;
			if(numBlockW_m <= 0 || numBlockH_m <= 0)
			{
				numBlockW_m = bmih.biWidth - 1;
				numBlockH_m = bmih.biHeight - 1;
			}
		}
		else
			if(bmih.biBitCount == 8)
			{
			RGBQUAD sRGB[256];

			fread(&sRGB,sizeof(RGBQUAD),256,file);

			for(int y = bmih.biHeight - 1; y >= 0; y--)
			{
				for(int x = 0; x < bmih.biWidth; x++)
				{
					fread(&Height,1,1,file);
					if(Height == '\0')
					{

						fread(&Height,1,1,file);
					}

					height_m[bmih.biWidth * y + x] = (float)sRGB[Height].rgbBlue - 255;
					height_m[bmih.biWidth * y + x] += 255;
					height_m[bmih.biWidth * y + x] *= scale;
				}
			}
			imgSizeW_m = bmih.biWidth;
			imgSizeH_m = bmih.biHeight;
			if(numBlockW_m <= 0 || numBlockH_m <= 0)
			{
				numBlockW_m = bmih.biWidth - 1;
				numBlockH_m = bmih.biHeight - 1;
			}

			}
		fclose(file);

	}
	else{

		//失敗したら10x10の平面
		height_m = new float[11 * 11];
		memset(height_m,0,sizeof(float));
		numBlockW_m = 10;
		numBlockH_m = 10;
	}
}
//初期化
void Object3DFigure::initMeshField(void)
{
	CollisionManager::setFiledMesh(getObjHandle());
	VERTEX_3D *pvtx;
	//col_m = D3DXCOLOR(0.4,0.6,0.2,1);
	setTexScl(D3DXVECTOR2(50,50));

	nor_m = new D3DXVECTOR3[numBlockW_m * numBlockH_m * 2];

	D3DVtxBuff_m->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック
	int nNum = 0;
	for(int nLoopZ = 0;nLoopZ <= numBlockH_m;nLoopZ++)//Zブロック数+1まで
	{
		for(int nLoopX = 0;nLoopX <= numBlockW_m;nLoopX++)//Xブロック数+1まで
		{
			pvtx[nNum].vtx = D3DXVECTOR3(( sizeBlockW_m / (numBlockW_m)* nLoopX ) - sizeBlockW_m / 2,
										 height_m[nLoopZ * imgSizeW_m + nLoopX],
										 ( sizeBlockH_m / (numBlockH_m)* ( numBlockH_m - nLoopZ ) ) - sizeBlockH_m / 2);//頂点の座標格納
			pvtx[nNum].diffuse = col_m; //色設定
			pvtx[nNum].tex = D3DXVECTOR2(( texScl_m.x / numBlockW_m ) * nLoopX,( texScl_m.y / numBlockH_m ) * nLoopZ);
			nNum++;

		}
	}


	//各面の方選出
	int nNumNor = 0;
	int vtx = 0;
	for(int nBuff = 0; nBuff < numBlockW_m * numBlockH_m;nBuff++)
	{
		D3DXVECTOR3 vec0,vec1;
		D3DXVECTOR3 nor0,nor1;
		vec0 = pvtx[vtx].vtx - pvtx[vtx + numBlockW_m + 1].vtx;
		vec1 = pvtx[vtx + numBlockW_m + 2].vtx - pvtx[vtx + numBlockW_m + 1].vtx;
		D3DXVec3Cross(&nor0,&vec0,&vec1);
		D3DXVec3Normalize(&nor_m[nNumNor],&nor0);

		vec0 = pvtx[vtx + 1].vtx - pvtx[vtx].vtx;
		vec1 = pvtx[vtx + numBlockW_m + 2].vtx - pvtx[vtx].vtx;
		D3DXVec3Cross(&nor0,&vec0,&vec1);
		D3DXVec3Normalize(&nor_m[nNumNor + 1],&nor0);
		nNumNor += 2;

		vtx++;

		if(( vtx + 1 ) % ( numBlockW_m + 1 ) == 0)
		{
			vtx++;
		}
	}

	//面を設定
	for(int nBuff = 0; nBuff < ( numBlockW_m + 1 ) * ( numBlockH_m + 1 );nBuff++)
	{
		D3DXVECTOR3 nor0;
		//最初か最後の頂点か
		if(nBuff == 0 || nBuff == ( numBlockW_m + 1 ) * ( numBlockH_m + 1 ) - 1)
		{
			if(nBuff == 0)
			{
				nor0 = nor_m[nBuff] + nor_m[nBuff + 1];
			}
			else{
				nor0 = nor_m[( numBlockW_m + 1 ) * ( numBlockH_m + 1 ) + 1] + nor_m[( numBlockW_m + 1 ) * ( numBlockH_m + 1 )];
			}
		}
		else
			//角の頂点か
			if(nBuff == numBlockW_m || nBuff == ( numBlockW_m + 1 ) * numBlockH_m)
			{
			if(nBuff == numBlockW_m)
			{
				nor0 = nor_m[(numBlockW_m)* 2 - 1];
			}
			else{
				nor0 = nor_m[numBlockW_m * ( numBlockH_m - 1 )];

			}

			}
			else
				//四隅以外の外側の頂点か
				if(nBuff < numBlockW_m || nBuff % ( numBlockW_m + 1 ) == 0 || ( nBuff + 1 ) % ( numBlockW_m + 1 ) == 0 || nBuff >(numBlockW_m + 1) * numBlockH_m)
				{
			if(nBuff < numBlockW_m)
			{

				nor0 = nor_m[nBuff * 2 - 1] + nor_m[nBuff * 2] + nor_m[nBuff * 2 + 1];

			}
			else
				if(nBuff % ( numBlockW_m + 1 ) == 0)
				{
				int buff = ( nBuff / ( numBlockW_m + 1 ) * ( numBlockW_m * 2 ) );

				nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff - ( numBlockW_m * 2 )];

				}
				else
					if(( nBuff + 1 ) % ( numBlockW_m + 1 ) == 0)
					{
				int buff = ( ( nBuff + 1 ) / ( numBlockW_m + 1 ) * ( numBlockW_m * 2 ) - 1 );

				nor0 = nor_m[buff] + nor_m[buff - 1] + nor_m[buff - ( numBlockW_m * 2 ) - 1];
					}
					else{
						int buff = ( numBlockW_m * ( numBlockH_m - 1 ) * 2 ) - 1 + ( nBuff % numBlockW_m ) * 2;
						nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff + 2];

					}
				}
				else{
					//それ以外の頂点か
					int buff = ( numBlockW_m * 2 ) * ( nBuff / ( numBlockW_m + 1 ) ) + ( ( ( nBuff % ( numBlockW_m + 1 ) ) - 1 ) * 2 );
					nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff + 2] + nor_m[buff - ( numBlockW_m * 2 )] + nor_m[buff + 1 - ( numBlockW_m * 2 )] + nor_m[buff + 2 - ( numBlockW_m * 2 )];


				}
				D3DXVec3Normalize(&nor0,&nor0);
				pvtx[nBuff].nor = nor0;

	}

	D3DVtxBuff_m->Unlock();//ロックの解除を忘れずに

	DWORD *pIndex;

	D3DIndexBuff_m->Lock(0,0,(void**)&pIndex,0);

	int nIndexNum = 0;//インデックス頂点の上位置
	int nIndexZcnt = numBlockW_m + 1;//下頂点の位置
	int nIndexReturn = 0;//折り返し回数

	for(int nBuff = 0; nBuff < numVertex_m;)
	{
		pIndex[nBuff] = nIndexZcnt;//下の設定
		nBuff++;
		pIndex[nBuff] = nIndexNum;//上の設定
		nIndexZcnt++;//下の数加算
		nBuff++;

		if(numBlockH_m > 0 && nIndexNum == (numBlockW_m)+( ( numBlockW_m + 1 ) * nIndexReturn ) && nBuff < numVertex_m)//上の頂点が横サイズで割り切れれば
		{
			pIndex[nBuff] = nIndexNum;//上の設定
			nBuff++;
			pIndex[nBuff] = nIndexZcnt;//下の設定
			nBuff++;
			nIndexReturn++;
		}
		nIndexNum++;//上の数加算	
	}
	D3DIndexBuff_m->Unlock();
}
//End Of FIle