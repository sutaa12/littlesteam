/******************************************************************************/
/*! @addtogroup Object_Object
@file       Object.h
@brief      Objectの基本クラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef OBJECT_FILE
#define OBJECT_FILE

/*! @enum OBJTYPE
@brief オブジェクトの種類
*/
typedef enum{
	OBJTYPE_NONE = 0,
	OBJTYPE_2D,
	OBJTYPE_3D,
	OBJTYPE_3DBILLBOARD,
	OBJTYPE_FIGURE,
	OBJTYPE_3DXFILE,
	OBJTYPE_3DXFILEANIM,
	OBJTYPE_MAX
}OBJTYPE;
/*! @enum PRIORITY_MODE
@brief 描画する順番
*/
enum PRIORITY_MODE
{
	PRIORITY_0,
	PRIORITY_1,
	PRIORITY_2,
	PRIORITY_3,
	PRIORITY_4,
	PRIORITY_5,
	PRIORITY_6,
	PRIORITY_7,
	PRIORITY_POSE,
	PRIORITY_FEED,
	PRIORITY_MAX
};

/*! @class Object
@brief  オブジェクトの基本クラス　全てのオブジェクトをリストに追加する ハンドルの登録も行う
*/

class Object
{
	//メンバ関数
	//コンストラクタ
	public:

	/*! Object　コンストラクタ
	@param[in]     priority    設定する描画順番
	@param[in]     objType    設定するオブジェタイプ
	*/
	Object(PRIORITY_MODE priority,OBJTYPE objType);
	//デストラクタ
	public:
	/*! ~Object　デストラクタ*/
	virtual ~Object(void){};
	//操作
	public:
	/*! Init　オブジェクト初期化
	*/
	virtual void init(void) = 0;
	/*! Object　オブジェクト解放*/
	virtual void uninit(void) = 0;
	/*! Object　オブジェクト更新*/
 virtual void update(void) = 0;
	/*! release　オブジェクトを解放*/
	void release(void);
	/*! linkList　登録*/
	void linkList(void);
	/*! unLinkList　登録解除*/
	void unLinkList(void);
	/*! updateAll　全更新*/
	static void updateAll(void);
	/*! releaseAll　全解放*/
	static void releaseAll(void);
	/*! releasePriority　プライオリティまでのオブジェクト解放*/
	static void releasePriority(PRIORITY_MODE Priorty);
	/*! searchObject　オブジェクト検索*/
	static void searchObject(OBJTYPE objType,int &num,Object** &object);
	/*! setUpdateFlagPriority　プライオリティのオブジェクトの更新フラグを変更*/
	static void setUpdateFlagPriority(PRIORITY_MODE Priorty,bool update);
	/*! setDrawFlagPriority　プライオリティのオブジェクトの描画フラグを変更*/
	static void setDrawFlagPriority(PRIORITY_MODE Priorty,bool flag);
	static void updatePrioritySet(PRIORITY_MODE Priorty,bool update);
	//属性
	public:
	/*! getPriority　描画順番取得
	@return         描画順番が返ってくる
	*/
	PRIORITY_MODE getPriority(void){ return priority_m; }
	/*! getPriority　描画順番取得
	@return         オブジェの種類が返ってくる
	*/
	OBJTYPE getObjType(void){ return objType_m; }
	/*! setDrawFlag　描画フラグを設定
	@param[in]      drawFlag    設定する描画フラグ true false
	*/
	void setDrawFlag(bool drawFlag){ drawFlag_m = drawFlag; }
	/*! setUpdateFlag　更新フラグ設定
	@param[in]      updateFlag    設定する更新フラグ true false
	*/
	void setUpdateFlag(bool updateFlag){ updateFlag_m = updateFlag; }
	/*! getDrawFlag　描画フラグ取得
	@return         描画フラグを返す true false
	*/
	bool getDrawFlag(void){ return drawFlag_m ; }
	/*! getUpdateFlag　更新フラグ取得
	@return         更新フラグを返す true false
	*/
	bool getUpdateFlag(void){ return updateFlag_m; }
	/*! getObjHandle　オブジェクトのハンドル取得取得
	@return         オブジェクトのハンドルを返す 
	*/
	unsigned int getObjHandle(void){ return objHandle; }

	//メンバ変数
	private:
	Object* nextPointer_m;/*!< オブジェクトの次のアドレス*/
	Object* prevPointer_m;/*!< オブジェクトの前のアドレス*/
	PRIORITY_MODE priority_m;/*!< プライオリティ番号*/
	bool deleteFlag_m;/*!< 削除フラグ*/
	OBJTYPE objType_m;/*!< オブジェタイプ*/
	bool drawFlag_m;/*!< 描画フラグ*/
	bool updateFlag_m;/*!< 更新フラグ*/
	unsigned int objHandle;/*< objのハンドル*/


	static Object* top_m[PRIORITY_MAX];/*!< シーンのトップアドレス*/
	static Object* cur_m[PRIORITY_MAX];/*!< シーンの現在アドレス*/
};
#endif

//End Of File