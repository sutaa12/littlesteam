/******************************************************************************
@file       Object3DBillboard.cpp
@brief      Object3DBillboardの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "Object3DBillboard.h"
//================================================================================
//タイプ別作成
//================================================================================
Object3DBillboard* Object3DBillboard::Create(TEXTURE_LIST texName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	Object3DBillboard *objectPointer;

	objectPointer = new Object3DBillboard(priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setSize(size);
		objectPointer->setTexName(texName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
Object3DBillboard::Object3DBillboard(PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3D(priority,objType)
{
	init();
}
//==============================================================================
//デストラクタ
Object3DBillboard::~Object3DBillboard(void)
{

}
//==============================================================================
//初期化
void Object3DBillboard::init(void)
{

}
//==============================================================================
//解放
void Object3DBillboard::uninit(void)
{

	Object3D::uninit();
}
//==============================================================================
//更新
void Object3DBillboard::update(void)
{
	Object3D::update();
}

//End Of FIle