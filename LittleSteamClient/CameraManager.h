/******************************************************************************/
/*! @addtogroup Camera_CameraManager
@file       CameraManager.h
@brief      ゲームのカメラを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef CAMERAMANAGER_FILE
#define CAMERAMANAGER_FILE
#include "common.h"
#include "Object.h"
typedef struct 
{
	D3DXVECTOR3 cameraP;//カメラの位置
	D3DXVECTOR3 cameraR;//カメラの注視点
	D3DXVECTOR3 cameraV;//カメラのの姿勢方向
	D3DXMATRIX cameraView;//カメラのビュー行列
	D3DXMATRIX cameraProjection;//カメラのプロジェクション行列
	D3DXMATRIX cameraWorld;//カメラのワールド行列
	D3DXVECTOR3 cameraPDef;//差分用カメラの位置
	D3DXVECTOR3 cameraRDef;//差分用カメラの位置
	D3DXVECTOR3 rot;//カメラの角度
	D3DXVECTOR3 rotDef;//カメラの差分角度
	float posSpeed;//移動速度
	float rotSpeed;//回転速度
	float posDeffSpeed;//カメラ位置の差分の係数
	float lookDeffSpeed;//カメラ注視点の差分の係数
	float rotDeffSpeed;//カメラ回転の差分の係数
	float distance;//カメラと注視点の距離
	float lookObjDist;//注視点からずらす距離
	bool cameraCollision;//当たり判定モード化
	bool cameraLookObject;//追尾するカメラか
	bool cameraRotMode;//追尾して回転するか
	bool cameraUpdateMode;//カメラをアップデートするか
	int cnt;//カメラのタイマ
	unsigned int handleKey;//追尾対象のハンドルID
	OBJTYPE objType;//追尾するオブジェクトの種類
	void setCameraPos(D3DXVECTOR3 pos){ cameraP = cameraPDef = pos; }
	void setCameraLook(D3DXVECTOR3 look){ cameraR = cameraRDef = look; }
	void setCameraRot(D3DXVECTOR3 camrot){ rot = rotDef = camrot; }
}CAMERA_DATA;
class CameraManager
{
	//メンバ関数
	//コンストラクタ
	public:
	/*! CameraManager　コンストラクタ*/
	CameraManager(void);
	//デストラクタ
	public:
	/*! ~CameraManager　デストラクタ*/
	virtual ~CameraManager(void);

	//操作

	/*! init カメラの初期化
	*/
	void init(void);
	/*! update カメラの更新*/
	void update(void);
	/*! update カメラのセット*/
	void setCamera(void);
	//属性
	public:
	static void setLookObjHundle(unsigned hundle,OBJTYPE objtype){ cameraLookObjhudle_m = hundle;cameraObjtype_m = objtype; }
	CAMERA_DATA* getCameraData(void){ return &cameraData_m; }

	//メンバ変数
	private:
	LPDIRECT3DDEVICE9	D3DDevice_m;/*!< デバイスのポインタ*/
	CAMERA_DATA cameraData_m;//カメラの情報
	static unsigned int cameraLookObjhudle_m;//カメラ捉えるオブジェのハンドル
	static OBJTYPE cameraObjtype_m;//オブジェのタイプ
};
#endif

//End Of FIle