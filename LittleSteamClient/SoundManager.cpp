/******************************************************************************
@file       SoundManager.cpp
@brief      ゲームの音を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#include "SoundManager.h"
IXAudio2SourceVoice* SoundManager::sourceVoice_m[SOUND_LABEL_MAX];
BYTE* SoundManager::dataAudio_m[SOUND_LABEL_MAX];
DWORD SoundManager::sizeAudio_m[SOUND_LABEL_MAX];
// 各音素材のパラメータ
SoundManager::PARAM SoundManager::fileName[SOUND_LABEL_MAX] =
{
 {"data/SE/button000.wav",false},		// BGM0
 {"data/SE/steam.wav",true},		// BGM0
 {"data/SE/exolosion.wav",false},		// BGM0
 {"data/BGM/game.wav",true},		// BGM0
 {"data/BGM/gameover.wav",true},		// BGM0
 {"data/BGM/title.wav",true},		// BGM0
};
//コンストラクタ
SoundManager::SoundManager(void)
{
	xAudio2_m = NULL;
	masteringVoice_m = NULL;
}
//デストラクタ
SoundManager::~SoundManager(void)
{
	uninit();
}

//=============================================================================
// 初期化
//=============================================================================
HRESULT SoundManager::init(HWND hWnd)
{

	HRESULT hr;

	// COMライブラリの初期化
	CoInitializeEx(NULL,COINIT_MULTITHREADED);

	// XAudio2オブジェクトの作成
	hr = XAudio2Create(&xAudio2_m,0);
	if(FAILED(hr))
	{
		MessageBox(hWnd,"XAudio2オブジェクトの作成に失敗！","警告！",MB_ICONWARNING);

		// COMライブラリの終了処理
		CoUninitialize();
		return E_FAIL;
	}

	// マスターボイスの生成
	hr = xAudio2_m->CreateMasteringVoice(&masteringVoice_m);
	if(FAILED(hr))
	{
		MessageBox(hWnd,"マスターボイスの生成に失敗！","警告！",MB_ICONWARNING);

		if(xAudio2_m)
		{
			// XAudio2オブジェクトの開放
			xAudio2_m->Release();
			xAudio2_m = NULL;
		}

		// COMライブラリの終了処理
		CoUninitialize();

		return E_FAIL;
	}

	// サウンドデータの初期化
	for(int cntSound = 0; cntSound < SOUND_LABEL_MAX; cntSound++)
	{
		HANDLE hFile;
		DWORD dwChunkSize = 0;
		DWORD dwChunkPosition = 0;
		DWORD dwFiletype;
		WAVEFORMATEXTENSIBLE wfx;
		XAUDIO2_BUFFER buffer;

		// バッファのクリア
		memset(&wfx,0,sizeof(WAVEFORMATEXTENSIBLE));
		memset(&buffer,0,sizeof(XAUDIO2_BUFFER));

		// サウンドデータファイルの生成
		hFile = CreateFile(fileName[cntSound].fileName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL);
		if(hFile == INVALID_HANDLE_VALUE)
		{
			MessageBox(hWnd,"サウンドデータファイルの生成に失敗！(1)","警告！",MB_ICONWARNING);
			return HRESULT_FROM_WIN32(GetLastError());
		}
		if(SetFilePointer(hFile,0,NULL,FILE_BEGIN) == INVALID_SET_FILE_POINTER)
		{// ファイルポインタを先頭に移動
			MessageBox(hWnd,"サウンドデータファイルの生成に失敗！(2)","警告！",MB_ICONWARNING);
			return HRESULT_FROM_WIN32(GetLastError());
		}

		// WAVEファイルのチェック
		hr = checkChunk(hFile,'FFIR',&dwChunkSize,&dwChunkPosition);
		if(FAILED(hr))
		{
			MessageBox(hWnd,"WAVEファイルのチェックに失敗！(1)","警告！",MB_ICONWARNING);
			return S_FALSE;
		}
		hr = readChunkData(hFile,&dwFiletype,sizeof(DWORD),dwChunkPosition);
		if(FAILED(hr))
		{
			MessageBox(hWnd,"WAVEファイルのチェックに失敗！(2)","警告！",MB_ICONWARNING);
			return S_FALSE;
		}
		if(dwFiletype != 'EVAW')
		{
			MessageBox(hWnd,"WAVEファイルのチェックに失敗！(3)","警告！",MB_ICONWARNING);
			return S_FALSE;
		}

		// フォーマットチェック
		hr = checkChunk(hFile,' tmf',&dwChunkSize,&dwChunkPosition);
		if(FAILED(hr))
		{
			MessageBox(hWnd,"フォーマットチェックに失敗！(1)","警告！",MB_ICONWARNING);
			return S_FALSE;
		}
		hr = readChunkData(hFile,&wfx,dwChunkSize,dwChunkPosition);
		if(FAILED(hr))
		{
			MessageBox(hWnd,"フォーマットチェックに失敗！(2)","警告！",MB_ICONWARNING);
			return S_FALSE;
		}

		// オーディオデータ読み込み
		hr = checkChunk(hFile,'atad',&sizeAudio_m[cntSound],&dwChunkPosition);
		if(FAILED(hr))
		{
			MessageBox(hWnd,"オーディオデータ読み込みに失敗！(1)","警告！",MB_ICONWARNING);
			return S_FALSE;
		}
		dataAudio_m[cntSound] = (BYTE*)malloc(sizeAudio_m[cntSound]);
		hr = readChunkData(hFile,dataAudio_m[cntSound],sizeAudio_m[cntSound],dwChunkPosition);
		if(FAILED(hr))
		{
			MessageBox(hWnd,"オーディオデータ読み込みに失敗！(2)","警告！",MB_ICONWARNING);
			return S_FALSE;
		}

		// ソースボイスの生成
		hr = xAudio2_m->CreateSourceVoice(&sourceVoice_m[cntSound],&( wfx.Format ));
		if(FAILED(hr))
		{
			MessageBox(hWnd,"ソースボイスの生成に失敗！","警告！",MB_ICONWARNING);
			return S_FALSE;
		}

		memset(&buffer,0,sizeof(XAUDIO2_BUFFER));
		buffer.AudioBytes = sizeAudio_m[cntSound];
		buffer.pAudioData = dataAudio_m[cntSound];
		buffer.Flags = XAUDIO2_END_OF_STREAM;
		buffer.LoopCount = 0;
  if(fileName[cntSound].loop)
  {
   buffer.LoopCount = XAUDIO2_LOOP_INFINITE;

  }
		// オーディオバッファの登録
		sourceVoice_m[cntSound]->SubmitSourceBuffer(&buffer);
	}

	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void SoundManager::uninit(void)
{
	// 一時停止
	for(int cntSound = 0; cntSound < SOUND_LABEL_MAX; cntSound++)
	{
		if(sourceVoice_m[cntSound])
		{
			// 一時停止
			sourceVoice_m[cntSound]->Stop(0);

			// ソースボイスの破棄
			sourceVoice_m[cntSound]->DestroyVoice();
			sourceVoice_m[cntSound] = NULL;

			// オーディオデータの開放
			free(dataAudio_m[cntSound]);
			dataAudio_m[cntSound] = NULL;
		}
	}
	if(masteringVoice_m)
	{
		// マスターボイスの破棄
		masteringVoice_m->DestroyVoice();
		masteringVoice_m = NULL;
	}
	SAFE_RELEASE(xAudio2_m);

	// COMライブラリの終了処理
	CoUninitialize();
}

//=============================================================================
// セグメント再生(停止)
//=============================================================================
HRESULT SoundManager::playSound(SOUND_LABEL label)
{
	XAUDIO2_VOICE_STATE xa2state;
	XAUDIO2_BUFFER buffer;

	memset(&buffer,0,sizeof(XAUDIO2_BUFFER));
	buffer.AudioBytes = sizeAudio_m[label];
	buffer.pAudioData = dataAudio_m[label];
	buffer.Flags = XAUDIO2_END_OF_STREAM;
	buffer.LoopCount = 0;
 if(fileName[label].loop)
 {
  buffer.LoopCount = XAUDIO2_LOOP_INFINITE;

 }
	// 状態取得
	sourceVoice_m[label]->GetState(&xa2state);
	if(xa2state.BuffersQueued != 0)
	{// 再生中
		// 一時停止
		sourceVoice_m[label]->Stop(0);

		// オーディオバッファの削除
		sourceVoice_m[label]->FlushSourceBuffers();
	}

	// オーディオバッファの登録
	sourceVoice_m[label]->SubmitSourceBuffer(&buffer);

	// 再生
	sourceVoice_m[label]->Start(0);

	return S_OK;
}

//=============================================================================
// セグメント停止
//=============================================================================
void SoundManager::stopSound(SOUND_LABEL label)
{
	XAUDIO2_VOICE_STATE xa2state;

	// 状態取得
	sourceVoice_m[label]->GetState(&xa2state);
	if(xa2state.BuffersQueued != 0)
	{// 再生中
		// 一時停止
		sourceVoice_m[label]->Stop(0);

		// オーディオバッファの削除
		sourceVoice_m[label]->FlushSourceBuffers();
	}
}

//=============================================================================
// セグメント停止
//=============================================================================
void SoundManager::stopSound(void)
{
	// 一時停止
	for(int cntSound = 0; cntSound < SOUND_LABEL_MAX; cntSound++)
	{
		if(sourceVoice_m[cntSound])
		{
			// 一時停止
			sourceVoice_m[cntSound]->Stop(0);
		}
	}
}

//=============================================================================
// チャンクのチェック
//=============================================================================
HRESULT SoundManager::checkChunk(HANDLE hFile,DWORD format,DWORD *pChunkSize,DWORD *pChunkDataPosition)
{
	HRESULT hr = S_OK;
	DWORD dwRead;
	DWORD dwChunkType;
	DWORD dwChunkDataSize;
	DWORD dwRIFFDataSize = 0;
	DWORD dwFileType;
	DWORD dwBytesRead = 0;
	DWORD dwOffset = 0;

	if(SetFilePointer(hFile,0,NULL,FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{// ファイルポインタを先頭に移動
		return HRESULT_FROM_WIN32(GetLastError());
	}

	while(hr == S_OK)
	{
		if(ReadFile(hFile,&dwChunkType,sizeof(DWORD),&dwRead,NULL) == 0)
		{// チャンクの読み込み
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		if(ReadFile(hFile,&dwChunkDataSize,sizeof(DWORD),&dwRead,NULL) == 0)
		{// チャンクデータの読み込み
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		switch(dwChunkType)
		{
			case 'FFIR':
			dwRIFFDataSize = dwChunkDataSize;
			dwChunkDataSize = 4;
			if(ReadFile(hFile,&dwFileType,sizeof(DWORD),&dwRead,NULL) == 0)
			{// ファイルタイプの読み込み
				hr = HRESULT_FROM_WIN32(GetLastError());
			}
			break;

			default:
			if(SetFilePointer(hFile,dwChunkDataSize,NULL,FILE_CURRENT) == INVALID_SET_FILE_POINTER)
			{// ファイルポインタをチャンクデータ分移動
				return HRESULT_FROM_WIN32(GetLastError());
			}
		}

		dwOffset += sizeof(DWORD) * 2;
		if(dwChunkType == format)
		{
			*pChunkSize = dwChunkDataSize;
			*pChunkDataPosition = dwOffset;

			return S_OK;
		}

		dwOffset += dwChunkDataSize;
		if(dwBytesRead >= dwRIFFDataSize)
		{
			return S_FALSE;
		}
	}

	return S_OK;
}

//=============================================================================
// チャンクデータの読み込み
//=============================================================================
HRESULT SoundManager::readChunkData(HANDLE hFile,void *pBuffer,DWORD dwBuffersize,DWORD dwBufferoffset)
{
	DWORD dwRead;

	if(SetFilePointer(hFile,dwBufferoffset,NULL,FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{// ファイルポインタを指定位置まで移動
		return HRESULT_FROM_WIN32(GetLastError());
	}

	if(ReadFile(hFile,pBuffer,dwBuffersize,&dwRead,NULL) == 0)
	{// データの読み込み
		return HRESULT_FROM_WIN32(GetLastError());
	}

	return S_OK;
}

