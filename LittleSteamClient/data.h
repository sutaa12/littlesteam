#pragma once


typedef enum{
	DATA_EVENT_TYPE_NONE = 0,
 DATA_EVENT_TYPE_RECIEVE,
 DATA_EVENT_TYPE_CONNECT,
 DATA_EVENT_TYPE_START,
	DATA_EVENT_TYPE_END,
	DATA_EVENT_TYPE_DISCONECT,
	DATA_EVENT_TYPE_MAX
}DATA_EVENTS;

struct DATA_EVENT
{
	short Type;
};


struct DATA_POSITION
{
	float x, y, z;
};


struct DATA_ROTATION
{
	float x, y, z;
};

typedef enum{
	DATA_TYPE_NONE = 0,
	DATA_TYPE_EVENT,
	DATA_TYPE_POSITION,
	DATA_TYPE_ROTATION,
	DATA_TYPE_LIFE,
	DATA_TYPE_MANA,
	DATA_TYPE_TIME,
	DATA_TYPE_MAX
}DATA_TYPE;

struct DATA
{
	int		ID;
 int		Type;

	union
	{
		DATA_EVENT		Event;
		DATA_POSITION	Position;
		DATA_ROTATION	Rotation;
		int Life;
		int Mana;
		int Time;
	};
};
