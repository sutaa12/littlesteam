//=============================================================================
//
// シーンの規定クラス [Scene.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef SCENE_FILE
#define SCENE_FILE
#include "common.h"
#include "InputKeyboard.h"
#include "InputMouse.h"
#include "SoundManager.h"

class Scene
{
	public:
	Scene(void){};//コンストラクタ
	virtual ~Scene(void){};//デストラクタ
	virtual HRESULT Init(void) = 0;//初期化
	virtual void Uninit(void) = 0;//解放
	virtual void Update(void) = 0;//更新
#ifdef _DEBUG
#endif
	private:

};

#endif

//End Of FIle