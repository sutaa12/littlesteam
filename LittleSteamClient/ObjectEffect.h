/******************************************************************************/
/*! @addtogroup Object_ObjectEffect
@file       Object.h
@brief      ObjectEffectクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef OBJECTEFFECT_FILE
#define OBJECTEFFECT_FILE
#include "main.h"
#include "Object3DBillboard.h"
typedef enum{
	EFFECT_EXPLOSION = 0,
 EFFECT_WATER,
	EFFECT_TYPE_MAX
}EFFECT_TYPE;

class ObjectEffect: public Object3DBillboard
{
	//コンストラクタ
	public:
	/*! ObjectEffect ObjectEffectのコンストラクタ
	@param[in]     type    3DBillboardオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectEffect(EFFECT_TYPE effectType,float rot,PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_3DBILLBOARD,OBJECT3D_TYPE type = OBJCT3D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectEffect ObjectEffectのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectEffect(void);
	//操作
	public:
	/*! Create ObjectEffectの作成
	@param[in]     effectType    弾の名前定義から設定(EFFECT_TYPE)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static ObjectEffect* Create(EFFECT_TYPE effectType,D3DXVECTOR3 position,float rot,OBJECT3D_TYPE type = OBJCT3D_EFFECT,PRIORITY_MODE priority = PRIORITY_7);
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	EFFECT_TYPE effectType_m;
	float sclSpeed_m;
	float rotY_m;//進行する角度
	int cnt_m;
	int effecthundle_m;
};
#endif

//End Of File
