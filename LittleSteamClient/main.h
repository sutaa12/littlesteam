/******************************************************************************/
/*! @addtogroup Common_Main
@file       Main.h
@brief      最初に読み込むデータ
@date       作成日(2014/08/19)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef MAIN_FILE
#define MAIN_FILE
#include "Common.h"
#define CLASS_NAME		"AppClass"			/*!< ウインドウのクラス名*/
#define WINDOW_NAME		"LittleSteam v0.1"	/*!< ウインドウのキャプション名*/

#endif

//End Of FIle