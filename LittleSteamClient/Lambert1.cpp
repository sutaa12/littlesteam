
//=============================================================================
//
// 光処理 [Lambert1.cpp]
// Author : NARITADA SUZUK
//
//=============================================================================
#include "Lambert1.h"
//****************************************************************
//コンストラクタ
//****************************************************************
LAMBERT1::LAMBERT1( LPDIRECT3DDEVICE9 pd3dDevice )
{
   m_pd3dDevice = pd3dDevice;
   m_pEffect = NULL;
}

//****************************************************************
//デストラクタ
//****************************************************************
LAMBERT1::~LAMBERT1()
{
   //SafeReleaseは関数ではなくマクロ
   //#define SafeRelease(x) { if(x) { (x)->Release(); (x)=NULL; } }
   SAFE_RELEASE( m_pEffect );
}

void LAMBERT1::Invalidate()
{
   if( m_pEffect )
      m_pEffect->OnLostDevice();
}

void LAMBERT1::Restore()
{
   if( m_pEffect )
      m_pEffect->OnResetDevice();
}

//****************************************************************
//初期化を行うメンバ関数
//****************************************************************
HRESULT LAMBERT1::Load()
{
   HRESULT hr;
   D3DCAPS9 caps;

   //ハードウェアがサポートするバーテックスシェーダーとピクセルシェーダーのバージョンをチェックする
   m_pd3dDevice->GetDeviceCaps( &caps );
   if( caps.VertexShaderVersion >= D3DVS_VERSION( 1, 1 ) && caps.PixelShaderVersion >= D3DPS_VERSION( 1, 1 ) )
   {
      LPD3DXBUFFER pErr = NULL;

      //fxファイルをロードし、シェーダーの準備を行う。他にobjファイルをロードしたり、ヘッダファイルをインクルードする方法もある。
      //隠蔽性を考慮するとobjファイルやヘッダファイルを使用する方法がよりベター。
      hr = D3DXCreateEffectFromFile( m_pd3dDevice, ("./data/SHADER/Lambert1.fx"), NULL, NULL, 0, NULL, &m_pEffect, &pErr );
      if( SUCCEEDED( hr ) )
      {
         //fxファイル内で宣言している変数のハンドルを取得する
         m_pTechnique = m_pEffect->GetTechniqueByName( "TShader" );
         m_pWVP = m_pEffect->GetParameterByName( NULL, "m_WVP" );
         m_pLightDir = m_pEffect->GetParameterByName( NULL, "m_LightDir" );
         m_pAmbient = m_pEffect->GetParameterByName( NULL, "m_Ambient" );
         
         m_pEffect->SetTechnique( m_pTechnique );
      }

      else
         return -1;
   }

   else
      return -2;

   return S_OK;
}

//****************************************************************
//シェーダー処理を開始する
//****************************************************************
void LAMBERT1::Begin()
{
   if( m_pEffect )
   {
      m_pd3dDevice->GetTransform( D3DTS_VIEW, &m_matView );
      m_pd3dDevice->GetTransform( D3DTS_PROJECTION, &m_matProj );

      m_pEffect->Begin( NULL, 0 );
   }
}

//****************************************************************
//パス０を開始する
//****************************************************************
void LAMBERT1::BeginPass()
{
   if( m_pEffect )
   {
      m_pEffect->BeginPass( 0 );
   }
}

//****************************************************************
//環境光を設定する。
//****************************************************************
void LAMBERT1::SetAmbient( float Ambient )
{
   //シェーダーが使用できるとき
   if( m_pEffect )
   {
      D3DXVECTOR4 A;
      A = D3DXVECTOR4( Ambient, Ambient, Ambient, 1.0f );
      m_pEffect->SetVector( m_pAmbient, &A );
   }

   //シェーダーが使用できないときは、固定機能パイプラインのマテリアルを設定する。(注意３)
   else
   {
      D3DMATERIAL9 old_material;
      m_pd3dDevice->GetMaterial( &old_material );
      old_material.Ambient.r = Ambient;
      old_material.Ambient.g = Ambient;
      old_material.Ambient.b = Ambient;
      old_material.Ambient.a = 1.0f;
      m_pd3dDevice->SetMaterial( &old_material );
   }
}

//****************************************************************
//環境光を設定する。
//****************************************************************
void LAMBERT1::SetAmbient( D3DXVECTOR4* pAmbient )
{
   if( m_pEffect )
      m_pEffect->SetVector( m_pAmbient, pAmbient );

   //シェーダーが使用できないときは、固定機能パイプラインのマテリアルを設定する
   else
   {
      D3DMATERIAL9 old_material;
      m_pd3dDevice->GetMaterial( &old_material );
      old_material.Ambient.r = pAmbient->x;
      old_material.Ambient.g = pAmbient->y;
      old_material.Ambient.b = pAmbient->z;
      old_material.Ambient.a = pAmbient->w;
      m_pd3dDevice->SetMaterial( &old_material );
   }
}

//****************************************************************
//ワールド座標系の行列変換マトリックスを設定する。
//****************************************************************
void LAMBERT1::SetMatrix( D3DXMATRIX* pMatWorld, D3DXVECTOR4* pLightDir )
{
   if( m_pEffect )
   {
      D3DXMATRIX m, m1;
      D3DXVECTOR4 LightDir;
      D3DXVECTOR4 v;

      //ワールド × ビュー × 射影
      m = (*pMatWorld) * m_matView * m_matProj;
      m_pEffect->SetMatrix( m_pWVP, &m );

      //平行光源の方向ベクトルを設定する
      LightDir = *pLightDir;
      D3DXMatrixInverse( &m1, NULL, pMatWorld );
      D3DXVec4Transform( &v, &LightDir, &m1 );      
      //正規化する
      D3DXVec3Normalize( (D3DXVECTOR3*)&v, (D3DXVECTOR3*)&v );      
      m_pEffect->SetVector( m_pLightDir, &v );
   }

   //シェーダーが使用できないときは、固定機能パイプラインのマトリックスを設定する
   else
      m_pd3dDevice->SetTransform( D3DTS_WORLD, pMatWorld );
}

//****************************************************************
//パスが開始されているときに発生したステートの変更を、レンダリング前にデバイスに通知する
//****************************************************************
void LAMBERT1::CommitChanges()
{
   if( m_pEffect )
      m_pEffect->CommitChanges();
} 

//****************************************************************
//パスを終了する
//****************************************************************
void LAMBERT1::EndPass()
{
   if( m_pEffect )
   {
      m_pEffect->EndPass();
   }
}

//****************************************************************
//シェーダー処理を終了する
//****************************************************************
void LAMBERT1::End()
{
   if( m_pEffect )
   {
      m_pEffect->End();
   }
}

//****************************************************************
//シェーダーが使用できるか
//****************************************************************
BOOL LAMBERT1::IsOK()
{
   if( m_pEffect )
      return TRUE;

   return FALSE;
}
//End Of File