/******************************************************************************/
/*! @addtogroup Input_InputPad
@file       InputPad.h
@brief      ゲームパッド制御クラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef PADINPUT_FILE
#define PADINPUT_FILE
#include "Common.h"
#include "InputManager.h"
class InputPad: public Input
{
	public:
	InputPad(void);//コンストラクタ
	~InputPad(void);//デストラクタ
	HRESULT init(HINSTANCE hInstance,HWND hWnd);//初期化
	void uninit(void);//解放
	void update(void);//更新
	static bool getPadPress(int nKey);
	static bool getPadTrigger(int nKey);
	static bool getPadRepeat(int nKey);
	static bool getPadRelease(int nKey);

	private:
	static const unsigned short MAX_KEY = 256;
	static BYTE akeyState_m[MAX_KEY];//ゲームパッドの状態格納
	static BYTE keyTrigger_m[MAX_KEY];
	static BYTE keyRelease_m[MAX_KEY];
	static BYTE keyRepeat_m[MAX_KEY];
	int repeatCnt_m[MAX_KEY];//リピートのカウント
};
#endif