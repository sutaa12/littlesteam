/******************************************************************************/
/*! @addtogroup Scene_SceneManager
@file       SceneManager.h
@brief      ゲームの各シーンを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef SCENEMANAGER_FILE
#define SCENEMANAGER_FILE
#include "main.h"
#include "SystemManager.h"
#include "ObjectFeed.h"
//レンダークラス
class Scene;

enum SCENE_MODE
{
	SCENE_NONE = 0,
	SCENE_TITLE,
	SCENE_GAME,
	SCENE_RESULT,
	SCENE_RANKING,
	SCENE_MAX
};

class SceneManager
{
	public:
	SceneManager(void);//コンストラクタ
	~SceneManager(void);//デストラクタ
	HRESULT Init(void);//初期化
	void Uninit(void);//解放
	void Update(void);//更新

	static void SetScene(SCENE_MODE NextMode){ m_NextMod = NextMode; }

#ifdef _DEBUG
#endif

	private:
	//メンバ変数

	SCENE_MODE m_CurMode;//現在のモード
	static SCENE_MODE m_NextMod;//次のモード
	Scene* m_pPhase;//フェーズの基底クラス所持

};

#endif

//End Of FIle