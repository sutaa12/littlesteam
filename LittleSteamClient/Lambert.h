//=============================================================================
//
// アニメーション処理処理 [Lambert.h]
// Author : NARITADA SUZUKI
//
//=============================================================================
#ifndef _LAMBERT3_H_
#define _LAMBERT3_H_
#include "Common.h"
class LAMBERT3
{
private:
   LPD3DXEFFECT m_pEffect;
//   D3DXHANDLE m_pTechnique, m_pWVP, m_pWVPT;
   D3DXHANDLE m_pTechnique, m_pWVP;
   LPDIRECT3DDEVICE9 m_pd3dDevice;

public:
   LAMBERT3( LPDIRECT3DDEVICE9 pd3dDevice );
   ~LAMBERT3();
   void Invalidate();
   void Restore();
   HRESULT Load();
   void Begin();
   void BeginPass( UINT Pass );
   void SetMatrix( D3DXMATRIX* pMatWVP );
   void CommitChanges();
   void EndPass();
   void End();
   BOOL IsOK();
   LPD3DXEFFECT GetEffect(){ return m_pEffect; };
};

#endif
//End Of File
