/******************************************************************************/
/*! @addtogroup Object_EnemyManager
@file       EnemyManager.h
@brief      ゲームの弾を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef ENEMYMANAGER_FILE
#define ENEMYMANAGER_FILE
#include "Common.h"
#include "main.h"
#include "XFileManager.h"
#include "ObjectEnemy.h"
class ObjectEnemy;
//敵管理クラス
class EnemyManager
{
	public:
	EnemyManager(void);//コンストラクタ
	~EnemyManager(void);//デストラクタ
	HRESULT init(void);//初期化
	void update(void);//更新
	static int addEnemyList(ENEMY_TYPE type,ObjectEnemy* bulletPointer);
	static void removeEnemyList(ENEMY_TYPE type,int num);
	static bool hitchk(COLLISION_DATA* obj1,ENEMY_TYPE type);
	static void setEnemyUpdate(bool update){ update_m = update; }
	static void setEnemy(void);
	static int getEnemy(void){ return enemyNum_m; }
	static ObjectEnemy* getEnemyObject(ENEMY_TYPE type,int enemyID);
	static ObjectEnemy* getEnemyObject(void);
	private:
	//メンバ変数
	static const unsigned int MAX_ENEMY = 3;
	static ObjectEnemy* enemyList_m[ENEMY_TYPE_MAX][MAX_ENEMY];//リスト
	static bool update_m;
	XFILEANIM_DATA* xfileData_m[ENEMY_TYPE_MAX];//ロード済みxファイルのデータのメンバ
	static int cnt_m[ENEMY_TYPE_MAX][MAX_ENEMY];
	static int enemyNum_m;
	static int enemyID_m[ENEMY_TYPE_MAX][MAX_ENEMY];
};

#endif

//End Of FIle