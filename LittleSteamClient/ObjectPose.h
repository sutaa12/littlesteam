//=============================================================================
//
// ポーズ管理処理 [ObjectPose.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef POSE_FILE
#define POSE_FILE
#include "main.h"
#include "Object2D.h"
enum POSE_BUTTOM_MODE
{
	POSE_RETURN,
	POSE_RETRY,
	POSE_TITLE,
	POSE_RESULT,
	POSE_MAX
};
//ポーズクラス
class ObjectPose: public Object2D
{
	public:
	ObjectPose(PRIORITY_MODE nPriority = PRIORITY_POSE,OBJECT2D_TYPE objType = OBJCT2D_NORMAL);//コンストラクタ
	~ObjectPose(void);//デストラクタ

	static ObjectPose* Create();//作成時設定

	void init(void);//初期化
	void uninit(void);//解放
	void update(void);//更新
	int getPoseNum(void){ return m_PoseButton; }
	void setPose(bool pose){ m_bPose = pose; }
	bool getPose(void){ return m_bPose; }
#ifdef _DEBUG
#endif

	private:
	//メンバ変数
	Object2D* m_Button[POSE_MAX];
 Object2D* tutolial_m;
	bool m_bPose;
	unsigned int m_PoseButton;
	float m_fAlfa;//ボタンのアルファ値
	float m_fAlfaSpeed;//アルファスピード
};

#endif

//End Of FIle