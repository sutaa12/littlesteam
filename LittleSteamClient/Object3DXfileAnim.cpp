/******************************************************************************
@file       Object3DXfileAnim.cpp
@brief      Object3DXfileAnimの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "Object3DXfileAnim.h"
//================================================================================
//タイプ別作成
//================================================================================
Object3DXfileAnim* Object3DXfileAnim::Create(XFILEANIM_LIST xfileName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	Object3DXfileAnim *objectPointer;

	objectPointer = new Object3DXfileAnim(priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setSize(size);
		objectPointer->setXfile(xfileName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
Object3DXfileAnim::Object3DXfileAnim(PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3D(priority,objType)
{
	init();
}
//==============================================================================
//デストラクタ
Object3DXfileAnim::~Object3DXfileAnim(void)
{

}
//==============================================================================
//初期化
void Object3DXfileAnim::init(void)
{

}
//==============================================================================
//解放
void Object3DXfileAnim::uninit(void)
{
	Object3D::uninit();
}
//==============================================================================
//更新
void Object3DXfileAnim::update(void)
{
	Object3D::update();
}

//End Of FIle