
/******************************************************************************
@file       InputKeyboard.cpp
@brief      キーボードを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//==============================================================================
#include"InputKeyboard.h"

//==============================================================================
//初期化
//==============================================================================
BYTE InputKeyboard::akeyState_m[MAX_KEY];//キーボードの状態格納
BYTE InputKeyboard::keyTrigger_m[MAX_KEY];
BYTE InputKeyboard::keyRelease_m[MAX_KEY];
BYTE InputKeyboard::keyRepeat_m[MAX_KEY];

//============================================================================
// キーボードのコンストラクタ
//=============================================================================
InputKeyboard::InputKeyboard()
{

}
//============================================================================
// キーボードデストラクタ
//=============================================================================
InputKeyboard::~InputKeyboard()
{
	uninit();
}

//============================================================================
// キーボードの初期化
//=============================================================================
HRESULT InputKeyboard::init(HINSTANCE hInstance,HWND hWnd)
{
	HRESULT hr;
	if(dInput_m == NULL)//本体ができてない場合
	{
		// DirectInputオブジェクトの作成
		hr = DirectInput8Create(hInstance,
								DIRECTINPUT_VERSION,
								IID_IDirectInput8,
								(void**)&dInput_m,
								NULL);
		if(FAILED(hr))
		{
			//メッセージボックス表示
			MessageBox(NULL,"作成失敗","DI作成失敗",MB_OK);
			return hr;
		}
	}
	// デバイスオブジェクトを作成
	hr = dInput_m->CreateDevice(GUID_SysKeyboard,
								&dirDevice_m,
								NULL);
	if(FAILED(hr))
	{
		//メッセージボックス表示
		MessageBox(NULL,"作成失敗","CD作成失敗",MB_OK);
		return hr;
	}


	// データフォーマットを設定
	hr = dirDevice_m->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(hr))
	{
		//メッセージボックス表示
		MessageBox(NULL,"作成失敗","SDF作成失敗",MB_OK);
		return hr;
	}


	// 協調モードを設定（フォアグラウンド＆非排他モード）
	hr = dirDevice_m->SetCooperativeLevel(hWnd,
										   ( DISCL_FOREGROUND | DISCL_NONEXCLUSIVE ));
	if(FAILED(hr))
	{
		//メッセージボックス表示
		MessageBox(NULL,"作成失敗","SCL作成失敗",MB_OK);
		return hr;
	}

	for(int cnt = 0;cnt<MAX_KEY;cnt++)
	{
		repeatCnt_m[cnt] = 0;
	}

	// キーボードへのアクセス権を獲得(入力制御開始)

	dirDevice_m->Acquire();

	return S_OK;
}

//=============================================================================
// キーボードの終了処理
//=============================================================================
void InputKeyboard::uninit(void)
{

}

//=============================================================================
// キーボードの更新処理
//=============================================================================
void InputKeyboard::update(void)
{
	BYTE akeyState[MAX_KEY];//キーボードの格納状態

	if(SUCCEEDED(dirDevice_m->GetDeviceState(sizeof(akeyState),&akeyState[0])))
	{
		for(int cnt = 0;cnt<MAX_KEY;cnt++)
		{

			keyRelease_m[cnt] = ( akeyState[cnt] ^ akeyState_m[cnt] )&akeyState_m[cnt];//リリース情報作成

			//リピート情報作成
			if(repeatCnt_m[cnt]< 70)//開始以下なら
			{
				keyRepeat_m[cnt] = keyTrigger_m[cnt];//トリガーに
				if(repeatCnt_m[cnt]>0)//開始以下なら
				{
					repeatCnt_m[cnt]++;
				}
			}
			else{//開始以上なら
				keyRepeat_m[cnt] = akeyState[cnt];//トリガーに
			}


			keyTrigger_m[cnt] = ( akeyState[cnt] ^ akeyState_m[cnt] )&~akeyState_m[cnt];//トリガー情報作成
			akeyState_m[cnt] = akeyState[cnt];//キー情報更新
		}
	}
	else
	{
		dirDevice_m->Acquire();
	}
}

//=============================================================================
// キーボードのプレス状態を取得
//=============================================================================
bool InputKeyboard::getKeyboardPress(int nKey)
{
	return( akeyState_m[nKey] & 0x80 ) ? TRUE : FALSE;
}

//=============================================================================
// キーボードのトリガー状態を取得
//=============================================================================
bool InputKeyboard::getKeyboardTrigger(int nKey)
{

	return( keyTrigger_m[nKey] & 0x80 ) ? TRUE : FALSE;//トリガーの情報と一致するか
}

//=============================================================================
// キーボードのリピート状態を取得
//=============================================================================
bool InputKeyboard::getKeyboardRepeat(int nKey)
{
	return( keyRepeat_m[nKey] & 0x80 ) ? TRUE : FALSE;

}

//=============================================================================
// キーボードのリリ−ス状態を取得
//=============================================================================
bool InputKeyboard::getKeyboardRelease(int nKey)
{
	return( keyRelease_m[nKey] & 0x80 ) ? TRUE : FALSE;
}

//End Of File
