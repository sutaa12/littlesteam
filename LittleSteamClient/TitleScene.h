//=============================================================================
//
// タイトルシーン [TitleScene.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef TITILESCENE_FILE
#define TITILESCENE_FILE
#include "main.h"
#include "Scene.h"

class Object3DXfileAnim;
class Imp;
class TitleScene: public Scene
{
	public:
	TitleScene(void);//コンストラクタ
	~TitleScene(void);//デストラクタ
	HRESULT Init(void);//初期化
	void Uninit(void);//解放
	void Update(void);//更新
 bool getNetMode(void){return netMode_m;}
 void UpdateReplay(void);
	private:
	int timer_m;
	int timerSecond_m;
 Imp* imp_m;
 static bool netMode_m;
 bool tutolialMode_m;
};

#endif

//End Of FIle