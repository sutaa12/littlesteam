/******************************************************************************/
/*! @addtogroup Draw_DrawManager
@file       DrawManager.h
@brief      ゲームの描画を管理するクラス D3DDevice持ってる
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef DRAW_MANAGE_FILE
#define DRAW_MANAGE_FILE
#include "Common.h"
/*! @enum OBJTYPE
@brief オブジェクトの種類
*/
class Draw2D;
class Draw3D;
class Render;
class MessageProc;
class TextureManager;
class XFileManager;
class SOFTSHADOW1;
class BLURFILTER2;
class LAMBERT3;
class LAMBERT1;
class D3D2DSQUARE;
/*! @class DrawManager
@brief  描画管理クラス 各描画管理クラスの管理をする D3DDeviceを持ってる
*/
class DrawManager
{
	//メンバ関数
	//コンストラクタ
	public:
	/*! DrawManager　コンストラクタ*/
	DrawManager(void);
	//デストラクタ
	public:
	/*! ~DrawManager　デストラクタ*/
	virtual ~DrawManager(void);

	//操作
	/*! drawAll 全てのObject描画*/
	void drawAll(void);
	/*! init Drawデバイス用意
	@param[in]       hWnd       windowハンドル
	@param[in]       bWindow        windowサイズをどうするか
	*/
	HRESULT init(HWND hWnd,BOOL bWindow);

	void Invalidate(void);
	void Restore(void);
 void SetScreenShot(void);

	//属性
	public:
	static LPDIRECT3DDEVICE9 getD3DDEVICE(void){ return D3DDevice_m; }
	//メンバ変数
	private:

	static LPDIRECT3DDEVICE9	D3DDevice_m;/*!< デバイスのポインタ*/
	Render* render_m;/*!< 描画デバイスを持っている*/
	Draw2D* draw2D_m;/*!< 2D描画をする*/
	Draw3D* draw3D_m;/*!< 3D描画をする*/
	TextureManager* textureManager_m;/*< テクスチャーを割り当てる*/
	XFileManager* xFileManager_m;/*< Xfileを割り当てる*/
	MessageProc* messageProc_m;/*!< メッセージを表示する*/
	SOFTSHADOW1* soft_m;
	BLURFILTER2* blur_m;
	LAMBERT3* lambert3_m;
	LAMBERT1* lambert1_m;
	D3D2DSQUARE* sqrobj_m;
	D3DPRESENT_PARAMETERS m_d3dParm;
	//シャドーマップ
	LPDIRECT3DTEXTURE9    m_pZBufferTexture;
	LPDIRECT3DSURFACE9    m_pZBufferSurface;

	//Zバッファ
	LPDIRECT3DSURFACE9    m_pZBuffer;

	//影イメージ
	LPDIRECT3DTEXTURE9    m_pShadowTexture;
	LPDIRECT3DSURFACE9    m_pShadowSurface;

	//ぼかした影イメージ
	LPDIRECT3DTEXTURE9    m_pBlurTexture;
	LPDIRECT3DSURFACE9    m_pBlurSurface;
};
#endif

//End Of File