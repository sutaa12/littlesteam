/******************************************************************************/
/*! @addtogroup Collision_CollisionManager
@file       CollisionManager.h
@brief      ゲームの当たり判定を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef COLLISION_MANAGE_FILE
#define COLLISION_MANAGE_FILE
#include "Common.h"
#include "Object3D.h"
#include "Object3DFigure.h"

/*! @class CollisionManager
@brief  描画管理クラス 各描画管理クラスの管理をする D3DDeviceを持ってる
*/
class CollisionManager
{
	//メンバ関数
	//コンストラクタ
	public:
	/*! CollisionManager　コンストラクタ*/
	CollisionManager(void);
	//デストラクタ
	public:
	/*! ~CollisionManager　デストラクタ*/
	virtual ~CollisionManager(void);

	//操作
	/*! UpdateAll 全ての当たり判定チェック*/
	void UpdateAll(void);

	/*! HitchkCircle 円と円の当たり判定*/
	static bool HitchkCircle(COLLISION_DATA* obj1,COLLISION_DATA* obj2);

	/*! HitchkBoard 壁と点の外積の当たり判定*/
	static void HitchkBoard(COLLISION_DATA* obj1,COLLISION_DATA* obj2);

	/*! HitchkField 起伏の当たり判定当たり判定*/
	static float HitchkField(D3DXVECTOR3 pos,D3DXVECTOR3* pNormal);

	/*! Hitchk 指定したものと当たり判定(相手側は入力がなければ全ての3Dオブジェクトを参照)*/
	void Hitchk(OBJECT3D_TYPE objTypeA,OBJECT3D_TYPE objTypeB);

	static void setFiledMesh(unsigned int hundle){ fieldHundle_m = hundle; }
	//属性
	public:
	//メンバ変数
	private:
	static float getHeghitPolygon(D3DXVECTOR3& P0,D3DXVECTOR3& P1,D3DXVECTOR3& P2,D3DXVECTOR3& pos,D3DXVECTOR3* pNormal);
	OBJECT3D_TYPE objType_m;//指定してるオブジェクト
	static unsigned int fieldHundle_m;
};
#endif

//End Of File