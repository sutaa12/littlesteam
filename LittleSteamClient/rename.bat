@echo off
for %%f in ( * ) do call :sub "%%f"
exit /b

:sub
set fname=%1
set fname=%fname:C*=*%
ren %1 %fname%
pause