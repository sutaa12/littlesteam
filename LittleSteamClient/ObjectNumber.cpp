/******************************************************************************
@file       ObjectNumber.cpp
@brief      ObjectNumberの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectNumber.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectNumber* ObjectNumber::Create(TEXTURE_LIST texName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXCOLOR col,D3DXVECTOR3 rot,OBJECT2D_TYPE type,PRIORITY_MODE priority)
{
	ObjectNumber *objectPointer;

	objectPointer = new ObjectNumber(col,priority,type);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setColor(col);
		objectPointer->setSize(size);
		objectPointer->setTexName(texName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectNumber::ObjectNumber(D3DXCOLOR col,PRIORITY_MODE priority,OBJECT2D_TYPE type,OBJTYPE objType):Object2D(priority,objType,type)
{
	init();
}
//==============================================================================
//デストラクタ
ObjectNumber::~ObjectNumber(void)
{

}
//==============================================================================
//初期化
void ObjectNumber::init(void)
{
	number_m = 0;

}
//==============================================================================
//解放
void ObjectNumber::uninit(void)
{

	Object2D::uninit();
}
//==============================================================================
//更新
void ObjectNumber::update(void)
{
	D3DXVECTOR2* texPos;
	texPos = Object2D::getTexPos();
	for(int loop = 0;loop < 4;loop++)
	{
		texPos[0] = D3DXVECTOR2((float)number_m / 10.0f,0.0f);
		texPos[1] = D3DXVECTOR2((float)( number_m / 10.0f + 0.1 ),0.0f);
		texPos[2] = D3DXVECTOR2((float)number_m / 10.0f,1.0f);
		texPos[3] = D3DXVECTOR2((float)number_m / 10.0f + 0.1f,1.0f);
	}
	Object2D::update();

}

//End Of FIle