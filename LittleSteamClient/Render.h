
/******************************************************************************/
/*! @addtogroup Draw_Render
@file       Object.h
@brief      描画デバイス作製クラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef RENDER_FILE
#define RENDER_FILE
#include "main.h"
#include "Common.h"

/*! @class Render
@brief  描画用デバイスを用意するクラス D3DDeviceの設定をする
*/
class CMessageProc;
class Render
{
	public:
	Render(void);//コンストラクタ
	~Render(void);//デストラクタ
	HRESULT Init(HWND hWnd,BOOL bWindow);//初期化
	void Uninit(void);//解放
	LPDIRECT3DDEVICE9 getD3DDevice(void){ return D3DDevice_m; }
#ifdef _DEBUG
	void DrawFPS(void);// FPS表示
#endif

	private:
	HRESULT InitPolygon(void);//頂点バッファ用意
	void SetVertexPolygon(void);//頂点バッファセット

	//メンバ変数
	LPDIRECT3D9	D3D_m;	//Direct3Dインタフェースのポインタ
	LPDIRECT3DDEVICE9	D3DDevice_m;//デバイスのポインタ

};

#endif

//End Of FIle