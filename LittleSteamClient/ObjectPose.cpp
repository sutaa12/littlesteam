/******************************************************************************
@file       ObjectPose.cpp
@brief      ObjectPoseの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectPose.h"
#include "SystemManager.h"
#include "InputKeyboard.h"
#include "ObjectFeed.h"
#include "Object.h"
#include "SceneManager.h"
#include "TextureManager.h"
#include "SoundManager.h"
#define BUTTONSIZEW (210)
#define BUTTONSIZEH (40)
//================================================================================
//タイプ別作成
//================================================================================
ObjectPose* ObjectPose::Create()
{
	ObjectPose *pScene2D;
	pScene2D = new ObjectPose;
	if(pScene2D)
	{
		return pScene2D;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectPose::ObjectPose(PRIORITY_MODE nPriority,OBJECT2D_TYPE objType):Object2D(nPriority,OBJTYPE_2D,objType)
{
	init();
}
//==============================================================================
//デストラクタ
ObjectPose::~ObjectPose(void)
{
	uninit();
}
//==============================================================================
//初期化
void ObjectPose::init(void)
{
	setDrawFlag(false);
	const TEXTURE_LIST tex[POSE_MAX] = {
		TEXTURE_RETURN,
		TEXTURE_RETRY,
		TEXTURE_TITLE,
		TEXTURE_RESULT
	};
	Object2D* pScene;
	pScene = Object2D::Create(TEXTURE_NONE,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2,0),D3DXVECTOR3(SCREEN_WIDTH,SCREEN_HEIGHT,0),D3DXVECTOR3(0,0,0),OBJCT2D_NORMAL,PRIORITY_POSE);
	pScene->setColor(D3DXCOLOR(0,0,0,0.6f));

	pScene = Object2D::Create(TEXTURE_POSELOGO,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 5.0f,0),D3DXVECTOR3(BUTTONSIZEW * 2,BUTTONSIZEW * 2,0),D3DXVECTOR3(0,0,0),OBJCT2D_NORMAL,PRIORITY_POSE);

	for(int loop = 0;loop < POSE_MAX;loop++)
	{
  m_Button[loop] = Object2D::Create(tex[loop],D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2 - SCREEN_HEIGHT / 6 + ( loop * BUTTONSIZEH + 20 ),0),D3DXVECTOR3(BUTTONSIZEW,BUTTONSIZEH,0),D3DXVECTOR3(0,0,0),OBJCT2D_NORMAL,PRIORITY_POSE);

	}
 tutolial_m = Object2D::Create(TEXTURE_TUTOLIAL,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 4 + SCREEN_HEIGHT / 2 ,0),D3DXVECTOR3(SCREEN_WIDTH / 3,SCREEN_HEIGHT / 3,0),D3DXVECTOR3(0,0,0),OBJCT2D_NORMAL,PRIORITY_POSE);
	m_bPose = false;
	m_PoseButton = POSE_RETURN;
	m_fAlfa = 1.0f;
	m_fAlfaSpeed = -0.05f;
}
//==============================================================================
//解放
void ObjectPose::uninit(void)
{
	Object2D::uninit();
}
//==============================================================================
//更新
void ObjectPose::update(void)
{
	setDrawFlag(false);
	if(m_bPose)
	{
		m_fAlfa += m_fAlfaSpeed;
		if(SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
		{
			if(InputKeyboard::getKeyboardTrigger(DIK_UP) || InputKeyboard::getKeyboardTrigger(DIK_Z))
			{
				SoundManager::playSound(SOUND_LABEL_BUTTON000);
				m_PoseButton--;
			}
			else
				if(InputKeyboard::getKeyboardTrigger(DIK_DOWN) || InputKeyboard::getKeyboardTrigger(DIK_X))
				{
				SoundManager::playSound(SOUND_LABEL_BUTTON000);
				m_PoseButton++;
				}

			m_PoseButton = m_PoseButton % POSE_MAX;
		}
		for(int loop = 0;loop < POSE_MAX;loop++)
		{
			m_Button[loop]->setColor(D3DXCOLOR(0.70f,0.7f,1.0f,1.0f));
		}

		if(InputKeyboard::getKeyboardTrigger(DIK_RETURN) && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
		{
			SoundManager::playSound(SOUND_LABEL_BUTTON000);
			SCENE_MODE setMode[POSE_MAX] = {
				SCENE_NONE,
				SCENE_GAME,
				SCENE_TITLE,
				SCENE_RESULT
			};

			if(setMode[m_PoseButton] != SCENE_NONE && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
			{

				SystemManager::getObjectFeed()->feedStart(FEED_IN_PROGRESS);

			}

			if(setMode[m_PoseButton] == SCENE_NONE)
			{
				m_bPose = false;
				SystemManager::getCameraData().cameraUpdateMode = true;
			}

		}
		m_Button[m_PoseButton]->setColor(D3DXCOLOR(1.0f,0.3f,0.3f,m_fAlfa));
		if(SystemManager::getObjectFeed()->getFeedMode() != FEED_NONE)
		{
			m_Button[m_PoseButton]->setColor(D3DXCOLOR(1.0f,1.0f,0.3f,1.0f));

		}

	}
	Object::setDrawFlagPriority(PRIORITY_POSE,m_bPose);
	if(m_fAlfa > 1.0f || m_fAlfa < 0.10f)
	{
		m_fAlfaSpeed *= -1;
	}
}
