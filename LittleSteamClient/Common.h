/******************************************************************************/
/*! @addtogroup Common_CommonData
@file       common.h
@brief      どのファイルでも使うデータ
@date       作成日(2014/08/19)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef COMMON_FILE
#define COMMON_FILE
#define _CRTDBG_MAP_ALLOC/*! @brief メモリリーク */ 
#include <stdlib.h>
#include <crtdbg.h>

#include <windows.h>
#include <time.h>
#include <stdio.h>
#include "d3dx9.h"
#include"xaudio2.h"

#define DIRECTINPUT_VERSION (0x0800)/*! @brief 警告対策用 */ 
#include "dinput.h"
/******************************************************************************/
/* ライブラリのリンク
******************************************************************************/
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "dxguid.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "winmm.lib")

#ifdef _DEBUG
#ifndef DBG_NEW
#define DBG_NEW new(_NORMAL_BLOCK,__FILE__,__LINE__)
#define new DBG_NEW
#endif
#endif //DEBUG

/*! @addtogroup Common_CommonData */
/* @{ */

/******************************************************************************/
/* マクロ定義
******************************************************************************/

/*! @brief２Ｄポリゴン頂点フォーマット( 頂点座標[2D] / 反射光 / テクスチャ座標 )*/
#define	FVF_VERTEX_2D	(D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
/*! @brief３Ｄポリゴン頂点フォーマット( 頂点座標[3D] / 法線 / 反射光 / テクスチャ座標 )*/
#define	FVF_VERTEX_3D	(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1)
/*! @brief 180度のPI*/
#define DATA_PI ((float)3.141592654f)
/*! @brief 1度のPI*/
#define DATA_1BYPI ((float)0.318309886f)
/*! @brief 度からラジアンへ
@param[out]     degree    度を入れるとラジアンで返す
*/
#define DATAToRadian( degree ) ((degree) * (DATA_PI / 180.0f))
/*! @brief ラジアンから度へ
@param[out]     radian    ラジアンを入れると度で返す
*/
#define DATAToDegree( radian ) ((radian) * (180.0f / DATA_PI))

/*! @brief 角度を正規化する
@param[out]     rot    角度　0度以下や360度を超えたら0〜360度に収める
*/
template <class T>
inline void ROT_CHK(T &rot)
{
	if(rot > DATA_PI || rot < -DATA_PI)
	{
		if(rot > DATA_PI)
		{
			rot -= 2 * DATA_PI;
		}
		else
			if(rot < -DATA_PI)
			{
			rot += 2 * DATA_PI;
			}
	}
};
/*! @brief 3軸の角度を正規化する
@param[out]     rots    角度　0度以下や360度を超えたら0〜360度に収める
*/
template <class T>
inline void ROTS_CHK(T &rot)
{
	ROT_CHK(rot.x);
	ROT_CHK(rot.y);
	ROT_CHK(rot.z);
};
/*! @brief 色が指定より大きくなったら符号を逆にするにする
@param[out]     p    色の変化速度
@param[in]      s    現在の色
@param[in]      a    変化開始の色
@param[in]      b    変化終了の色
*/
template <class T>
inline void COLOR_CHK(T &p,T &s,T &a,T &b)
{
	if(s > a || s < b)
	{
		p *= -1;
	}

};
/*! @brief 解放時NULLチェック */
template <class T>
inline void SAFE_RELEASE(T &p)
{
	if(p)
	{
		( p )->Release();
		( p ) = NULL;
	}
};
/*! @brief 削除時NULLチェック */
template <class T>
inline void SAFE_DELETE(T &p)
{
	if(p)
	{
		delete ( p );
		( p ) = NULL;
	}
};
/*! @brief 配列削除時NULLチェック */

template <class T>
inline void SAFE_DELETE_ARRAY(T &p)
{
	if(p)
	{
		delete[](p);
		( p ) = NULL;
	}
};
/* @} */

/*! @brief 数の大きさをチェックする
@param[out]     num    数　現在の数
@param[out]     min    最小値　超えていたらfalseを返す
@param[out]     max    最大値　超えていたらfalseを返す
@return         min maxを超えていたらfalseを返す
*/
inline bool NUM_CHK(const int num,const int &min,const int &max)
{
	//現在の数がmin以下max以上ならfalseを返す
	if(num < min || num > max)
	{
		return false;
	}
	return true;
};

/*! @struct VERTEX_2D
@brief  2Dポリゴンの定義
*/
typedef struct
{
	D3DXVECTOR3 vtx;		/*!< 頂点座標*/
	float rhw;				/*!< テクスチャのパースペクティブコレクト用*/
	D3DCOLOR diffuse;		/*!< 反射光*/
	D3DXVECTOR2 tex;		/*!< テクスチャ座標*/
}VERTEX_2D;

/*! @struct VERTEX_3D
@brief   上記３Ｄポリゴン頂点フォーマットに合わせた構造体を定義
*/
typedef struct
{
	D3DXVECTOR3 vtx;		/*!< 頂点座標*/
	D3DXVECTOR3 nor;		/*!< 法線ベクトル*/
	D3DCOLOR diffuse;		/*!< 反射光*/
	D3DXVECTOR2 tex;		/*!< テクスチャ座標*/
}VERTEX_3D;
/*! @addtogroup Common_CommonData */
/* @{ */
#define SCREEN_WIDTH	(1280)				/*!< ウインドウの幅*/
#define SCREEN_HEIGHT	(720)				/*!< ウインドウの高さ*/
#ifdef _DEBUG
#endif
#endif
/* @} */

//End Of File