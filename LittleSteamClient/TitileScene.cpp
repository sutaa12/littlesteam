//=============================================================================
//
// タイトルシーン [TitleScene.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "SceneManager.h"
#include "ObjectManager.h"
#include "TitleScene.h"
#include "Object3DFigure.h"
#include "Object3DXfileAnim.h"
#include "XFileManager.h"
#include "MessageProc.h"
#include "TreeManager.h"
#include "EnemyManager.h"
#include "ObjectPlayer.h"
#include "BulletManager.h"
bool TitleScene::netMode_m = false;

//==============================================================================
//マクロ定義
//==============================================================================
class Imp
{
 public:
 Imp(void)
  :treeMnager_m()
  ,sky_m()
  ,EnenmyRot_m()
  ,player_m()
  ,tutolial_m()
 {}
 ~Imp(void){
  SAFE_DELETE(treeMnager_m);
 }
 static const int TITLE_ENEMY_MAX = 6;
 static const int MOVESPEED = -15000;
 static const int MOVESPEED_Y = 50;
 static const int POS_Y = 6000;
 static const int ENEMY_RADIUS = 100;
 static const int PLAYER_RADIUS = 100;
 static const int PLAYER_SPEED = 20;
 static const int ENEMY_SPEED = 20;
 float EnenmyRot_m[TITLE_ENEMY_MAX];
 TreeManager* treeMnager_m;
 Object3DFigure* sky_m;
 ObjectPlayer* player_m;
 Object2D* tutolial_m;

};
//==============================================================================
//コンストラクタ
TitleScene::TitleScene(void)
{
 imp_m = new Imp();
}
//==============================================================================
//デストラクタ
TitleScene::~TitleScene(void)
{
	Uninit();
}
//==============================================================================
//初期化
HRESULT TitleScene::Init(void)
{
 tutolialMode_m = false;
 Object2D* obj2DPointer;
	Object3DFigure::Create(FIGURE_MESHCYRINDER,TEXTURE_MOUNTAIN,12,2,D3DXVECTOR3(0,-300,0),D3DXVECTOR3(16000,0,1500));
 imp_m->sky_m = Object3DFigure::Create(FIGURE_MESHDOOM,TEXTURE_SKY,12,12,D3DXVECTOR3(0,-500,0),D3DXVECTOR3(30000,0,40000));
 imp_m->sky_m->setRotSpeed(D3DXVECTOR3(0,0.002f,0));
 Object3DFigure::Create(FIGURE_MESHFIELD,TEXTURE_FIELD,0,0,D3DXVECTOR3(0,0,0),D3DXVECTOR3(FIELD_MAX,0,FIELD_MAX));

 imp_m->player_m = ObjectPlayer::Create();
 imp_m->player_m->setPos(D3DXVECTOR3(0,-80,0));
 imp_m->player_m->setAImode(true);
 CAMERA_DATA* cameraData = &SystemManager::getCameraData();
	D3DXVECTOR3 pos = D3DXVECTOR3(400,500,100);
	D3DXVECTOR3 cameraR = pos;
	D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0);
	cameraData->rot.y = cameraData->rotDef.y = rot.y;
	cameraR.y  = 0;
 D3DXVECTOR3 cameraP = D3DXVECTOR3(500,5000,1500);
	cameraData->cameraR = cameraData->cameraRDef = cameraR;
	cameraData->cameraP = cameraData->cameraPDef = cameraP;
	cameraData->cameraLookObject = true;
	cameraData->cameraUpdateMode = true;

	Object2D::Create(TEXTURE_TITLELOGO,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 4.0f,0),D3DXVECTOR3(800,400,0));
	obj2DPointer = Object2D::Create(TEXTURE_PRESSENETER,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2 + 200,0),D3DXVECTOR3(200,40,0));
	obj2DPointer->setColor(D3DXCOLOR(0.6f,0.6f,1.0f,1.0f));
	obj2DPointer->setColorEd(D3DXCOLOR(0.6f,0.6f,1.0f,0.2f));
	obj2DPointer->setColorSpeed(D3DXCOLOR(0,0,0,0.015f));
	timerSecond_m = timer_m = 0;
	SoundManager::playSound(SOUND_LAVEL_SCENEBGM);
 imp_m->treeMnager_m = new TreeManager();

 imp_m->tutolial_m = Object2D::Create(TEXTURE_TUTOLIAL,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2,0),D3DXVECTOR3(SCREEN_WIDTH,SCREEN_HEIGHT,0));
 SystemManager::getObjectFeed()->feedStart(FEED_OUT_PROGRESS);
 imp_m->tutolial_m->setDrawFlag(false);
 EnemyManager::setEnemy();

	return S_OK;
}
//==============================================================================
//解放
void TitleScene::Uninit(void)
{
 SAFE_DELETE(imp_m);
 EnemyManager::setEnemyUpdate(false);
 SoundManager::stopSound();
	Object::releasePriority(PRIORITY_FEED);
}
//==============================================================================
//更新
void TitleScene::Update(void)
{
 BulletManager::update();
 imp_m->treeMnager_m->Update();
 timer_m++;
 if(timer_m % 60 == 0 && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
 {
  timerSecond_m++;
 }
 if(( ( ( !tutolialMode_m && timerSecond_m > 30 )
  || ( tutolialMode_m && InputKeyboard::getKeyboardTrigger(DIK_RETURN) ) ) ) && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
 {
  SoundManager::playSound(SOUND_LABEL_BUTTON000);
  SystemManager::getObjectFeed()->feedStart(FEED_IN_PROGRESS);

 }
 if(SystemManager::getObjectFeed()->getFeedMode() == FEED_IN_END)
 {
  if(!tutolialMode_m && timerSecond_m > 30)
  {
   SceneManager::SetScene(SCENE_RANKING);
  }
  else{
   SceneManager::SetScene(SCENE_GAME);
  }
 }
 if(InputKeyboard::getKeyboardTrigger(DIK_RETURN) && !tutolialMode_m)
 {
  tutolialMode_m = true;
  imp_m->tutolial_m->setDrawFlag(tutolialMode_m);
 }
 if(InputKeyboard::getKeyboardTrigger(DIK_BACKSPACE) && tutolialMode_m)
 {
  tutolialMode_m = false;
  imp_m->tutolial_m->setDrawFlag(tutolialMode_m);
  EnemyManager::setEnemy();

 }

}

//==============================================================================
//更新
void TitleScene::UpdateReplay(void)
{
}
//End Of FIle

