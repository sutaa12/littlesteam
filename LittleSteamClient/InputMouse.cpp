/******************************************************************************
@file       InputMouse.h
@brief      マウス制御クラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//==============================================================================
#include"InputMouse.h"

//==============================================================================
//初期化
//==============================================================================
POINT InputMouse::posMouse_m;
POINT InputMouse::moveMouse_m;
DIMOUSESTATE2 InputMouse::dimouseState_m;
//============================================================================
// マウスコンストラクタ
//=============================================================================
InputMouse::InputMouse()
{

}
//============================================================================
// マウスデストラクタ
//=============================================================================
InputMouse::~InputMouse()
{
	uninit();
}

//============================================================================
// マウス初期化
//=============================================================================
HRESULT InputMouse::init(HINSTANCE hInstance,HWND hWnd)
{
	posMouse_m.x = 0;
	posMouse_m.y = 0;
	moveMouse_m.x = 0;
	moveMouse_m.y = 0;
	HRESULT hr;
	// DirectInputの作成
	hr = DirectInput8Create(hInstance,DIRECTINPUT_VERSION,IID_IDirectInput8,(void**)&dInput_m,NULL);
	if(FAILED(hr))
	{
		MessageBox(NULL,"DirectInput8オブジェクトの作成に失敗","Direct Input Error",MB_OK);
		return hr;
	}
	// デバイス・オブジェクトを作成
	hr = dInput_m->CreateDevice(GUID_SysMouse,&dirDevice_m,NULL);
	if(FAILED(hr))
	{
		MessageBox(NULL,"DirectInputDevice8オブジェクトのの作成に失敗","Direct Input Error",MB_OK);
		return hr;
	}
	// データ・フォーマットを設定
	hr = dirDevice_m->SetDataFormat(&c_dfDIMouse2);
	if(FAILED(hr))
	{
		MessageBox(NULL,"c_dfDIMouse2 形式の設定に失敗","Direct Input Error",MB_OK);
		return hr;
	}
	// 協調モードを設定（フォアグラウンド＆非排他モード）
	hr = dirDevice_m->SetCooperativeLevel(hWnd,DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	if(FAILED(hr))
	{
		MessageBox(NULL,"フォアグラウンド＆非排他モードの設定に失敗","Direct Input Error",MB_OK);
		return hr;
	}
	// 軸モードを設定（相対値モードに設定）
	DIPROPDWORD diprop;
	diprop.diph.dwSize = sizeof(diprop);
	diprop.diph.dwHeaderSize = sizeof(diprop.diph);
	diprop.diph.dwObj = 0;
	diprop.diph.dwHow = DIPH_DEVICE;
	diprop.dwData = DIPROPAXISMODE_REL;
	//diprop.dwData       = DIPROPAXISMODE_ABS; // 絶対値モードの場合
	hr = dirDevice_m->SetProperty(DIPROP_AXISMODE,&diprop.diph);
	if(FAILED(hr))
	{
		MessageBox(NULL,"軸モードの設定に失敗","Direct Input Error",MB_OK);
		return hr;
	}
	m_hwnd = hWnd;
	// 入力制御開始
	dirDevice_m->Acquire();

	return S_OK;
}

//=============================================================================
// マウス終了処理
//=============================================================================
void InputMouse::uninit(void)
{

}

//=============================================================================
// マウス更新処理
//=============================================================================
void InputMouse::update(void)
{
	if(FAILED(dirDevice_m->GetDeviceState(sizeof(DIMOUSESTATE2),&dimouseState_m)))
	{

		dirDevice_m->Acquire();
	}
	else{
	}
	moveMouse_m.x = posMouse_m.x;
	moveMouse_m.y = posMouse_m.y;
	GetCursorPos(&posMouse_m);
	ScreenToClient(m_hwnd,&posMouse_m);
	moveMouse_m.x = posMouse_m.x - moveMouse_m.x;
	moveMouse_m.y = posMouse_m.y - moveMouse_m.y;
}
//=============================================================================
// マウスのプレス状態を取得
//=============================================================================
bool InputMouse::getMouseRightPress(void)
{
	return( dimouseState_m.rgbButtons[0] & 0x80 ) ? TRUE : FALSE;
}
//=============================================================================
// マウスのプレス状態を取得
//=============================================================================
bool InputMouse::getMouseLeftPress(void)
{
	return( dimouseState_m.rgbButtons[1] & 0x80 ) ? TRUE : FALSE;
}
//End Of FIle
