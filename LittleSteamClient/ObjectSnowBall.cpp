/******************************************************************************
@file       ObjectSnowBall.cpp
@brief      ObjectSnowBallの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectSnowBall.h"
#include "MessageProc.h"
#include "SystemManager.h"
#include "CollisionManager.h"
#include "FrustumCulling.h"
int ObjectSnowBall::snow_ball_num_m = 0;
ObjectSnowBall* ObjectSnowBall::objectSnowBall_m;
//================================================================================
//タイプ別作成
//================================================================================
ObjectSnowBall* ObjectSnowBall::Create(D3DXVECTOR3 position,float rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	ObjectSnowBall *objectPointer;

	objectPointer = new ObjectSnowBall(rot,priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		return objectPointer;//アドレスを返す
	}

	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectSnowBall::ObjectSnowBall(float rot,PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3DFigure(FIGURE_MESHSPHERE,OBJCT3D_MESHSPHERE,16,16,priority,objType)
{
 TEXTURE_LIST texlist[SNOW_TYPE_MAX] = {
		TEXTURE_FIELD,
	};
	rotY_m = rot;
 rotX_m = 0;
	setTexName(texlist[0]);
	init();
	if(snow_ball_num_m > 1)
	{
		uninit();
	}
 objectSnowBall_m = this;
}
//==============================================================================
//デストラクタ
ObjectSnowBall::~ObjectSnowBall(void)
{

}
//==============================================================================
//初期化
void ObjectSnowBall::init(void)
{
	cnt_m = 0;
	sclSpeed_m = 0;
	D3DXVECTOR3 moveSpeed[SNOW_TYPE_MAX] =
	{
		D3DXVECTOR3(( rand() % 20 ) + 90,rand() % 10,0),
	};
 D3DXVECTOR3 size[SNOW_TYPE_MAX] =
	{
  D3DXVECTOR3(300,300,300),
	};
  setColor(D3DXCOLOR(0.9f,0.9f,1.0f,1));
  setColorSpeed(D3DXCOLOR(0,0,0,0.015f));
		setColorEd(D3DXCOLOR(1,1,1,0));
		sclSpeed_m = 0.02f;
		rotY_m += ( ( rand() & 4000 ) - 2000 ) / 10000.0f;
	posSpeed_m = moveSpeed[0];
	setSize(size[0]);
	getCollisionData()->radius = size_m.x / 2;
	getCollisionData()->pos = pos_m;
 Object3DFigure::init(FIGURE_MESHSPHERE);
 setScl(D3DXVECTOR3(0.2f,0.2f,0.2f));
 getCollisionData()->radius = ( scl_m.x * size_m.x ) / 2;
 shot_mode_m = SNOW_DISABLE;
}
//==============================================================================
//解放
void ObjectSnowBall::uninit(void)
{
	Object3DFigure::uninit();
}
//==============================================================================
//更新
void ObjectSnowBall::update(void)
{
 rot_m.y = rotY_m;
 rot_m.x = rotX_m;
 Object3DFigure::update();
 getCollisionData()->radius = ( scl_m.x * size_m.x ) / 2;

 if(shot_mode_m == SNOW_CLLECT)
 {
  cnt_m = 0;
  if(scl_m.x <= 2.0f)
  {
   getCollisionData()->pos = pos_m;
   scl_m.x += sclSpeed_m;
   scl_m.y += sclSpeed_m;
   scl_m.z += sclSpeed_m;

  }
 }
 if(shot_mode_m == SNOW_SHOT)
 {
  getCollisionData()->pos = pos_m;
  pos_m.x += sinf(rotY_m)*posSpeed_m.x;
  pos_m.y += posSpeed_m.y;
  pos_m.z += cosf(rotY_m)*posSpeed_m.x;
  AddRotX(0.1f);
  cnt_m++;
  if(cnt_m >= 300)
  {
   shot_mode_m = SNOW_DISABLE;
  }
 }
 else{
  float height = CollisionManager::HitchkField(pos_m,NULL);
  pos_m.y = height + ( ( scl_m.x * size_m.x ) / 2 );

 }
 if(shot_mode_m == SNOW_DISABLE)
 {
  setDrawFlag(false);
 }
 else{
  setDrawFlag(true);
 }
 float height = CollisionManager::HitchkField(pos_m,NULL);
 if(pos_m.y - ( scl_m.x * size_m.x ) <= height)
 {
  pos_m.y = height + ( ( scl_m.x * size_m.x ) / 2 );
 }
}
bool ObjectSnowBall::hitchk(COLLISION_DATA* obj1)
{
 if(shot_mode_m == SNOW_SHOT)
 {
  if(CollisionManager::HitchkCircle(obj1,&collisionData_m))
  {
   return true;
  }
 }
  return false;

}


//End Of FIle