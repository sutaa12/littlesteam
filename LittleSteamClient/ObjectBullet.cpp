/******************************************************************************
@file       ObjectBullet.cpp
@brief      ObjectBulletの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectBullet.h"
#include "BulletManager.h"
#include "MessageProc.h"
#include "SystemManager.h"
#include "CollisionManager.h"
#include "FrustumCulling.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectBullet* ObjectBullet::Create(BULLET_TYPE bulletType,D3DXVECTOR3 position,float rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	ObjectBullet *objectPointer;

	objectPointer = new ObjectBullet(bulletType,rot,priority);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectBullet::ObjectBullet(BULLET_TYPE bulletType,float rot,PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3DBillboard(priority,objType)
{
	TEXTURE_LIST texlist[BULLET_TYPE_MAX] = {
		TEXTURE_SMOKE,
		TEXTURE_SMOKE,
	};
	rotY_m = rot;
	bulletType_m = bulletType;
	setTexName(texlist[bulletType]);
	bullethundle_m = BulletManager::addBulletList(bulletType_m,this);
	init();
	if(bullethundle_m < 0)
	{
		uninit();
	}
}
//==============================================================================
//デストラクタ
ObjectBullet::~ObjectBullet(void)
{

}
//==============================================================================
//初期化
void ObjectBullet::init(void)
{
	cnt_m = 0;
	sclSpeed_m = 0;
	D3DXVECTOR3 moveSpeed[BULLET_TYPE_MAX] =
	{
		D3DXVECTOR3(( rand() % 20 ) + 90,rand() % 10,0),
		D3DXVECTOR3(-20,-2,0)
	};
	D3DXVECTOR3 size[BULLET_TYPE_MAX] =
	{
  D3DXVECTOR3(300,300,0),
		D3DXVECTOR3(256,256,0)
	};
	if(bulletType_m == BULLET_PLAYER)
	{
		setDrawAlphaMode();
  setColor(D3DXCOLOR(0.2f,0.2f,0.7f,1));
  setColorSpeed(D3DXCOLOR(0,0,0,0.015f));
		setColorEd(D3DXCOLOR(1,1,1,0));
		sclSpeed_m = 0.02f;
		rotY_m += ( ( rand() & 4000 ) - 2000 ) / 10000.0f;
	}
	else
		if(bulletType_m == BULLET_ENEMY)
		{
		setDrawAlphaMode();
  setColor(D3DXCOLOR(0.2f,0.9f,0.2f,1));
  setColorSpeed(D3DXCOLOR(0,0,0,0.01f));
		setColorEd(D3DXCOLOR(0.4f,0.4f,0.8f,0));
		sclSpeed_m = 0.01f;
		rotY_m += ( ( rand() & 400 ) - 200 ) / 1000.0f;
		}
	posSpeed_m = moveSpeed[bulletType_m];
	setSize(size[bulletType_m]);
	getCollisionData()->radius = size_m.x / 2;
	getCollisionData()->pos = pos_m;
}
//==============================================================================
//解放
void ObjectBullet::uninit(void)
{
	BulletManager::removeBulletList(bulletType_m,bullethundle_m);
	Object3DBillboard::uninit();
}
//==============================================================================
//更新
void ObjectBullet::update(void)
{

	Object3DBillboard::update();
	getCollisionData()->pos = pos_m;
	pos_m.x += sinf(rotY_m)*posSpeed_m.x;
	pos_m.y += posSpeed_m.y;
	pos_m.z += cosf(rotY_m)*posSpeed_m.x;
	cnt_m++;
	if(bulletType_m == BULLET_PLAYER)
	{
		scl_m.x += sclSpeed_m;
		scl_m.y += sclSpeed_m;
		scl_m.z += sclSpeed_m;
		getCollisionData()->radius = ( scl_m.x * size_m.x ) / 2;
  float height = CollisionManager::HitchkField(pos_m,NULL);
		if(pos_m.y - ( scl_m.x * size_m.x ) <= height)
		{
   pos_m.y = height + ( ( scl_m.x * size_m.x ) / 2 );
		}

	}
	else
		if(bulletType_m == BULLET_ENEMY)
		{
		scl_m.x += sclSpeed_m;
		scl_m.y += sclSpeed_m;
		scl_m.z += sclSpeed_m;
		getCollisionData()->radius = ( scl_m.x * size_m.x ) / 2;
  float height = CollisionManager::HitchkField(pos_m,NULL);
  if(pos_m.y - ( scl_m.x * size_m.x ) <= height)
  {
   pos_m.y = height + ( scl_m.x * size_m.x );
  }

  }
 int cnt = 0;
 D3DXMATRIX mtxview = SystemManager::getCameraData().cameraView;
 FRUSTUM	sFrustum;

 SetupFOVClipPlanes(( D3DX_PI / 2 ),//視野
                    (float)SCREEN_WIDTH / SCREEN_HEIGHT,//アスペクト比
                    1.0f,	//距離
                    12000.0f,
                    sFrustum);

 if(!MeshFOVCheck(&pos_m,collisionData_m.radius * 10,sFrustum,SystemManager::getCameraData().cameraView))  // 視錐台から、外れていたら
 {
  setDrawFlag(false);
 }
 else{
  setDrawFlag(true);

 }

	if(getColor().a <= 0.05f || cnt_m >= 100)
	{
		uninit();
	}

}

//End Of FIle