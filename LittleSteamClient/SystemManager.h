/******************************************************************************/
/*! @addtogroup System_SystemManager
@file       SystemManager.h
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef SYSTEM_MANAGE_FILE
#define SYSTEM_MANAGE_FILE
#include "Common.h"
#include "CameraManager.h"
#define FIELD_MAX (200000)
#define MOVE_MAX ((FIELD_MAX / 2) - (FIELD_MAX / 4))

//前方宣言
class InputManager;
class SoundManager;
class CameraManager;
class SceneManager;
class ObjectManager;
class CollisionManager;
class DrawManager;
class LightManager;
class EnemyManager;
class EffectManager;
class ObjectFeed;
/*! @class SystemManager
@brief  各管理クラスを更新する
*/

class SystemManager {
	//コンストラクタ
	/*! SystemManager　コンストラクタ*/
	public:
	SystemManager(HINSTANCE hInstance,HWND hWnd,BOOL bWindow);
	//デストラクタ
	/*! ~SystemManager　デストラクタ*/
	public:
	~SystemManager(void);
	//操作
	public:
	/*! update　管理クラスを更新*/
	void update(void);
	/*! setFPSTimer 1フレームの時間を入れる*/
	void setFPSTimer(unsigned long fpsTimer){ fpsTimer_m = fpsTimer; }
	//属性
	static CAMERA_DATA& getCameraData(void){ return *cameraDataPointer_m; }
	static ObjectFeed* getObjectFeed(void){ return objFeed_m; }
	private:
	InputManager* inputManager_m; /*!< 入力管理 */
	SoundManager* soundManager_m; /*!< サウンド管理 */
	CameraManager* cameraManager_m; /*!< カメラ管理 */
	SceneManager* sceneManager_m; /*!< シーン管理*/
	ObjectManager* objectManager_m; /*!< オブジェクト管理 */
	CollisionManager* collisionManager_m; /*!< 当たり判定管理 */
	DrawManager*  drawManager_m; /*!< 描画管理 */
	LightManager* lightManager_m;/*!< ライト管理*/
	EnemyManager* enemyManager_m;/*!< 敵の管理*/
	EffectManager* effectManger_m;
	unsigned long fpsTimer_m;/*!< 1フレームの計測値*/
	static ObjectFeed* objFeed_m;
	static CAMERA_DATA* cameraDataPointer_m;//カメラデータの受け渡しポンタ
};

#endif
//End Of File