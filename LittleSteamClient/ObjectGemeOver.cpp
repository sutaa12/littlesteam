/******************************************************************************
@file       ObjectGameOver.cpp
@brief      ObjectGameOverの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectGameOver.h"
#include "SystemManager.h"
#include "InputKeyboard.h"
#include "ObjectFeed.h"
#include "Object.h"
#include "SceneManager.h"
#include "TextureManager.h"
#include "SoundManager.h"
#define BUTTONSIZEW (210)
#define BUTTONSIZEH (40)
//================================================================================
//タイプ別作成
//================================================================================
ObjectGameOver* ObjectGameOver::Create()
{
	ObjectGameOver *pScene2D;
	pScene2D = new ObjectGameOver;
	if(pScene2D)
	{
		return pScene2D;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectGameOver::ObjectGameOver(PRIORITY_MODE nPriority,OBJECT2D_TYPE objType):Object2D(nPriority,OBJTYPE_2D,objType)
{
	init();
}
//==============================================================================
//デストラクタ
ObjectGameOver::~ObjectGameOver(void)
{
	uninit();
}
//==============================================================================
//初期化
void ObjectGameOver::init(void)
{

	setDrawFlag(false);
	const TEXTURE_LIST tex[GAMEOVER_MAX] = {
		TEXTURE_RETRY,
		TEXTURE_TITLE,
		TEXTURE_RESULT
	};
	gameOverFlag_m = false;
	Object2D* pScene;
	pScene = Object2D::Create(TEXTURE_NONE,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2,0),D3DXVECTOR3(SCREEN_WIDTH,SCREEN_HEIGHT,0),D3DXVECTOR3(0,0,0),OBJCT2D_NORMAL);
	pScene->setColor(D3DXCOLOR(0,0,0,0.6f));
	m_Scene[0] = pScene;
	pScene = Object2D::Create(TEXTURE_GAMEOVERLOGO,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 4.0f,0),D3DXVECTOR3(BUTTONSIZEW * 2,BUTTONSIZEW * 2,0),D3DXVECTOR3(0,0,0));
	m_Scene[1] = pScene;

	for(int loop = 0;loop < GAMEOVER_MAX;loop++)
	{
		m_Button[loop] = Object2D::Create(tex[loop],D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2 + ( loop * BUTTONSIZEH + 20 ),0),D3DXVECTOR3(BUTTONSIZEW,BUTTONSIZEH,0),D3DXVECTOR3(0,0,0));

	}
	m_fAlfa = 1.0f;
	m_fAlfaSpeed = -0.05f;
}
//==============================================================================
//解放
void ObjectGameOver::uninit(void)
{
	Object2D::uninit();
}
//==============================================================================
//更新
void ObjectGameOver::update(void)
{
	setDrawFlag(false);
	if(gameOverFlag_m)
	{
		m_fAlfa += m_fAlfaSpeed;
		if(SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
		{
			if(InputKeyboard::getKeyboardTrigger(DIK_UP) || InputKeyboard::getKeyboardTrigger(DIK_Z))
			{
				SoundManager::playSound(SOUND_LABEL_BUTTON000);
				m_GameOverButton--;
			}
			else
				if(InputKeyboard::getKeyboardTrigger(DIK_DOWN) || InputKeyboard::getKeyboardTrigger(DIK_X))
				{
				SoundManager::playSound(SOUND_LABEL_BUTTON000);
				m_GameOverButton++;
				}

			m_GameOverButton = m_GameOverButton % GAMEOVER_MAX;
		}
		for(int loop = 0;loop < GAMEOVER_MAX;loop++)
		{
			m_Button[loop]->setColor(D3DXCOLOR(0.70f,0.7f,1.0f,1.0f));
		}

		if(InputKeyboard::getKeyboardTrigger(DIK_RETURN) && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
		{
			SoundManager::playSound(SOUND_LABEL_BUTTON000);
			SCENE_MODE setMode[GAMEOVER_MAX] = {
				SCENE_GAME,
				SCENE_TITLE,
				SCENE_RESULT
			};

			if(setMode[m_GameOverButton] != SCENE_NONE && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
			{

				SystemManager::getObjectFeed()->feedStart(FEED_IN_PROGRESS);

			}

		}
		m_Button[m_GameOverButton]->setColor(D3DXCOLOR(1.0f,0.3f,0.3f,m_fAlfa));
		if(SystemManager::getObjectFeed()->getFeedMode() != FEED_NONE)
		{
			m_Button[m_GameOverButton]->setColor(D3DXCOLOR(1.0f,1.0f,0.3f,1.0f));

		}

		if(m_fAlfa > 1.0f || m_fAlfa < 0.10f)
		{
			m_fAlfaSpeed *= -1;
		}
	}
	else{
	}
	for(int loop = 0;loop < GAMEOVER_MAX;loop++)
	{
		m_Button[loop]->setDrawFlag(gameOverFlag_m);
	}
	m_Scene[0]->setDrawFlag(gameOverFlag_m);
	m_Scene[1]->setDrawFlag(gameOverFlag_m);


}
