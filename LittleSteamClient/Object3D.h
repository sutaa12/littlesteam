/******************************************************************************/
/*! @addtogroup Object_Object3D
@file       Object.h
@brief      Object3Dクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECT3D_FILE
#define COBJECT3D_FILE
#include "main.h"
#include "Object.h"
#include "TextureManager.h"
//3Dの種類
typedef enum
{
	OBJCT3D_NONE = 0,
	OBJCT3D_NORMAL,
	OBJCT3D_MESHBOARD,
	OBJCT3D_MESHCYLINDER,
	OBJCT3D_MESHDOOM,
	OBJCT3D_MESHWALL,
 OBJCT3D_MESHFIELD,
 OBJCT3D_MESHSPHERE,
 OBJCT3D_BULLET,
 OBJCT3D_SNOW_BALL,
	OBJCT3D_EFFECT,
	OBJCT3D_PLAYER,
	OBJCT3D_ENEMY,
	OBJCT3D_MAX
}OBJECT3D_TYPE;

/*! @enum COLLISION_TYPE
@brief 当たり判定の種類
*/
typedef enum{
	HIT_CIRCLE = 0,
	HIT_BOARD,
	HIT_FIELD,
	HIT_BOX,
	HIT_MAX
}COLLISION_TYPE;
/*! @brief COLLISION_DATA
@brief 当たり判定のデータ
*/
typedef struct
{
	COLLISION_TYPE coltype;//当たり判定の種類
	D3DXVECTOR3 rotOut;//出力角度
	D3DXVECTOR3 pos;//当たり判定の位置
	D3DXVECTOR3 posOut;//変更後の位置
	D3DXVECTOR3 size;//当たり判定の大きさ
	float radius;//円の半径
	bool collisionChk[OBJCT3D_MAX];//当たり判定
	bool collision;//当たり判定をするか
}COLLISION_DATA;

class Object3D: public Object
{
	//コンストラクタ
	public:
	/*! Object3D Object3Dのコンストラクタ
	@param[in]     type    3Dオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	Object3D(PRIORITY_MODE priority = PRIORITY_4,OBJTYPE objType = OBJTYPE_3D,OBJECT3D_TYPE type = OBJCT3D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! Object3D Object3Dのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~Object3D(void);
	//操作
	public:
	/*! Create Object3Dの作成
	@param[in]     texName    テクスチャの名前定義から設定(TEXTURE_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static Object3D* Create(TEXTURE_LIST texName = TEXTURE_NONE,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT3D_TYPE type = OBJCT3D_NORMAL,PRIORITY_MODE priority = PRIORITY_7);
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	/*! setPos 位置の設定
	@param[in]     pos    位置の設定 x,y,z
	*/
	void setPos(D3DXVECTOR3 pos){ pos_m = pos; }
	/*! getPos 位置の取得
	@return     位置の取得
	*/
	D3DXVECTOR3 getPos(void){ return pos_m; }
	/*! getSize 大きさの取得
	@return     大きさの取得
	*/
	D3DXVECTOR3 getSize(void){ return size_m; }
	/*! setSize 中心からの大きさ設定
	@return     中心からの大きさ設定
	*/
	void setSize(D3DXVECTOR3 size){ size_m = size; }
	/*! setScl 拡大率設定
	@param         scl     拡大率設定
	*/
	void setScl(D3DXVECTOR3 scl){ scl_m = scl; }
	/*! setRot 角度の設定
	@param[in]     rot    角度の設定
	*/
	void setRot(D3DXVECTOR3 rot){ rot_m = rot; }
	/*! getPos 角度の加速度の設定
	@param[in]     rotSpeed    角度の加速度の設定
	*/
	void setRotSpeed(D3DXVECTOR3 rotSpeed) { rotSpeed_m = rotSpeed; }
	/*! getScl 拡大率取得
	@return       拡大率取得
	*/
	D3DXVECTOR3 getScl(void){ return scl_m; }
	/*! getRot 角度の取得
	@return     角度の取得
	*/
	D3DXVECTOR3 getRot(void){ return rot_m; }
	/*! setColor 色の設定
	@param[in]     col    色の設定 0〜255の色
	*/
	void setColor(D3DXCOLOR col){ col_m = colSt_m = col; }
	/*! setColorSpeed 色の変化する速度の設定
	@param[in]     col    色の変化する速度の設定
	*/
	void setColorSpeed(D3DXCOLOR col){ colSpeed_m = col; }
	/*! setColorSpeed rgbaすべての色の変化する速度をいっぺんに設定
	@param[in]     col    色の設定 0〜255の色
	*/
	void setColorSpeed(float col){ colSpeed_m = D3DXCOLOR(col,col,col,col); }
	/*! setColorSt 開始する色の設定
	@param[in]     col    開始する色の設定
	*/
	void setColorSt(D3DXCOLOR col){ colSt_m = col; }
	/*! setColorEd 折り返す色の設定
	@param[in]     col    折り返す色の設定
	*/
	void setColorEd(D3DXCOLOR col){ colEd_m = col; }
	/*! getRot 色の取得
	@return     現在の色を返す
	*/
	D3DXCOLOR getColor(void){ return col_m; }
	/*! getTexPos テクスチャーの位置取得
	@return     ４つの位置の配列の先頭を返す
	*/
	D3DXVECTOR2* getTexPos(void){ return &texPos_m[0]; }
	/*! setTexPos テクスチャーの定義設定
	@param[in]     texName    定義したいテクスチャ名(TEXTURE_LIST)
	*/
	void setTexName(TEXTURE_LIST texName){ textureName_m = texName; }
	/*! getTexName テクスチャーの名前取得
	@return     テクスチャの名前を返す
	*/
	TEXTURE_LIST getTexName(void){ return textureName_m; }
	/*! getObject3DType 3Dオブジェのタイプ取得
	@return     3Dオブジェのタイプを返す OBJECT3D_TYPE
	*/
	OBJECT3D_TYPE getObject3DType(void){ return type3D_m; }
	/*! getCollisionData 当たり判定データ取得
	@return     当たり判定データを返す
	*/
	COLLISION_DATA* getCollisionData(void){ return &collisionData_m; }
	/*! getLightMode ライトの状態取得
	@return     ライトの状態を返す
	*/
	void setDrawAlphaMode(void){ drawAlphaMode_m = true; }
	bool getDrawAlphaMode(void){ return drawAlphaMode_m; }
	bool getLightMode(void){ return lightMode; }
	protected:
	//メンバ変数
	D3DXVECTOR3 posOld_m;/*!< 座標*/
	D3DXVECTOR3 pos_m;/*!< 座標*/
	D3DXVECTOR3 posDest_m;/*!< 座標*/
	D3DXVECTOR3 posSpeed_m;/*!< 移動量*/
	D3DXVECTOR3 rotOld_m;/*!< 回転角度*/
	D3DXVECTOR3 rotDest_m;/*!< 回転角度*/
	D3DXVECTOR3 rot_m;/*!< 回転角度*/
	D3DXVECTOR3 rotSpeed_m;/*!< 回転移動量*/
	D3DXVECTOR3 size_m;/*!< 大きさ*/
	D3DXVECTOR3 scl_m;/*!< 拡大率*/
	D3DXVECTOR2 texPos_m[4];/*!< テクスチャーの位置４つ*/
	D3DXCOLOR col_m;/*!< 色*/
	D3DXCOLOR colSpeed_m;/*!< 色の変化量*/
	D3DXCOLOR colSt_m;/*!< 変化させたい色*/
	D3DXCOLOR colEd_m;/*!< 変化させたい色*/
	OBJECT3D_TYPE type3D_m;/*!< 3Dオブジェクトの種類*/
	TEXTURE_LIST textureName_m;//テクスチャのマクロから取得
	COLLISION_DATA collisionData_m;//当たり判定のデータ
	float posDeffSpeed;
	float rotDeffSpeed;
	bool lightMode;
	bool drawAlphaMode_m;
	float height_m;//高さの補正

};
#endif

//End Of FIle