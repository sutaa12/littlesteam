/******************************************************************************/
/*! @addtogroup Input_InputManager
@file       InputManager.h
@brief      入力を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef INPUT_FILE
#define INPUT_FILE
#include "Common.h"

//入力クラス
class Input
{
	public:
	Input(void);//コンストラクタ
	virtual ~Input(void);//デストラクタ
	virtual HRESULT init(HINSTANCE hInstance,HWND hWnd);//初期化
	virtual void uninit(void);//解放
	virtual void update(void) = 0;//更新
	protected:
	//メンバ変数
	static LPDIRECTINPUT8 dInput_m;
	LPDIRECTINPUTDEVICE8 dirDevice_m;
};
class InputKeyboard;
class InputMouse;
//入力管理クラス
class InputManager
{
	public:
	InputManager(void);//コンストラクタ
	virtual ~InputManager(void);//デストラクタ
	virtual HRESULT init(HINSTANCE hInstance,HWND hWnd);//初期化
	virtual void uninit(void);//解放
	virtual void update(void);//更新
	private:
	InputKeyboard* inputKeyboard_m;
	InputMouse* inputMouse_m;
};
#endif

//End Of FIle