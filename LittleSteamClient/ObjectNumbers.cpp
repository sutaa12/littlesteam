/******************************************************************************
@file       ObjectNumbers.cpp
@brief      ObjectNumbersの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectNumber.h"
#include "ObjectNumbers.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectNumbers* ObjectNumbers::Create(int number,TEXTURE_LIST texName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXCOLOR col,D3DXVECTOR3 rot,OBJECT2D_TYPE type,PRIORITY_MODE priority)
{
	ObjectNumbers *objectPointer;

	objectPointer = new ObjectNumbers(number,texName,position,size,col,priority,type);
	if(objectPointer)
	{
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectNumbers::ObjectNumbers(int number,TEXTURE_LIST texName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXCOLOR col,PRIORITY_MODE priority,OBJECT2D_TYPE type,OBJTYPE objType):Object2D(priority,objType,type)
{

	numbers_m = nullptr;
	numberMax_m = number;
	number_m = numberDef_m = 0;
	setTexName(texName);
	setSize(size);
	setColor(col);
	setPos(position);
 draw_m = true;
 init();
 setDrawFlag(false);
}
//==============================================================================
//デストラクタ
ObjectNumbers::~ObjectNumbers(void)
{

}
//==============================================================================
//初期化
void ObjectNumbers::init(void)
{
 Object2D::init();
 numbers_m = new ObjectNumber*[numberMax_m];

	for(int loop = 0;loop < numberMax_m;loop++)
	{
		D3DXVECTOR3 pos = getPos();
		pos.x -= ( getSize().x * numberMax_m ) / 2;
		pos.x += ( getSize().x*loop );
		numbers_m[loop] = ObjectNumber::Create(getTexName(),pos,getSize(),getColor());
	}
}
//==============================================================================
//解放
void ObjectNumbers::uninit(void)
{
	SAFE_DELETE_ARRAY(numbers_m);
	Object2D::uninit();
}
//==============================================================================
//更新
void ObjectNumbers::update(void)
{

	if(numberDef_m > 0)
	{
		numberDef_m--;
		number_m++;
	}
	else{
		numberDef_m = 0;
	}
	//最大桁数超えたら
	if(number_m >= pow((float)10,(float)numberMax_m))
	{
		number_m = (int)pow((float)10,(float)numberMax_m) - 1;
	}
	else
		if(number_m < 0)
		{
		number_m = 0;
		}
	int noldNumber = 0;
	int nNumberC = number_m;
	for(int nCnt = 1;nCnt <= numberMax_m; nCnt++)
	{
		noldNumber = (int)( nNumberC / powf(10,( numberMax_m - nCnt )) );
		numbers_m[nCnt - 1]->setNumber(noldNumber);
  numbers_m[nCnt - 1]->setDrawFlag(draw_m);
		nNumberC -= (int)( noldNumber * powf(10,( numberMax_m - nCnt )) );
	}
	Object2D::update();

}

//End Of FIle