float4x4 m_WVP;                  // ワールド座標系 × ビュー(カメラ基準)座標系 × 正射影座標系
float4x4 m_LWVP;                 // ワールド座標系 × ビュー(ライト基準)座標系 × 正射影座標系
float4x4 m_LWVPT;                // ワールド座標系 × ビュー(ライト基準)座標系 × 正射影座標系 × テクスチャー座標系

float    m_Bias = 0.0f;          // Z値の比較による誤差を補正する
float    m_ShadowColor = 0.1f;   // 影によるメッシュのカラーの減算値

float4   m_LightDir;
float4   m_Ambient = 0.0f;

sampler tex0 : register(s0);     // オブジェクトのテクスチャー
sampler tex1 : register(s1);     // デプスバッファサーフェイスのテクスチャー

struct VS_OUTPUT
{
   float4 Pos       : POSITION;
   float4 Col       : COLOR0;
   float2 Tex       : TEXCOORD0;
   float2 Depth     : TEXCOORD1; //正射影座標系 に変換した頂点座標
   float4 LightUV   : TEXCOORD2; //テクスチャー座標系に変換した頂点座標
};

VS_OUTPUT VS( float4 Pos     : POSITION,
              float4 Normal  : NORMAL,
              float2 Tex     : TEXCOORD0 )
{
   VS_OUTPUT Out;

   Out.Pos    = mul( Pos, m_WVP );
   Out.Tex    = Tex;

   // ランバート拡散照明によるメッシュの色情報を計算する
   float3 L = -m_LightDir.xyz;
   float3 N = normalize( Normal.xyz );
   Out.Col = max( m_Ambient, dot(N, L) );

   // 正射影座標系に変換したZ値を計算
   Out.Depth.xy   = mul(Pos, m_LWVP).zw;

   // テクスチャー座標系に変換した頂点座標を計算
   Out.LightUV = mul(Pos, m_LWVPT);

   return Out;
}

float4 PS( VS_OUTPUT In ) : COLOR0
{  
   float4 Out = tex2D( tex0, In.Tex );
   float3 ShadowColor;
      
   //Zバッファサーフェイスから深度値を取得する
   float d = tex2Dproj( tex1, In.LightUV ).r;

   //陰と影をなめらかにするために陰と影の両方で減算せず、暗い方で減算するようにする
   if( d * In.Depth.y < In.Depth.x - m_Bias )
      ShadowColor = min( ( In.Col.r + In.Col.g + In.Col.b ) * 0.3333f, m_ShadowColor );
   else
      ShadowColor = In.Col.rgb;
   
   Out.rgb *= ShadowColor;
   
   return Out;
}

technique TShader
{
   pass P0
   {
      VertexShader = compile vs_1_1 VS();
      PixelShader  = compile ps_2_0 PS();
   }     
}
