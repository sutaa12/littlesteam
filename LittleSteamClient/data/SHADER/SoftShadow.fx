float4x4 m_WVP;                 // ワールド座標系 × ビュー座標系 × 射影座標系
float4x4 m_WVPT;                // ワールド座標系 × ビュー座標系 × 射影座標系 × テクスチャー座標系
float4x4 m_LWVP;                // ライト基準の行列変換
float4x4 m_LWVPT;               // ライト基準の行列変換をテクスチャー座標系に変換

float    m_Bias = 0.0f;         // Z値の比較による誤差を補正する
float    m_ShadowColor = 0.1f;  // 影によるメッシュのカラーの減算値

float4   m_LightDir;            // 平行光源の方向ベクトル
float4   m_Ambient = 0.0f;      // 環境光

sampler tex0 : register(s0);    // オブジェクトのテクスチャー
sampler tex1 : register(s1);    // Pass0:Zバッファテクスチャー  Pass1:影イメージテクスチャー

// パス０では影イメージを作成する
struct VS1_OUTPUT
{
   float4 Pos       : POSITION;
   float4 LightUV   : TEXCOORD0;      // Zバッファテクスチャーのテクセル座標
   float2 Depth     : TEXCOORD1;    // カメラ基準の行列変換により計算した深度値
};

VS1_OUTPUT VS1( float4 Pos     : POSITION,
                float4 Normal  : NORMAL,
                float2 Tex     : TEXCOORD0 )
{
   VS1_OUTPUT Out;
   
   Out.Pos    = mul( Pos, m_WVP );

   // Zバッファの深度情報を取得するためにテクセル座標をセット
   Out.LightUV = mul(Pos, m_LWVPT);

   // ライト基準の行列変換により深度値を計算する
   Out.Depth.xy   = mul(Pos, m_LWVP).zw;   

   return Out;
}

float4 PS1( VS1_OUTPUT In ) : COLOR0
{
   //出力するデフォルト色は白
   float4 Out = (float4)1;

   //Zバッファから深度値を取得する
   float d = tex2Dproj( tex1, In.LightUV ).r;
   
   //影となるので影の色を出力
   if( d * In.Depth.y < In.Depth.x - m_Bias )
      Out = m_ShadowColor;

   return Out;
}

// パス１で影を適応する
struct VS2_OUTPUT
{
   float4 Pos       : POSITION;
   float4 Col       : COLOR0;
   float2 Tex       : TEXCOORD0;
   float4 WVPTPos   : TEXCOORD1;   // テクスチャー座標系に行列変換した頂点座標
};

VS2_OUTPUT VS2( float4 Pos     : POSITION,
                float4 Normal  : NORMAL,
                float2 Tex     : TEXCOORD0 )
{
   VS2_OUTPUT Out;
   
   Out.Pos    = mul( Pos, m_WVP );
   Out.Tex    = Tex;
   
   // ランバート拡散照明
   float3 L = -m_LightDir.xyz;   
   float3 N = normalize( Normal.xyz );
   Out.Col = max( m_Ambient, dot(N, L) );
   
   // テクスチャー座標系に行列変換
   Out.WVPTPos = mul(Pos, m_WVPT);
   
   return Out;
}

float4 PS2( VS2_OUTPUT In ) : COLOR0
{
   float4 Out = (float4)0;

   // 影イメージテクスチャーから影情報を取得する
   float s = tex2Dproj( tex1, In.WVPTPos ).r;
   
   // 適当に調整する(注意１)
   s = max( m_ShadowColor, s * s );
   
   // 陰と影をなめらかにするために暗い方を適応する
   Out = tex2D( tex0, In.Tex ) * min( ( In.Col.r + In.Col.g + In.Col.b ) * 0.3333f, s );
   
   return Out;
}

technique TShader
{
   // 影イメージを作成する
   pass P0
   {
      VertexShader = compile vs_1_1 VS1();
      PixelShader  = compile ps_2_0 PS1();
   }

   // 影を適応する
   pass P1
   {
      VertexShader = compile vs_1_1 VS2();
      PixelShader  = compile ps_2_0 PS2();
   }
}
