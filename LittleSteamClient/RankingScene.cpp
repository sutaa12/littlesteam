//=============================================================================
//
// リザルトシーン [RankingScene.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "SceneManager.h"
#include "ObjectManager.h"
#include "RankingScene.h"
#include "ObjectNumber.h"
#include "ObjectNumbers.h"
#include "Object3DFigure.h"
#include "Object3DXfileAnim.h"
#include "XFileManager.h"
#include "ObjectBullet.h"
#include "TreeManager.h"
class Imple
{
 public:
 Imple(void)
  :treeMnager_m()
  ,sky_m()
 {}
 ~Imple(void){
  SAFE_DELETE(treeMnager_m);
 }
 static const int TITLE_ENEMY_MAX = 6;
 static const int MOVESPEED = -15000;
 static const int MOVESPEED_Y = 50;
 static const int POS_Y = 6000;
 static const int ENEMY_RADIUS = 100;
 static const int PLAYER_RADIUS = 100;
 static const int PLAYER_SPEED = 20;
 static const int ENEMY_SPEED = 20;
 TreeManager* treeMnager_m;
 Object3DFigure* sky_m;

};

//==============================================================================
//コンストラクタ
RankingScene::RankingScene(void)
{

}
//==============================================================================
//デストラクタ
RankingScene::~RankingScene(void)
{
	Uninit();
}
//==============================================================================
//初期化
HRESULT RankingScene::Init(void)
{
 imp_m = new Imple;
 Object3DFigure::Create(FIGURE_MESHCYRINDER,TEXTURE_MOUNTAIN,12,2,D3DXVECTOR3(0,0,0),D3DXVECTOR3(16000,0,1500));
 imp_m->sky_m = Object3DFigure::Create(FIGURE_MESHDOOM,TEXTURE_SKY,12,12,D3DXVECTOR3(0,0,0),D3DXVECTOR3(30000,0,40000));
 imp_m->sky_m->setRotSpeed(D3DXVECTOR3(0,0.002f,0));
 Object3DFigure::Create(FIGURE_MESHFIELD,TEXTURE_FIELD,0,0,D3DXVECTOR3(0,0,0),D3DXVECTOR3(FIELD_MAX,0,FIELD_MAX));
 player_m = Object3DXfileAnim::Create();
 player_m->setPos(D3DXVECTOR3(0,3120,0));
 player_m->setRot(D3DXVECTOR3(0,0,0));

 CAMERA_DATA* cameraData = &SystemManager::getCameraData();
 D3DXVECTOR3 pos = D3DXVECTOR3(-800,3120,100);
 D3DXVECTOR3 cameraR = pos;
 D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0);
 cameraData->rot.y = cameraData->rotDef.y = rot.y;
 cameraR.y = 3300;
 D3DXVECTOR3 cameraP = D3DXVECTOR3(500,3200,1500);
 cameraData->cameraR = cameraData->cameraRDef = cameraR;
 cameraData->cameraP = cameraData->cameraPDef = cameraP;
 cameraData->cameraLookObject = false;
 cameraData->cameraUpdateMode = true;
 for(int loop = 0; loop < rankingMax_m; loop++)
	{
		numbers_m[loop] = ObjectNumbers::Create(10,TEXTURE_NUMBERFONT,D3DXVECTOR3(SCREEN_WIDTH / 2 + 40,SCREEN_HEIGHT / 4.0f + 60 + (60 * loop),0),D3DXVECTOR3(40,40,0));
		number_m[loop] = ObjectNumber::Create(TEXTURE_NUMBERFONT,D3DXVECTOR3(SCREEN_WIDTH / 2 - 220,SCREEN_HEIGHT / 4.0f + 60 + ( 60 * loop ),0),D3DXVECTOR3(48,48,0));
		number_m[loop]->setNumber(loop + 1);
	}
	loadScore();
	Object2D* obj2DPointer;
	Object2D::Create(TEXTURE_RANKINGLOGO,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 5.0f,0),D3DXVECTOR3(500,200,0));
	obj2DPointer = Object2D::Create(TEXTURE_PRESSENETER,D3DXVECTOR3(SCREEN_WIDTH / 2,SCREEN_HEIGHT / 2 + 200,0),D3DXVECTOR3(200,40,0));
	obj2DPointer->setColor(D3DXCOLOR(0.6f,0.6f,1.0f,1.0f));
	obj2DPointer->setColorEd(D3DXCOLOR(0.6f,0.6f,1.0f,0.2f));
	obj2DPointer->setColorSpeed(D3DXCOLOR(0,0,0,0.015f));
	timerSecond_m = timer_m = 0;
	SoundManager::playSound(SOUND_LAVEL_SCENEBGM);
	player_m->getXfileAnim()->AnimCtr.ChangeAnimation(1);
	player_m->getXfileAnim()->AnimCtr.AdvanceTime(1.0f);
 imp_m->treeMnager_m = new TreeManager();

 SystemManager::getObjectFeed()->feedStart(FEED_OUT_PROGRESS);
 return S_OK;
}
//==============================================================================
//解放
void RankingScene::Uninit(void)
{
 SAFE_DELETE(imp_m);
	SoundManager::stopSound();
	Object::releasePriority(PRIORITY_FEED);
}
//==============================================================================
//更新
void RankingScene::Update(void)
{
 imp_m->treeMnager_m->Update();
	player_m->getXfileAnim()->AnimCtr.ChangeAnimation(1);
	player_m->getXfileAnim()->AnimCtr.AdvanceTime(0.01f);
 ObjectBullet::Create(BULLET_PLAYER,player_m->getPos(),player_m->getRot().y);
	timer_m++;
	if(timer_m % 60 == 0)
	{
		timerSecond_m++;
	}
	if((timerSecond_m > 15 || InputKeyboard::getKeyboardTrigger(DIK_RETURN) )&& SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
	{
		SoundManager::playSound(SOUND_LABEL_BUTTON000);
		SystemManager::getObjectFeed()->feedStart(FEED_IN_PROGRESS);

	}
	if(SystemManager::getObjectFeed()->getFeedMode() == FEED_IN_END)
	{
		SceneManager::SetScene(SCENE_TITLE);
	}
}
//==============================================================================
//ロードスコア
void RankingScene::loadScore(void)
{
	//読み込み
	FILE *fp;
	fp = fopen("./data/SAVE/score.bin","rb");
	if(fp != NULL)
	{
		int Seek = 0;
		int *p = 0;
		fseek(fp,0,SEEK_END);
		Seek = ftell(fp) / 4;
		fseek(fp,0,SEEK_SET);
		p = new int[Seek];
		fread(p,sizeof(int),Seek,fp);
		fclose(fp);
		int Temp;
		//並べ替え
		if(Seek >= 1)
		{
			for(int loop = 0;loop < Seek - 1;loop++)
			{
				for(int loop2 = Seek - 1;loop2 > loop;loop2--)
				{
					if(p[loop2] > p[loop2 - 1])
					{
						Temp = p[loop2];
						p[loop2] = p[loop2 - 1];
						p[loop2 - 1] = Temp;
					}
				}
			}
		}
		int number[rankingMax_m] = {0};
		if(Seek >= 1)
		{
			for(int loop = 0;loop < Seek - 1;loop++)
			{
				if(loop > rankingMax_m - 1)
				{
					break;
				}
				number[loop] = p[loop];
			}
		}
		for(int loop = 0;loop < rankingMax_m;loop++)
		{

			numbers_m[loop]->setNumber(number[loop]);
		}
		SAFE_DELETE_ARRAY(p);
	}
}


//End Of FIle

