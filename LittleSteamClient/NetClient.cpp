/******************************************************************************
@file       NetClient.cpp
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#include "NetClient.h"
#include "SystemManager.h"
#include "MessageProc.h"
#include "PlayerManager.h"
//*****************************************************************************
// 静的変数
//*****************************************************************************
SOCKET		NetClient::m_Socket = INVALID_SOCKET;
int			NetClient::m_ID = -1;
SOCKADDR_IN	NetClient::m_ServerAddress;


//=============================================================================
// 初期化処理
//=============================================================================
void NetClient::Init(void)
{
	int ret;

	// IDをランダムに設定（本来ならばユーザID等を設定すべき）
	srand(timeGetTime());
	m_ID = rand();


	char adress[256];
	int port;


	//テキストファイルからアドレス取得
	FILE *file = fopen("adress.txt","rt");
	fscanf(file,"%s",adress);
	fscanf(file,"%d",&port);
	fclose(file);

	// 送信先アドレス
	m_ServerAddress.sin_port = htons((unsigned short)port);
	m_ServerAddress.sin_family = AF_INET;
 m_ServerAddress.sin_addr.s_addr = inet_addr(INADDR_ANY);


	// WinSock初期化
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2,2),&wsaData);



	// ソケット生成
	m_Socket = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);


	// マルチキャスト受信許可
	int value = 1;
	ret = setsockopt(m_Socket,SOL_SOCKET,SO_REUSEADDR,(char*)&value,sizeof(value));


	// 受信アドレス
	sockaddr_in addressServer;
	addressServer.sin_port = htons(20001);
	addressServer.sin_family = AF_INET;
	addressServer.sin_addr.s_addr = INADDR_ANY;


	// バインド
	ret = bind(m_Socket,(sockaddr*)&addressServer,sizeof(addressServer));


	// マルチキャストグループに参加
	ip_mreq mreq;
	memset(&mreq,0,sizeof(mreq));
	mreq.imr_multiaddr.s_addr = inet_addr("239.0.0.17");//マルチキャストアドレス
	mreq.imr_interface.s_addr = INADDR_ANY;
	ret = setsockopt(m_Socket,IPPROTO_IP,IP_ADD_MEMBERSHIP,(char*)&mreq,sizeof(mreq));

 DATA Data = {0};
 Data.Type = DATA_EVENT_TYPE_CONNECT;
 Data.Event.Type = DATA_TYPE_EVENT;
  Data.ID = m_ID;
  // サーバにデータ送信
  ret = sendto(m_Socket,(char*)&Data,sizeof(Data),0,(sockaddr*)&m_ServerAddress,sizeof(m_ServerAddress));

 if(ret == SOCKET_ERROR)
 {
  //printf( "切断: [%d]\n", g_Socket );
  m_Socket = INVALID_SOCKET;
 }



	uintptr_t ptr;
	// 受信スレッド開始
	ptr = _beginthreadex(NULL,0,ReceiveThread,NULL,0,NULL);

	PlayerManager::init();
}


//=============================================================================
// 終了処理
//=============================================================================
void NetClient::Uninit(void)
{

	// ソケット終了
	closesocket(m_Socket);

	// WinSock終了処理
	WSACleanup();

}

//=============================================================================
// 受信スレッド
//=============================================================================
unsigned __stdcall NetClient::ReceiveThread(LPVOID Param)
{
	int ret;
	DATA data;

	while(true)
	{

		// データ受信
		ret = recv(m_Socket,(char*)&data,sizeof(data),0);

		if(ret == SOCKET_ERROR)
		{

		}
		else
		{

			// データタイプ解析
			switch(data.Type)
			{
				//イベント
				case DATA_TYPE_EVENT:
				{
					switch(data.Event.Type)
					{
						case DATA_EVENT_TYPE_DISCONECT:
						{
							if(data.ID != m_ID)
							{
								PlayerManager::removePlayerList(data.ID);
							}
							break;
						}
					}
					break;
				}

				// 座標
				case DATA_TYPE_POSITION:
				{
					if(data.ID != m_ID)
					{

						Object3DXfileAnim *enemy = PlayerManager::getPlayer(data.ID);

						if(enemy)
						{
							D3DXVECTOR3 pos;

							pos.x = data.Position.x;
							pos.y = data.Position.y;
							pos.z = data.Position.z;

							enemy->setPos(pos);
						}
					}

					break;
				}

				// 回転角度
				case DATA_TYPE_ROTATION:
				{

					if(data.ID != m_ID)
					{
						Object3DXfileAnim *enemy = PlayerManager::getPlayer(data.ID);

						if(enemy)
						{
							D3DXVECTOR3 rot;

							rot.x = data.Rotation.x;
							rot.y = data.Rotation.y;
							rot.z = data.Rotation.z;

							enemy->setRot(rot);
						}
					}
				}

				break;
			}

		}
	}
	return 0;

}


//=============================================================================
// データ送信
//=============================================================================
void NetClient::SendData(DATA Data)
{
	// ID設定
	Data.ID = m_ID;

	int ret;

	// サーバにデータ送信
	ret = sendto(m_Socket,(char*)&Data,sizeof(Data),0,(sockaddr*)&m_ServerAddress,sizeof(m_ServerAddress));

	if(ret == SOCKET_ERROR)
	{
		//printf( "切断: [%d]\n", g_Socket );
		m_Socket = INVALID_SOCKET;
	}

}
