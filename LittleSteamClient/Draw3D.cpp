//=============================================================================
//
// シーン3D処理 [Draw3D.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "Draw3D.h"
#include "DrawManager.h"
#include "CameraManager.h"
#include "SystemManager.h"
#include "ObjectManager.h"
#include "Object3D.h"
#include "Object3DBillboard.h"
#include "Object3DFigure.h"
#include "Object3DXfile.h"
#include "Object3DXfileAnim.h"
//==============================================================================
//コンストラクタ
Draw3D::Draw3D(void)
{
 //メンバ初期化
 D3DDevice_m = NULL;
 D3DVtxBuff_m = NULL;
 D3DIndexBuff_m = NULL;
 D3DDevice_m = DrawManager::getD3DDEVICE();
 for(int loop = 0; loop < 2;loop++)
 {
  nor_m[loop] = D3DXVECTOR3(0,1,0);
 }
 numBlockW_m = 1;
 numBlockH_m = 1;
 sizeBlockW_m = 0;
 sizeBlockH_m = 0;
 numVertex_m = ( numBlockW_m + 2 )*( numBlockH_m * 2 ) - 2;//総数検出
 numPolygon_m = ( numBlockW_m + 1 ) * ( numBlockH_m + 1 );//ポリゴン数
 InitPolygon();
}
//==============================================================================
//デストラクタ
Draw3D::~Draw3D(void)
{
 Uninit();
}

//==============================================================================
//解放
void Draw3D::Uninit(void)
{
 SAFE_RELEASE(D3DVtxBuff_m);
 SAFE_RELEASE(D3DIndexBuff_m);
}

//==============================================================================
//描画
void Draw3D::Draw(unsigned int handle,OBJTYPE objType)
{
 Object* obj = nullptr;
 obj = ObjectManager::getGameObjectHandle(handle,objType);

 switch(objType)
 {
  case OBJTYPE_3D:
  Draw3DMode((Object3D*)obj);
  break;
  case OBJTYPE_3DBILLBOARD:
  Draw3DBillboardMode((Object3DBillboard*)obj);
  break;
  case OBJTYPE_FIGURE:
  Draw3DFigure((Object3DFigure*)obj);
  break;
  case OBJTYPE_3DXFILE:
  Draw3DXFile((Object3DXfile*)obj);
  break;
  case OBJTYPE_3DXFILEANIM:
  Draw3DXFileAnim((Object3DXfileAnim*)obj);
  break;
  default:
  break;
 }
 D3DXMatrixIdentity(&mtxWorld_m);

}
//==============================================================================
//頂点バッファ用意
HRESULT Draw3D::InitPolygon(void)
{
 //頂点情報の設定
 if(FAILED(D3DDevice_m->CreateVertexBuffer(	//頂点バッファ作成
  sizeof(VERTEX_3D) * numPolygon_m,
  D3DUSAGE_WRITEONLY,
  FVF_VERTEX_3D,
  D3DPOOL_MANAGED,
  &D3DVtxBuff_m,
  NULL)))
 {
  return E_FAIL;//読み込めなかったらエラー
 }
 //インデックス頂点情報の設定
 if(FAILED(D3DDevice_m->CreateIndexBuffer(	//頂点バッファ作成
  sizeof(DWORD)*numVertex_m,
  D3DUSAGE_WRITEONLY,
  D3DFMT_INDEX32,
  D3DPOOL_MANAGED,
  &D3DIndexBuff_m,
  NULL)))
 {
  return E_FAIL;//読み込めなかったらエラー
 }
 return S_OK;
}
//==============================================================================
//頂点バッファセット
void Draw3D::SetVertexPolygon(D3DXVECTOR3 size,D3DXVECTOR2* texPos,D3DXCOLOR col)
{
 sizeBlockW_m = size.x;
 sizeBlockH_m = size.y;
 VERTEX_3D *pvtx;
 D3DVtxBuff_m->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック VRAMの擬似アドレスをもらう

 int nNum = 0;
 for(int nLoopZ = 0;nLoopZ <= numBlockH_m;nLoopZ++)//Zブロック数+1まで
 {
  for(int nLoopX = 0;nLoopX <= numBlockW_m;nLoopX++)//Xブロック数+1まで
  {
   pvtx[nNum].vtx = D3DXVECTOR3(( sizeBlockW_m / (numBlockW_m)* nLoopX ) - sizeBlockW_m / 2,
           ( sizeBlockH_m / (numBlockH_m)* ( numBlockH_m - nLoopZ ) ) - sizeBlockH_m / 2,
           0);//頂点の座標格納
   pvtx[nNum].diffuse = col; //色設定
   texPos[nNum] = D3DXVECTOR2(( 1.0f / numBlockW_m ) * nLoopX,( 1.0f / numBlockH_m ) * nLoopZ);
   pvtx[nNum].tex = texPos[nNum];
   nNum++;
  }
 }


 //各面の法線算出
 int nNumNor = 0;
 int vtx = 0;
 for(int nBuff = 0; nBuff < numBlockW_m * numBlockH_m;nBuff++)
 {
  D3DXVECTOR3 vec0,vec1;
  D3DXVECTOR3 nor0,nor1;
  vec0 = pvtx[vtx].vtx - pvtx[vtx + numBlockW_m + 1].vtx;
  vec1 = pvtx[vtx + numBlockW_m + 2].vtx - pvtx[vtx + numBlockW_m + 1].vtx;
  D3DXVec3Cross(&nor0,&vec0,&vec1);
  D3DXVec3Normalize(&nor_m[nNumNor],&nor0);

  vec0 = pvtx[vtx + 1].vtx - pvtx[vtx].vtx;
  vec1 = pvtx[vtx + numBlockW_m + 2].vtx - pvtx[vtx].vtx;
  D3DXVec3Cross(&nor0,&vec0,&vec1);
  D3DXVec3Normalize(&nor_m[nNumNor + 1],&nor0);
  nNumNor += 2;

  vtx++;

  if(( vtx + 1 ) % ( numBlockW_m + 1 ) == 0)
  {
   vtx++;
  }
 }

 //面を設定
 for(int nBuff = 0; nBuff < ( numBlockW_m + 1 ) * ( numBlockH_m + 1 );nBuff++)
 {
  D3DXVECTOR3 nor0;
  //最初か最後の頂点か
  if(nBuff == 0 || nBuff == ( numBlockW_m + 1 ) * ( numBlockH_m + 1 ) - 1)
  {
   if(nBuff == 0)
   {
    nor0 = nor_m[nBuff] + nor_m[nBuff + 1];
   }
   else{
    nor0 = nor_m[( numBlockW_m + 1 ) * ( numBlockH_m + 1 ) + 1] + nor_m[( numBlockW_m + 1 ) * ( numBlockH_m + 1 )];
   }
  }
  else
   //角の頂点か
   if(nBuff == numBlockW_m || nBuff == ( numBlockW_m + 1 ) * numBlockH_m)
   {
   if(nBuff == numBlockW_m)
   {
    nor0 = nor_m[(numBlockW_m)* 2 - 1];
   }
   else{
    nor0 = nor_m[numBlockW_m * ( numBlockH_m - 1 )];

   }

   }
   else
    //四隅以外の外側の頂点か
    if(nBuff < numBlockW_m || nBuff % ( numBlockW_m + 1 ) == 0 || ( nBuff + 1 ) % ( numBlockW_m + 1 ) == 0 || nBuff >(numBlockW_m + 1) * numBlockH_m)
    {
   if(nBuff < numBlockW_m)
   {

    nor0 = nor_m[nBuff * 2 - 1] + nor_m[nBuff * 2] + nor_m[nBuff * 2 + 1];

   }
   else
    if(nBuff % ( numBlockW_m + 1 ) == 0)
    {
    int buff = ( nBuff / ( numBlockW_m + 1 ) * ( numBlockW_m * 2 ) );

    nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff - ( numBlockW_m * 2 )];

    }
    else
     if(( nBuff + 1 ) % ( numBlockW_m + 1 ) == 0)
     {
    int buff = ( ( nBuff + 1 ) / ( numBlockW_m + 1 ) * ( numBlockW_m * 2 ) - 1 );

    nor0 = nor_m[buff] + nor_m[buff - 1] + nor_m[buff - ( numBlockW_m * 2 ) - 1];
     }
     else{
      int buff = ( numBlockW_m * ( numBlockH_m - 1 ) * 2 ) - 1 + ( nBuff % numBlockW_m ) * 2;
      nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff + 2];

     }
    }
    else{
     //それ以外の頂点か
     int buff = ( numBlockW_m * 2 ) * ( nBuff / ( numBlockW_m + 1 ) ) + ( ( ( nBuff % ( numBlockW_m + 1 ) ) - 1 ) * 2 );
     nor0 = nor_m[buff] + nor_m[buff + 1] + nor_m[buff + 2] + nor_m[buff - ( numBlockW_m * 2 )] + nor_m[buff + 1 - ( numBlockW_m * 2 )] + nor_m[buff + 2 - ( numBlockW_m * 2 )];
    }
    D3DXVec3Normalize(&nor0,&nor0);
    pvtx[nBuff].nor = nor0;

 }

 D3DVtxBuff_m->Unlock();//ロックの解除を忘れずに 擬似アドレスをVRAMに送る
 DWORD *pIndex;

 D3DIndexBuff_m->Lock(0,0,(void**)&pIndex,0);

 int nIndexNum = 0;//インデックス頂点の上位置
 int nIndexZcnt = numBlockW_m + 1;//下頂点の位置
 int nIndexReturn = 0;//折り返し回数

 for(int nBuff = 0; nBuff < numVertex_m;)
 {
  pIndex[nBuff] = nIndexZcnt;//下の設定
  nBuff++;
  pIndex[nBuff] = nIndexNum;//上の設定
  nIndexZcnt++;//下の数加算
  nBuff++;

  if(numBlockW_m > 0 && nIndexNum == (numBlockW_m)+( ( numBlockW_m + 1 ) * nIndexReturn ) && nBuff < numVertex_m)//上の頂点が横サイズで割り切れれば
  {
   pIndex[nBuff] = nIndexNum;//上の設定
   nBuff++;
   pIndex[nBuff] = nIndexZcnt;//下の設定
   nBuff++;
   nIndexReturn++;
  }
  nIndexNum++;//上の数加算	
 }
 D3DIndexBuff_m->Unlock();
}
//==============================================================================
//描画
void Draw3D::Draw3DMode(Object3D* object3D)
{
 D3DXMATRIX mtxWorld,mtxScl,mtxRot,mtxTranslate;
 LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object3D->getTexName());
 D3DXVECTOR3 pos = object3D->getPos();
 D3DXVECTOR3 rot = object3D->getRot();
 D3DXVECTOR3 scl = object3D->getScl();

 SetVertexPolygon(object3D->getSize(),object3D->getTexPos(),object3D->getColor());
 D3DXMatrixIdentity(&mtxWorld);
 D3DXMatrixScaling(&mtxScl,scl.x,scl.y,scl.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxScl);//スケールの反映

 D3DXMatrixRotationYawPitchRoll(&mtxRot,rot.y,rot.x,rot.z);//YXZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxRot);//回転の反映

 D3DXMatrixTranslation(&mtxTranslate,pos.x,pos.y,pos.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxTranslate);//ワールドの反映

 if(object3D->getDrawAlphaMode())
 {
  //影のセット　加算処理作成
  D3DDevice_m->SetRenderState(D3DRS_BLENDOP,D3DBLENDOP_ADD);
  D3DDevice_m->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
  D3DDevice_m->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_ONE);
 }
  D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000066);
  D3DDevice_m->SetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
  D3DDevice_m->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
 D3DDevice_m->SetTransform(D3DTS_WORLD,&mtxWorld);

 //ポリゴンの描画
 D3DDevice_m->SetStreamSource(0,D3DVtxBuff_m,0,sizeof(VERTEX_3D));//ポリゴンセット

 D3DDevice_m->SetIndices(D3DIndexBuff_m);

 D3DDevice_m->SetFVF(FVF_VERTEX_3D); //頂点フォーマットの設定

 D3DDevice_m->SetTexture(0,texpointer);//テクスチャセット

 D3DDevice_m->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP,	//ポリゴンの設定
           0,
           0,
           numVertex_m,
           0,
           ( numBlockW_m * numBlockH_m ) * 2 + ( ( numBlockH_m - 1 ) * 4 ));//個数

  D3DDevice_m->SetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
  D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000000);
  D3DDevice_m->SetRenderState(D3DRS_ALPHATESTENABLE,FALSE);
  D3DDevice_m->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
  D3DDevice_m->SetRenderState(D3DRS_FILLMODE,D3DFILL_SOLID);
  if(object3D->getDrawAlphaMode())
  {
   //影のセット　加算処理作成
   D3DDevice_m->SetRenderState(D3DRS_BLENDOP,D3DBLENDOP_ADD);
   D3DDevice_m->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
   D3DDevice_m->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
   D3DDevice_m->SetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
  }
 D3DDevice_m->SetRenderState(D3DRS_LIGHTING,TRUE);//ライトON
}
//==============================================================================
//描画ビルボード
void Draw3D::Draw3DBillboardMode(Object3DBillboard* object3DBillboard)
{
 D3DXMATRIX mtxWorld,mtxScl,mtxRot,mtxTranslate;
 CAMERA_DATA camView = SystemManager::getCameraData();
 //ビルボードの行列を出す
 D3DXMatrixIdentity(&mtxWorld);
 mtxWorld._11 = camView.cameraView._11;
 mtxWorld._12 = camView.cameraView._21;
 mtxWorld._13 = camView.cameraView._31;
 mtxWorld._14 = 0.0f;
 mtxWorld._21 = camView.cameraView._12;
 mtxWorld._22 = camView.cameraView._22;
 mtxWorld._23 = camView.cameraView._32;
 mtxWorld._24 = 0.0f;
 mtxWorld._31 = camView.cameraView._13;
 mtxWorld._32 = camView.cameraView._23;
 mtxWorld._33 = camView.cameraView._33;
 mtxWorld._34 = 0.0f;

 D3DXVECTOR3 vDir = camView.cameraR - camView.cameraP;
 if(vDir.x > 0.0f)
 {
  D3DXMatrixRotationY(&mtxWorld,-atanf(vDir.z / vDir.x) + D3DX_PI / 2);
 }
 else
  if(vDir.x == 0.0f)
  {
  //0除算をふせぐため
  D3DXMatrixIdentity(&mtxWorld);
  }
  else
  {
   D3DXMatrixRotationY(&mtxWorld,-atanf(vDir.z / vDir.x) - D3DX_PI / 2);
  }

 D3DDevice_m->SetRenderState(D3DRS_LIGHTING,FALSE);//ライトＯＦＦ
 LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object3DBillboard->getTexName());
 D3DXVECTOR3 pos = object3DBillboard->getPos();
 D3DXVECTOR3 rot = object3DBillboard->getRot();
 D3DXVECTOR3 scl = object3DBillboard->getScl();
 SetVertexPolygon(object3DBillboard->getSize(),object3DBillboard->getTexPos(),object3DBillboard->getColor());
 D3DXMatrixScaling(&mtxScl,scl.x,scl.y,scl.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxScl);//スケールの反映

 D3DXMatrixRotationYawPitchRoll(&mtxRot,rot.y,rot.x,rot.z);//YXZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxRot);//回転の反映

 D3DXMatrixTranslation(&mtxTranslate,pos.x,pos.y,pos.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxTranslate);//ワールドの反映



 D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000066);
  D3DDevice_m->SetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
  D3DDevice_m->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
  if(object3DBillboard->getDrawAlphaMode())
  {
   D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000000);
   D3DDevice_m->SetRenderState(D3DRS_LIGHTING,FALSE);//ライトON
   D3DDevice_m->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
   D3DDevice_m->SetRenderState(D3DRS_BLENDOP,D3DBLENDOP_ADD);
   D3DDevice_m->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
   D3DDevice_m->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_ONE);
  }
 D3DDevice_m->SetTransform(D3DTS_WORLD,&mtxWorld);

 //ポリゴンの描画
 D3DDevice_m->SetStreamSource(0,D3DVtxBuff_m,0,sizeof(VERTEX_3D));//ポリゴンセット

 D3DDevice_m->SetIndices(D3DIndexBuff_m);

 D3DDevice_m->SetFVF(FVF_VERTEX_3D); //頂点フォーマットの設定

 D3DDevice_m->SetTexture(0,texpointer);//テクスチャセット

 D3DDevice_m->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP,	//ポリゴンの設定
           0,
           0,
           numVertex_m,
           0,
           ( numBlockW_m * numBlockH_m ) * 2 + ( ( numBlockH_m - 1 ) * 4 ));//個数
 if(object3DBillboard->getDrawAlphaMode())
 {
  //影のセット　加算処理作成
  D3DDevice_m->SetRenderState(D3DRS_BLENDOP,D3DBLENDOP_ADD);
  D3DDevice_m->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
  D3DDevice_m->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
  D3DDevice_m->SetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);	//影のセット　加算処理作成
  D3DDevice_m->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
  D3DDevice_m->SetRenderState(D3DRS_LIGHTING,TRUE);//ライトON

 }
  D3DDevice_m->SetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
  D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000000);
  D3DDevice_m->SetRenderState(D3DRS_ALPHATESTENABLE,FALSE);
  D3DDevice_m->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
  D3DDevice_m->SetRenderState(D3DRS_LIGHTING,TRUE);//ライトON
  D3DDevice_m->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
}

//描画図形
void Draw3D::Draw3DFigure(Object3DFigure* object3D)
{
 D3DXMATRIX mtxWorld,mtxScl,mtxRot,mtxTranslate;
 LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object3D->getTexName());
 D3DXVECTOR3 pos = object3D->getPos();
 D3DXVECTOR3 rot = object3D->getRot();
 D3DXVECTOR3 scl = object3D->getScl();
 LPDIRECT3DVERTEXBUFFER9 D3DVtxBuff = object3D->getVtxBuff(); //頂点バッファのポインタ
 LPDIRECT3DINDEXBUFFER9 D3DIndexBuff = object3D->getIndexBuff();//インデックスバッファ
 int numVertex = object3D->getNumVtx();
 int numPolygon = object3D->getNumPolygon();
 bool lightMode = object3D->getLightMode();

 D3DXMatrixIdentity(&mtxWorld);
 D3DXMatrixScaling(&mtxScl,scl.x,scl.y,scl.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxScl);//スケールの反映

 D3DXMatrixRotationYawPitchRoll(&mtxRot,rot.y,rot.x,rot.z);//YXZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxRot);//回転の反映

 D3DXMatrixTranslation(&mtxTranslate,pos.x,pos.y,pos.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxTranslate);//ワールドの反映
 if(lightMode)
 {
  D3DDevice_m->SetRenderState(D3DRS_LIGHTING,FALSE);//ライトＯＦＦ
 }
 DrawManager::getD3DDEVICE()->SetSamplerState(0,D3DSAMP_MIPFILTER,D3DTEXF_NONE);

 D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000066);
 D3DDevice_m->SetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
 D3DDevice_m->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
 D3DDevice_m->SetTransform(D3DTS_WORLD,&mtxWorld);

 //ポリゴンの描画
 D3DDevice_m->SetStreamSource(0,D3DVtxBuff,0,sizeof(VERTEX_3D));//ポリゴンセット

 D3DDevice_m->SetIndices(D3DIndexBuff);

 D3DDevice_m->SetFVF(FVF_VERTEX_3D); //頂点フォーマットの設定

 D3DDevice_m->SetTexture(0,texpointer);//テクスチャセット

 D3DDevice_m->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP,	//ポリゴンの設定
           0,
           0,
           numVertex,
           0,
           numPolygon);//個数
 D3DDevice_m->SetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
 D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000000);
 D3DDevice_m->SetRenderState(D3DRS_ALPHATESTENABLE,FALSE);
 D3DDevice_m->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
 DrawManager::getD3DDEVICE()->SetSamplerState(0,D3DSAMP_MIPFILTER,D3DTEXF_LINEAR);
 D3DDevice_m->SetRenderState(D3DRS_LIGHTING,TRUE);//ライトON
}
//==============================================================================
//描画
void Draw3D::Draw3DXFile(Object3DXfile* object3D)
{
 D3DXMATRIX mtxWorld,mtxScl,mtxRot,mtxTranslate;
 LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object3D->getTexName());
 D3DXVECTOR3 pos = object3D->getPos();
 D3DXVECTOR3 rot = object3D->getRot();
 D3DXVECTOR3 scl = object3D->getScl();
 XFILE_DATA* xfile = object3D->getXfile();
 D3DXMATERIAL *pD3DXMat;	//デフォルトマテリアル
 D3DMATERIAL9 matDef;	//退避用マテリアル

 SetVertexPolygon(object3D->getSize(),object3D->getTexPos(),object3D->getColor());
 D3DXMatrixIdentity(&mtxWorld);
 D3DXMatrixScaling(&mtxScl,scl.x,scl.y,scl.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxScl);//スケールの反映

 D3DXMatrixRotationYawPitchRoll(&mtxRot,rot.y,rot.x,rot.z);//YXZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxRot);//回転の反映

 D3DXMatrixTranslation(&mtxTranslate,pos.x,pos.y,pos.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxTranslate);//ワールドの反映
 D3DDevice_m->SetTransform(D3DTS_WORLD,&mtxWorld);

 //ポリゴンの描画
 D3DDevice_m->SetTexture(0,xfile->d3DTexture);
 //描画
 D3DDevice_m->GetMaterial(&matDef);//マテリアル退避
 pD3DXMat = (D3DXMATERIAL*)xfile->d3DXBuffMatModel->GetBufferPointer();
 for(int nCntMat = 0; nCntMat<xfile->numMatModel;nCntMat++)
 {
  D3DDevice_m->SetMaterial(&pD3DXMat[nCntMat].MatD3D);
  xfile->d3DXMeshModel->DrawSubset(nCntMat);//モデルのぱつを描画
 }
 D3DDevice_m->SetMaterial(&matDef);
}

//==============================================================================
//描画
void Draw3D::Draw3DXFileAnim(Object3DXfileAnim* object3D)
{
 D3DXMATRIX mtxWorld,mtxScl,mtxRot,mtxTranslate;
 LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object3D->getTexName());
 D3DXVECTOR3 pos = object3D->getPos();
 D3DXVECTOR3 rot = object3D->getRot();
 D3DXVECTOR3 scl = object3D->getScl();
 XFILEANIM_DATA* xfileAnim = object3D->getXfileAnim();

 SetVertexPolygon(object3D->getSize(),object3D->getTexPos(),object3D->getColor());
 D3DXMatrixIdentity(&mtxWorld);
 D3DXMatrixScaling(&mtxScl,scl.x,scl.y,scl.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxScl);//スケールの反映

 D3DXMatrixRotationYawPitchRoll(&mtxRot,rot.y,rot.x,rot.z);//YXZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxRot);//回転の反映

 D3DXMatrixTranslation(&mtxTranslate,pos.x,pos.y,pos.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxTranslate);//ワールドの反映
 D3DDevice_m->SetTransform(D3DTS_WORLD,&mtxWorld);

 //ポリゴンの描画
 xfileAnim->Alloc.SetWorld(&mtxWorld);
 xfileAnim->Alloc.Draw(D3DDevice_m);
}
//==============================================================================
//描画
void Draw3D::setDraw(Object3D* object3D)
{
 if(object3D->getObjType() == OBJTYPE_FIGURE)
 {
  Object3DFigure* objanim = (Object3DFigure*)object3D;

  D3DXMATRIX mtxWorld,mtxScl,mtxRot,mtxTranslate;
  LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object3D->getTexName());
  D3DXVECTOR3 pos = objanim->getPos();
  D3DXVECTOR3 rot = objanim->getRot();
  D3DXVECTOR3 scl = objanim->getScl();
  LPDIRECT3DVERTEXBUFFER9 D3DVtxBuff = objanim->getVtxBuff(); //頂点バッファのポインタ
  LPDIRECT3DINDEXBUFFER9 D3DIndexBuff = objanim->getIndexBuff();//インデックスバッファ
  int numVertex = objanim->getNumVtx();
  int numPolygon = objanim->getNumPolygon();
  bool lightMode = objanim->getLightMode();

  D3DXMatrixIdentity(&mtxWorld);
  D3DXMatrixScaling(&mtxScl,scl.x,scl.y,scl.z);//XYZ
  D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxScl);//スケールの反映

  D3DXMatrixRotationYawPitchRoll(&mtxRot,rot.y,rot.x,rot.z);//YXZ
  D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxRot);//回転の反映

  D3DXMatrixTranslation(&mtxTranslate,pos.x,pos.y,pos.z);//XYZ
  D3DXMatrixMultiply(&mtxWorld,&mtxWorld,&mtxTranslate);//ワールドの反映
  if(lightMode)
  {
   D3DDevice_m->SetRenderState(D3DRS_LIGHTING,FALSE);//ライトＯＦＦ
  }
  D3DDevice_m->SetRenderState(D3DRS_ALPHAREF,0x00000066);
  D3DDevice_m->SetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
  D3DDevice_m->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
  D3DDevice_m->SetTransform(D3DTS_WORLD,&mtxWorld);

  //ポリゴンの描画
  D3DDevice_m->SetStreamSource(0,D3DVtxBuff,0,sizeof(VERTEX_3D));//ポリゴンセット

  D3DDevice_m->SetIndices(D3DIndexBuff);

  D3DDevice_m->SetFVF(FVF_VERTEX_3D); //頂点フォーマットの設定

  D3DDevice_m->SetTexture(0,texpointer);//テクスチャセット

  D3DDevice_m->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP,	//ポリゴンの設定
            0,
            0,
            numVertex,
            0,
            numPolygon);//個数

 }else
 if(object3D->getObjType() == OBJTYPE_3DXFILE)
 {
  Object3DXfile* objanim = (Object3DXfile*)object3D;
  XFILE_DATA* xfile = objanim->getXfile();
  //描画
  D3DXMATERIAL *pD3DXMat;	//デフォルトマテリアル
  D3DMATERIAL9 matDef;	//退避用マテリアル
  D3DDevice_m->GetMaterial(&matDef);//マテリアル退避
  pD3DXMat = (D3DXMATERIAL*)xfile->d3DXBuffMatModel->GetBufferPointer();
  for(int nCntMat = 0; nCntMat<xfile->numMatModel;nCntMat++)
  {
   D3DDevice_m->SetMaterial(&pD3DXMat[nCntMat].MatD3D);
   xfile->d3DXMeshModel->DrawSubset(nCntMat);//モデルのぱつを描画
  }
  D3DDevice_m->SetMaterial(&matDef);
 }
 else
 if(object3D->getObjType() == OBJTYPE_3DXFILE)
 {
  Object3DXfileAnim* objanim = (Object3DXfileAnim*)object3D;
  XFILEANIM_DATA* xfileAnim = objanim->getXfileAnim();
  //ポリゴンの描画
  xfileAnim->Alloc.SetWorld(&mtxWorld_m);
  xfileAnim->Alloc.Draw(D3DDevice_m);
 }
 D3DDevice_m->SetRenderState(D3DRS_LIGHTING,TRUE);//ライトON
}
//==============================================================================
//描画
void Draw3D::setMtx(Object3D* object3D)
{
 D3DXMATRIX mtxWorld,mtxScl,mtxRot,mtxTranslate;
 LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object3D->getTexName());
 D3DXVECTOR3 pos = object3D->getPos();
 D3DXVECTOR3 rot = object3D->getRot();
 D3DXVECTOR3 scl = object3D->getScl();

 D3DXMatrixIdentity(&mtxWorld_m);
 D3DXMatrixScaling(&mtxScl,scl.x,scl.y,scl.z);//XYZ
 D3DXMatrixMultiply(&mtxWorld_m,&mtxWorld_m,&mtxScl);//スケールの反映

 D3DXMatrixRotationYawPitchRoll(&mtxRot,rot.y,rot.x,rot.z);//YXZ
 D3DXMatrixMultiply(&mtxWorld_m,&mtxWorld_m,&mtxRot);//回転の反映

 D3DXMatrixTranslation(&mtxTranslate,pos.x,pos.y,pos.z);//XYZ

}

//End Of FIle