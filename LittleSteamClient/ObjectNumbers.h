/******************************************************************************/
/*! @addtogroup Object_ObjectNumberss
@file       Object.h
@brief      ObjectNumbersクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECTNUMBERS_FILE
#define COBJECTNUMBERS_FILE
#include "main.h"
#include "Common.h"
#include "Object2D.h"
#include "TextureManager.h"
class ObjectNumber;
class ObjectNumbers: public Object2D
{
	//コンストラクタ
	public:
	/*! ObjectNumbers ObjectNumbersのコンストラクタ
	@param[in]     type    2Dオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectNumbers(int number,TEXTURE_LIST texName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXCOLOR col,PRIORITY_MODE priority,OBJECT2D_TYPE type,OBJTYPE objType = OBJTYPE_2D);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectNumbers ObjectNumbersのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectNumbers(void);
	//操作
	public:
	/*! Create ObjectNumbersの作成
	@param[in]     maxLifeSize   ライフのサイズ
	@param[in]     col    色設定
	@param[in]     texName    テクスチャの名前定義から設定(TEXTURE_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot    角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    2Dのタイプの設定の設定
	*/
	static ObjectNumbers* Create(int number,TEXTURE_LIST texName = TEXTURE_NUMBERFONT,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXCOLOR col = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT2D_TYPE type = OBJCT2D_NORMAL,PRIORITY_MODE priority = PRIORITY_7);/*!< 作成時設定*/
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	void setNumber(int Num){ number_m = Num; };
	int getNumber(void){ return number_m + numberDef_m; };
	void settingNumber(void){ number_m += numberDef_m;numberDef_m = 0; };
 void AddNumber(int Num){ numberDef_m += Num; };
 void setDraw(bool draw){ draw_m = draw; };
	private:
	//メンバ変数
	int number_m;//数字の大きさ
	int numberDef_m;//数字の大きさ
	int numberMax_m;//桁数
	ObjectNumber** numbers_m;
 bool draw_m;
};
#endif

//End Of FIle