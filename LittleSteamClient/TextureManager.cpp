/******************************************************************************
@file       TextureManager.cpp
@brief      テクスチャを管理するクラス テクスチャのポインタを返す
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//==============================================================================
#include "TextureManager.h"
#include "DrawManager.h"
LPDIRECT3DTEXTURE9 TextureManager::d3DTextures_m[TEXTURE_MAX];	// テクスチャへのポインタの配列
//==============================================================================
//コンストラクタ
TextureManager::TextureManager(void)
{
	init();
}
//==============================================================================
//デストラクタ
TextureManager::~TextureManager(void)
{
	for(int loop = 0;loop < TEXTURE_MAX;loop++)
	{
		SAFE_RELEASE(d3DTextures_m[loop]);
	}
}
//==============================================================================
//初期化
HRESULT TextureManager::init(void)
{
	const char* TEXTURE_NAME_LIST[TEXTURE_MAX] =
	{
		"",
		"./data/TEXTURE/fence.dds",
		"./data/TEXTURE/Title.dds",
		"./data/TEXTURE/Result.dds",
		"./data/TEXTURE/Ranking.dds",
		"./data/TEXTURE/GameOver.dds",
		"./data/TEXTURE/GamePose.dds",
		"./data/TEXTURE/Retrun.png",
		"./data/TEXTURE/Retry.png",
		"./data/TEXTURE/Title.png",
		"./data/TEXTURE/ResultLogo.png",
		"./data/TEXTURE/NumFont.dds",
		"./data/TEXTURE/PressStart.png",
		"./data/TEXTURE/lifebar.dds",
		"./data/TEXTURE/skybox.jpg",
		"./data/TEXTURE/Field.jpg",
		"./data/TEXTURE/Mountain.png",
		"./data/TEXTURE/lolil.dds",
  "./data/TEXTURE/Enemy.dds",
  "./data/TEXTURE/Enemy2.dds",
		"./data/TEXTURE/SmokeParticle.dds",
		"./data/TEXTURE/Billding01.dds",
		"./data/TEXTURE/treebill1.png",
		"./data/TEXTURE/treebill2.png",
		"./data/TEXTURE/treebill3.png",
  "./data/TEXTURE/heightmap.bmp",
  "./data/TEXTURE/obhect_map.png",
  "./data/TEXTURE/tutolial.png",
	};
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//ミップマップ使用時のフィルターを設定する。
	//D3DTEXF_NONEを指定するとミップマップを使用しない。デフォルトでD3DTEXF_NONEに設定されているので変更すること。
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	DrawManager::getD3DDEVICE()->SetSamplerState(0,D3DSAMP_MIPFILTER,D3DTEXF_LINEAR);

	//★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★
	//ミップマップの詳細レベル (LOD) バイアスを指定する。
	//これは、使用するミップマップの範囲をテクスチャーのサイズの大きい方か小さい方どちらか一方に偏らせることです。
	//バイアスに負の数を指定すると画面奥がくっきりレンダリングされるようになります。
	//逆に正の数を指定すると画面手前がぼけてレンダリングされるようになります。
	//★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★
	float LODBias = 0;
	DrawManager::getD3DDEVICE()->SetSamplerState(0,D3DSAMP_MIPMAPLODBIAS,*( (LPDWORD)( &LODBias ) ));

	HRESULT hr = S_OK;
	//****************************************************************
	//テクスチャ（＆ミップマップ）を作成
	//****************************************************************
		for(int loop = 0;loop < TEXTURE_MAX;loop++)
		{
			{
				//テクスチャーを作成。
				hr = D3DXCreateTextureFromFileEx(DrawManager::getD3DDEVICE(),
												 TEXTURE_NAME_LIST[loop],
												 D3DX_DEFAULT,
												 D3DX_DEFAULT,
												 //★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
#if		0	//1 :ミップマップ作らない。遠くがくっきり≒ジャギる。使用メモリは最小
												 1,
#elif	0	//2~:ミップマップ数を指定。
												 3,
#else		//0 又は D3DX_DEFAULT:ミップマップ サーフェイスのサイズが1*1になるまで作成される。遠くがぼやける。使用メモリは一番多い。
												 0,	//作成するミップマップの数。
#endif
												 //★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
												 0,
												 D3DFMT_UNKNOWN,
												 D3DPOOL_MANAGED,
												 D3DX_DEFAULT,
												 D3DX_DEFAULT,
												 0,
												 NULL,
												 NULL,
												 &d3DTextures_m[loop]);
				if(FAILED(hr))
				{

				}
			}
		}
	return S_OK;
}

//==============================================================================

//End Of FIle

