//=============================================================================
//
// 描画処理 [Render.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "Render.h"

//==============================================================================
//コンストラクタ
Render::Render(void)
{
	//メンバ初期化
	D3D_m = NULL;
	D3DDevice_m = NULL;
}
//==============================================================================
//デストラクタ
Render::~Render(void)
{
	Uninit();
}
//==============================================================================
//初期化
HRESULT Render::Init(HWND hWnd,BOOL bWindow)
{
	D3DPRESENT_PARAMETERS d3dpp;
	D3DDISPLAYMODE d3ddm;

	// Direct3Dオブジェクトの作成
	D3D_m = Direct3DCreate9(D3D_SDK_VERSION);
	if(D3D_m == NULL)
	{
		return E_FAIL;
	}

	// 現在のディスプレイモードを取得
	if(FAILED(D3D_m->GetAdapterDisplayMode(D3DADAPTER_DEFAULT,&d3ddm)))
	{
		return E_FAIL;
	}

	// デバイスのプレゼンテーションパラメータの設定
	ZeroMemory(&d3dpp,sizeof(d3dpp));							// ワークをゼロクリア
	d3dpp.BackBufferCount = 1;						// バックバッファの数
	d3dpp.BackBufferWidth = SCREEN_WIDTH;				// ゲーム画面サイズ(幅)
	d3dpp.BackBufferHeight = SCREEN_HEIGHT;			// ゲーム画面サイズ(高さ)
	d3dpp.BackBufferFormat = d3ddm.Format;				// バックバッファフォーマットはディスプレイモードに合わせて使う
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;	// 映像信号に同期してフリップする
	d3dpp.Windowed = bWindow;					// ウィンドウモード
	d3dpp.EnableAutoDepthStencil = TRUE;						// デプスバッファ（Ｚバッファ）とステンシルバッファを作成
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;				// デプスバッファとして16bitを使う
	d3dpp.BackBufferFormat = d3ddm.Format;			//カラーモード指定
	d3dpp.FullScreen_RefreshRateInHz = 0;					//リフレッシュレート
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;	//ティアリング行うかどうか

	if(bWindow)
	{// ウィンドウモード
		d3dpp.FullScreen_RefreshRateInHz = 0;								// リフレッシュレート
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;	// インターバル
	}
	else
	{// フルスクリーンモード
		d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;			// リフレッシュレート
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;		// インターバル
	}

	// デバイスオブジェクトの生成
	// [デバイス作成制御]<描画>と<頂点処理>をハードウェアで行なう
	if(FAILED(D3D_m->CreateDevice(D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		&d3dpp,&D3DDevice_m)))
	{
		// 上記の設定が失敗したら
		// [デバイス作成制御]<描画>をハードウェアで行い、<頂点処理>はCPUで行なう
		if(FAILED(D3D_m->CreateDevice(D3DADAPTER_DEFAULT,
			D3DDEVTYPE_HAL,
			hWnd,
			D3DCREATE_SOFTWARE_VERTEXPROCESSING,
			&d3dpp,&D3DDevice_m)))
		{
			// 上記の設定が失敗したら
			// [デバイス作成制御]<描画>と<頂点処理>をCPUで行なう
			if(FAILED(D3D_m->CreateDevice(D3DADAPTER_DEFAULT,
				D3DDEVTYPE_REF,
				hWnd,
				D3DCREATE_SOFTWARE_VERTEXPROCESSING,
				&d3dpp,&D3DDevice_m)))
			{
				// 初期化失敗
				return E_FAIL;
			}
		}
	}

	if(D3D_m != NULL)
	{// Direct3Dオブジェクトの開放
		D3D_m->Release();
		D3D_m = NULL;
	}

	// レンダーステートパラメータの設定
	D3DDevice_m->SetRenderState(D3DRS_CULLMODE,D3DCULL_CCW);				// 裏面をカリング
	D3DDevice_m->SetRenderState(D3DRS_ZENABLE,TRUE);						// Zバッファを使用
	D3DDevice_m->SetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);				// αブレンドを行う
	D3DDevice_m->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);		// αソースカラーの指定
	D3DDevice_m->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);	// αデスティネーションカラーの指定

	// サンプラーステートパラメータの設定
	D3DDevice_m->SetSamplerState(0,D3DSAMP_ADDRESSU,D3DTADDRESS_WRAP);	// テクスチャアドレッシング方法(U値)を設定
	D3DDevice_m->SetSamplerState(0,D3DSAMP_ADDRESSV,D3DTADDRESS_WRAP);	// テクスチャアドレッシング方法(V値)を設定
	D3DDevice_m->SetSamplerState(0,D3DSAMP_MINFILTER,D3DTEXF_LINEAR);	// テクスチャ縮小フィルタモードを設定
	D3DDevice_m->SetSamplerState(0,D3DSAMP_MAGFILTER,D3DTEXF_LINEAR);	// テクスチャ拡大フィルタモードを設定

	// テクスチャステージステートの設定
	D3DDevice_m->SetTextureStageState(0,D3DTSS_ALPHAOP,D3DTOP_MODULATE);	// アルファブレンディング処理
	D3DDevice_m->SetTextureStageState(0,D3DTSS_ALPHAARG1,D3DTA_TEXTURE);	// 最初のアルファ引数
	D3DDevice_m->SetTextureStageState(0,D3DTSS_ALPHAARG2,D3DTA_CURRENT);	// ２番目のアルファ引数
#ifdef _DEBUG

#endif
	return S_OK;
}
//==============================================================================
//解放
void Render::Uninit(void)
{
	SAFE_RELEASE(D3DDevice_m);
	SAFE_RELEASE(D3D_m);

}
//==============================================================================

//End Of FIle

