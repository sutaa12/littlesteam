/******************************************************************************
@file       LightManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"LightManager.h"
#include "DrawManager.h"
#include "MessageProc.h"
D3DLIGHT9 LightManager::light_m[LIGHT_MAX];

//==============================================================================
//コンストラクタ
LightManager::LightManager(void)
{
	for(int loop = 0;loop < LIGHT_MAX;loop++)
	{
		lightPos_m[loop] = D3DXVECTOR3(0,0,0);
	}
}
//==============================================================================
//デストラクタ
LightManager::~LightManager(void)
{

}
//==============================================================================
//初期化
HRESULT LightManager::init(void)
{
	D3DXVECTOR3 vecDir;
	d3DDevice_m = DrawManager::getD3DDEVICE();
	ZeroMemory(&light_m[0],sizeof(D3DLIGHT9) * LIGHT_MAX);//ライト初期化
 for(int loop = 0;loop < LIGHT_MAX;loop++)
 {
  light_m[loop].Diffuse = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
  light_m[loop].Ambient = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
  light_m[loop].Specular = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
  light_m[loop].Direction = D3DXVECTOR3(0,1,0);
  light_m[loop].Type= D3DLIGHT_DIRECTIONAL;

 }
	light_m[0].Type = D3DLIGHT_DIRECTIONAL;//平行光源　種類
	light_m[0].Diffuse = D3DXCOLOR(0.58f,0.55f,0.65f,1.0f);
 vecDir = D3DXVECTOR3(1.0f,-1.0f,-1.0f);
	D3DXVec3Normalize((D3DXVECTOR3*)&light_m[0].Direction,&vecDir);
	//0

	light_m[1].Type = D3DLIGHT_DIRECTIONAL;//平行光源　種類
 light_m[1].Diffuse = D3DXCOLOR(0.45f,0.5f,0.55f,1.0f);
 vecDir = D3DXVECTOR3(0.1f,-1.0f,0.9f);
	D3DXVec3Normalize((D3DXVECTOR3*)&light_m[1].Direction,&vecDir);
	//1

	light_m[2].Type = D3DLIGHT_DIRECTIONAL;//平行光源　種類
 light_m[2].Diffuse = D3DXCOLOR(0.25f,0.2f,0.4f,1.0f);
 vecDir = D3DXVECTOR3(0.7f,0.3f,-1.0f);
	D3DXVec3Normalize((D3DXVECTOR3*)&light_m[2].Direction,&vecDir);
	//2
	light_m[3].Type = D3DLIGHT_DIRECTIONAL;//平行光源　種類
 light_m[3].Diffuse = D3DXCOLOR(0.1f,0.15f,0.2f,1.0f);
	vecDir = D3DXVECTOR3(-20,-20.0f,0.0f);
	D3DXVec3Normalize((D3DXVECTOR3*)&light_m[3].Direction,&vecDir);
	//3
 for(int loop = 0;loop < LIGHT_MAX;loop++)
 {
  d3DDevice_m->SetLight(loop,&light_m[loop]);
  d3DDevice_m->LightEnable(loop,TRUE);
 }

	d3DDevice_m->SetRenderState(D3DRS_LIGHTING,TRUE);

	return S_OK;
}

//==============================================================================
//更新
void LightManager::update(void)
{
}
//End Of FIle

