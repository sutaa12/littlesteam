/******************************************************************************/
/*! @addtogroup Draw_MessageProc
@file       MessageProc.h
@brief      デバッグメッセージ表示
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef MESSAGEPROC_FILE
#define MESSAGEPROC_FILE
#include "Common.h"
#include "main.h"
//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
#define MAX_CHAR (1024)//確保する最大文字数

//メッセージクラス

class MessageProc
{
	public:
	MessageProc(void);//コンストラクタ
	~MessageProc(void);//デストラクタ
	HRESULT Init(void);//初期化
	void Update(void);//更新
	static void Draw(void);
	void Uninit(void);
	static void SetDebugMessage(const char* cMessage,...);
	static bool IsEnableDisp(void){ return m_bDisp; }
	static void EnableDisp(bool bDisp){ m_bDisp = bDisp; }
	private:
	static LPD3DXFONT m_pD3DXFont; //フォント設定
	static char m_aStrDebug[MAX_CHAR];	//デバック用文字
	static bool m_bDisp;//ディスプチェック

	//メンバ変数

};

#endif

//End Of FIle