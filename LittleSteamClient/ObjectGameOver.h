//=============================================================================
//
// ポーズ管理処理 [ObjectGameOver.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef GAMEOVER_FILE
#define GAMEOVER_FILE
#include "main.h"
#include "Object2D.h"
enum GAMEOVER_BUTTOM_MODE
{
	GAMEOVER_RETRY,
	GAMEOVER_TITLE,
	GAMEOVER_RESULT,
	GAMEOVER_MAX
};
//ポーズクラス
class ObjectGameOver: public Object2D
{
	public:
	ObjectGameOver(PRIORITY_MODE nPriority = PRIORITY_7,OBJECT2D_TYPE objType = OBJCT2D_NORMAL);//コンストラクタ
	~ObjectGameOver(void);//デストラクタ

	static ObjectGameOver* Create();//作成時設定

	void init(void);//初期化
	void uninit(void);//解放
	void update(void);//更新
	int getGameOverNum(void){ return m_GameOverButton; }
	void setGameOverFlag(bool flag){ gameOverFlag_m = flag; }

	private:
	//メンバ変数
	Object2D* m_Button[GAMEOVER_MAX];
	Object2D* m_Scene[2];
	bool gameOverFlag_m;
	unsigned int m_GameOverButton;
	float m_fAlfa;//ボタンのアルファ値
	float m_fAlfaSpeed;//アルファスピード
};

#endif

//End Of FIle