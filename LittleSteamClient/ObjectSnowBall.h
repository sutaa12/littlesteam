/******************************************************************************/
/*! @addtogroup Object_ObjectSnowBall
@file       Object.h
@brief      ObjectSnowBallクラス
@date       作成日(2015/01/07)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef OBJECTSNOWBALL_FILE
#define OBJECTSNOWBALL_FILE
#include "main.h"
#include "Object3DFigure.h"
enum SNOW_TYPE{
	SNOW_BALL_PLAYER = 0,
 SNOW_TYPE_MAX
};
enum SNOW_MODE{
 SNOW_NONE,
 SNOW_CLLECT,
 SNOW_SHOT,
 SNOW_DISABLE,
 SNOW_MAX
};

class ObjectSnowBall: public Object3DFigure
{
	//コンストラクタ
	public:
	/*! ObjectSnowBall ObjectSnowBallのコンストラクタ
	@param[in]     type    3DBillboardオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectSnowBall(float rot,PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_FIGURE,OBJECT3D_TYPE type = OBJCT3D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectSnowBall ObjectSnowBallのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectSnowBall(void);
	//操作
	public:
	/*! Create ObjectSnowBallの作成
	@param[in]     bulletType    弾の名前定義から設定(BULLET_TYPE)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static ObjectSnowBall* Create(D3DXVECTOR3 position,float rot,OBJECT3D_TYPE type = OBJCT3D_SNOW_BALL,PRIORITY_MODE priority = PRIORITY_7);
	/*! init 変数を全て初期化
	*/
 bool hitchk(COLLISION_DATA* obj1);
 void SetRotY(float rotY){ rotY_m = rotY; }
 void SetRotX(float rotX){ rotX_m = rotX; }
 void AddRotX(float rotX){ rotX_m += rotX; }
 void setSnowMode(short mode){ shot_mode_m = mode; }
 short getSnowMode(void){ return shot_mode_m; }
 static ObjectSnowBall* getSnowBall(void){ return objectSnowBall_m; }
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	float sclSpeed_m;
 float rotY_m;//進行する角度
 float rotX_m;//進行する角度
	int cnt_m;
	static int snow_ball_num_m;
 short shot_mode_m;
 static ObjectSnowBall* objectSnowBall_m;
};
#endif

//End Of File
