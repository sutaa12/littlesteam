/******************************************************************************
@file       InputManager.cpp
@brief      入力を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//==============================================================================
#include"InputManager.h"
#include "InputKeyboard.h"
#include "InputMouse.h"
//==============================================================================
//初期化
//==============================================================================
LPDIRECTINPUT8 Input::dInput_m = NULL;
//==============================================================================
//コンストラクタ
Input::Input(void)
{

}
//==============================================================================
//デストラクタ
Input::~Input(void)
{
	uninit();
}
//==============================================================================
//初期化
HRESULT Input::init(HINSTANCE hInstance,HWND hWnd)
{
	HRESULT hr;
	if(dInput_m == NULL)//本体ができてない場合
	{
		// DirectInputオブジェクトの作成
		hr = DirectInput8Create(hInstance,
								DIRECTINPUT_VERSION,
								IID_IDirectInput8,
								(void**)&dInput_m,
								NULL);
		if(FAILED(hr))
		{
			//メッセージボックス表示
			MessageBox(NULL,"作成失敗","DI作成失敗",MB_OK);
			return hr;
		}
	}
	return S_OK;
}

//==============================================================================
//解放
void Input::uninit(void)
{
	// DirectInputオブジェクトの開放
	SAFE_RELEASE(dInput_m);
	SAFE_RELEASE(dirDevice_m);
}


//==============================================================================
//コンストラクタ
InputManager::InputManager(void)
{

}
//==============================================================================
//デストラクタ
InputManager::~InputManager(void)
{
	uninit();
}
//==============================================================================
//初期化
HRESULT InputManager::init(HINSTANCE hInstance,HWND hWnd)
{
	inputKeyboard_m = new InputKeyboard;
	inputMouse_m = new InputMouse;
	inputKeyboard_m->init(hInstance,hWnd);
	inputMouse_m->init(hInstance,hWnd);
	return S_OK;
}

//==============================================================================
//解放
void InputManager::uninit(void)
{
	SAFE_DELETE(inputKeyboard_m);
	SAFE_DELETE(inputMouse_m);
}

//==============================================================================
//更新
void InputManager::update(void)
{
	inputKeyboard_m->update();
	inputMouse_m->update();
}

//End Of FIle

