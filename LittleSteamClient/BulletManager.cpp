/******************************************************************************
@file       BulletManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"BulletManager.h"
#include "ObjectBullet.h"
#include"ObjectManager.h"
#include "DrawManager.h"
#include "MessageProc.h"
#include "CollisionManager.h"
ObjectBullet* BulletManager::bulletList_m[BULLET_TYPE_MAX][MAX_BULLET];
//==============================================================================
//コンストラクタ
BulletManager::BulletManager(void)
{
	for(int loop = 0;loop < BULLET_TYPE_MAX;loop++)
	{
		for(int loop1 = 0;loop1 < MAX_BULLET;loop1++)
		{
			bulletList_m[loop][loop1] = nullptr;
		}
	}
}
//==============================================================================
//デストラクタ
BulletManager::~BulletManager(void)
{
}
//==============================================================================
//初期化
HRESULT BulletManager::init(void)
{
	return S_OK;
}

//==============================================================================
//更新
void BulletManager::update(void)
{
}
//==============================================================================
//追加
int BulletManager::addBulletList(BULLET_TYPE type,ObjectBullet* bulletPointer)
{
	for(int loop = 0; loop < MAX_BULLET;loop++)
	{
		if(!bulletList_m[type][loop])
		{
			bulletList_m[type][loop] = bulletPointer;
			return loop;
		}
	}
	return -1;
}
//==============================================================================
//削除
void BulletManager::removeBulletList(BULLET_TYPE type,int num)
{
	if(num >= 0)
	{
		bulletList_m[type][num] = nullptr;
	}
}

bool BulletManager::hitchk(COLLISION_DATA* obj1,BULLET_TYPE type)
{
	for(int loop = 0; loop < MAX_BULLET;loop++)
	{
		if(bulletList_m[type][loop])
		{
			if(bulletList_m[type][loop])
				if(CollisionManager::HitchkCircle(obj1,bulletList_m[type][loop]->getCollisionData()))
				{
				bulletList_m[type][loop]->uninit();
				return true;
				}
		}
	}
	return false;
}
//End Of FIle

