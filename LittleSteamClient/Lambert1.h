//=============================================================================
//
// アニメーション処理処理 [Lambert.h]
// Author : NARITADA SUZUKI
//
//=============================================================================
#ifndef _LAMBERT_H_
#define _LAMBERT_H_
#include "Common.h"
class  LAMBERT1
{
private:
   LPD3DXEFFECT m_pEffect;
   D3DXHANDLE m_pTechnique, m_pWVP, m_pLightDir, m_pAmbient;
   D3DXMATRIX m_matView, m_matProj;
   LPDIRECT3DDEVICE9 m_pd3dDevice;

public:
   LAMBERT1( LPDIRECT3DDEVICE9 pd3dDevice );
   ~LAMBERT1();
   void Invalidate();
   void Restore();
   HRESULT Load();
   void Begin();
   void BeginPass();
   void SetAmbient( float Ambient );
   void SetAmbient( D3DXVECTOR4* pAmbient );
   void SetMatrix( D3DXMATRIX* pMatWorld, D3DXVECTOR4* pLightDir );
   void CommitChanges();
   void EndPass();
   void End();
   BOOL IsOK();
   LPD3DXEFFECT GetEffect(){ return m_pEffect; };
};
#endif
//End Of File