/******************************************************************************/
/*! @addtogroup Draw_TextureManager
@file       TextureManager.h
@brief      テクスチャを管理するクラス テクスチャのポインタを返す
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef TEXTURE_MANAGE_FILE
#define TEXTURE_MANAGE_FILE
#include "Common.h"
/*! TEXTURE_LIST テクスチャの割り当て*/
typedef enum TEXTURE_LIST
{
 TEXTURE_NONE = 0,
 TEXTURE_ERROR,
 TEXTURE_TITLELOGO,
 TEXTURE_RESULTLOGO,
 TEXTURE_RANKINGLOGO,
 TEXTURE_GAMEOVERLOGO,
 TEXTURE_POSELOGO,
 TEXTURE_RETURN,
 TEXTURE_RETRY,
 TEXTURE_TITLE,
 TEXTURE_RESULT,
 TEXTURE_NUMBERFONT,
 TEXTURE_PRESSENETER,
 TEXTURE_LIFEBAR,
 TEXTURE_SKY,
 TEXTURE_FIELD,
 TEXTURE_MOUNTAIN,
 TEXTURE_PLAYER,
 TEXTURE_ENEMY,
 TEXTURE_ENEMY2,
 TEXTURE_SMOKE,
 TEXTURE_BILL01,
 TEXTURE_TREE01,
 TEXTURE_TREE02,
 TEXTURE_TREE03,
 TEXTURE_MINIMAP,
 TEXTURE_MAPCHARACTER,
 TEXTURE_TUTOLIAL,
	TEXTURE_MAX,
};

/*! @class TextureManager
@brief  オブジェクト管理クラス 各オブジェクトの管理クラスの管理をする ハンドルの登録も行う
*/
class TextureManager
{
	//メンバ関数
	//コンストラクタ
	public:
	/*! TextureManager　コンストラクタ*/
	TextureManager(void);
	//デストラクタ
	public:
	/*! ~TextureManager　デストラクタ*/
	~TextureManager(void);

	//操作
	/*! init 用意したテクスチャをポインタにしておく*/
	HRESULT init(void);
	/*! getTexPointer テクスチャのポインタを取得*/
	static LPDIRECT3DTEXTURE9 getTexturePointer(TEXTURE_LIST texturelist){ return d3DTextures_m[texturelist]; }


	//属性
	public:
	static LPDIRECT3DTEXTURE9 d3DTextures_m[TEXTURE_MAX];	// テクスチャへのポインタの配列
	private:

};
#endif

//End Of File