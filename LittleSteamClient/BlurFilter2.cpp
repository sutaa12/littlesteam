
//=============================================================================
//
// 影処理 [DepthBufferShadow.cpp]
// Author : NARITADA SUZUK
//
//=============================================================================
#include"BlurFilter2.h"

BLURFILTER2::BLURFILTER2( LPDIRECT3DDEVICE9 pd3dDevice, D3DPRESENT_PARAMETERS* pd3dParameters ) : D3D2DSQUARE( pd3dDevice, pd3dParameters )
{
   m_pd3dDevice = pd3dDevice;

   m_pEffect = NULL;
}

BLURFILTER2::BLURFILTER2( LPDIRECT3DDEVICE9 pd3dDevice, UINT Width, UINT Height ) : D3D2DSQUARE( pd3dDevice, Width, Height )
{
   m_pd3dDevice = pd3dDevice;

   m_pEffect = NULL;
}

BLURFILTER2::~BLURFILTER2()
{
  SAFE_RELEASE( m_pEffect );
}

void BLURFILTER2::Invalidate()
{
   if( m_pEffect )
      m_pEffect->OnLostDevice();
}

void BLURFILTER2::Restore()
{
   if( m_pEffect )
      m_pEffect->OnResetDevice();
}

HRESULT BLURFILTER2::Load()
{
   D3DCAPS9 caps;
   HRESULT hr;

   m_pd3dDevice->GetDeviceCaps( &caps );
   if( caps.VertexShaderVersion >= D3DVS_VERSION( 1, 1 ) && caps.PixelShaderVersion >= D3DPS_VERSION( 2, 0 ) )
   {
      hr = D3D2DSQUARE::Load();
      if( FAILED( hr ) )
         return -1;

      //シェーダーの初期化
      LPD3DXBUFFER pErr = NULL;
      hr = D3DXCreateEffectFromFile( m_pd3dDevice, ("./data/SHADER/BlurFilter2.fx"), NULL, NULL, 0, NULL, &m_pEffect, &pErr );
      if( FAILED( hr ) )
         return -2;

      m_pTechnique = m_pEffect->GetTechniqueByName( "TShader" );
      m_pTU        = m_pEffect->GetParameterByName( NULL, "m_TU" );
      m_pTV        = m_pEffect->GetParameterByName( NULL, "m_TV" );

      //１テクセルの大きさをセット
      float TU = 1.0f / D3D2DSQUARE::GetWidth();
      float TV = 1.0f / D3D2DSQUARE::GetHeight();
      
      const int Array = 5;

      float u[Array];
      float v[Array];
      for( int i=0; i<Array; i++ )
      {
         u[i] = TU * ( i + 1 );
         v[i] = TV * ( i + 1 );
      }

      m_pEffect->SetFloatArray( m_pTU, u, Array );      
      m_pEffect->SetFloatArray( m_pTV, v, Array );

      m_pEffect->SetTechnique( m_pTechnique );
   }

   else
   {
      return -3;
   }

   return S_OK;
}

void BLURFILTER2::SetTexel( float TU, float TV )
{
   if( m_pEffect )
   {
      m_pEffect->SetFloat( m_pTU, TU );
      m_pEffect->SetFloat( m_pTV, TV );
   }
}

void BLURFILTER2::Render( UINT Pass )
{
   if( m_pEffect )
   {
      m_pEffect->Begin( NULL, 0 );
      m_pEffect->BeginPass( Pass );

      D3D2DSQUARE::Render();   //2Dスプライトのレンダリング

      m_pEffect->EndPass();
      m_pEffect->End();
   }
}

BOOL BLURFILTER2::IsOK()
{
   if( m_pEffect == NULL )
      return FALSE;

   return TRUE;
}
