/******************************************************************************
@file       CameraManager.cpp
@brief      ゲームのカメラを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#include "CameraManager.h"
#include "ObjectManager.h"
#include "InputKeyboard.h"
#include "InputMouse.h"
#include "MessageProc.h"
#include "DrawManager.h"
#include "CollisionManager.h"
#include "Object3D.h"
unsigned int CameraManager::cameraLookObjhudle_m;//カメラ捉えるオブジェのハンドル
OBJTYPE CameraManager::cameraObjtype_m;//オブジェのタイプ
//==============================================================================
//コンストラクタ
CameraManager::CameraManager(void)
{
	//メンバ初期化
	D3DDevice_m = nullptr;
	cameraData_m.cameraP = D3DXVECTOR3(0,0,0);
	cameraData_m.cameraR = D3DXVECTOR3(0,0,0);
	cameraData_m.cameraV = D3DXVECTOR3(0,1.0f,0);
	cameraData_m.cameraPDef = D3DXVECTOR3(0,0,0);
	cameraData_m.rot = D3DXVECTOR3(0,0.001f,0);
	cameraData_m.rotDef = D3DXVECTOR3(0,0,0);
	cameraData_m.cnt = 0;
	D3DXMatrixIdentity(&cameraData_m.cameraView);
	D3DXMatrixIdentity(&cameraData_m.cameraProjection);
	D3DXMatrixIdentity(&cameraData_m.cameraWorld);
	cameraData_m.posSpeed = 30;
	cameraData_m.lookObjDist = 800;
	cameraData_m.rotSpeed = DATA_1BYPI;
	cameraData_m.distance = 2500;
	cameraData_m.cameraR.x = cameraData_m.cameraP.x + sinf(cameraData_m.rot.y)*cameraData_m.distance;
	cameraData_m.cameraR.z = cameraData_m.cameraP.z + cosf(cameraData_m.rot.y)*cameraData_m.distance;
	cameraData_m.cameraRDef = cameraData_m.cameraR;
	cameraData_m.cameraCollision = false;
	cameraData_m.cameraLookObject = false;
	cameraData_m.cameraRotMode = false;
	cameraData_m.posDeffSpeed = cameraData_m.lookDeffSpeed = cameraData_m.rotDeffSpeed = 0.23f;
	cameraData_m.cameraUpdateMode = false;
}
//==============================================================================
//デストラクタ
CameraManager::~CameraManager(void)
{
}
//==============================================================================
//初期化
void CameraManager::init(void)
{
	D3DDevice_m = DrawManager::getD3DDEVICE();	
}

//==============================================================================
void CameraManager::update(void)
{
	if(cameraData_m.cameraUpdateMode)
	{
		if(cameraData_m.cameraLookObject)
		{
			Object3D* target3d;
			target3d = (Object3D*)ObjectManager::getGameObjectHandle(cameraLookObjhudle_m,cameraObjtype_m);
   if(target3d)
   {
    D3DXVECTOR3 rotcamera = target3d->getRot();
    D3DXVECTOR3 cameraR = target3d->getPos();
    //カメラが回転モードなら
    if(cameraData_m.cameraRotMode)
    {
     //ゆっくり回転
     float diffRotY;
     cameraData_m.rotDef.y = rotcamera.y;

     diffRotY = cameraData_m.rotDef.y - cameraData_m.rot.y;
     ROT_CHK(diffRotY);

     cameraData_m.rot.y += diffRotY * cameraData_m.rotDeffSpeed;
     ROT_CHK(cameraData_m.rot.y);

     rotcamera.y = cameraData_m.rot.y;
    }
    else{
     if(rotcamera.y != cameraData_m.rot.y)
     {
      cameraR.x += sinf(rotcamera.y) * ( cameraData_m.lookObjDist / 2.0f );
      cameraR.z += cosf(rotcamera.y) * ( cameraData_m.lookObjDist / 2.0f );
     }
     rotcamera.y = cameraData_m.rotDef.y;
     rotcamera.y = cameraData_m.rot.y;
    }

    cameraR.x += sinf(rotcamera.y) * cameraData_m.lookObjDist;
    cameraR.z += cosf(rotcamera.y) * cameraData_m.lookObjDist;

    cameraData_m.cameraRDef = cameraR;
    //ゆっくり移動
    D3DXVECTOR3 defR,defP;
    defR = cameraData_m.cameraRDef - cameraData_m.cameraR;
    cameraData_m.cameraR += defR * cameraData_m.lookDeffSpeed;

    D3DXVECTOR3 cameraP = D3DXVECTOR3(cameraData_m.cameraR.x - sinf(rotcamera.y) * cameraData_m.distance,cameraR.y + 600.0f,cameraData_m.cameraR.z - cosf(rotcamera.y) * cameraData_m.distance);
    cameraData_m.cameraPDef = cameraP;

    float posY = CollisionManager::HitchkField(cameraData_m.cameraPDef,NULL);
    //前の高さと現在の高さが違いすぎていたら修正
    if(( posY >= cameraData_m.cameraPDef.y + 400 || posY <= cameraData_m.cameraPDef.y - 400 ))
    {
    }
    else
     if(cameraData_m.cameraPDef.y <= posY)
     {
      cameraData_m.cameraPDef.y = posY + 600;
     }

    defP = cameraData_m.cameraPDef - cameraData_m.cameraP;
    cameraData_m.cameraP += D3DXVECTOR3(defP.x * cameraData_m.posDeffSpeed,
                                        defP.y * cameraData_m.posDeffSpeed,
                                        defP.z * cameraData_m.posDeffSpeed);

   }
			else{
				cameraData_m.cameraLookObject = false;
			}
		}
#ifdef _DEBUG
		else{
			if(InputKeyboard::getKeyboardPress(DIK_W))
			{
				cameraData_m.cameraP.x += sinf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraP.z += cosf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraR.x = cameraData_m.cameraP.x + sinf(cameraData_m.rot.y)*cameraData_m.distance;
				cameraData_m.cameraR.z = cameraData_m.cameraP.z + cosf(cameraData_m.rot.y)*cameraData_m.distance;
			}
			if(InputKeyboard::getKeyboardPress(DIK_S))
			{
				cameraData_m.cameraP.x -= sinf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraP.z -= cosf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraR.x = cameraData_m.cameraP.x + sinf(cameraData_m.rot.y)*cameraData_m.distance;
				cameraData_m.cameraR.z = cameraData_m.cameraP.z + cosf(cameraData_m.rot.y)*cameraData_m.distance;
			}
			if(InputKeyboard::getKeyboardPress(DIK_D))
			{
				cameraData_m.cameraP.x += cosf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraP.z -= sinf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraR.x = cameraData_m.cameraP.x + sinf(cameraData_m.rot.y)*cameraData_m.distance;
				cameraData_m.cameraR.z = cameraData_m.cameraP.z + cosf(cameraData_m.rot.y)*cameraData_m.distance;
			}
			if(InputKeyboard::getKeyboardPress(DIK_A))
			{
				cameraData_m.cameraP.x -= cosf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraP.z += sinf(cameraData_m.rot.y)*cameraData_m.posSpeed;

				cameraData_m.cameraR.x = cameraData_m.cameraP.x + sinf(cameraData_m.rot.y)*cameraData_m.distance;
				cameraData_m.cameraR.z = cameraData_m.cameraP.z + cosf(cameraData_m.rot.y)*cameraData_m.distance;
			}
   if(InputKeyboard::getKeyboardPress(DIK_T))
   {
    cameraData_m.cameraP.y+= 10;
   }
   if(InputKeyboard::getKeyboardPress(DIK_B))
   {
    cameraData_m.cameraP.y-= 10;

   }

    if(InputMouse::getMouseRightPress() && InputMouse::getMouseLeftPress())
			{
				POINT mousePos = InputMouse::getMouseSpeed();
				if(mousePos.y != 0)
				{
					cameraData_m.cameraP.y += mousePos.y;
					cameraData_m.cameraR.y += mousePos.y;
				}
			}
			else{
				if(InputMouse::getMouseRightPress())
				{
					POINT mousePos = InputMouse::getMouseSpeed();
					if(mousePos.x != 0)
					{
						cameraData_m.rot.y += mousePos.x / 100.0f;
						cameraData_m.cameraR.x = cameraData_m.cameraP.x + sinf(cameraData_m.rot.y)*cameraData_m.distance;
						cameraData_m.cameraR.z = cameraData_m.cameraP.z + cosf(cameraData_m.rot.y)*cameraData_m.distance;
					}

				}
				if(InputMouse::getMouseLeftPress())
				{
					POINT mousePos = InputMouse::getMouseSpeed();
					if(mousePos.x != 0)
					{
						cameraData_m.rot.y += mousePos.x / 100.0f;
						cameraData_m.cameraP.x = cameraData_m.cameraR.x - ( cameraData_m.distance*sinf(cameraData_m.rot.y) );
						cameraData_m.cameraP.z = cameraData_m.cameraR.z - ( cameraData_m.distance*cosf(cameraData_m.rot.y) );
					}
				}
			}
			cameraData_m.cameraPDef = cameraData_m.cameraP;
			cameraData_m.rotDef = cameraData_m.rot;
			cameraData_m.cameraRDef = cameraData_m.cameraR;
		}
		if(InputKeyboard::getKeyboardTrigger(DIK_F2))
		{
			cameraData_m.cameraLookObject = !cameraData_m.cameraLookObject ? true : false;
		}

		ROTS_CHK(cameraData_m.rot);
#endif
		setCamera();
	}
}
//==============================================================================
//カメラセット
void CameraManager::setCamera(void)
{
	D3DXMATRIX mtxScl,mtxRot,mtxTranslate;

	//ビューマトリックス
	D3DXMatrixIdentity(&cameraData_m.cameraView);
	D3DXMatrixLookAtLH(&cameraData_m.cameraView,&cameraData_m.cameraP,&cameraData_m.cameraR,&cameraData_m.cameraV);
	D3DDevice_m->SetTransform(D3DTS_VIEW,&cameraData_m.cameraView);
	D3DXMatrixIdentity(&cameraData_m.cameraProjection);
	D3DXMatrixPerspectiveFovLH(&cameraData_m.cameraProjection,//マトリックス作成
							   ( D3DX_PI / 4 ),//視野
							   (float)SCREEN_WIDTH / SCREEN_HEIGHT,//アスペクト比
							   10.0f,	//距離
							   60000.0f);//描画距離
	D3DDevice_m->SetTransform(D3DTS_PROJECTION,&cameraData_m.cameraProjection);

	//ブロジェクトマトリックス
	D3DXMatrixIdentity(&cameraData_m.cameraWorld);
	D3DXMatrixScaling(&mtxScl,1.0f,1.0f,1.0f);//XYZ
	D3DXMatrixMultiply(&cameraData_m.cameraWorld,&cameraData_m.cameraWorld,&mtxScl);//スケールの反映

	MessageProc::SetDebugMessage("カメラの位置(%f,%f,%f)\n",cameraData_m.cameraP.x,cameraData_m.cameraP.y,cameraData_m.cameraP.z);
	MessageProc::SetDebugMessage("カメラの注視点(%f,%f,%f)\n",cameraData_m.cameraR.x,cameraData_m.cameraR.y,cameraData_m.cameraR.z);
	MessageProc::SetDebugMessage("カメラの角度(%f,%f,%f)\n",cameraData_m.rot.x,cameraData_m.rot.y,cameraData_m.rot.z);
}
//End Of FIle

