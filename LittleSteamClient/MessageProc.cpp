/******************************************************************************
@file       MessageProc.h
@brief      デバッグメッセージ表示
@date       作成日(2014 / 08 / 20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"MessageProc.h"
#include "DrawManager.h"
#include<string.h>
LPD3DXFONT MessageProc::m_pD3DXFont = NULL;
char MessageProc::m_aStrDebug[MAX_CHAR];
bool MessageProc::m_bDisp = true;

//==============================================================================
//コンストラクタ
MessageProc::MessageProc(void)
{
	Init();
}
//==============================================================================
//デストラクタ
MessageProc::~MessageProc(void)
{
	Uninit();
}
//==============================================================================
//初期化
HRESULT MessageProc::Init(void)
{
	LPDIRECT3DDEVICE9 pDevice = DrawManager::getD3DDEVICE();
	// 情報表示用フォントを設定
	D3DXCreateFont(pDevice,18,0,0,0,FALSE,SHIFTJIS_CHARSET,
				   OUT_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"Terminal",&m_pD3DXFont);
	m_bDisp = false;
#ifdef _DEBUG
	m_bDisp = true;
#endif
	return S_OK;
}

//==============================================================================
//解放
void MessageProc::Uninit(void)
{
	SAFE_RELEASE(m_pD3DXFont);
}
//==============================================================================
//更新
void MessageProc::Update(void)
{
	memset(m_aStrDebug,'\0',strlen(m_aStrDebug));

}
//==============================================================================
//描画
void MessageProc::Draw(void)
{

	if(m_bDisp)
	{
		RECT rectDeb = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};
		// テキスト描画
		m_pD3DXFont->DrawText(NULL,m_aStrDebug,-1,&rectDeb,DT_LEFT,D3DCOLOR_ARGB(0xff,0xff,0xff,0xff));
	}

}

void MessageProc::SetDebugMessage(const char* cMessage,...)
{

	va_list ap;//可変個の引数まとめる
	va_start(ap,cMessage);//個数を格納

	vsprintf(&m_aStrDebug[strlen(m_aStrDebug)],cMessage,ap);//格納
	va_end(ap);//終了
}

//End Of FIle

