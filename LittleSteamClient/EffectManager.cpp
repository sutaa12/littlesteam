/******************************************************************************
@file       EffectManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"EffectManager.h"
#include "ObjectEffect.h"
#include"ObjectManager.h"
#include "DrawManager.h"
#include "CollisionManager.h"
ObjectEffect* EffectManager::effectList_m[EFFECT_TYPE_MAX][MAX_EFFECT];
//==============================================================================
//コンストラクタ
EffectManager::EffectManager(void)
{
	for(int loop = 0;loop < EFFECT_TYPE_MAX;loop++)
	{
		for(int loop1 = 0;loop1 < MAX_EFFECT;loop1++)
		{
			effectList_m[loop][loop1] = nullptr;
		}
	}
}
//==============================================================================
//デストラクタ
EffectManager::~EffectManager(void)
{
}
//==============================================================================
//初期化
HRESULT EffectManager::init(void)
{
	return S_OK;
}

//==============================================================================
//更新
void EffectManager::update(void)
{

}
//==============================================================================
//追加
int EffectManager::addEffectList(EFFECT_TYPE type,ObjectEffect* effectPointer)
{
	for(int loop = 0; loop < MAX_EFFECT;loop++)
	{
		if(!effectList_m[type][loop])
		{
			effectList_m[type][loop] = effectPointer;
			return loop;
		}
	}
	return -1;
}
//==============================================================================
//削除
void EffectManager::removeEffectList(EFFECT_TYPE type,int num)
{
	if(num >= 0)
	{
		effectList_m[type][num] = nullptr;
	}
}

//End Of FIle

