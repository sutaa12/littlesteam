/******************************************************************************
@file       ObjectBar.cpp
@brief      ObjectBarの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectBar.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectBar* ObjectBar::Create(int maxLifeSize,D3DXCOLOR col,D3DXVECTOR3 position,D3DXVECTOR3 size,TEXTURE_LIST texName,D3DXVECTOR3 rot,OBJECT2D_TYPE type,PRIORITY_MODE priority)
{
	ObjectBar *objectPointer;

	objectPointer = new ObjectBar(maxLifeSize,col,priority,type);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setColor(col);
		objectPointer->setSize(size);
		objectPointer->setTexName(texName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectBar::ObjectBar(int maxLifeSize,D3DXCOLOR col,PRIORITY_MODE priority,OBJECT2D_TYPE type,OBJTYPE objType):Object2D(priority,objType,type)
{
	init();
	barLifeMax_m = barLife_m = maxLifeSize;
	barLifeSpeed_m = 0;

}
//==============================================================================
//デストラクタ
ObjectBar::~ObjectBar(void)
{

}
//==============================================================================
//初期化
void ObjectBar::init(void)
{

}
//==============================================================================
//解放
void ObjectBar::uninit(void)
{
	Object2D::uninit();
}
//==============================================================================
//更新
void ObjectBar::update(void)
{
	int barCur = barLife_m;
	barCur += barLifeSpeed_m;
	barLifeSpeed_m = 0;
	if(barCur < 0)
	{
		barCur = 0;
	}
	else
	if(barCur >= barLifeMax_m)
	{
		barCur = barLifeMax_m;
	}
	if(barCur - barLife_m != 0)
	{
		D3DXVECTOR3 size = getSize();
		D3DXVECTOR3 pos = getPos();
		int barDef = barCur - barLife_m;
		size.x += barDef;
		setSize(size);
		pos.x -= (barDef / 2.0f);
		setPos(pos);
		barLife_m = barCur;
	}
	Object2D::update();

}

//End Of FIle