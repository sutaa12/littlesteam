/******************************************************************************/
/*! @addtogroup Object_Object3DBillboard
@file       Object.h
@brief      Object3DBillboardクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECT3DBILBOARD_FILE
#define COBJECT3DBILBOARD_FILE
#include "main.h"
#include "Object3D.h"

class Object3DBillboard: public Object3D
{
	//コンストラクタ
	public:
	/*! Object3DBillboard Object3DBillboardのコンストラクタ
	@param[in]     type    3DBillboardオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	Object3DBillboard(PRIORITY_MODE priority = PRIORITY_5,OBJTYPE objType = OBJTYPE_3DBILLBOARD,OBJECT3D_TYPE type = OBJCT3D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! Object3DBillboard Object3DBillboardのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~Object3DBillboard(void);
	//操作
	public:
	/*! Create Object3Dの作成
	@param[in]     texName    テクスチャの名前定義から設定(TEXTURE_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static Object3DBillboard* Create(TEXTURE_LIST texName = TEXTURE_NONE,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT3D_TYPE type = OBJCT3D_NORMAL,PRIORITY_MODE priority = PRIORITY_7);
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
};
#endif

//End Of File