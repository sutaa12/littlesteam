/******************************************************************************/
/*! @addtogroup Input_InputKeyboard
@file       InputKeyboard.h
@brief      キーボード制御クラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef KEYBORDINPUT_FILE
#define KEYBORDINPUT_FILE
#include "Common.h"
#include "InputManager.h"
class InputKeyboard: public Input
{
	public:
	InputKeyboard(void);//コンストラクタ
	~InputKeyboard(void);//デストラクタ
	HRESULT init(HINSTANCE hInstance,HWND hWnd);//初期化
	void uninit(void);//解放
	void update(void);//更新
	static bool getKeyboardPress(int nKey);
	static bool getKeyboardTrigger(int nKey);
	static bool getKeyboardRepeat(int nKey);
	static bool getKeyboardRelease(int nKey);

	private:
	static const unsigned short MAX_KEY = 256;
	static BYTE akeyState_m[MAX_KEY];//キーボードの状態格納
	static BYTE keyTrigger_m[MAX_KEY];
	static BYTE keyRelease_m[MAX_KEY];
	static BYTE keyRepeat_m[MAX_KEY];
	int repeatCnt_m[MAX_KEY];//リピートのカウント
};
#endif