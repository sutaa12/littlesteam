/******************************************************************************/
/*! @addtogroup Object_ObjectPlayer
@file       Object.h
@brief      ObjectPlayerクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECTPLAYER_FILE
#define COBJECTPLAYER_FILE
#include "Object3DXfileAnim.h"

typedef enum
{
	PLAYER_MODE_DOWN = 0,
	PLAYER_MODE_ATTACK,
	PLAYER_MODE_MOVE,
	PLAYER_MODE_WAIT,
	PLAYER_MODE_NONE,
	PLAYER_MODE_MAX
}PLAYER_MODE;
typedef enum
{
	PLAYER_DIRECTION_NONE = 0,
	PLAYER_DIRECTION_UP,
	PLAYER_DIRECTION_DOWN,
	PLAYER_DIRECTION_LEFT,
	PLAYER_DIRECTION_RIGHT,
	PLAYER_DIRECTION_MAX
}PLAYER_DIRECTION;
class ObjectBar;
class ObjectNumbers;
class Object2D;
class ObjectSnowBall;
class ObjectPlayer: public Object3DXfileAnim
{
	//コンストラクタ
	public:
	/*! ObjectPlayer ObjectPlayerのコンストラクタ
	@param[in]     type    3DXfileオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	ObjectPlayer(PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_3DXFILEANIM,OBJECT3D_TYPE type = OBJCT3D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! ObjectPlayer ObjectPlayerのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	~ObjectPlayer(void);
	//操作
	public:
	/*! Create Object3Dの作成
	@param[in]     xfileName    Xfileの名前定義から設定(XFILEANIM_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot     角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    3Dのタイプの設定の設定
	*/
	static ObjectPlayer* Create(XFILEANIM_LIST xfileName = XFILEANIM_PLAYER,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT3D_TYPE type = OBJCT3D_PLAYER,PRIORITY_MODE priority = PRIORITY_7);
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
 void updatePlayerKey(void);/*!< 入力更新*/
 void updatePlayerAI(void);/*!< AI更新*/
 void updatePlayerAIMove(void);
	void hitChk(void);/*!< 当たり判定*/
 void setAImode(bool aiMode){ aiMode_m = aiMode; }
	//属性
	public:
	static unsigned int getPlalyer(void){ return playerHundle_m; }
	static ObjectNumbers* getScorePlayer(void){ return score_m; }
	private:
	PLAYER_MODE getPlayerMode(void){ return playerMode_m; }
	private:
 static const int PLAYER_GRAVITY = 6;
 static const int PLAYER_JUMP_MAX = 160;
	PLAYER_MODE playerMode_m;//プレイヤーの状態
	float playerAnimTime_m[PLAYER_MODE_MAX];//アニメーション時間
	PLAYER_DIRECTION playerDir_m[2];//プレイヤーがどっちを動いたか
	static unsigned int playerHundle_m;//プレイヤーのハンドル
	int moveCnt;//移動のカウント
	int attackCnt_m;//攻撃カウント
	bool playerDamege_m;
	int damegeCnt_m;
	ObjectBar* barMp_m;
	ObjectBar* barHp_m;
	int hpMax;
	int mpMax;
	int mpCnt_m;
	int playerDownTime;
	static ObjectNumbers* score_m;
	Object2D* damegeFeed_m;
 float jumpSpeed;
 bool aiMode_m;
 Object2D* barHpBack_m;
 Object2D* barMpBack_m;
 ObjectSnowBall* snowBallModel_m;
};
#endif

//End Of File