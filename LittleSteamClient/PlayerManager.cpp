/******************************************************************************
@file       PlayerManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"PlayerManager.h"
#include "ObjectPlayer.h"
#include"ObjectManager.h"
#include "DrawManager.h"
#include "SystemManager.h"
#include "CollisionManager.h"
#include "Object3DXfileAnim.h"
#include "MessageProc.h"
#include "data.h"
Object3DXfileAnim* PlayerManager::enemyList_m[MAX_PLAYER];
int PlayerManager::cnt_m[MAX_PLAYER];
int PlayerManager::enemyNum_m;
bool PlayerManager::update_m;
int PlayerManager::idNeum[MAX_PLAYER];
XFILEANIM_DATA* PlayerManager::xfileData_m;
//==============================================================================
//コンストラクタ
PlayerManager::PlayerManager(void)
{

}
//==============================================================================
//デストラクタ
PlayerManager::~PlayerManager(void)
{
}
//==============================================================================
//リセット
void PlayerManager::resetPlayer(void)
{
	for(int loop = 0;loop < MAX_PLAYER;loop++)
	{
		cnt_m[loop] = 1;
		idNeum[loop] = -1;
	}

}
//==============================================================================
//初期化
HRESULT PlayerManager::init(void)
{
	xfileData_m = XFileManager::getXFileAnimPointer(XFILEANIM_PLAYER);

	for(int loop = 0;loop < MAX_PLAYER;loop++)
	{
		enemyList_m[loop] = nullptr;
		cnt_m[loop] = 1;
		idNeum[loop] = -1;
	}
	update_m = false;

	for(int loop = 0;loop < MAX_PLAYER;loop++)
	{
		cnt_m[loop] = 1;
		idNeum[loop] = -1;
		enemyList_m[loop] = Object3DXfileAnim::Create(XFILEANIM_PLAYER);
		enemyList_m[loop]->setDrawFlag(false);


	}

	return S_OK;
}

//==============================================================================
//更新
void PlayerManager::update(void)
{

	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		MessageProc::SetDebugMessage("ID:%d",idNeum[loop]);
	}
	MessageProc::SetDebugMessage("\n");

}
//==============================================================================
//追加
int PlayerManager::addPlayerList(int num,ObjectPlayer* enemyPointer)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(!enemyList_m[loop])
		{
			enemyList_m[loop] = enemyPointer;
			idNeum[loop] = num;
			return loop;
		}
	}
	return -1;
}
//==============================================================================
//削除
void PlayerManager::removePlayerList(int num)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(idNeum[loop] == num)
		{
			cnt_m[loop] = 1;
			idNeum[loop] = -1;
			if(enemyList_m[loop])
			{
				enemyList_m[loop]->setDrawFlag(false);
			}
		}
	}
}

bool PlayerManager::hitchk(COLLISION_DATA* obj1)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(enemyList_m[loop])
		{
			if(enemyList_m[loop])
				if(CollisionManager::HitchkCircle(obj1,enemyList_m[loop]->getCollisionData()))
				{
				obj1->rotOut = enemyList_m[loop]->getRot();
				return true;
				}
		}
	}
	return false;
}

void PlayerManager::setPlayer(void)
{

}
Object3DXfileAnim* PlayerManager::getPlayer(int id)
{
	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(idNeum[loop] == id)
		{
			return enemyList_m[loop];

		}
	}

	for(int loop = 0; loop < MAX_PLAYER;loop++)
	{
		if(idNeum[loop] == -1)
		{
			idNeum[loop] = id;
			if(enemyList_m[loop])
			{
				enemyList_m[loop]->setDrawFlag(true);
				return enemyList_m[loop];
			}
		}
	}
	return nullptr;

}
//End Of FIle

