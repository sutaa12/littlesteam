/******************************************************************************/
/*! @addtogroup System_FrustumCulling
@file       FrustumCulling.h
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/12/03)
@author     NARITADA SUZUKI
******************************************************************************/
#pragma once
#include <stdio.h>
#include <math.h>
#include "Common.h"
#ifdef	_WIN32
#include <windows.h>
#endif//_WIN32

#ifndef	_WIN32
typedef	unsigned	int	BOOL;
#define	TRUE			1
#define	FALSE			0
#endif//_WIN32

typedef struct _PLANE
{
	float a;
	float b;
	float c;
	float d;
}PLANE;		//平面

typedef struct _FRUSTUM
{
	PLANE	LeftPlane;
	PLANE	RightPlane;
	PLANE	TopPlane;
	PLANE	BottomPlane;
	float	NearClip;
	float	FarClip;
}FRUSTUM;	//視錐台


float Vct3LengthPow2( const D3DXVECTOR3 &sv );

//	|sv|
float Vct3Length( const D3DXVECTOR3 &sv );

//	1/(|sv|*|sv|)
float Vct3LengthPow2Rev( const D3DXVECTOR3 &sv );

//	1/|sv|
float Vct3LengthRev( const D3DXVECTOR3 &sv );

//	ov = sv * f
void Vct3MulScl( D3DXVECTOR3 &ov, const D3DXVECTOR3 &sv, const float f );

//	ov=正規化(sv)
void Vct3Normalize( D3DXVECTOR3 &ov, const D3DXVECTOR3 &sv );

//	ov=正規化(ov)
void Vct3Normalize( D3DXVECTOR3 &osv );

//	内積(sv1・sv2)
float Vct3Dot( const D3DXVECTOR3 &sv1, const D3DXVECTOR3 &sv2 );

//	外積(sv1ｘsv2)
void Vct3Cross( D3DXVECTOR3 &ov, const D3DXVECTOR3 &sv1, const D3DXVECTOR3 &sv2 );

//	ov = sv1 - sv2
void Vct3Sub( D3DXVECTOR3 &ov, const D3DXVECTOR3 &sv1, const D3DXVECTOR3 &sv2 );

//	ビュー行列
void	Mtx4x4LookToLH(	D3DXMATRIX &om, const D3DXVECTOR3	&EyePosition,	const D3DXVECTOR3	&FocusPosition,	const D3DXVECTOR3	&UpDirection	);

//３点から平面パラメータ作成
void	PlaneFromPoints(D3DXVECTOR3 *pP0 , D3DXVECTOR3 *pP1 , D3DXVECTOR3 *pP2 , PLANE *pPlane);

//視野パラメータから視錐台用平面作成
void	SetupFOVClipPlanes( float Angle , float Aspect , float NearClip , float FarClip, FRUSTUM &Frustum );

//視錐台判定
BOOL	MeshFOVCheck(D3DXVECTOR3 *pBSpherePos , float BSphereRadius,FRUSTUM &Frustum,D3DXMATRIX	&mView);


