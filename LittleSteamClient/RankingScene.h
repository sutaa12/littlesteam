//=============================================================================
//
// リザルトシーン [RankingScene.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef RANKINGSCENE_FILE
#define RANKINGSCENE_FILE
#include "main.h"
#include "Scene.h"

class ObjectNumbers;
class ObjectNumber;
class Object3DXfileAnim;
class Imple;
class RankingScene: public Scene
{
	public:
	RankingScene(void);//コンストラクタ
	~RankingScene(void);//デストラクタ
	HRESULT Init(void);//初期化
	void Uninit(void);//解放
	void Update(void);//更新
	void loadScore(void);
	private:
	static const short rankingMax_m = 5;
	ObjectNumbers* numbers_m[rankingMax_m];
	ObjectNumber* number_m[rankingMax_m];
	int timer_m;
	int timerSecond_m;
	Object3DXfileAnim* player_m;
 Imple* imp_m;
};

#endif

//End Of FIle