/******************************************************************************
@file       ObjectMiniMap.cpp
@brief      ObjectMiniMapの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectMiniMap.h"
#include "SystemManager.h"
#include "MessageProc.h"
//================================================================================
//タイプ別作成
//================================================================================
ObjectMiniMap* ObjectMiniMap::Create(D3DXVECTOR3 position,TEXTURE_LIST texName,D3DXCOLOR col,D3DXVECTOR3 size,OBJECT2D_TYPE type,PRIORITY_MODE priority)
{
	ObjectMiniMap *objectPointer;

	objectPointer = new ObjectMiniMap(col,priority,type);
	if(objectPointer)
	{
		objectPointer->setPos(position);
		objectPointer->setColor(col);
		objectPointer->setSize(size);
		objectPointer->setTexName(texName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectMiniMap::ObjectMiniMap(D3DXCOLOR col,PRIORITY_MODE priority,OBJECT2D_TYPE type,OBJTYPE objType):Object2D(priority,objType,type)
{
	init();
 float mapResize = FIELD_MAX / MINI_MAP_SIZE;
 mapResize = MOVE_MAX / mapResize * 2;
 mapMax_m = Object2D::Create(TEXTURE_NONE,pos_m,D3DXVECTOR3(mapResize,mapResize,0));
 mapMax_m->setColor(D3DXCOLOR(1,1,1,0.1f));
}
//==============================================================================
//デストラクタ
ObjectMiniMap::~ObjectMiniMap(void)
{

}
//==============================================================================
//初期化
void ObjectMiniMap::init(void)
{
}
//==============================================================================
//解放
void ObjectMiniMap::uninit(void)
{
	Object2D::uninit();
 mapMax_m->uninit();
}
//==============================================================================
//更新
void ObjectMiniMap::update(void)
{
 mapMax_m->setPos(pos_m);
	for(int end = 0;end < mapOldPos_m.size();end++)
	{
		mapOldPos_m.at(end)->uninit();
	}
	mapOldPos_m.clear();
	Object2D::update();
	for(int end = 0;end < mapPos_m.size();end++)
	{
		mapOldPos_m.push_back(mapPos_m.at(end));
	}
	mapPos_m.clear();
}

void ObjectMiniMap::setMapPos(D3DXVECTOR3 pos,D3DXCOLOR col,float rot,bool size_large)
{

	float mapResize = FIELD_MAX / MINI_MAP_SIZE;
	pos.x = pos.x / mapResize + pos_m.x;
	pos.z = pos.z / mapResize + pos_m.y;
	pos.y = pos.z;
	pos.z = 0.0f;
 float char_size = MAP_CHARACTOR_SIZE;
 if(size_large)
 {
  char_size = MAP_CHARACTOR_SIZE * 2;
 }
 rot -= D3DX_PI;
 rot += rot_m.x;
 ROT_CHK(rot);
 mapPos_m.push_back(( Object2D::Create(TEXTURE_MAPCHARACTER,pos,D3DXVECTOR3(char_size,char_size,1.0f)) ));
	mapPos_m.back()->setColor(col);
 mapPos_m.back()->setRot(D3DXVECTOR3(rot,rot,rot));

}

//End Of FIle