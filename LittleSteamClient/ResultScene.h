//=============================================================================
//
// リザルトシーン [ResultScene.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef RESULTSCENE_FILE
#define RESULTSCENE_FILE
#include "main.h"
#include "Scene.h"
class Impl;
class ObjectNumbers;
class Object3DXfileAnim;
class ResultScene: public Scene
{
	public:
	ResultScene(void);//コンストラクタ
	~ResultScene(void);//デストラクタ
	HRESULT Init(void);//初期化
	void Uninit(void);//解放
	void Update(void);//更新
	void saveScore(void);//保存
	static void setScore(int score){ score_m = score; }
	private:
	static int score_m;
	ObjectNumbers* number_m;
	Object3DXfileAnim* player_m;
 Impl* imp_m;
};

#endif

//End Of FIle