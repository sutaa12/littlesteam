/******************************************************************************
@file       TreeManager.cpp
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include"TreeManager.h"
#include "Object3DBillboard.h"
#include"ObjectManager.h"
#include "DrawManager.h"
#include "CollisionManager.h"
#include "SystemManager.h"
#include "CameraManager.h"
#include "MessageProc.h"
#include "MT.h"
#include "FrustumCulling.h"
//==============================================================================
//コンストラクタ
TreeManager::TreeManager(int posisiton)
{
	TEXTURE_LIST TREETEX[TREE_MAX] =
	{
		TEXTURE_TREE01,
		TEXTURE_TREE02,
		TEXTURE_TREE03
	};
	float TREEHEIGHT[TREE_MAX] =
	{
		950,
		950,
		800,
	};
	init_genrand(MAX_TREE);
	for(int loop = 0;loop < TREE_MAX;loop++)
	{
		for(int loop1 = 0;loop1 < MAX_TREE;loop1++)
		{
			int Distance = FIELD_MAX;
			D3DXVECTOR3 pos = D3DXVECTOR3(( genrand_int32() % Distance ) - Distance / 2.0f,0,( genrand_int32() % Distance ) - Distance / 2.0f);
			pos.y = CollisionManager::HitchkField(pos,NULL);
   pos.y += TREEHEIGHT[loop] + posisiton;
			treeList_m[loop][loop1] = Object3DBillboard::Create(TREETEX[loop],pos,D3DXVECTOR3(1200,2000,0));
			COLLISION_DATA *coltre = treeList_m[loop][loop1]->getCollisionData();
			coltre->radius = 1200;
			coltre->pos = pos;
   treeList_m[loop][loop1]->setUpdateFlag(false);
		}
	}
}
//==============================================================================
//デストラクタ
TreeManager::~TreeManager(void)
{
}
//==============================================================================
//初期化
HRESULT TreeManager::init(void)
{
	return S_OK;
}
//==============================================================================
//更新
void TreeManager::Update(void)
{
	int cnt = 0;
 D3DXMATRIX mtxview = SystemManager::getCameraData().cameraView;
 FRUSTUM	sFrustum;

 SetupFOVClipPlanes(( D3DX_PI / 2 ),//視野
                    (float)SCREEN_WIDTH / SCREEN_HEIGHT,//アスペクト比
                    1.0f,	//距離
                    12000.0f,
                    sFrustum);


	for(int loop = 0;loop < TREE_MAX;loop++)
	{
		for(int loop1 = 0;loop1 < MAX_TREE;loop1++)
		{

   if(MeshFOVCheck(&treeList_m[loop][loop1]->getPos(),treeList_m[loop][loop1]->getCollisionData()->radius * 10,sFrustum,mtxview))  // 視錐台から、外れていたら
    {
				treeList_m[loop][loop1]->setDrawFlag(true);
				cnt++;
			}
			else{
				treeList_m[loop][loop1]->setDrawFlag(false);

			}
		}
	}
	MessageProc::SetDebugMessage("木の本数:%d\n",cnt);

}
//End Of FIle

