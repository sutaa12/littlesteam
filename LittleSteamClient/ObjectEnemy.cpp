/******************************************************************************
@file       ObjectEnemy.cpp
@brief      ObjectEnemyの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "ObjectEnemy.h"
#include "InputKeyboard.h"
#include "SystemManager.h"
#include "CameraManager.h"
#include "MessageProc.h"
#include "ObjectBullet.h"
#include "CollisionManager.h"
#include "ObjectBullet.h"
#include "ObjectEffect.h"
#include "BulletManager.h"
#include "EnemyManager.h"
#include "ObjectManager.h"
#include "ObjectPlayer.h"
#include "ObjectNumbers.h"
#include "SoundManager.h"
#include "ObjectMiniMap.h"
#include "GameScene.h"
#include "FrustumCulling.h"
#include "ObjectSnowBall.h"
#define DRAGON_MOVER_HEIGHT (100)
#define DRAGON_SHOOTER_HEIGHT (900)
//================================================================================
//タイプ別作成
//================================================================================
ObjectEnemy* ObjectEnemy::Create(ENEMY_TYPE enemytype,XFILEANIM_LIST xfileName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT3D_TYPE type,PRIORITY_MODE priority)
{
	ObjectEnemy *objectPointer;

	objectPointer = new ObjectEnemy(enemytype,position,rot,priority);
	if(objectPointer)
	{
		objectPointer->setSize(size);
		objectPointer->setXfile(xfileName);
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
ObjectEnemy::ObjectEnemy(ENEMY_TYPE enemytype,D3DXVECTOR3 position,D3DXVECTOR3 rot,PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object3DXfileAnim(priority,objType)
{
	enemyType_m = enemytype;
	enemyNumber_m = EnemyManager::addEnemyList(enemyType_m,this);
 rotDeffSpeed = 0.1f;
	pos_m = posDest_m = position;
	rot_m.y = rotDest_m.y = rot.y;
	init();
	if(enemyHundle_m < 0)
	{
		uninit();
	}
}
//==============================================================================
//デストラクタ
ObjectEnemy::~ObjectEnemy(void)
{

}
//==============================================================================
//初期化
void ObjectEnemy::init(void)
{
	enemyHundle_m = getObjHandle();
	enemyMode_m = ENEMY_MODE_MOVE;
	posSpeed_m.x = posSpeed_m.z = 10;
	posDeffSpeed = 0.5f;
	rotSpeed_m.y = 0.03f;
	moveCnt = 0;
 height_m = -10;
 height_m = ( DRAGON_MOVER_HEIGHT * scl_m.x ) / 2.0f;
 getCollisionData()->radius = ( DRAGON_MOVER_HEIGHT * scl_m.x ) / 2;
 targetPos_m = D3DXVECTOR3(0,0,0);

	if(enemyType_m != ENEMY_MOVER)
 {
  height_m = 0;
		getCollisionData()->radius = ( DRAGON_SHOOTER_HEIGHT * scl_m.x ) / 2;

	}
	float posY = CollisionManager::HitchkField(pos_m,NULL);
	if(posY != -99999)
	{
		posDest_m.y = pos_m.y = posY + height_m;
	}

	if(enemyType_m == ENEMY_SHOOTER)
	{
		posSpeed_m.x = posSpeed_m.z = 0;
	}
	if(enemyType_m == ENEMY_DRAGON_MOVER)
	{
		posSpeed_m.x = posSpeed_m.z = 40;
	}

	attackCnt_m = 0;
	getCollisionData()->pos = pos_m;
 target_m = false;
}
//==============================================================================
//解放
void ObjectEnemy::uninit(void)
{
	EnemyManager::removeEnemyList(enemyType_m,enemyNumber_m);
	Object3DXfileAnim::uninit();
}
//==============================================================================
//更新
void ObjectEnemy::update(void)
{

	Object3DXfileAnim::update();
	Hitchk();
	moveCnt++;
	moveCnt %= 300;
	COLLISION_DATA col;
	Object3D* target3d;
	target3d = (Object3D*)ObjectManager::getGameObjectHandle(ObjectPlayer::getPlalyer(),OBJTYPE_3DXFILEANIM);
 targetPos_m = target3d->getPos();
 col.pos = target3d->getCollisionData()->pos;
	col.radius = 1000;
 
 if(enemyType_m != ENEMY_MOVER)
	{
		col.radius = 4000;

	}

 if(enemyType_m != ENEMY_SHOOTER)
 {
  if(CollisionManager::HitchkCircle(&col,&collisionData_m))
  {
   if(moveCnt <= 200)
   {
    target_m = true;

    float fRot = 0;
    fRot = atan2f(( targetPos_m.z - pos_m.z ),( targetPos_m.x - pos_m.x ));

    if(( targetPos_m.z - pos_m.z ) == 0 && ( targetPos_m.x - pos_m.x ) == 0)
    {
     fRot = 0;;
    }
    else{
     fRot = -D3DX_PI / 2 + atan2f(-( targetPos_m.z - pos_m.z ),( targetPos_m.x - pos_m.x ));
    }
    rotDest_m.y = fRot;
   }
  }
  else{
   if(targetPos_m == pos_m)
   {
    target_m = false;
   }
   if(moveCnt == 0)
   {
    int Rot = 1000 * DATA_PI;
    rotDest_m.y = ( rand() % ( Rot ) ) / 1000;
    target_m = false;
   }
  }
 }else
	if(enemyType_m == ENEMY_SHOOTER)
	{
		rotDest_m.y = rot_m.y + 0.01f;
		if(moveCnt % 10 == 0)
		{
			D3DXVECTOR3 posBullet = pos_m;
   posBullet.x -= sinf(rot_m.y)*(collisionData_m.radius * 3);
   posBullet.z -= cosf(rot_m.y)*(collisionData_m.radius * 3);
   posBullet.y = pos_m.y + scl_m.x * 700;
   ObjectBullet::Create(BULLET_ENEMY,posBullet,rot_m.y);
		}
 }


//位置制御
 //ゆっくり回転
 ROTS_CHK(rotDest_m);

 float diffRotY;
 diffRotY = rotDest_m.y - rot_m.y;
 ROT_CHK(diffRotY);
 rot_m.y += ( diffRotY * rotDeffSpeed );
 ROTS_CHK(rot_m);
 if((pos_m.x >= -MOVE_MAX || pos_m.x <= MOVE_MAX || pos_m.z >= -MOVE_MAX || pos_m.z <= MOVE_MAX))
 {
  posDest_m.x -= sinf(rot_m.y)*posSpeed_m.x;
  posDest_m.z -= cosf(rot_m.y)*posSpeed_m.z;
 }

 if(pos_m.x <= -MOVE_MAX)
 {
  posDest_m.x = -MOVE_MAX + 1;
  pos_m.x = -MOVE_MAX + 1;

 }
 else
  if(pos_m.x >= MOVE_MAX)
  {
   posDest_m.x = MOVE_MAX - 1;
   pos_m.x = MOVE_MAX - 1;
  }
 if(pos_m.z <= -MOVE_MAX)
 {
  posDest_m.z = -MOVE_MAX + 1;
  pos_m.z = -MOVE_MAX + 1;
 }
 else
  if(pos_m.z >= MOVE_MAX)
  {
   posDest_m.z = MOVE_MAX - 1;
   pos_m.z = MOVE_MAX - 1;
  }

 //移動量セット
 D3DXVECTOR3 posDiff;
 posDiff.x = ( posDest_m.x - pos_m.x ) * posDeffSpeed;
 posDiff.z = ( posDest_m.z - pos_m.z ) * posDeffSpeed;

 pos_m.x += posDiff.x;
 pos_m.z += posDiff.z;
 float posY = CollisionManager::HitchkField(pos_m,NULL);
 //前の高さと現在の高さが違いすぎていたら修正
 if(posY >= pos_m.y + 400 || posY <= pos_m.y - 400)
 {
 }
 else{
  pos_m.y = posY + height_m;
 }
 getCollisionData()->pos = pos_m;

	if(enemyType_m != ENEMY_MOVER)
	{
		if(scl_m.x <= 0.1f)
		{
			if(ObjectPlayer::getScorePlayer())
			{
				ObjectPlayer::getScorePlayer()->AddNumber(150);
				for(int loop = 0;loop < 30;loop++)
				{
					D3DXVECTOR3 pos = pos_m;
     float rot = ( 2 * DATA_PI / 30.0f * loop ) - DATA_PI;
					ObjectEffect::Create(EFFECT_EXPLOSION,pos,rot);
					ObjectEffect::Create(EFFECT_EXPLOSION,pos,rot);
					ObjectEffect::Create(EFFECT_EXPLOSION,pos,rot);
					ObjectEffect::Create(EFFECT_EXPLOSION,pos,rot);
				}
			}
			SoundManager::playSound(SOUND_LAVEL_EXPLOSE);
			uninit();
		}

	}else
	if(scl_m.x >= 6)
	{
		if(ObjectPlayer::getScorePlayer())
		{
			ObjectPlayer::getScorePlayer()->AddNumber(50);
			for(int loop = 0;loop < 30;loop++)
			{
				D3DXVECTOR3 pos = pos_m;
				height_m += height_m;
				float rot = ( 2 * DATA_PI / 30.0f * loop) - DATA_PI;
				ObjectEffect::Create(EFFECT_WATER,pos,rot);
				ObjectEffect::Create(EFFECT_WATER,pos,rot);
				ObjectEffect::Create(EFFECT_WATER,pos,rot);
				ObjectEffect::Create(EFFECT_WATER,pos,rot);
			}
		}
		SoundManager::playSound(SOUND_LAVEL_EXPLOSE);
		uninit();
	}
 if(GameScene::getObjectMiniMap())
 {
  GameScene::getObjectMiniMap()->setMapPos(pos_m,D3DXCOLOR(1.0f,0,0,0.5f));
 }
 FRUSTUM	sFrustum;

 SetupFOVClipPlanes(( D3DX_PI / 4 ),//視野
                    (float)SCREEN_WIDTH / SCREEN_HEIGHT,//アスペクト比
                    100.0f,	//距離
                    60000.0f,
                    sFrustum);

 if(!MeshFOVCheck(&pos_m,collisionData_m.radius * 50,sFrustum,SystemManager::getCameraData().cameraView))  // 視錐台から、外れていたら
 {
  setDrawFlag(false);
 }
 else{
  setDrawFlag(true);

 }
}
//==============================================================================
//当たり判定
void ObjectEnemy::Hitchk(void)
{

	if(BulletManager::hitchk(getCollisionData(),BULLET_PLAYER))
	{
		if(enemyType_m != ENEMY_MOVER)
		{
			scl_m.z -= 0.02f;
			scl_m.x -= 0.02f;
			scl_m.y -= 0.02f;
		}
		else{
			scl_m.x += 0.1f;
			scl_m.y += 0.1f;
			scl_m.z += 0.1f;
		}
	}
 if(ObjectSnowBall::getSnowBall()->hitchk(&collisionData_m))
 {
  float radian = ObjectSnowBall::getSnowBall()->getCollisionData()->radius / 500.0f;
  if(enemyType_m != ENEMY_MOVER)
  {
   scl_m.z -= radian;
   scl_m.x -= radian;
   scl_m.y -= radian;
  }
  else{
   scl_m.x += radian;
   scl_m.y += radian;
   scl_m.z += radian;
  }
 }
 collisionData_m.radius = ( DRAGON_MOVER_HEIGHT * scl_m.x ) / 2.0f;
 height_m = ( DRAGON_MOVER_HEIGHT * scl_m.x ) / 2.0f;
 if(enemyType_m != ENEMY_MOVER)
	{
		collisionData_m.radius = ( DRAGON_SHOOTER_HEIGHT * scl_m.x ) / 2.0f;
  height_m = -( DRAGON_SHOOTER_HEIGHT * scl_m.x ) / 10.0f;

	}

}
//End Of FIle