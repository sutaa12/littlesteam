/******************************************************************************/
/*! @addtogroup Common_SingleTone
@file       SingleTone.h
@brief      シングルトーンのテンプレートクラス
@date       作成日(2014/08/19)
@author     NARITADA SUZUKI
******************************************************************************/
#ifndef SIGLETONE_FILE
#define SIGLETONE_FILE

#include <memory>

/*! @class Singleton
@brief  同じクラスを生成させない 継承して使用する
*/
template <class T>
class Singleton
{
	public:
	/*! singleton　シングルトーンのポインタ取得
	@return         シングルトーンのユニークポインタを返す　ない場合新しく作る
	*/
	static T& singleton()
	{
		static typename T::singleton_pointer_type s_singleton(T::createInstance());

		return getReference(s_singleton);
	}

	private:
	/*! getUpdateFlag　更新フラグ取得
	@return         更新フラグを返す true false
	*/
	typedef std::unique_ptr<T> singleton_pointer_type;

	inline static T *createInstance() { return new T(); }

	inline static T &getReference(const singleton_pointer_type &ptr) { return *ptr; }

	protected:
	/*! Singleton　コンストラクタ*/
	Singleton() {}

	/*! コンストラクタのコピーや移動を止める*/
	private:
	Singleton(const Singleton &) = delete;
	Singleton& operator=( const Singleton & ) = delete;
	Singleton(Singleton &&) = delete;
	Singleton& operator=( Singleton && ) = delete;
};

#endif
//End Of File