//=============================================================================
//
// シーン2D処理 [Draw2D.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "Draw2D.h"
#include "DrawManager.h"
#include "Object2D.h"
//==============================================================================
//コンストラクタ
Draw2D::Draw2D(void)
{
	//メンバ初期化
	D3DDevice_m = NULL;
	D3DVtxBuff_m = NULL;
	D3DIndexBuff_m = NULL;
	D3DDevice_m = DrawManager::getD3DDEVICE();
	InitPolygon();
}
//==============================================================================
//デストラクタ
Draw2D::~Draw2D(void)
{
	Uninit();
}

//==============================================================================
//解放
void Draw2D::Uninit(void)
{
	SAFE_RELEASE(D3DVtxBuff_m);
	SAFE_RELEASE(D3DIndexBuff_m);
}

//==============================================================================
//描画
void Draw2D::Draw(Object2D* object2D)
{
	LPDIRECT3DTEXTURE9 texpointer = TextureManager::getTexturePointer(object2D->getTexName());
	SetVertexPolygon(object2D);
	D3DDevice_m->SetStreamSource(0,D3DVtxBuff_m,0,sizeof(VERTEX_2D));//ポリゴンセット

	D3DDevice_m->SetFVF(FVF_VERTEX_2D); //頂点フォーマットの設定

	D3DDevice_m->SetTexture(0,texpointer);//テクスチャセット

	D3DDevice_m->DrawPrimitive(D3DPT_TRIANGLESTRIP,	//ポリゴンの設定
								0,
								2);//個数
}
//==============================================================================
//頂点バッファ用意
HRESULT Draw2D::InitPolygon(void)
{
	//頂点情報の設定
	if(FAILED(D3DDevice_m->CreateVertexBuffer(	//頂点バッファ作成
		sizeof(VERTEX_2D) * 4,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_2D,
		D3DPOOL_MANAGED,
		&D3DVtxBuff_m,
		NULL)))
	{
		return E_FAIL;//読み込めなかったらエラー
	}

	return S_OK;
}
//==============================================================================
//頂点バッファセット
void Draw2D::SetVertexPolygon(Object2D* object2D)
{
	D3DXVECTOR3 vtx = object2D->getPos();
	D3DXVECTOR3 rot = object2D->getRot();
	float length = object2D->getLength();
	float angle = object2D->getAngle();
	D3DXCOLOR col = object2D->getColor();
	D3DXVECTOR2* tex = object2D->getTexPos();
	VERTEX_2D *pvtx;
	D3DVtxBuff_m->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック VRAMの擬似アドレスをもらう

	pvtx[0].vtx = D3DXVECTOR3(vtx.x + sinf(-angle - rot.z)*length,
							  vtx.y - cosf(-angle - rot.z) * length,0.0f);//頂点の座標格納
	pvtx[1].vtx = D3DXVECTOR3(vtx.x - sinf(-angle + rot.z) * length,
							  vtx.y - cosf(-angle + rot.z)  *length,0.0f);//頂点の座標格納
	pvtx[2].vtx = D3DXVECTOR3(vtx.x - sinf(angle - rot.z)  *length,
							  vtx.y + cosf(angle - rot.z)  *length,0.0f);//頂点の座標格納
	pvtx[3].vtx = D3DXVECTOR3(vtx.x + sinf(angle + rot.z)  *length,
							  vtx.y + cosf(angle + rot.z)  *length,0.0f);//頂点の座標格納

	for(int nloop = 0; nloop <4; nloop++)
	{
		pvtx[nloop].diffuse = col; //色設定
		pvtx[nloop].rhw = 1.0f;
	}

	pvtx[0].tex = tex[0];
	pvtx[1].tex = tex[1];
	pvtx[2].tex = tex[2];
	pvtx[3].tex = tex[3];
	D3DVtxBuff_m->Unlock();//ロックの解除を忘れずに 擬似アドレスをVRAMに送る
}

//End Of FIle