/******************************************************************************
@file       Object.cpp
@brief      Objectの基本クラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//
// シーン処理 [Object.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "Object.h"
#include "ObjectManager.h"
#include "MessageProc.h"
//static初期化
Object* Object::top_m[PRIORITY_MAX];
Object* Object::cur_m[PRIORITY_MAX];
//==============================================================================
//コンストラクタ
//==============================================================================
Object::Object(PRIORITY_MODE priority,OBJTYPE objType)
{
	objHandle = ObjectManager::MAX_OBJECT;
	priority_m = priority;//今のプライオリティセット
	deleteFlag_m = false;
	//ハンドルのセットが出来ない場合削除する
	if(!ObjectManager::setGameObjectHandle(this,objHandle))
	{
		deleteFlag_m = true;
	}
	else{
		deleteFlag_m = false;
	}
	objType_m = objType;
	drawFlag_m = true;
	updateFlag_m = true;
	linkList();
}
//==============================================================================
//解放
//==============================================================================
void Object::release(void)
{
	deleteFlag_m = true;
}
//==============================================================================
//リンク登録
//==============================================================================
void Object::linkList(void)
{
	//先頭オブジェクトが存在しない場合
	if(top_m[priority_m] == nullptr)
	{
		top_m[priority_m] = this;//トップをこのオブジェクトにする
	}

	//最後尾のオブジェクトが存在しない場合
	if(cur_m[priority_m] == nullptr)
	{

		//最後尾のオブジェクトの次のオブジェクトをそのオブジェクトにする
		cur_m[priority_m] = this;
	}
	//現在オブジェクトが先頭の場合　そのオブジェクトの前のオブジェクトをnullptrにする
	if(this == top_m[priority_m])
	{
		this->prevPointer_m = nullptr;
		this->nextPointer_m = nullptr;
	}
	else{
		//現在のオブジェクトが先頭でない場合　このオブジェクトの前のオブジェクトを最後尾にする
		this->nextPointer_m = nullptr;
		this->prevPointer_m = cur_m[priority_m];
		cur_m[priority_m]->nextPointer_m = this;
	}
	//最後尾のオブジェクトを現在のオブジェクトにする
	cur_m[priority_m] = this;
	//このオブジェクトの\次のオブジェクトをnullptrにする
	this->nextPointer_m = nullptr;
}
//==============================================================================
//リンク解除
//==============================================================================
void Object::unLinkList(void)
{
	//アドレスが先頭なら
	if(this == top_m[priority_m])
	{
		if(this->nextPointer_m != nullptr)//一個しかなかったら
		{
			this->nextPointer_m->prevPointer_m = nullptr;//次のアドレスをnullptrに
			top_m[priority_m] = this->nextPointer_m;//先頭アドレスを次に
			if(this == cur_m[priority_m])
			{
				cur_m[priority_m] = this->nextPointer_m;
				//現在のアドレスが同じなら一つ先に
			}
		}
		else{
			cur_m[priority_m] = nullptr;//現在もＮＵＬＬに
			top_m[priority_m] = nullptr;
		}
	}
	else
		//アドレスが最後なら
		if(this->nextPointer_m == nullptr)
		{
		this->prevPointer_m->nextPointer_m = nullptr;
		//一つ前のアドレスを空に
		if(this == cur_m[priority_m])
		{
			cur_m[priority_m] = this->prevPointer_m;
			//現在のアドレスが同じなら一つ前に
		}
		}
		else{
			//中間なら
			this->nextPointer_m->prevPointer_m = this->prevPointer_m;
			//一つ先を前のアドレスの次のアドレスに
			this->prevPointer_m->nextPointer_m = this->nextPointer_m;
			//一つ前を次のアドレスの前のアドレスに
			if(this == cur_m[priority_m])
			{
				cur_m[priority_m] = this->nextPointer_m;
				//現在のアドレスが同じなら一つ前に
			}
		}
		//ハンドルの登録を消す
		ObjectManager::unsetGameObjectHandle(objHandle);
}

//==============================================================================
//全更新(指定した範囲まで
//==============================================================================
void Object::updateAll(void)
{
	int cnt = 0;
	for(int nLoop = 0;nLoop < PRIORITY_MAX;nLoop++)
	{
		Object *objectPointer = top_m[nLoop];
		Object *objectPointerNext;
		int nIdx = 1;
		while(objectPointer)
		{
			objectPointerNext = objectPointer->nextPointer_m;
			if(objectPointer->updateFlag_m)
			{
				objectPointer->update();
			}
			objectPointer = objectPointerNext;
			nIdx++;
			cnt++;
		}
	}

	for(int nLoop = 0;nLoop < PRIORITY_MAX;nLoop++)
	{
		Object *objectPointer = top_m[nLoop];
		Object *objectPointerNext;
		int nIdx = 1;
		while(objectPointer)
		{
			objectPointerNext = objectPointer->nextPointer_m;
			if(objectPointer->deleteFlag_m)
			{
				objectPointer->unLinkList();
				delete objectPointer;
			}
			objectPointer = objectPointerNext;
			nIdx++;
		}
	}
	MessageProc::SetDebugMessage("オブジェの数%d\n",cnt);
}

//==============================================================================
//全解放
//==============================================================================
void Object::releaseAll(void)
{
	for(int nLoop = 0;nLoop < PRIORITY_MAX;nLoop++)
	{

		Object *objectPointer = top_m[nLoop];
		Object *objectPointerNext;
		int nIdx = 0;
		while(objectPointer)
		{
			objectPointerNext = objectPointer->nextPointer_m;

			objectPointer->uninit();
			objectPointer->unLinkList();
			delete objectPointer;
			objectPointer = objectPointerNext;
			nIdx++;
		}
	}
}
//==============================================================================
//プライオリティまで解放
//==============================================================================
void Object::releasePriority(PRIORITY_MODE Priorty)
{
	for(int nLoop = 0;nLoop < Priorty;nLoop++)
	{

		Object *objectPointer = top_m[nLoop];
		Object *objectPointerNext;
		int nIdx = 0;
		while(objectPointer)
		{
			objectPointerNext = objectPointer->nextPointer_m;

			objectPointer->uninit();
			objectPointer->unLinkList();
			delete objectPointer;
			objectPointer = objectPointerNext;
			nIdx++;
		}
	}
}


//==============================================================================
//オブジェ検索
//==============================================================================
void Object::searchObject(OBJTYPE objType,int &nNum,Object** &Scene)
{
	nNum = 0;
	for(int nLoop = 0;nLoop < PRIORITY_MAX;nLoop++)
	{
		Object *objectPointer = top_m[nLoop];
		Object *objectPointerNext;
		int nIdx = 1;
		while(objectPointer)
		{
			objectPointerNext = objectPointer->nextPointer_m;
			if(objectPointer->objType_m == objType)
			{
				nNum++;
			}
			objectPointer = objectPointerNext;
			nIdx++;
		}
	}
	if(nNum == 0)
	{
		return;
	}
	Scene = new Object*[nNum];
	nNum = 0;

	for(int nLoop = 0;nLoop < PRIORITY_MAX;nLoop++)
	{
		Object *objectPointer = top_m[nLoop];
		Object *objectPointerNext;
		int nIdx = 1;
		while(objectPointer)
		{
			objectPointerNext = objectPointer->nextPointer_m;
			if(objectPointer->objType_m == objType)
			{
				Scene[nNum] = objectPointer;
				nNum++;
			}
			objectPointer = objectPointerNext;
			nIdx++;
		}
	}
}


//==============================================================================
//指定したものだけ更新
//==============================================================================
void Object::setUpdateFlagPriority(PRIORITY_MODE Priorty,bool update)
{
	Object *objectPointer = top_m[Priorty];
	Object *objectPointerNext;
	int nIdx = 1;
	while(objectPointer)
	{
		objectPointerNext = objectPointer->nextPointer_m;
		objectPointer->setUpdateFlag(update);
		objectPointer = objectPointerNext;
		nIdx++;
	}

}
//==============================================================================
//プライオリティまで更新フラグセット
//==============================================================================
void Object::updatePrioritySet(PRIORITY_MODE Priorty,bool update)
{
	for(int nLoop = 0;nLoop < Priorty;nLoop++)
	{

		Object *pScene = top_m[nLoop];
		Object *pSceneNext;
		int nIdx = 0;
		while(pScene)
		{
			pSceneNext = pScene->nextPointer_m;

			pScene->setUpdateFlag(update);

			pScene = pSceneNext;
			nIdx++;
		}
	}
}


//==============================================================================
//描画フラグ指定
//==============================================================================
void Object::setDrawFlagPriority(PRIORITY_MODE Priorty,bool flag)
{
	Object *objectPointer = top_m[Priorty];
	Object *objectPointerNext;
	int nIdx = 1;
	while(objectPointer)
	{
		objectPointerNext = objectPointer->nextPointer_m;

		objectPointer->drawFlag_m = flag;
		objectPointer = objectPointerNext;
		nIdx++;
	}
}
