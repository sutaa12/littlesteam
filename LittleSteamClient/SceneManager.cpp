/******************************************************************************/
/*
@file       SceneManager.cpp
@brief      ゲームの各シーンを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#include"SceneManager.h"
#include "TitleScene.h"
#include "GameScene.h"
#include "ResultScene.h"
#include "RankingScene.h"

//static初期化
SCENE_MODE SceneManager::m_NextMod;

//==============================================================================
//コンストラクタ
SceneManager::SceneManager(void)
{

}
//==============================================================================
//デストラクタ
SceneManager::~SceneManager(void)
{
	Uninit();
}
//==============================================================================
//初期化
HRESULT SceneManager::Init(void)
{

	m_CurMode = SCENE_NONE;
	m_NextMod = SCENE_NONE;
	m_pPhase = NULL;

	SetScene(SCENE_TITLE);

	return S_OK;
}
//==============================================================================
//解放
void SceneManager::Uninit(void)
{
	SAFE_DELETE(m_pPhase);
}
//==============================================================================
//更新
void SceneManager::Update(void)
{
	
	//そのフェーズの更新
	if(m_pPhase)
	{
		m_pPhase->Update();
	}
	//もし次のフェーズがあれば
	if(m_NextMod != SCENE_NONE)
	{
		SAFE_DELETE(m_pPhase);
		m_CurMode = m_NextMod;//次のフェーズ導入
		m_NextMod = SCENE_NONE;//次のフェーズなしに

		switch(m_CurMode)
		{
			case SCENE_GAME:
			m_pPhase = new GameScene;
			m_pPhase->Init();
			break;

			case SCENE_TITLE:
			m_pPhase = new TitleScene;
			m_pPhase->Init();
			break;

			case SCENE_RESULT:
			m_pPhase = new ResultScene;
			m_pPhase->Init();
			break;

			case SCENE_RANKING:
			m_pPhase = new RankingScene;
			m_pPhase->Init();
			break;
			default:
			break;

		}

	}
}

//End Of FIle

