/******************************************************************************
@file       SystemManager.cpp
@brief      ゲームの各管理クラスに橋渡しをするクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#include "SystemManager.h"
#include "ObjectManager.h"
#include "DrawManager.h"
#include "MessageProc.h"
#include "InputManager.h"
#include "InputKeyboard.h"
#include "InputMouse.h"
#include "SoundManager.h"
#include "LightManager.h"
#include "CameraManager.h"
#include "SceneManager.h"
#include "EnemyManager.h"
#include "ObjectFeed.h"
#include "EffectManager.h"

CAMERA_DATA* SystemManager::cameraDataPointer_m;
ObjectFeed* SystemManager::objFeed_m;
//コンストラクタ
SystemManager::SystemManager(HINSTANCE hInstance,HWND hWnd,BOOL bWindow)
{
	//各管理クラスを作成
	inputManager_m = new InputManager;
	drawManager_m = new DrawManager;
	soundManager_m = new SoundManager;
	lightManager_m = new LightManager;
	cameraManager_m = new CameraManager;
	sceneManager_m = new SceneManager;
	enemyManager_m = new EnemyManager;
	effectManger_m = new EffectManager;
	fpsTimer_m = 0;
	//引数の必要な物は初期化
	inputManager_m->init(hInstance,hWnd);
	drawManager_m->init(hWnd,bWindow);
	soundManager_m->init(hWnd);
	lightManager_m->init();
	cameraManager_m->init();
	enemyManager_m->init();
	effectManger_m->init();

	objectManager_m = new ObjectManager;
	//受け渡し用ポインター初期化
	cameraDataPointer_m = cameraManager_m->getCameraData();
	objFeed_m = ObjectFeed::Create();
	//一番最後にシーンマネージャー初期化
	sceneManager_m->Init();
}
//デストラクタ
SystemManager::~SystemManager(void)
{
	//各管理クラスを解放
	SAFE_DELETE(sceneManager_m);
	SAFE_DELETE(drawManager_m);
	SAFE_DELETE(objectManager_m);
	SAFE_DELETE(inputManager_m);
	SAFE_DELETE(soundManager_m);
	SAFE_DELETE(lightManager_m);
	SAFE_DELETE(cameraManager_m);
	SAFE_DELETE(enemyManager_m);
	SAFE_DELETE(effectManger_m);
}
//更新
void SystemManager::update(void)
{
	if(InputKeyboard::getKeyboardTrigger(DIK_F4))
	{
		MessageProc::EnableDisp(( !MessageProc::IsEnableDisp() ) ? true : false);
		SoundManager::playSound(SOUND_LABEL_BUTTON000);
	}

	MessageProc::SetDebugMessage("FPS:%d\n",fpsTimer_m);
	//各管理クラスを更新
 lightManager_m->update();
 cameraManager_m->update();
	objectManager_m->update();
	sceneManager_m->Update();
	enemyManager_m->update();
	drawManager_m->drawAll();
	inputManager_m->update();
	effectManger_m->update();
}
