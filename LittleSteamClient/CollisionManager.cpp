/******************************************************************************
@file       CollisionManager.cpp
@brief      ゲームの当たり判定を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//=============================================================================
//インクルード
//=============================================================================
#include "CollisionManager.h"
#include "ObjectManager.h"
unsigned int CollisionManager::fieldHundle_m;
//==============================================================================
//コンストラクタ
CollisionManager::CollisionManager(void)
{
	//メンバ初期化
	objType_m = OBJCT3D_NONE;
}
//==============================================================================
//デストラクタ
CollisionManager::~CollisionManager(void)
{

}


//==============================================================================
//当たり判定のチェック開始
void CollisionManager::UpdateAll(void)
{

}
//==============================================================================
/*! HitchkCircle 球と球の当たり判定*/
bool CollisionManager::HitchkCircle(COLLISION_DATA* obj1,COLLISION_DATA* obj2)
{
	//距離を算出
	D3DXVECTOR3 Length;
	Length = obj1->pos - obj2->pos;

	float fLength = D3DXVec3Length(&Length);

	//2つの距離が2つの半径の合計より小さかったら
	if(fLength < ( obj1->radius + obj2->radius ))
	{
		return true;
	}

	return false;
}
//==============================================================================
/*! HitchkBoard 壁と点の外積の当たり判定*/
void CollisionManager::HitchkBoard(COLLISION_DATA* obj1,COLLISION_DATA* obj2)
{
}
//==============================================================================
/*! CollisionManager::HitchkField 起伏の当たり判定当たり判定*/
float CollisionManager::HitchkField(D3DXVECTOR3 pos,D3DXVECTOR3* pNormal)
{
	if(fieldHundle_m)
	{
		Object* obje;
		Object3D* obje3D;
		obje = ObjectManager::getGameObjectHandle(fieldHundle_m,OBJTYPE_FIGURE);
		if(obje)
		{
			obje3D = (Object3D*)obje;
			if(obje3D->getObject3DType() == OBJCT3D_MESHFIELD)
			{
				Object3DFigure* objField = (Object3DFigure*)obje3D;
				float m_fSizeBlockX = objField->getSize().x;
				float m_fSizeBlockZ = objField->getSize().z;
				int m_nNumBlockX = objField->getNumBlockW();
				int m_nNumBlockZ = objField->getNumBlockH();
				LPDIRECT3DVERTEXBUFFER9 m_pD3DVtxBuff = objField->getVtxBuff();

				float fHeihgt = 0;
				float defPosx = pos.x + m_fSizeBlockX / 2;
				float defPosz = pos.z + m_fSizeBlockZ / 2;
				float xs = m_fSizeBlockX / m_nNumBlockX;
				float ys = m_fSizeBlockZ / m_nNumBlockZ;
				int xp = ( defPosx / xs );
				int zp = ( defPosz / ys );
				zp = ( m_nNumBlockZ - 1 ) - zp;
				D3DXVECTOR3 nor;
				if(pNormal == NULL)
				{
					pNormal = &nor;
				}

				//座標から分割数割り当て
				if(xp >= 0 && zp >= 0 && xp < m_nNumBlockX&& zp < m_nNumBlockZ)
				{
					D3DXVECTOR3 vec0,vec1,vec2,vec3,vec4,vec5;
					float cross0,cross1,cross2;
					VERTEX_3D *pvtx;
					int vtx0 = xp + ( m_nNumBlockX * zp + zp );
					int vtx1 = vtx0 + 1;
					int vtx2 = vtx0 + m_nNumBlockX + 1;
					int vtx3 = vtx0 + m_nNumBlockX + 2;
					m_pD3DVtxBuff->Lock(0,0,(void**)&pvtx,0);//頂点の位置をロック VRAMの擬似アドレスをもらう

					D3DXVECTOR3 vtx[4] = {
						pvtx[vtx0].vtx,pvtx[vtx1].vtx,pvtx[vtx2].vtx,pvtx[vtx3].vtx
					};

					m_pD3DVtxBuff->Unlock();//ロックの解除を忘れずに 擬似アドレスをVRAMに送る

					vec0 = vtx[3] - vtx[0];
					vec1 = pos - vtx[0];
					vec2 = vtx[2] - vtx[3];
					vec3 = pos - vtx[3];
					vec4 = vtx[0] - vtx[2];
					vec5 = pos - vtx[2];
					cross0 = vec0.z * vec1.x - vec0.x * vec1.z;
					cross1 = vec2.z * vec3.x - vec2.x * vec3.z;
					cross2 = vec4.z * vec5.x - vec4.x * vec5.z;

					if(( cross0 >= 0.0f ) && ( cross1 >= 0.0f ) && ( cross2 >= 0.0f ))
					{
						fHeihgt = getHeghitPolygon(vtx[0],vtx[3],vtx[2],pos,pNormal);
						return fHeihgt;

					}
					vec0 = vtx[1] - vtx[0];
					vec1 = pos - vtx[0];
					vec2 = vtx[3] - vtx[1];
					vec3 = pos - vtx[1];
					vec4 = vtx[0] - vtx[3];
					vec5 = pos - vtx[3];
					if(( vec0.z * vec1.x - vec0.x * vec1.z >= 0.0f ) && ( vec2.z * vec3.x - vec2.x * vec3.z >= 0.0f ) && ( vec4.z * vec5.x - vec4.x * vec5.z >= 0.0f ))
					{

						fHeihgt = getHeghitPolygon(vtx[0],vtx[1],vtx[3],pos,pNormal);
						return fHeihgt;
					}
				}

				return fHeihgt;
			}
		}
	}
	return -99999;
}

//高さ算出
float CollisionManager::getHeghitPolygon(D3DXVECTOR3& P0,D3DXVECTOR3& P1,D3DXVECTOR3& P2,D3DXVECTOR3& pos,D3DXVECTOR3* pNormal)
{
	D3DXVECTOR3 vec0,vec1;
	vec0 = P1 - P0;
	vec1 = P2 - P0;
	D3DXVec3Cross(pNormal,&vec0,&vec1);
	D3DXVec3Normalize(pNormal,pNormal);
	if(pNormal->y == 0.0f)
	{
		return 0.0f;
	}
	//計算
	float fHeight = P0.y - ( pNormal->x * ( pos.x - P0.x ) +
							pNormal->z * ( pos.z - P0.z ) ) / pNormal->y;
	return fHeight;
}
//==============================================================================
/*! Hitchk 指定したものと当たり判定(相手側は入力がなければ全ての3Dオブジェクトを参照)*/
void CollisionManager::Hitchk(OBJECT3D_TYPE objTypeA,OBJECT3D_TYPE objTypeB)
{
	unsigned int num;
	Object** objList = nullptr;
	Object3D* obj3DA = nullptr;
	objList = ObjectManager::getGameObjectHandleNotOnly(OBJTYPE_2D,num);
	objType_m = objTypeA;
	if(objList)
	{
		//3Dオブジェのリストを全て参照する
		for(int loop = 0; loop < num;loop++)
		{
			obj3DA = (Object3D*)objList[loop];
			if(obj3DA->getObject3DType() == objTypeA && obj3DA->getCollisionData()->collision)
			{
				Object3D* obj3DB = nullptr;
				//調べる対象のオブジェが全てなら
				if(objTypeB == OBJCT3D_NONE)
				{
					//3Dオブジェのリストを全て参照する
					for(int loop2 = 0; loop2 < num;loop2++)
					{
						obj3DB = (Object3D*)objList[loop];
						if(obj3DB->getObject3DType() != objTypeA && obj3DB->getCollisionData()->collision)
						{

						}
					}
				}
				else{

				}
			}
		}
	}
}
	//End Of FIle

