/******************************************************************************/
/*! @addtogroup Light_LightManager
@file       LightManager.h
@brief      ゲームのライトを管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef CLIGHT_FILE
#define CLIGHT_FILE
#include "Common.h"
#include "main.h"
typedef enum LIGHT_NUMBER
{
	LIGHT_0 = 0,
	LIGHT_1,
	LIGHT_2,
	LIGHT_3,
	LIGHT_MAX
};

//ライト管理クラス
class LightManager
{
	public:
	LightManager(void);//コンストラクタ
	~LightManager(void);//デストラクタ
	HRESULT init(void);//初期化
	void update(void);//ライト更新
	void enableLight(LIGHT_NUMBER lightNum){ d3DDevice_m->LightEnable(lightNum,TRUE); }//番号のライト有効化
	void disableLight(LIGHT_NUMBER lightNum){ d3DDevice_m->LightEnable(lightNum,TRUE); }//番号のライト無効化
	static D3DLIGHT9 getLightData(LIGHT_NUMBER lightNum){ return light_m[lightNum]; }
	static void setLightData(LIGHT_NUMBER lightNum,D3DLIGHT9 light){ light_m[lightNum] = light; }
	void SetPositionLight(LIGHT_NUMBER lightNum,D3DXVECTOR3 lightPos){ lightPos_m[lightNum] = lightPos;D3DXVec3Normalize((D3DXVECTOR3*)&light_m[lightNum].Direction,&lightPos_m[lightNum]); };
	private:
	//メンバ変数
	static D3DLIGHT9 light_m[LIGHT_MAX];
	D3DXVECTOR3 lightPos_m[LIGHT_MAX];
	LPDIRECT3DDEVICE9 d3DDevice_m;
};

#endif

//End Of FIle