//=============================================================================
//
// シーン2D処理 [Draw2D.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef DRAW2D_FILE
#define DRAW2D_FILE
#include "main.h"
#include "Common.h"

//シーン2Dクラス
class Object2D;
class Draw2D
{
	public:
	Draw2D(void);//コンストラクタ
	~Draw2D(void);//デストラクタ

	HRESULT Init(LPDIRECT3DDEVICE9 pD3DDevice);//初期化
	void Uninit(void);//解放
	void Draw(Object2D* object2D);//描画

	private:
	HRESULT InitPolygon(void);//頂点バッファ用意
	void SetVertexPolygon(Object2D* object2D);//頂点バッファセット

	//メンバ変数

	// Direct3D オブジェクト
	LPDIRECT3DDEVICE9	D3DDevice_m;//デバイスのポインタ
	LPDIRECT3DVERTEXBUFFER9 D3DVtxBuff_m; //頂点バッファのポインタ
	LPDIRECT3DINDEXBUFFER9 D3DIndexBuff_m;//インデックスバッファ

};

#endif

//End Of FIle