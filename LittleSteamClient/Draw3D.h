//=============================================================================
//
// シーン3D処理 [Draw3D.h]
// Author : NARITADA SUZUKI
//
//=============================================================================

#ifndef DRAW3D_FILE
#define DRAW3D_FILE
#include "main.h"
#include "Object.h"
#include "Common.h"
//シーン3Dクラス
class Object3D;
class Object3DBillboard;
class Object3DFigure;
class Object3DXfile;
class Object3DXfileAnim;
class Draw3D
{
	public:
	Draw3D(void);//コンストラクタ
	~Draw3D(void);//デストラクタ

	HRESULT Init(LPDIRECT3DDEVICE9 pD3DDevice);//初期化
	void Uninit(void);//解放
	void Draw(unsigned int handle,OBJTYPE objType);//3Dの種類選別して描画

	void setMtx(Object3D* obj3D);
	void setMtxWorld(D3DXMATRIX mat){ mtxWorld_m = mat; }
	void setDraw(Object3D* object3D);
 D3DXMATRIX getMtx(void){ return mtxWorld_m; }
 void setMtx(D3DXMATRIX mtx){ mtxWorld_m = mtx; }

	private:
	HRESULT InitPolygon(void);//頂点バッファ用意
	void SetVertexPolygon(D3DXVECTOR3 size,D3DXVECTOR2* texPos,D3DXCOLOR col);//頂点バッファセット

	void Draw3DMode(Object3D* object3D);
	void Draw3DBillboardMode(Object3DBillboard* object3D);
	void Draw3DFigure(Object3DFigure* object3D);
	void Draw3DXFile(Object3DXfile* object3D);
	void Draw3DXFileAnim(Object3DXfileAnim* object3D);
	//メンバ変数

	// Direct3D オブジェクト
	LPDIRECT3DDEVICE9	D3DDevice_m;//デバイスのポインタ
	LPDIRECT3DVERTEXBUFFER9 D3DVtxBuff_m; //頂点バッファのポインタ
	LPDIRECT3DINDEXBUFFER9 D3DIndexBuff_m;//インデックスバッファ

	int numBlockW_m;
	int	numBlockH_m;				// ブロック数
	int numVertex_m;							// 総頂点数
	int numPolygon_m;							// 総ポリゴン数
	float sizeBlockW_m;// ブロックサイズ
	float sizeBlockH_m;
	
	D3DXVECTOR3 nor_m[2];//法線
	D3DXMATRIX mtxWorld_m;
};

#endif

//End Of FIle