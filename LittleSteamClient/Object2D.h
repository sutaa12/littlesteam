/******************************************************************************/
/*! @addtogroup Object_Object2D
@file       Object.h
@brief      Object2Dクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/

#ifndef COBJECT2D_FILE
#define COBJECT2D_FILE
#include "main.h"
#include "Common.h"
#include "Object.h"
#include "TextureManager.h"

//2Dの種類
typedef enum
{
	OBJCT2D_NORMAL
}OBJECT2D_TYPE;


class Object2D: public Object
{
	//コンストラクタ
	public:
	/*! Object2D Object2Dのコンストラクタ
	@param[in]     type    2Dオブジェクトの種類設定
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/
	Object2D(PRIORITY_MODE priority = PRIORITY_7,OBJTYPE objType = OBJTYPE_2D,OBJECT2D_TYPE type = OBJCT2D_NORMAL);/*!< コンストラクタ*/
	//デストラクタ
	public:
	/*! Object2D Object2Dのデストラクタ
	@param[in]     priority    プライオリティの設定
	@param[in]     objType    オブジェタイプの設定
	*/	
	~Object2D(void);
	//操作
	public:
	/*! Create Object2Dの作成
	@param[in]     texName    テクスチャの名前定義から設定(TEXTURE_LIST)
	@param[in]     position    中心位置設定
	@param[in]     size    大きさの設定
	@param[in]     rot    角度の設定
	@param[in]     priority    プライオリティの設定
	@param[in]     type    2Dのタイプの設定の設定
	*/
	static Object2D* Create(TEXTURE_LIST texName = TEXTURE_NONE,D3DXVECTOR3 position = D3DXVECTOR3(0,0,0),D3DXVECTOR3 size = D3DXVECTOR3(0,0,0),D3DXVECTOR3 rot = D3DXVECTOR3(0,0,0),OBJECT2D_TYPE type = OBJCT2D_NORMAL,PRIORITY_MODE priority = PRIORITY_7);/*!< 作成時設定*/
	/*! init 変数を全て初期化
	*/
	void init(void);/*!< 初期化*/
	/*! uninit 解放deleteフラグ立てる
	*/
	void uninit(void);/*!< 解放*/
	/*! update 更新
	*/
	void update(void);/*!< 更新*/
	//属性
	public:
	/*! setPos 位置の設定
	@param[in]     pos    位置の設定 x,y,z
	*/
	void setPos(D3DXVECTOR3 pos){ pos_m = pos; }
	/*! getPos 位置の取得
	@return     位置の取得
	*/
	D3DXVECTOR3 getPos(void){ return pos_m; }
	/*! getLength 中心からの位置取得
	@return     中心からの位置取得
	*/
	float getLength(void){ return length_m; } 
	/*! getAngle 中心からの角度の取得
	@return     中心からの角度の取得
	*/
	float getAngle(void){ return angle_m; }
	/*! getAngle サイズの取得
	@return     サイズの取得
	*/
	D3DXVECTOR3 getSize(void){ return size_m; }
	/*! setSize サイズの設定
	@param[in]     size    サイズの設定 width,height,depth
	*/
	void setSize(D3DXVECTOR3 size){ size_m = size; }
	/*! setRot 角度の設定
	@param[in]     rot    角度の設定
	*/
	void setRot(D3DXVECTOR3 rot){ rot_m = rot; }
	/*! getPos 角度の加速度の設定
	@param[in]     rotSpeed    角度の加速度の設定
	*/
	void setRotSpeed(D3DXVECTOR3 rotSpeed) { rotSpeed_m = rotSpeed; }
	/*! getRot 角度の取得
	@return     角度の取得
	*/
	D3DXVECTOR3 getRot(void){ return rot_m; }
	/*! setColor 色の設定
	@param[in]     col    色の設定 0〜255の色
	*/
	void setColor(D3DXCOLOR col){ col_m = colSt_m = col; }
	/*! setColorSpeed 色の変化する速度の設定
	@param[in]     col    色の変化する速度の設定
	*/
	void setColorSpeed(D3DXCOLOR col){ colSpeed_m = col; }
	/*! setColorSpeed rgbaすべての色の変化する速度をいっぺんに設定
	@param[in]     col    色の設定 0〜255の色
	*/
	void setColorSpeed(float col){ colSpeed_m = D3DXCOLOR(col,col,col,col); }
	/*! setColorSt 開始する色の設定
	@param[in]     col    開始する色の設定
	*/
	void setColorSt(D3DXCOLOR col){ colSt_m = col; }
	/*! setColorEd 折り返す色の設定
	@param[in]     col    折り返す色の設定
	*/
	void setColorEd(D3DXCOLOR col){ colEd_m = col; }
	/*! getRot 色の取得
	@return     現在の色を返す
	*/
	D3DXCOLOR getColor(void){ return col_m; }
	/*! getTexPos テクスチャーの位置取得
	@return     ４つの位置の配列の先頭を返す
	*/
	D3DXVECTOR2* getTexPos(void){ return &texPos_m[0]; }
	/*! setTexPos テクスチャーの定義設定
	@param[in]     texName    定義したいテクスチャ名(TEXTURE_LIST)
	*/
	void setTexName(TEXTURE_LIST texName){ textureName_m = texName; }
	/*! getTexName テクスチャーの名前取得
	@return     テクスチャの名前を返す
	*/
	TEXTURE_LIST getTexName(void){ return textureName_m; }
	protected:
	//メンバ変数
	D3DXVECTOR3 pos_m;/*!< 座標*/
	D3DXVECTOR3 rot_m;/*!< 回転角度*/
	D3DXVECTOR3 rotSpeed_m;/*!< 回転移動量*/
	D3DXVECTOR3 size_m;/*!< 大きさ*/
	D3DXVECTOR2 texPos_m[4];/*!< テクスチャーの位置４つ*/
	D3DXCOLOR col_m;/*!< 色*/
	D3DXCOLOR colSpeed_m;/*!< 色の変化量*/
	D3DXCOLOR colSt_m;/*!< 変化させたい色*/
	D3DXCOLOR colEd_m;/*!< 変化させたい色*/
	float length_m;/*!< 中心から頂点までの長さ*/
	float angle_m;/*!< 中心から頂点までの角度*/
	OBJECT2D_TYPE type2D_m;/*!< 2Dオブジェクトの種類*/
	TEXTURE_LIST textureName_m;//テクスチャのマクロから取得
};
#endif

//End Of FIle