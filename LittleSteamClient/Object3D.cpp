/******************************************************************************
@file       Object3D.cpp
@brief      Object3Dの基本データ
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************/
//インクルード
//==============================================================================
#include "Object3D.h"
#include "CameraManager.h"
//================================================================================
//タイプ別作成
//================================================================================
Object3D* Object3D::Create(TEXTURE_LIST texName,D3DXVECTOR3 position,D3DXVECTOR3 size,D3DXVECTOR3 rot,OBJECT3D_TYPE type,PRIORITY_MODE nPriority)
{
	Object3D *objectPointer;

	objectPointer = new Object3D(nPriority);
	if(objectPointer)
	{
		return objectPointer;//アドレスを返す
	}
	return nullptr;
}
//==============================================================================
//コンストラクタ
Object3D::Object3D(PRIORITY_MODE priority,OBJTYPE objType,OBJECT3D_TYPE type):Object(priority,objType)
{
	//メンバ初期化
	pos_m = posDest_m = posSpeed_m = posOld_m = D3DXVECTOR3(0,0,0);
	scl_m = D3DXVECTOR3(1.0f,1.0f,1.0f);
	rot_m = rotDest_m = rotOld_m = D3DXVECTOR3(0,0,0);
	rotSpeed_m = D3DXVECTOR3(0,0,0);
	texPos_m[0] = D3DXVECTOR2(0.0f,0.0f);
	texPos_m[1] = D3DXVECTOR2(1.0f,0.0f);
	texPos_m[2] = D3DXVECTOR2(0.0f,1.0f);
	texPos_m[3] = D3DXVECTOR2(1.0f,1.0f);
	size_m = D3DXVECTOR3(0,0,0);
	col_m = colSt_m = colEd_m = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
	colSpeed_m = D3DXCOLOR(0,0,0,0);
	rot_m.z += rotSpeed_m.z;
	type3D_m = type;
	textureName_m = TEXTURE_NONE;
	lightMode = false;
	posDeffSpeed = rotDeffSpeed = 1.0f;
	height_m = 0;
	drawAlphaMode_m = false;
	collisionData_m.collision = false;
	collisionData_m.pos = collisionData_m.posOut = pos_m;
	collisionData_m.size = size_m;
	collisionData_m.radius = 0;
	collisionData_m.rotOut = rot_m;
	collisionData_m.coltype = HIT_CIRCLE;
	for(int loop = 0;loop < OBJCT3D_MAX;loop++)
	{
		collisionData_m.collisionChk[loop] = false;
	}
	init();

}
//==============================================================================
//デストラクタ
Object3D::~Object3D(void)
{

}
//==============================================================================
//初期化
void Object3D::init(void)
{

}
//==============================================================================
//解放
void Object3D::uninit(void)
{
	release();
}
//==============================================================================
//更新
void Object3D::update(void)
{
 rot_m += rotSpeed_m;
 ROTS_CHK(rot_m);
	col_m += colSpeed_m;
	COLOR_CHK(colSpeed_m.r,col_m.r,colSt_m.r,colEd_m.r);
	COLOR_CHK(colSpeed_m.g,col_m.g,colSt_m.g,colEd_m.g);
	COLOR_CHK(colSpeed_m.b,col_m.b,colSt_m.b,colEd_m.b);
	COLOR_CHK(colSpeed_m.a,col_m.a,colSt_m.a,colEd_m.a);
	ROTS_CHK(rot_m);
	posOld_m = pos_m;
	rotOld_m = rot_m;
}

//End Of FIle