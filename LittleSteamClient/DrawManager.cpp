/******************************************************************************
@file       DrawManager.cpp
@brief      ゲームの描画を管理するクラス
@date       作成日(2014/08/20)
@author     NARITADA SUZUKI
******************************************************************************///=============================================================================

//=============================================================================
//インクルード
//==============================================================================
#include "DrawManager.h"
#include "ObjectManager.h"
#include "Render.h"
#include "Draw2D.h"
#include "Draw3D.h"
#include "MessageProc.h"
#include "TextureManager.h"
#include "InputKeyboard.h"
#include "XFileManager.h"

LPDIRECT3DDEVICE9 DrawManager::D3DDevice_m = nullptr;
//==============================================================================
//コンストラクタ
DrawManager::DrawManager(void)
{
	//メンバ初期化
	D3DDevice_m = nullptr;
	render_m = nullptr;
	draw2D_m = nullptr;
	messageProc_m = nullptr;
	textureManager_m = nullptr;
	xFileManager_m = nullptr;
}
//==============================================================================
//デストラクタ
DrawManager::~DrawManager(void)
{
	SAFE_DELETE(draw2D_m);
	SAFE_DELETE(draw3D_m);
	SAFE_DELETE(render_m);
	SAFE_DELETE(messageProc_m);
	SAFE_DELETE(textureManager_m);
	SAFE_DELETE(xFileManager_m);
}
//==============================================================================
//初期化
HRESULT DrawManager::init(HWND hWnd,BOOL bWindow)
{
	render_m = new Render();
	render_m->Init(hWnd,bWindow);

	D3DDevice_m = render_m->getD3DDevice();
	draw2D_m = new Draw2D();
	draw3D_m = new Draw3D();
	messageProc_m = new MessageProc;
	textureManager_m = new TextureManager;
	xFileManager_m = new XFileManager;
	return S_OK;
}

//==============================================================================
//描画
void DrawManager::drawAll(void)
{
	// バックバッファ＆Ｚバッファのクリア
	D3DDevice_m->Clear(0,nullptr,( D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER ),D3DCOLOR_RGBA(255,255,255,0),1.0f,0);

	// Direct3Dによる描画の開始
	if(SUCCEEDED(D3DDevice_m->BeginScene()))
	{
		unsigned int objectCnt = 0;
		OBJTYPE objectType = OBJTYPE_NONE;
		Object** objects;

		//ポリゴンの描画
		if(InputKeyboard::getKeyboardPress(DIK_F1))
		{
			D3DDevice_m->SetRenderState(D3DRS_FILLMODE,D3DFILL_WIREFRAME);
  }
  else{
   D3DDevice_m->SetRenderState(D3DRS_FILLMODE,D3DFILL_SOLID);
  }

		//3Dポリゴン描画
		objects = ObjectManager::getGameObjectHandleNotOnly(OBJTYPE_2D,objectCnt);
		if(objectCnt != 0)
		{
			for(unsigned int loop = 0; loop < objectCnt;loop++)
			{

				if(objects[loop]->getDrawFlag())
				{
					draw3D_m->Draw(objects[loop]->getObjHandle(),objects[loop]->getObjType());
				}
			}
		}

		//2Dポリゴン描画
		objects = ObjectManager::getGameObjectHandle(OBJTYPE_2D,objectCnt);
		if(objectCnt != 0)
		{
			for(unsigned int loop = 0; loop < objectCnt;loop++)
			{
				if(objects[loop]->getDrawFlag())
				{
					draw2D_m->Draw((Object2D*)objects[loop]);
				}
			}
		}
		//メッセージ描画
		messageProc_m->Draw();
		// Direct3Dによる描画の終了
		D3DDevice_m->EndScene();
	}
 if(InputKeyboard::getKeyboardTrigger(DIK_F12))
 {
  SetScreenShot();
 }
	// バックバッファとフロントバッファの入れ替え
	D3DDevice_m->Present(nullptr,nullptr,nullptr,nullptr);
	//メッセージの更新
	messageProc_m->Update();
}
//==============================================================================
//スクリーンショット撮影
void DrawManager::SetScreenShot(void)
{
 // 1フレームの描画が終わった後
#ifdef _DEBUG
 //ファイル名チェック
 FILE *fp;
 int nCnt = 0;
 char FileName[256];
 do
 {
  sprintf(FileName,"data/SCREENSHOT/screenshot%d.jpg",nCnt);
  fp = fopen(FileName,"r");
  nCnt++;
 } while(fp != NULL);

 if(fp != NULL)
 {
  fclose(fp);
 }
 // バックバファの取得
 LPDIRECT3DSURFACE9 pBackBuf;
 D3DDevice_m->GetRenderTarget(0,&pBackBuf);

 // スクショ出力
 D3DXSaveSurfaceToFile(FileName,D3DXIFF_JPG,pBackBuf,NULL,NULL);

 // Get系で取得したサーフェイスはAddRefが呼ばれているので忘れずに解放する
 pBackBuf->Release();
#endif
}
//End Of FIle

