//=============================================================================
//
// ゲームシーン [GameScene.cpp]
// Author : NARITADA SUZUKI
//
//=============================================================================
//インクルード
//==============================================================================
#include "SceneManager.h"
#include "ObjectManager.h"
#include "GameScene.h"
#include "Object2D.h"
#include "Object3D.h"
#include "Object3DBillboard.h"
#include "Object3DFigure.h"
#include "Object3DXfile.h"
#include "ObjectEnemy.h"
#include "ObjectPlayer.h"
#include "Object3DXfileAnim.h"
#include "ObjectPose.h"
#include "EnemyManager.h"
#include "ResultScene.h"
#include "ObjectNumbers.h"
#include "ObjectGameOver.h"
#include "NetClient.h"
#include "PlayerManager.h"
#include "BulletManager.h"
#include "TreeManager.h"
#include "ObjectMiniMap.h"
bool GameScene::gameOverFlag_m = false;
bool GameScene::gameClearFlag_m = false;
ObjectMiniMap* GameScene::minimap_m = nullptr;


//==============================================================================
//コンストラクタ
GameScene::GameScene(void)
{
	SystemManager::getCameraData().cameraUpdateMode = true;
 Object3DFigure* objsky;
	Object3DFigure::Create(FIGURE_MESHCYRINDER,TEXTURE_MOUNTAIN,12,2,D3DXVECTOR3(0,2000,0),D3DXVECTOR3(55000,0,4000));
 objsky = Object3DFigure::Create(FIGURE_MESHDOOM,TEXTURE_SKY,12,12,D3DXVECTOR3(0,-2000,0),D3DXVECTOR3(60000,0,40000));
 objsky->setRotSpeed(D3DXVECTOR3(0,0.001f,0));
	Object3DFigure::Create(FIGURE_MESHFIELD,TEXTURE_FIELD,0,0,D3DXVECTOR3(0,0,0),D3DXVECTOR3(FIELD_MAX,0,FIELD_MAX));
	ObjectPlayer::Create();
	pose_m = ObjectPose::Create();
	gameOver_m = ObjectGameOver::Create();
	minimap_m = ObjectMiniMap::Create(D3DXVECTOR3(SCREEN_WIDTH - 150,200,1.0f),TEXTURE_MINIMAP,D3DXCOLOR(0.6f,0.4f,0.3f,0.6f));
	gameOverFlag_m = false;
	gameClearFlag_m = false;
}
//==============================================================================
//デストラクタ
GameScene::~GameScene(void)
{
	Uninit();
}
//==============================================================================
//初期化
HRESULT GameScene::Init(void)
{
	gameOverFlag_m = false;
	gameClearFlag_m = false;
	SoundManager::playSound(SOUND_LAVEL_GAMEBGM);
	NetClient::Init();
	EnemyManager::setEnemy();
	treeMnager_m = new TreeManager;
 SystemManager::getObjectFeed()->feedStart(FEED_OUT_PROGRESS);

	return S_OK;
}
//==============================================================================
//解放
void GameScene::Uninit(void)
{
	// データ送信
	DATA data;
	SAFE_DELETE(treeMnager_m);
	data.Type = DATA_TYPE_EVENT;
	data.Event.Type = DATA_EVENT_TYPE_DISCONECT;
	NetClient::SendData(data);

	if(ObjectPlayer::getScorePlayer())
	{
		ResultScene::setScore(ObjectPlayer::getScorePlayer()->getNumber());
	}
	NetClient::Uninit();
	EnemyManager::setEnemyUpdate(false);
	SoundManager::stopSound();
	SystemManager::getCameraData().cameraUpdateMode = false;
	Object::releasePriority(PRIORITY_FEED);
 minimap_m = nullptr;
}
//==============================================================================
//更新
void GameScene::Update(void)
{
 if(EnemyManager::getEnemy() <= 0)
 {
  gameClearFlag_m = true;
 }
treeMnager_m->Update();
PlayerManager::update();
BulletManager::update();
if(gameClearFlag_m && SystemManager::getObjectFeed()->getFeedMode() == FEED_IN_END)
{
	SceneManager::SetScene(SCENE_RESULT);
}
else
	if(gameClearFlag_m && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
{
	SystemManager::getObjectFeed()->feedStart(FEED_IN_PROGRESS);

}
bool PoseFlag = pose_m->getPose();
if(gameOverFlag_m)
{
	Object::updatePrioritySet(PRIORITY_POSE,true);
	gameOver_m->setGameOverFlag(true);
	if(SystemManager::getObjectFeed()->getFeedMode() == FEED_IN_END)
	{
		SCENE_MODE SetMode[GAMEOVER_MAX] = {
			SCENE_GAME,
			SCENE_TITLE,
			SCENE_RESULT
		};
		SceneManager::SetScene(SetMode[gameOver_m->getGameOverNum()]);

	}
}
else{
	if(!PoseFlag)
	{
		Object::updatePrioritySet(PRIORITY_POSE,true);
	}
	else{
		Object::updatePrioritySet(PRIORITY_POSE,false);
	}

	if(!gameClearFlag_m && InputKeyboard::getKeyboardTrigger(DIK_P) && SystemManager::getObjectFeed()->getFeedMode() == FEED_NONE)
	{
		PoseFlag = !PoseFlag ? true : false;
		pose_m->setPose(PoseFlag);
		SystemManager::getCameraData().cameraUpdateMode = !PoseFlag ? true : false;
		EnemyManager::setEnemyUpdate(!PoseFlag ? true : false);
		SoundManager::playSound(SOUND_LABEL_BUTTON000);

	}
 if(SystemManager::getObjectFeed()->getFeedMode() == FEED_IN_END)
	{
		SceneManager::SetScene(SCENE_RESULT);
		}
		if(SystemManager::getObjectFeed()->getFeedMode() == FEED_IN_END)
		{
			if(PoseFlag)
			{
				SCENE_MODE SetMode[POSE_MAX] = {
					SCENE_NONE,
					SCENE_GAME,
					SCENE_TITLE,
					SCENE_RESULT
				};
				SceneManager::SetScene(SetMode[pose_m->getPoseNum()]);
			}
			else{
				SceneManager::SetScene(SCENE_RESULT);

			}
			SystemManager::getCameraData().cameraUpdateMode = true;
		}
	}

}
//End Of FIle